//
//  cpIsland.h
//  Chipmunk7
//
//  Created by Federico Spadoni on 22/07/14.
//
//

#ifndef Chipmunk7_cpIsland_h
#define Chipmunk7_cpIsland_h

#include "cpMemorypool.h"

struct cpContactIsland {
    
    cpSpaceSoftBody* space;
    
    cpBody** bodies;
    cpArbiter** arbiters;
    cpConstraint** constraints;
    
    int bodyCount;
    int arbiterCount;
    int constraintCount;

    struct cpIslandParams
    {
        cpFloat collisionSlop;
        cpFloat collisionBias;
        cpFloat damping;
        cpVect gravity;
        int iterations;
        
    } params;
};

void cpSolveIsland(cpContactIsland* island, const cpFloat dt);

void cpSolveIslands(cpContactIsland* islands, int num, const cpFloat dt);

                    
// compute out = J*in.

struct PGSsolverParams{
    unsigned  int num_iterations;
    cpFloat sor_w; 	// SOR over-relaxation parameter
};


/*
struct Solver {
    
    struct Info1
    {
        int m, nub;
    };
    
};*/



struct cpIsland {
    
    cpStackAllocator** stack;
    
    cpSpaceSoftBody* space;
    
    cpBody** bodies;
    cpArbiter** arbiters;
    cpConstraint** constraints;
    
    int bodyCount;
    int arbiterCount;
    int constraintCount;
    
    int bodyCapacity;
    int arbiterCapacity;
    int constraintCapacity;
    
    struct PGSsolverParams solverparams;
};


void allocateContactIsland( cpContactIsland* cisland, struct cpIsland* island );


void SOR_LCP( struct cpIsland* island, int m, cpFloat* J, int* jb,
             cpFloat* lambda, cpFloat* cforce, cpFloat* rhs, cpFloat* lo, cpFloat* hi,
             const cpFloat* cfm, const int* findex );



void initIsland(struct cpIsland* island, cpSpaceSoftBody* space, cpStackAllocator** stack, unsigned int bodyCapacity, unsigned int arbiterCapacity, unsigned int constraintCapacity);


void destroyIsland(struct cpIsland* island);


void resetIsland(struct cpIsland* island);


//inline static void setValue(cpFloat *a, int n, cpFloat value)
//{
//    while (n > 0) {
//        *(a++) = value;
//        n--;
//    }
//}

void solveOdeIsland(struct cpIsland* island, const cpFloat timestep, const cpVect gravity, const cpFloat global_cfm,
                 const cpFloat global_erp );


void addBodyToIsland(struct cpIsland* island, cpBody* body);

void addArbiterToIsland(struct cpIsland* island, cpArbiter* arbiter);

void addConstraintToIsland(struct cpIsland* island, cpConstraint* constraint);






#endif
