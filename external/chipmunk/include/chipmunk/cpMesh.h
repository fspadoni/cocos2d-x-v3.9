//
//  cpMesh.h
//  Chipmunk7
//
//  Created by Federico Spadoni on 29/06/14.
//
//

#ifndef Chipmunk7_cpMesh_h
#define Chipmunk7_cpMesh_h

#include "cpMemorypool.h"
#include "chipmunk_fem_types.h"

struct cpMesh {
    
    const char* filename;
    
    unsigned int numVertices;
    unsigned int numTriangles;
    unsigned int graphSize;
    unsigned int numMaterials;
    
    struct cpMembuffer* bufferList;
    
    struct cpMembuffer* vertexBuffer;
    struct cpMembuffer* triangleBuffer;
    
    struct cpMembuffer* elemMaterials;
    
    // graph
    struct cpMembuffer* rows;
    struct cpMembuffer* columns;
    struct cpMembuffer* diagonal;

    
    int alignment;
};

// struct for fast creation of sodtbody
struct cpMeshBuffer {
    
    struct cpMembuffer buffer;
    
    unsigned int numVertices;
    unsigned int numTriangles;
    unsigned int graphSize;
    unsigned int numMaterials;
    
    cpVect* vertices;
    cpTriangle* triangles;
    
//    materials index
    cpIdx* elemMaterials;
    
    // graph
    cpIdx* rows;
    cpIdx* columns;
    cpIdx* diagonal;
    
};


struct cpPolyLine {
    
    const char* filename;
    
    unsigned int numVertices;
    unsigned int numSegments;
    unsigned int graphSize;
    
    struct cpMembuffer* bufferList;
    
    struct cpMembuffer* nodeBuffer;
    struct cpMembuffer* segmentBuffer;
    
    // graph
    struct cpMembuffer* rows;
    struct cpMembuffer* columns;
    struct cpMembuffer* diagonal;
    
    
    int alignment;
};

// struct for fast creation of sodtbody
struct cpPolyLineBuffer {
    
    struct cpMembuffer buffer;
    
    unsigned int numVertices;
    unsigned int numSegments;
    unsigned int graphSize;
    
    cpBeamNode* nodes;
    cpSegment* segments;
    
    // graph
    cpIdx* rows;
    cpIdx* columns;
    cpIdx* diagonal;
    
};


unsigned int cpMeshBufferComputeMemorySize(const unsigned int numVertices, const unsigned int numTriangles, const unsigned int graphSize);

struct cpMeshBuffer cpMeshBufferInit(void* data, const unsigned int numVertices, const unsigned int numTriangles, const unsigned int graphSize, unsigned int numMaterials);

struct cpMeshBuffer cpMeshBufferInitFromMesh(const struct cpMesh* mesh, const struct cpMembuffer* buffer);


struct cpMesh* cpMeshInitFromFile(const char* filename);

cpBool cpMeshLoadFromFile(struct cpMesh* mesh, const char* filename);

struct cpMeshBuffer cpMeshBufferInitFromBuffer( const unsigned int numVertices, const unsigned int numTriangles,
                                               const unsigned int graphSize , const unsigned int numMaterials, const void* buffer, const unsigned int bufSize);

cpBool cpMeshRelease(struct cpMesh* mesh);


// PolyLine functions
unsigned int cpPolyLineBufferComputeMemorySize(const unsigned int numVertices, const unsigned int numSegments, const unsigned int graphSize);

struct cpPolyLineBuffer cpPolyLineBufferInit(void* data, const unsigned int numVertices, const unsigned int numSegments, const unsigned int graphSize);

struct cpPolyLineBuffer cpPolyLineBufferInitFromMesh(const struct cpPolyLine* mesh, const struct cpMembuffer* buffer);

struct cpPolyLine* cpPolyLineInitFromFile( const char* filename);

cpBool cpPolyLineLoadFromFile(struct cpPolyLine* mesh, const char* filename);

struct cpPolyLineBuffer cpPolyLineBufferInitFromBuffer( const unsigned int numVertices, const unsigned int numSegments,
                                               const unsigned int graphSize ,const void* buffer, const unsigned int bufSize);



void computeElasticityMat(ElasticityMatrix* m, const cpFloat youngModulus, const cpFloat poissontRatio );

void computeBeamElasticityMat(BeamElasticityMatrix* m, const cpVect youngModulus, const cpFloat beamLength, const cpFloat radius );

void printElasticityMat(const ElasticityMatrix* m, const char* filename);

void computeStrainMat(StrainMatrix* m, const cpVect6* x0 );

void printStrainMatrix(const StrainMatrix* m, const char* filename);


void elementStiffnessMatrix(ElementStiffnessMatrix* stiffmat, const ElasticityMatrix* elasticmat,
                            const StrainMatrix* strianmat, const cpFloat elemarea);

void printElementStiffnessMat(const ElementStiffnessMatrix* stiffmat, const char* filename);

void plasticityMatrix(PlasticityMatrix* plastmat, const ElasticityMatrix* elasticmat,
                      const StrainMatrix* strianmat, const cpFloat elemarea);

void printElementPlasticityMat(const PlasticityMatrix* plastmat, const char* filename);


void computeBeamStrainMat(struct BeamStrainMatrix* m, const float cosB, const float sinB, const float beamLength);

void printBeamStrainMatrix(const struct BeamStrainMatrix* m, const char* filename);

void computeBeamElementStiffnessMatrixCor(BeamStiffnessMatrix* stiffmat, const BeamElasticityMatrix* elasticmat,
                                   const BeamStrainMatrix* strainmat );

void computeBeamElementMassMatrix(BeamStiffnessMatrix* massmat, const cpFloat radius,
                                  const cpFloat density, const cpFloat beamLength );

void computeBeamElementStiffnessMatrix(BeamStiffnessMatrix* stiffmat, const cpFloat radius, const cpVect youngModulus, const cpFloat beamLength, const cpFloat inertia );

void printBeamElementStiffnessMat(const BeamStiffnessMatrix* stiffmat, const char* filename);

void beamPlasticityMatrix(BeamPlasticityMatrix* plastmat, const BeamElasticityMatrix* elasticmat,
                      const BeamStrainMatrix* strianmat );

void printBeamPlasticityMat(const BeamPlasticityMatrix* plastmat, const char* filename);

//void elasticityStrainMatrix( StrainMatrix* elastStrainMat, const ElasticityMatrix* elasticmat,
//                            const StrainMatrix* strainmat );

void computeKx0(cpVect6* kx0buf, const ElementStiffnessMatrix* stiffmat, const cpVect6* x0 );

void computeDeformation(Matrix2* res, const Matrix3* deformed, const Matrix3* undeformed );



#endif
