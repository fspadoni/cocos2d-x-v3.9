//
//  cpMathSSE2.h
//  Chipmunk7
//
//  Created by Federico Spadoni on 30/03/15.
//
//

#ifndef Chipmunk7_cpMathSSE2_h
#define Chipmunk7_cpMathSSE2_h

// SSE + SSE2 (don't include intrin.h!)
#include <emmintrin.h>

union SSEFloat
{
	__m128 m128;
	float f[4];
};

//typedef struct Simd4f
//{
//	__m128  m128;
//} Simd4f;

//res = v0 + v1
static inline void multiplyElemByElem( const cpVect* v0, const cpVect* v1, cpVect* res, unsigned int size)
{
    unsigned int numIter = (size + 1) / 2;
    
    while ( numIter-- )
    {
        _mm_store_ps( (float*)res, _mm_mul_ps( _mm_load_ps((float *) v0), _mm_load_ps((float *) v1) ) );
        v0 += 2;
        v1 += 2;
        res += 2;
    }
    
    return;
}

//res = v0 + v1
static inline void multiplyElemByElem3( const cpVect3* v0, const cpVect3* v1, cpVect3* res, unsigned int size)
{
    
    while ( size-- )
    {
        _mm_store_ps( (float*)res, _mm_mul_ps( _mm_load_ps((float *) v0), _mm_load_ps((float *) v1) ) );
        ++v0;
        ++v1;
        ++res;
    }
    
    return;
}

//res += v0 * v1
static inline void multiplyElemByElemAddElem( const cpVect* v0, const cpVect* v1, cpVect* res, unsigned int size)
{
    unsigned int numIter = (size + 1) / 2;
    
    while ( numIter-- )
    {
        _mm_store_ps( (float*)res,
                     _mm_add_ps( _mm_load_ps((float *) res),
                                _mm_mul_ps(_mm_load_ps((float *) v0), _mm_load_ps((float *) v1) )
                                ) );
        v0 += 2;
        v1 += 2;
        res += 2;
    }
    
    return;
}

//res = a * v0
static inline void multiplyElemByScalar( const cpVect* v0, const float a, cpVect* res, unsigned int size)
{
    unsigned int numIter = (size + 1) / 2;
    
    const __m128 scalar = _mm_setr_ps(a,a,a,a);
    
    while ( numIter-- )
    {
        _mm_store_ps( (float*)res, _mm_mul_ps( _mm_load_ps((float *) v0), scalar) );
        v0 += 2;
        res += 2;
    }
    
    return;
}

//res = a * v0
static inline void multiplyElemByScalar3( const cpVect3* v0, const float a, cpVect3* res, unsigned int size)
{
//    unsigned int numIter = (size + 1) / 2;
    
    const __m128 scalar = _mm_setr_ps(a,a,a,a);
    
    while ( size-- )
    {
        _mm_store_ps( (float*)res, _mm_mul_ps( _mm_load_ps((float *) v0), scalar) );
        ++v0;
        ++res;
    }
    
    return;
}


//res = a * v0 + v1
static inline void multiplyElemByScalarAddElem(const cpVect* v0, const cpVect* a, const cpVect* v1, cpVect* res, unsigned int size)
{
    unsigned int numIter = (size + 1) / 2;
    
    const __m128 scalar = _mm_setr_ps(a->x, a->y, a->x, a->y );
    
    while ( numIter-- )
    {
        _mm_store_ps( (float*)res, _mm_add_ps( _mm_mul_ps( _mm_load_ps((float *) v0), scalar), _mm_load_ps((float *) v1) ) );
        v0 += 2;
        v1 += 2;
        res += 2;
    }

    return;
}

//res = v0 + v1
static inline void addElemByElem( const cpVect* v0, const cpVect* v1, cpVect* res, unsigned int size)
{
    unsigned int numIter = (size + 1) / 2;
    
    while ( numIter-- )
    {
        _mm_store_ps( (float*)res, _mm_add_ps(_mm_load_ps((float *) v0), _mm_load_ps((float *) v1)) );
        v0 += 2;
        v1 += 2;
        res += 2;
    }
    
    return;
}

//res = res + v1
static inline void addVec3byVec3( cpVect3* res, const cpVect3* v )
{
   
    _mm_store_ps( (float*)res, _mm_add_ps(_mm_load_ps((float *) res), _mm_load_ps((float *) v)) );
    return;
}

//res = v0 + v1
static inline void addElemByElem3( const cpVect3* v0, const cpVect3* v1, cpVect3* res, unsigned int size)
{
//    unsigned int numIter = (size + 1) / 2;
    
    while ( size-- )
    {
        _mm_store_ps( (float*)res, _mm_add_ps(_mm_load_ps((float *) v0), _mm_load_ps((float *) v1)) );
        ++v0;
        ++v1;
        ++res;
    }
    
    return;
}

//res = v0 - v1
static inline void subElemByElem( const cpVect* v0, const cpVect* v1, cpVect* res, unsigned int size)
{
//    ceiling (a+b-1)/b
    unsigned int numIter = (size + 1) / 2;
//    unsigned int mod = size % 2;

    while ( numIter-- )
    {
        _mm_store_ps( (float*)res, _mm_sub_ps(_mm_load_ps((float *) v0), _mm_load_ps((float *) v1)) );
        v0 += 2;
        v1 += 2;
        res += 2;
    }
    
//    if ( mod )
//    {
//        int last = size-1;
//        res[last].x = v0[last].x - v1[last].x;
//        res[last].y = v0[last].y - v1[last].y;
//    }
    return;
}


//res = v0 - v1
static inline void subElemByElem3( const cpVect3* v0, const cpVect3* v1, cpVect3* res, unsigned int size)
{
    //    ceiling (a+b-1)/b
//    unsigned int numIter = (size + 1) / 2;
    //    unsigned int mod = size % 2;
    
    while ( size-- )
    {
        _mm_store_ps( (float*)res, _mm_sub_ps(_mm_load_ps((float *) v0), _mm_load_ps((float *) v1)) );
        ++v0;
        ++v1;
        ++res;
    }
    
    return;
}


static inline cpFloat innProd( const cpVect* v0, const cpVect* v1, unsigned int size)
{
    unsigned int numIter = size / 2;
    unsigned int modSize = size % 2;
    
    
    __m128 sum = _mm_setzero_ps();
    float CP_ALIGN(16) tmp_aligned[4];
    
    while ( numIter-- )
    {
        _mm_add_ps( sum, _mm_mul_ps( _mm_load_ps((float *) v0), _mm_load_ps((float *) v1) ) );
        v0 += 2;
        v1 += 2;
    }
    
    _mm_store_ps( (float*)tmp_aligned, sum);
    
    if (modSize > 0)
    {
        tmp_aligned[0] += v0->x * v1->x;
        tmp_aligned[1] += v0->y * v1->y;
    }
    
    return tmp_aligned[0] + tmp_aligned[1] + tmp_aligned[2] + tmp_aligned[3];
    
//    if ( mod )
//    {
//        const cpVect* v0last = (const cpVect*)vm0;
//        const cpVect* v1last = (const cpVect*)vm1;
//        sum[0] += v0last->x * v1last->x;
//        sum[1] += v0last->y * v1last->y;
//    }
    
//    temp = _mm_add_ps(_mm_shuffle_ps(temp, temp, _MM_SHUFFLE(0,0,0,0)),
//                      _mm_add_ps(_mm_shuffle_ps(temp, temp, _MM_SHUFFLE(1,1,1,1)),
//                                 _mm_add_ps(_mm_shuffle_ps(temp, temp, _MM_SHUFFLE(2,2,2,2)),
//                                            _mm_shuffle_ps(temp, temp, _MM_SHUFFLE(3,3,3,3)))));

//    return sum[0] + sum[1] + sum[2] + sum[3];
}



/// Returns the length of v.
static inline cpFloat norm3(const cpVect3* v)
{
	return cpfsqrt(dot3(v, v));
}


static inline cpVect multiplyMat2Vect2(const Matrix2* m, const cpVect* v)
{
    cpVect res;
    res.x = m->col[0].x * v->x + m->col[1].x * v->y;
    res.y = m->col[0].y * v->x + m->col[1].y * v->y;
    return res;
}

static inline void multiplyMat2byMat2(Matrix2* res, const Matrix2* m0, const Matrix2* m1)
{
    res->col[0].x = m0->col[0].x * m1->col[0].x + m0->col[1].x * m1->col[0].y;
    res->col[0].y = m0->col[0].y * m1->col[0].x + m0->col[1].y * m1->col[0].y;
    res->col[1].x = m0->col[0].x * m1->col[1].x + m0->col[1].x * m1->col[1].y;
    res->col[1].y = m0->col[0].y * m1->col[1].x + m0->col[1].y * m1->col[1].y;
}

static inline cpVect multiplyTranspMat2Vect2(const Matrix2* m, const cpVect* v)
{
    cpVect res;
    res.x = m->col[0].x * v->x + m->col[0].y * v->y;
    res.y = m->col[1].x * v->x + m->col[1].y * v->y;
    return res;
}

static inline void multiplyMat2Vect2andAdd(cpVect* res, const Matrix2* m, const cpVect* v)
{
//    m is aligned
//    v not always
    const __m128* mat2x2 = (const __m128*)m;
    __m128 vm = _mm_setr_ps(v->x, v->x, v->y, v->y);
    __m128 temp = _mm_mul_ps(*mat2x2, vm);
    
    float* sum = (float*)&temp;
    
    res->x += sum[0] + sum[2];
    res->y += sum[1] + sum[3];
}

static inline void multiplyMat2Vect2andSub(cpVect* res, const Matrix2* m, const cpVect* v)
{
    //    m is aligned
    //    v not always
    const __m128* mat2x2 = (const __m128*)m;
    __m128 vm = _mm_setr_ps(v->x, v->x, v->y, v->y);
    __m128 temp = _mm_mul_ps(*mat2x2, vm);
    
    float* sum = (float*)&temp;
    
    res->x -= sum[0] + sum[2];
    res->y -= sum[1] + sum[3];
}


static inline void addMatrix2byelems(Matrix2* res, const Matrix2* m0, const Matrix2* m1)
{
    res->col[0] = cpvadd(m0->col[0], m1->col[0]);
    res->col[1] = cpvadd(m0->col[1], m1->col[1]);
}

static inline cpVect multiplyMat2x3byVect3(const cpMat2x3* m23, const cpVect3* v)
{
    cpVect res = { m23->col[0].x * v->x + m23->col[1].x * v->y + m23->col[2].x * v->z,
        m23->col[0].y * v->x + m23->col[1].y * v->y + m23->col[2].y * v->z };
    return res;
}


static inline cpVect3 subVec3(const cpVect3* v0, const cpVect3* v1)
{
//    cpVect3 res = { v0->x - v1->x, v0->y - v1->y, v0->z - v1->z, 0};
    
    cpVect3 __attribute__ ((aligned (16))) res;
    
    _mm_store_ps( (float*)&res, _mm_sub_ps( _mm_load_ps((float *)v0), _mm_load_ps((float *)v1) ) );
    
    return res;
}

static inline void subMatrix3byelems(Matrix3* res, const Matrix3* m0, const Matrix3* m1)
{
//    res->col[0] = subVec3( &m0->col[0], &m1->col[0]);
//    res->col[1] = subVec3( &m0->col[1], &m1->col[1]);
//    res->col[2] = subVec3( &m0->col[2], &m1->col[2]);
    
    _mm_store_ps( (float*)&res->col[0], _mm_sub_ps( _mm_load_ps((float *)&m0->col[0]), _mm_load_ps((float *)&m1->col[0])) );
    _mm_store_ps( (float*)&res->col[1], _mm_sub_ps( _mm_load_ps((float *)&m0->col[1]), _mm_load_ps((float *)&m1->col[1])) );
    _mm_store_ps( (float*)&res->col[2], _mm_sub_ps( _mm_load_ps((float *)&m0->col[2]), _mm_load_ps((float *)&m1->col[2])) );
    
}


//be sure matrix and vectors are aligned
static inline void multiplyMat3Vect3(cpVect3* res, const Matrix3* m, const cpVect3* v)
{
//    res->x = m->col[0].x * v->x + m->col[1].x * v->y + m->col[2].x * v->z;
//    res->y = m->col[0].y * v->x + m->col[1].y * v->y + m->col[2].y * v->z;
//    res->z = m->col[0].z * v->x + m->col[1].z * v->y + m->col[2].z * v->z;
    
    const __m128 col0 = _mm_load_ps((float *) &m->col[0]);
    const __m128 col1 = _mm_load_ps((float *) &m->col[1]);
    const __m128 col2 = _mm_load_ps((float *) &m->col[2]);
    
    const __m128 mv = _mm_load_ps((float *) v);
    const __m128 mvx = _mm_shuffle_ps(mv, mv, _MM_SHUFFLE(0,0,0,0));
    const __m128 mvy = _mm_shuffle_ps(mv, mv, _MM_SHUFFLE(1,1,1,1));
    const __m128 mvz = _mm_shuffle_ps(mv, mv, _MM_SHUFFLE(2,2,2,2));
    
    _mm_store_ps( (float*)res, _mm_add_ps( _mm_mul_ps(col0, mvx), _mm_add_ps( _mm_mul_ps(col1, mvy) ,  _mm_mul_ps(col2, mvz)) ) );
    
    return;
}

//be sure matrix and vectors are aligned
static inline cpVect3 multiplyMat3xVect3(const Matrix3* m, const cpVect3* v)
{
    cpVect3 res;
    res.x = m->col[0].x * v->x + m->col[1].x * v->y + m->col[2].x * v->z;
    res.y = m->col[0].y * v->x + m->col[1].y * v->y + m->col[2].y * v->z;
    res.z = m->col[0].z * v->x + m->col[1].z * v->y + m->col[2].z * v->z;
    
    return res;
    
//    cpVect3 __attribute__ ((aligned (16))) res;
//    
//    const __m128 col0 = _mm_load_ps((float *) &m->col[0]);
//    const __m128 col1 = _mm_load_ps((float *) &m->col[1]);
//    const __m128 col2 = _mm_load_ps((float *) &m->col[2]);
//    
//    const __m128 mv = _mm_load_ps((float *) v);
//    const __m128 mvx = _mm_shuffle_ps(mv, mv, _MM_SHUFFLE(0,0,0,0));
//    const __m128 mvy = _mm_shuffle_ps(mv, mv, _MM_SHUFFLE(1,1,1,1));
//    const __m128 mvz = _mm_shuffle_ps(mv, mv, _MM_SHUFFLE(2,2,2,2));
//    
//    _mm_store_ps( (float*)&res, _mm_add_ps( _mm_mul_ps(col0, mvx), _mm_add_ps( _mm_mul_ps(col1, mvy) ,  _mm_mul_ps(col2, mvz)) ) );
//    
//    return res;
}


static inline Matrix2 multiplyMat2Mat2(const Matrix2* m0, const Matrix2* m1)
{
    Matrix2 res;
    
    const __m128 vm0 = _mm_load_ps((float *) m0);
    const __m128 vm1 = _mm_load_ps((float *) m1);
    
    __m128 t1 = _mm_movelh_ps(vm0, vm0);   // t1 = [a11, a21, a11, a21]
    __m128 t2 = _mm_shuffle_ps( vm1, vm1, _MM_SHUFFLE(0, 0, 2, 2) );  // t2 = [b11, b11, b12, b12]
    __m128 t3 = _mm_movehl_ps(vm0, vm0);   // t3 = [a12, a22, a12, a22]
    __m128 t4 = _mm_shuffle_ps( vm1, vm1, _MM_SHUFFLE(1, 1, 3, 3) ); // t4 = [b21, b21, b22, b22]
    //    t1 = [a11 * b11, a21 * b11, a11 * b12, a21 * b12]
    //    t2 = [a12 * b21, a22 * b21, a12 * b22, a22 * b22]
    _mm_store_ps( (float*)&res, _mm_add_ps( _mm_mul_ps(t1, t2), _mm_mul_ps(t3, t4) ) );  // the result
    
    return res;
    
}

static inline Matrix2 multiplyMat2Mat2Mat2( const Matrix2* m0, const Matrix2* m1, const Matrix2* m2)
{
    
//    res.mat = multiplyMat2Mat2(m1, m2);
//    res.mat = multiplyMat2Mat2(m0, &res.mat);
    
    Matrix2 res;
    
    const __m128 vm0 = _mm_load_ps((float *) m0);
    const __m128 vm1 = _mm_load_ps((float *) m1);
    const __m128 vm2 = _mm_load_ps((float *) m2);
    
    __m128 t1 = _mm_movelh_ps(vm1, vm1);   // t1 = [a11, a21, a11, a21]
    __m128 t2 = _mm_shuffle_ps( vm2, vm2, _MM_SHUFFLE(0, 0, 2, 2) );  // t2 = [b11, b11, b12, b12]
    __m128 t3 = _mm_movehl_ps(vm1, vm1);   // t3 = [a12, a22, a12, a22]
    __m128 t4 = _mm_shuffle_ps( vm2, vm2, _MM_SHUFFLE(1, 1, 3, 3) ); // t4 = [b21, b21, b22, b22]
    //    t1 = [a11 * b11, a21 * b11, a11 * b12, a21 * b12]
    //    t2 = [a12 * b21, a22 * b21, a12 * b22, a22 * b22]
    __m128 vm1vm2 = _mm_add_ps( _mm_mul_ps(t1, t2), _mm_mul_ps(t3, t4) );  // the result
    
    t1 = _mm_movelh_ps(vm0, vm0);   // t1 = [a11, a21, a11, a21]
    t2 = _mm_shuffle_ps( vm1vm2, vm1vm2, _MM_SHUFFLE(0, 0, 2, 2) );  // t2 = [b11, b11, b12, b12]
    t3 = _mm_movehl_ps(vm0, vm0);   // t3 = [a12, a22, a12, a22]
    t4 = _mm_shuffle_ps( vm1vm2, vm1vm2, _MM_SHUFFLE(1, 1, 3, 3) ); // t4 = [b21, b21, b22, b22]
    
    _mm_store_ps( (float*)&res, _mm_add_ps( _mm_mul_ps(t1, t2), _mm_mul_ps(t3, t4) ) );  // the result
    
    return res;
}

static inline void addMat2ToMat2( Matrix2* sum, const Matrix2* m)
{
//    __m128* vmsum = (__m128*)sum;
//    const __m128* vm = (const __m128*)m;
    
//    *vmsum = _mm_add_ps( *vmsum, *vm );
//    sum->col[0].x += m->col[0].x;
//    sum->col[0].y += m->col[0].y;
//    sum->col[1].x += m->col[1].x;
//    sum->col[1].y += m->col[1].y;
    
    _mm_store_ps( (float*)sum, _mm_add_ps(_mm_load_ps((float *)sum), _mm_load_ps((float *)m) ) );
    
}


static inline void multiplyMat2Mat2Mat2AddToMat2(Matrix2* sum, const Matrix2* m0, const Matrix2* m1, const Matrix2* m2)
{
    
    //    multiply m0 * ( m1 * m2 )
    const __m128 vm0 = _mm_load_ps((float *) m0);
    const __m128 vm1 = _mm_load_ps((float *) m1);
    const __m128 vm2 = _mm_load_ps((float *) m2);
    __m128 vmsum = _mm_load_ps((float *) sum);
   
    __m128 t1 = _mm_movelh_ps(vm1, vm1);   // t1 = [a11, a21, a11, a21]
    __m128 t2 = _mm_shuffle_ps( vm2, vm2, _MM_SHUFFLE(0, 0, 2, 2) );  // t2 = [b11, b11, b12, b12]
    __m128 t3 = _mm_movehl_ps(vm1, vm1);   // t3 = [a12, a22, a12, a22]
    __m128 t4 = _mm_shuffle_ps( vm2, vm2, _MM_SHUFFLE(1, 1, 3, 3) ); // t4 = [b21, b21, b22, b22]
    //    t1 = [a11 * b11, a21 * b11, a11 * b12, a21 * b12]
    //    t2 = [a12 * b21, a22 * b21, a12 * b22, a22 * b22]
    __m128 tempRes = _mm_add_ps( _mm_mul_ps(t1, t2), _mm_mul_ps(t3, t4) );  // the result
    
    t1 = _mm_movelh_ps(vm0, vm0);   // t1 = [a11, a21, a11, a21]
    t2 = _mm_shuffle_ps( tempRes, tempRes, _MM_SHUFFLE(0, 0, 2, 2) );  // t2 = [b11, b11, b12, b12]
    t3 = _mm_movehl_ps(vm0, vm0);   // t3 = [a12, a22, a12, a22]
    t4 = _mm_shuffle_ps( tempRes, tempRes, _MM_SHUFFLE(1, 1, 3, 3) ); // t4 = [b21, b21, b22, b22]
    
    //    tempRes = _mm_add_ps( _mm_mul_ps(t1, t2), _mm_mul_ps(t3, t4) );  // mult result
    
    //    add to sum
    vmsum = _mm_add_ps( vmsum, _mm_add_ps( _mm_mul_ps(t1, t2), _mm_mul_ps(t3, t4) ) );
    _mm_store_ps( (float*)sum, vmsum );
    
}

static inline void multiplyMat3byScalar(Matrix3* m, float s)
{
    const __m128 scalar = _mm_setr_ps(s,s,s,s);
    
    _mm_store_ps( (float*)&m->col[0], _mm_mul_ps( _mm_load_ps((float *)&m->col[0]), scalar ) );
    _mm_store_ps( (float*)&m->col[1], _mm_mul_ps( _mm_load_ps((float *)&m->col[1]), scalar ) );
    _mm_store_ps( (float*)&m->col[2], _mm_mul_ps( _mm_load_ps((float *)&m->col[2]), scalar ) );
}

//res += M*scalar
static inline void multiplyMat3byScalarAndAdd(Matrix3* res, Matrix3* m, float s)
{
    const __m128 scalar = _mm_setr_ps(s,s,s,s);
    
    _mm_store_ps((float*)&res->col[0],_mm_add_ps(_mm_load_ps((float*)&res->col[0]),_mm_mul_ps(_mm_load_ps((float *)&m->col[0]), scalar)));
    
    _mm_store_ps((float*)&res->col[1],_mm_add_ps(_mm_load_ps((float*)&res->col[1]),_mm_mul_ps(_mm_load_ps((float *)&m->col[1]), scalar)));
    
    _mm_store_ps((float*)&res->col[2],_mm_add_ps(_mm_load_ps((float*)&res->col[2]),_mm_mul_ps(_mm_load_ps((float *)&m->col[2]), scalar)));
    
}


static inline void multiplyMat3xMat3(Matrix3* res, const Matrix3* m1, const Matrix3* m2)
{
    multiplyMat3Vect3( &res->col[0], m1, &m2->col[0] );
    multiplyMat3Vect3( &res->col[1], m1, &m2->col[1] );
    multiplyMat3Vect3( &res->col[2], m1, &m2->col[2] );
    
    //    res->col[0].x = m1->col[0].x * m2->col[0].x + m1->col[1].x * m2->col[0].y + m1->col[2].x * m2->col[0].z;
    //    res->col[0].y = m1->col[0].y * m2->col[0].x + m1->col[1].y * m2->col[0].y + m1->col[2].y * m2->col[0].z;
    //    res->col[0].z = m1->col[0].z * m2->col[0].x + m1->col[1].z * m2->col[0].y + m1->col[2].z * m2->col[0].z;
    //
    //    res->col[1].x = m1->col[0].x * m2->col[1].x + m1->col[1].x * m2->col[1].y + m1->col[2].x * m2->col[1].z;
    //    res->col[1].y = m1->col[0].y * m2->col[1].x + m1->col[1].y * m2->col[1].y + m1->col[2].y * m2->col[1].z;
    //    res->col[1].z = m1->col[0].z * m2->col[1].x + m1->col[1].z * m2->col[1].y + m1->col[2].z * m2->col[1].z;
    //
    //    res->col[2].x = m1->col[0].x * m2->col[2].x + m1->col[1].x * m2->col[2].y + m1->col[2].x * m2->col[2].z;
    //    res->col[2].y = m1->col[0].y * m2->col[2].x + m1->col[1].y * m2->col[2].y + m1->col[2].y * m2->col[2].z;
    //    res->col[2].z = m1->col[0].z * m2->col[2].x + m1->col[1].z * m2->col[2].y + m1->col[2].z * m2->col[2].z;
    
}


static inline void multiplyMat3Mat3Mat3(Matrix3* res, const Matrix3* m0, const Matrix3* m1, const Matrix3* m2)
{
//    res = m1*m2
    multiplyMat3Vect3( &res->col[0], m1, &m2->col[0] );
    multiplyMat3Vect3( &res->col[1], m1, &m2->col[1] );
    multiplyMat3Vect3( &res->col[2], m1, &m2->col[2] );
    
//    res = m0*res = m0*(m1*m2)
    multiplyMat3Vect3( &res->col[0], m0, &res->col[0] );
    multiplyMat3Vect3( &res->col[1], m0, &res->col[1] );
    multiplyMat3Vect3( &res->col[2], m0, &res->col[2] );
    
}

//sum = sum + m
static inline void addMat3ToMat3( Matrix3* sum, const Matrix3* m)
{
//    __m128* vmsum = (__m128*)sum;
//    const __m128* vm = (const __m128*)m;
//    
//    *vmsum = _mm_add_ps( *vmsum, *vm );
//    
//    ++vmsum;
//    ++vm;
//    
//    *vmsum = _mm_add_ps( *vmsum, *vm );
//    
//    ++vmsum;
//    ++vm;
//    
//    *vmsum = _mm_add_ps( *vmsum, *vm );
    
    
    _mm_store_ps( (float*)&sum->col[0], _mm_add_ps(_mm_load_ps((float *)&sum->col[0]), _mm_load_ps((float *)&m->col[0]) ) );
    _mm_store_ps( (float*)&sum->col[1], _mm_add_ps(_mm_load_ps((float *)&sum->col[1]), _mm_load_ps((float *)&m->col[1]) ) );
    _mm_store_ps( (float*)&sum->col[2], _mm_add_ps(_mm_load_ps((float *)&sum->col[2]), _mm_load_ps((float *)&m->col[2]) ) );
    
}

//sub = sub - m
static inline void subMat3ToMat3( Matrix3* sub, const Matrix3* m)
{
    _mm_store_ps( (float*)&sub->col[0], _mm_sub_ps(_mm_load_ps((float *)&sub->col[0]), _mm_load_ps((float *)&m->col[0]) ) );
    _mm_store_ps( (float*)&sub->col[1], _mm_sub_ps(_mm_load_ps((float *)&sub->col[1]), _mm_load_ps((float *)&m->col[1]) ) );
    _mm_store_ps( (float*)&sub->col[2], _mm_sub_ps(_mm_load_ps((float *)&sub->col[2]), _mm_load_ps((float *)&m->col[2]) ) );
}

static inline void multiplyMat3Mat3Mat3AddToMat3(Matrix3* sum, const Matrix3* m0, const Matrix3* m1, const Matrix3* m2)
{
    
    Matrix3 CP_ALIGN(16) temp;
    
    multiplyMat3Mat3Mat3( &temp, m0, m1, m2);
    
    addMat3ToMat3( sum, &temp );
    
}


// be sure cpVect is aligned
static inline void multiplyAddMat3x2byVect2(cpVect3* res, const cpMat3x2* m, const cpVect* v)
{
    
//    res->x += m->col[0].x * v->x + m->col[1].x * v->y;
//    res->y += m->col[0].y * v->x + m->col[1].y * v->y;
//    res->z += m->col[0].z * v->x + m->col[1].z * v->y;
    
    const __m128 mCol0 = _mm_load_ps((float *) &m->col[0]);
    const __m128 mCol1 = _mm_load_ps((float *) &m->col[1]);
    const __m128 mv = _mm_load_ps((float *) v);
    __m128 mRes = _mm_load_ps((float *) res);
    
    const __m128 mVx = _mm_shuffle_ps(mv, mv, _MM_SHUFFLE(0,0,0,0));
    const __m128 mVy = _mm_shuffle_ps(mv, mv, _MM_SHUFFLE(1,1,1,1));
    
    _mm_store_ps( (float*)res, _mm_add_ps( mRes, _mm_add_ps( _mm_mul_ps(mCol0, mVx), _mm_mul_ps(mCol1, mVy) ) ) );
   
    return;
}

static inline void multiplyAddMat3x3byVect3(cpVect3* res, const Matrix3* m, const cpVect3* v)
{
    res->x += m->col[0].x * v->x + m->col[1].x * v->y + m->col[2].x * v->z;
    res->y += m->col[0].y * v->x + m->col[1].y * v->y + m->col[2].y * v->z;
    res->z += m->col[0].z * v->x + m->col[1].z * v->y + m->col[2].z * v->z;
    
    const __m128 mCol0 = _mm_load_ps((float *) &m->col[0]);
    const __m128 mCol1 = _mm_load_ps((float *) &m->col[1]);
    const __m128 mCol2 = _mm_load_ps((float *) &m->col[2]);
    
    const __m128 mv = _mm_load_ps((float *) v);
    __m128 mRes = _mm_load_ps((float *) res);
    
    const __m128 mVx = _mm_shuffle_ps(mv, mv, _MM_SHUFFLE(0,0,0,0));
    const __m128 mVy = _mm_shuffle_ps(mv, mv, _MM_SHUFFLE(1,1,1,1));
    const __m128 mVz = _mm_shuffle_ps(mv, mv, _MM_SHUFFLE(2,2,2,2));
    
    _mm_store_ps( (float*)res, _mm_add_ps( mRes, _mm_add_ps( _mm_add_ps( _mm_mul_ps(mCol0, mVx), _mm_mul_ps(mCol1, mVy) ), _mm_mul_ps(mCol2, mVz) ) ) );
    
    return;
}

// not sse: called only during init
static inline void transposeMat3x2MultiplyByVect3(cpVect* res, const cpMat3x2* m, const cpVect3* v)
{
    const cpVect3* mrow0 = &m->col[0];
    const cpVect3* mrow1 = &m->col[1];
    res->x = mrow0->x * v->x + mrow0->y * v->y + mrow0->z * v->z;
    res->y = mrow1->x * v->x + mrow1->y * v->y + mrow1->z * v->z;
}

static inline void transposeMat3x3MultiplyByVect3(cpVect3* res, const Matrix3* m, const cpVect3* v)
{
    const cpVect3* mrow0 = &m->col[0];
    const cpVect3* mrow1 = &m->col[1];
    const cpVect3* mrow2 = &m->col[2];
    res->x = mrow0->x * v->x + mrow0->y * v->y + mrow0->z * v->z;
    res->y = mrow1->x * v->x + mrow1->y * v->y + mrow1->z * v->z;
    res->z = mrow2->x * v->x + mrow2->y * v->y + mrow2->z * v->z;
}

static inline void multiplyMat6xVect6(cpVect6* res, const cpMat6* mat, const cpVect6* vect )
{
    res->v[0] = cpvadd( multiplyMat2Vect2(&mat->m[0][0], &vect->v[0]) , cpvadd( multiplyMat2Vect2(&mat->m[1][0], &vect->v[1]) , multiplyMat2Vect2(&mat->m[2][0], &vect->v[2]) ) );
    res->v[1] = cpvadd( multiplyMat2Vect2(&mat->m[0][1], &vect->v[0]) , cpvadd( multiplyMat2Vect2(&mat->m[1][1], &vect->v[1]) , multiplyMat2Vect2(&mat->m[2][1], &vect->v[2]) ) );
    res->v[2] = cpvadd( multiplyMat2Vect2(&mat->m[0][2], &vect->v[0]) , cpvadd( multiplyMat2Vect2(&mat->m[1][2], &vect->v[1])  , multiplyMat2Vect2(&mat->m[2][2], &vect->v[2]) ) );
    
    
}

static inline cpVect multiplyMat2x2byMat2x3byVect3(const Matrix2* m22, const cpMat2x3* m23, const cpVect3* v3 )
{
    cpVect res = multiplyMat2x3byVect3(m23, v3);
    return multiplyMat2Vect2(m22, &res);
}

// res = m1*m2t
static inline void multiplyMat3xMat3Tranpose(Matrix3* res, const Matrix3* m1,const Matrix3* m2)
{
    Matrix3 __attribute__ ((aligned (16))) m2t;
    transposeMat3( &m2t, m2);
    multiplyMat3Vect3( &res->col[0], m1, &m2t.col[0] );
    multiplyMat3Vect3( &res->col[1], m1, &m2t.col[1] );
    multiplyMat3Vect3( &res->col[2], m1, &m2t.col[2] );
}



static inline void multiplyMat3Vect3andAdd(cpVect3* res, const Matrix3* m, const cpVect3* v)
{
    const __m128 col0 = _mm_load_ps((float *) &m->col[0]);
    const __m128 col1 = _mm_load_ps((float *) &m->col[1]);
    const __m128 col2 = _mm_load_ps((float *) &m->col[2]);
    
    const __m128 mv = _mm_load_ps((float *) v);
    const __m128 mvx = _mm_shuffle_ps(mv, mv, _MM_SHUFFLE(0,0,0,0));
    const __m128 mvy = _mm_shuffle_ps(mv, mv, _MM_SHUFFLE(1,1,1,1));
    const __m128 mvz = _mm_shuffle_ps(mv, mv, _MM_SHUFFLE(2,2,2,2));
    
    _mm_store_ps( (float*)res,  _mm_add_ps( _mm_load_ps((float *) res),
                                           _mm_add_ps( _mm_mul_ps(col0, mvx), _mm_add_ps( _mm_mul_ps(col1, mvy) ,  _mm_mul_ps(col2, mvz)) ) ) );
    
   
               
    return;
}

static inline void multiplyMat3Vect3andSub(cpVect3* res, const Matrix3* m, const cpVect3* v)
{
    const __m128 col0 = _mm_load_ps((float *) &m->col[0]);
    const __m128 col1 = _mm_load_ps((float *) &m->col[1]);
    const __m128 col2 = _mm_load_ps((float *) &m->col[2]);
    
    const __m128 mv = _mm_load_ps((float *) v);
    const __m128 mvx = _mm_shuffle_ps(mv, mv, _MM_SHUFFLE(0,0,0,0));
    const __m128 mvy = _mm_shuffle_ps(mv, mv, _MM_SHUFFLE(1,1,1,1));
    const __m128 mvz = _mm_shuffle_ps(mv, mv, _MM_SHUFFLE(2,2,2,2));
    
    _mm_store_ps( (float*)res,  _mm_sub_ps( _mm_load_ps((float *) res),
                                           _mm_add_ps( _mm_mul_ps(col0, mvx), _mm_add_ps( _mm_mul_ps(col1, mvy) ,  _mm_mul_ps(col2, mvz)) ) ) );
}


static inline  void matVecProd(cpVect* res, const Matrix2* mat, const cpVect* vec, int nbRows,
                               const cpIdx* cols, const cpIdx* rows )

{
    
    cpIdx nbcols;
    
    while ( nbRows-- > 0 )
    {
        nbcols = *(rows+1) - *rows;
        
        res->x = 0.0;
        res->y = 0.0;
        
        while (nbcols-- > 0)
        {
            multiplyMat2Vect2andAdd(res, mat++, &vec[(*cols++)]);
            //            *res += (*mat++) * vec[(*cols++)];
        }
        ++res;
        ++rows;
    }
    
}

static inline void invMatrix3_sse(Matrix3* m_inv, const Matrix3* m)
{
    
    cpVect3 CP_ALIGN(16) tmp0 = cross3( &m->col[1], &m->col[2]);
    cpVect3 CP_ALIGN(16) tmp1 = cross3( &m->col[2], &m->col[0]);
    cpVect3 CP_ALIGN(16) tmp2 = cross3( &m->col[0], &m->col[1]);
    cpFloat detinv = ( 1.0 / dot3( &m->col[2], &tmp2 ) );
    
//    m_inv->col[0].x = tmp0.x * detinv;
//    m_inv->col[0].y = tmp1.x * detinv;
//    m_inv->col[0].z = tmp2.x * detinv;
//    m_inv->col[1].x = tmp0.y * detinv;
//    m_inv->col[1].y = tmp1.y * detinv;
//    m_inv->col[1].z = tmp2.y * detinv;
//    m_inv->col[2].x = tmp0.z * detinv;
//    m_inv->col[2].y = tmp1.z * detinv;
//    m_inv->col[2].z = tmp2.z * detinv;

    const __m128 scalar = _mm_setr_ps(detinv,detinv,detinv,detinv);
    
    _mm_store_ps( (float*)&m_inv->col[0], _mm_mul_ps( scalar, _mm_setr_ps(tmp0.x, tmp1.x, tmp2.x, 0.0) ) );
    _mm_store_ps( (float*)&m_inv->col[1], _mm_mul_ps( scalar, _mm_setr_ps(tmp0.y, tmp1.y, tmp2.y, 0.0) ) );
    _mm_store_ps( (float*)&m_inv->col[2], _mm_mul_ps( scalar, _mm_setr_ps(tmp0.z, tmp1.z, tmp2.z, 0.0) ) );
    
    
}


#endif
