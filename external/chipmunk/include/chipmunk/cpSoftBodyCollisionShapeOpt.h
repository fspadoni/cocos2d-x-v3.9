//
//  cpSoftBodyCollisionShapeOpt.h
//  Chipmunk7
//
//  Created by Federico Spadoni on 13/11/14.
//
//

#ifndef Chipmunk7_cpSoftBodyCollisionShapeOpt_h
#define Chipmunk7_cpSoftBodyCollisionShapeOpt_h


struct cpMeshBuffer;
struct cpMembuffer;


unsigned int cpSoftBodyShapeOptComputeMemorySize(const unsigned int numNodes, const unsigned int numElems, const unsigned int graphSize);

unsigned int cpSoftBodyShapeOptGetBufferSize(const cpSoftBodyShapeOpt* shape);


/// Allocate a triangle shape.
cpSoftBodyShapeOpt* cpSoftBodyShapeOptAlloc(void);

cpSoftBodyShapeOpt* cpSoftBodyShapeOptInitFromMeshBuffer( struct cpMeshBuffer* mesh, const cpFloat collisionMargin);

cpSoftBodyShapeOpt* cpSoftBodyShapeOptCopy( cpSoftBodyShapeOpt* other, struct cpMembuffer* buffer);

//cpSoftBodyShapeOpt* cpSoftBodyShapeOptInitFromFile(cpSoftBodyShapeOpt *shape, char* filename);

//cpPointShape * cpPointShapeInit(cpPointShape *shape, cpBody *body, const cpIdx idx, const cpFloat margin, const cpVect* verts, const cpVect* velocities);
//
///// Initialize a triangle shape.
//cpTriangleShape* cpTriangleShapeInit(cpTriangleShape *shape, cpBody *body, const cpTriangle* triang, const cpVect* verts, const cpVect* velocities);

/// Initialize a triangle shape.
cpSoftBodyShapeOpt* cpSoftBodyShapeOptInit(cpSoftBodyShapeOpt *circle, cpSoftBody *body, cpVect* vertices, unsigned int numVertices,  cpIdx *triangles, unsigned int numTriangles );

void cpSoftBodyShapeOptAddToSpace(cpSoftBodyShapeOpt* shape, cpSpace* space );


/// Allocate and initialize a triangle shape.
cpShape* cpSoftBodyShapeOptNew(cpSoftBody *body, cpVect* vertices, unsigned int numVertices,
                            cpIdx *triangles, unsigned int numTriangles );


void cpSoftBodyShapeOptRemove(cpSpace* space, cpSoftBodyShapeOpt *shape);

void cpSoftBodyShapeOptDelete(cpSoftBodyShapeOpt *shape);


unsigned int cpSoftBodyShapeOptGetMemSize(cpSoftBodyShapeOpt *shape);

//CP_DeclareShapeGetter(cpSoftBodyShape, cpVect*, verts);
//CP_DeclareShapeGetter(cpSoftBodyShape, unsigned int, numVerts);
//CP_DeclareShapeGetter(cpSoftBodyShape, unsigned short*, triangs);
//CP_DeclareShapeGetter(cpSoftBodyShape, unsigned int, numTriangs);


cpVect* cpSoftBodyShapeOptGetVerts(const cpSoftBodyShapeOpt *shape);

unsigned int cpSoftBodyShapeOptGetNumVerts(const cpSoftBodyShapeOpt *shape);

unsigned int cpSoftBodyShapeOptGetNumActiveVerts(const cpSoftBodyShapeOpt *shape);

cpIdx* cpSoftBodyShapeOptGetTriangles(const cpSoftBodyShapeOpt *shape);

unsigned int cpSoftBodyShapeOptGetNumTriangles(const cpSoftBodyShapeOpt *shape);

unsigned int cpSoftBodyShapeOptGetNumActiveTriangles(const cpSoftBodyShapeOpt *shape);

cpIdx cpSoftBodyShapeOptGetFirstActiveTriangles(const cpSoftBodyShapeOpt *shape);

const cpIdx* cpSoftBodyShapeOptGetActiveTriangles(const cpSoftBodyShapeOpt *shape);


void cpSoftBodyShapeOptSetFilter(cpSoftBodyShapeOpt *shape, unsigned int category, unsigned int mask);

void cpSoftBodyShapeOptSetPointFilter(cpSoftBodyShapeOpt *shape, cpIdx nodeId, unsigned int category, unsigned int mask);

void cpSoftBodyShapeOptSetTriangleFilter(cpSoftBodyShapeOpt *shape, cpIdx triagleId, unsigned int category, unsigned int mask);



#endif
