//
//  cpSoftBody.h
//  Chipmunk7
//
//  Created by Federico Spadoni on 23/06/14.
//
//

#ifndef Chipmunk7_cpSoftBody_h
#define Chipmunk7_cpSoftBody_h


struct cpMesh;

/// Rigid body velocity update function type.
typedef void (*cpSoftBodyVelocityFunc)(cpSoftBody *body, cpVect gravity, cpFloat damping, cpFloat dt);
/// Rigid body position update function type.
typedef void (*cpSoftBodyPositionFunc)(cpSoftBody *body, cpFloat dt);

typedef cpFloat (*cpAssembleStiffMatrixFunc)(cpSoftBody *body);

typedef cpFloat (*cpSoftBodySolveFunc)(cpSoftBody* body);


size_t cpSoftBodyComputeMemorySize(const unsigned int numNodes, const unsigned int numElems, const unsigned int graphSize);

size_t cpSoftBodyComputeMemorySizeFromMesh(const struct cpMesh *mesh);

//ssize_t cpSoftBodyGetMemorySize(const cpSoftBody *body);

/// Allocate a cpSoftBody.
cpSoftBody* cpSoftBodyAlloc(void);
/// Initialize a cpSoftBody.
cpSoftBody* cpSoftBodyInit(cpSoftBody *body, const cpSoftBodyParams* params );
/// Allocate and initialize a cpSoftBody.
cpSoftBody* cpSoftBodyNew(const cpSoftBodyParams* params);



/// Destroy a cpSoftBody.
void cpSoftBodyDestroy(cpSoftBody *body);
/// Destroy and free a cpSoftBody.
void cpSoftBodyFree(cpSoftBody *body);

void cpSoftBodyAddShape(cpSoftBody *body, cpSoftBodyShape *shape);

cpSoftBodyShape* cpSoftBodyRemoveShape(cpSoftBody *body );


cpSoftBodyShape* cpGetSoftBodyShape(cpSoftBody *body);


// Defined in cpSpace.c
/// Wake up a sleeping or idle soft body.
void cpSoftBodyActivate(cpSoftBody *body);
/// Wake up any sleeping or idle soft bodies touching a static body.
void cpSoftBodyActivateStatic(cpSoftBody *body, cpShape *filter);

/// Force a soft body to fall asleep immediately.
void cpSoftBodySleep(cpSoftBody *body);
/// Force a soft body to fall asleep immediately along with other bodies in a group.
void cpSoftBodySleepWithGroup(cpSoftBody *body, cpSoftBody *group);

/// Returns true if the  soft body is sleeping.
cpBool cpSoftBodyIsSleeping(const cpSoftBody *body);




/// Get the space this body is added to.
cpSpaceSoftBody* cpSoftBodyGetSpace(const cpSoftBody *body);

/// Get the nodes of the body.
const cpVect* cpSoftBodyGetNodes(const cpSoftBody *body);



/// Get the mass of the body.
cpFloat cpSoftBodyGetMass(const cpSoftBody *body);
/// Set the mass of the body.
void cpSoftBodySetMass(cpSoftBody *body, cpFloat m);



/// Set the position of a body.
cpVect cpSoftBodyGetPosition(const cpSoftBody *body);
/// Set the position of the body.
void cpSoftBodySetPosition(cpSoftBody *body, const cpVect pos);


/// Rotate the soft body around the cog.
void cpSoftBodyRotateAroundCoG(cpSoftBody *body, cpFloat a);

/// Mirror the soft body around the cog.
void cpSoftBodyReflectAroundCoG(cpSoftBody *body, cpVect axis);


///// Get the offset of the center of gravity in body local coordinates.
//cpVect cpSoftBodyGetCenterOfGravity(const cpSoftBody *body);
///// Set the offset of the center of gravity in body local coordinates.
//void cpSoftBodySetCenterOfGravity(cpSoftBody *body, cpVect cog);

/// Get the velocity of the body.
cpVect cpSoftBodyGetVelocity(const cpSoftBody *body);
/// Set the velocity of the body.
void cpSoftBodySetVelocity(cpSoftBody *body, cpVect velocity);

/// Get the force applied to the body for the next time step.
cpVect cpSoftBodyGetForce(const cpSoftBody *body);
/// Set the force applied to the body for the next time step.
void cpSoftBodySetForce(cpSoftBody *body, cpVect force);


cpVect cpSoftBodygGetCoG(const cpSoftBody* body);

/// Get the moment of inertia of the soft body.
cpFloat cpSoftBodyGetInertia(const cpSoftBody *body);

/// Get the user data pointer assigned to the body.
cpDataPointer cpSoftBodyGetUserData(const cpSoftBody *body);
/// Set the user data pointer assigned to the body.
void cpSoftBodySetUserData(cpSoftBody *body, cpDataPointer userData);

/// Set the callback used to update a soft body's velocity.
void cpSoftBodySetVelocityUpdateFunc(cpSoftBody *body, cpSoftBodyVelocityFunc velocityFunc);
/// Set the callback used to update a body's position.
/// NOTE: It's not generally recommended to override this.
void cpSoftBodySetPositionUpdateFunc(cpSoftBody *body, cpSoftBodyPositionFunc positionFunc);

/// Default velocity integration function..
void cpSoftBodyUpdateVelocity(cpSoftBody *body, cpVect gravity, cpFloat damping, cpFloat dt);
/// Default position integration function.
void cpSoftBodyUpdatePosition(cpSoftBody *body, cpFloat dt);

/// Convert body relative/local coordinates to absolute/world coordinates.
cpVect cpSoftBodyLocalToWorld(const cpSoftBody *body, const cpVect point);
/// Convert body absolute/world coordinates to  relative/local coordinates.
cpVect cpSoftBodyWorldToLocal(const cpSoftBody *body, const cpVect point);


/// Apply a force to a body. Both the force and point are expressed in world coordinates.
void cpSoftBodyApplyForceAtWorldPoint(cpSoftBody *body, cpVect force, cpVect point);
/// Apply a force to a body. Both the force and point are expressed in body local coordinates.
void cpSoftBodyApplyForceAtLocalPoint(cpSoftBody *body, cpVect force, cpVect point);


/// Apply an impulse to a body. Both the impulse and point are expressed in world coordinates.
void cpSoftBodyApplyImpulseAtWorldPoint(cpSoftBody *body, cpVect impulse, cpVect point);
/// Apply an impulse to a body. Both the impulse and point are expressed in body local coordinates.
void cpSoftBodyApplyImpulseAtLocalPoint(cpSoftBody *body, cpVect impulse, cpVect point);



/// Get the amount of kinetic energy contained by the body.
cpFloat cpSoftBodyKineticEnergy(const cpSoftBody *body);

/// Body/shape iterator callback function type.
typedef void (*cpSoftBodyShapeIteratorFunc)(cpSoftBody *body, cpShape *shape, void *data);
/// Call @c func once for each shape attached to @c body and added to the space.
void cpSoftBodyEachShape(cpSoftBody *body, cpSoftBodyShapeIteratorFunc func, void *data);


/// Body/constraint iterator callback function type.
typedef void (*cpSoftBodyConstraintIteratorFunc)(cpSoftBody *body, cpConstraint *constraint, void *data);
/// Call @c func once for each constraint attached to @c body and added to the space.
void cpSoftBodyEachConstraint(cpSoftBody *body, cpSoftBodyConstraintIteratorFunc func, void *data);


/// Body/arbiter iterator callback function type.
typedef void (*cpSoftBodyArbiterIteratorFunc)(cpSoftBody *body, cpArbiter *arbiter, void *data);
/// Call @c func once for each arbiter that is currently active on the body.
void cpSoftBodyEachArbiter(cpSoftBody *body, cpSoftBodyArbiterIteratorFunc func, void *data);


cpFloat cpSoftBodyAssembleStiffnessMatrix(cpSoftBody *body );

cpFloat cpSoftBodyComputeStreesForces(cpSoftBody *body);

cpFloat computeCompliance(cpSoftBody* body);

cpFloat cpSoftBodyUpdateVertexVels(cpSoftBody *body);

cpFloat cpSoftBodyComputePlasticity(cpSoftBody *body);

cpFloat cpSoftBodyComputeRhs(cpSoftBody *body);

cpFloat cpSoftBodyComputeLhs(cpSoftBody *body);

cpFloat cpSoftBodyConjGradSolver(cpSoftBody *body);


cpFloat cpSoftBodySolve(cpSoftBody *body);

cpVect cpSoftBodyGetStressForce(const cpSoftBody *body, const cpIdx nodeId);

cpVect* cpSoftBodyGetStressForces( const cpSoftBody *body );

cpFloat cpSoftBodyGetVertexMass( const cpSoftBody *body, const cpIdx idx);

//void cpSoftBodySetPosition( cpSoftBody *body );

void cpSoftBodySetOrientation( cpSoftBody *body, cpMat2x2 rot );

void cpSoftBodyReset( cpSoftBody *body );

//void cpSoftBodySetVelocity( cpVect vel, cpSoftBody *body );

void cpSoftBodyAddForce( cpSoftBody *body, const unsigned short idx, cpVect force);

void cpSoftBodyGetCompliance(cpSoftBody *body, const cpIdx nodeId, Matrix2* compliance);


struct cpMembuffer;

void printVectbuffer(const struct cpMembuffer* buf, const char* filename);
void printStiffnessMatrix( const cpSoftBody *body, const char* filename );


// be careful
void removeElements(cpSoftBody *body);


// not used
void computeStressForces(cpSoftBody *body );

cpVect cpSoftBodyGetCoGvelocity(const cpSoftBody *body);


///@}


#endif
