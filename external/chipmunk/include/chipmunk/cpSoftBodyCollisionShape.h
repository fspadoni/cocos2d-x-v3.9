//
//  cpSoftBodyShape.h
//  Chipmunk6
//
//  Created by Federico Spadoni on 12/06/14.
//
//

#ifndef Chipmunk7_cpSoftBodyCollisionShape_h
#define Chipmunk7_cpSoftBodyCollisionShape_h


struct cpMesh;



size_t cpSoftBodyShapeComputeMemorySize(const unsigned int numNodes, const unsigned int numElems, const unsigned int graphSize);

size_t cpSoftBodyShapeComputeMemorySizeFromMesh(const struct cpMesh *mesh);


/// Allocate a triangle shape.
cpSoftBodyShape* cpSoftBodyShapeAlloc(void);

cpSoftBodyShape *cpSoftBodyShapeInitFromMesh(cpSoftBodyShape* shape, struct cpMesh* m);


//cpSoftBodyShape* cpSoftBodyShapeInitFromFile(cpSoftBodyShape *shape, char* filename);

//cpPointShape * cpPointShapeInit(cpPointShape *shape, cpBody *body, const cpIdx idx, const cpFloat margin, const cpVect* verts, const cpVect* velocities);
//
///// Initialize a triangle shape.
//cpTriangleShape* cpTriangleShapeInit(cpTriangleShape *shape, cpBody *body, const cpTriangle* triang, const cpVect* verts, const cpVect* velocities);

/// Initialize a triangle shape.
cpSoftBodyShape* cpSoftBodyShapeInit(cpSoftBodyShape *circle, cpSoftBody *body, cpVect* vertices, unsigned int numVertices,
                                     cpIdx *triangles, unsigned int numTriangles );

void cpSoftBodyShapeAddToSpace(cpSoftBodyShape* shape, cpSpace* space );

/// Allocate and initialize a triangle shape.
cpShape* cpSoftBodyShapeNew(cpSoftBody *body, cpVect* vertices, unsigned int numVertices,
                            cpIdx *triangles, unsigned int numTriangles );


void cpSoftBodyShapeRemove(cpSpace* space, cpSoftBodyShape *shape);

void cpSoftBodyShapeDelete(cpSoftBodyShape *shape);

void cpSoftBodyShapeCopy(cpSoftBodyShape *dest, cpSoftBodyShape *src);


unsigned int cpSoftBodyShapeGetMemSize(cpSoftBodyShape *shape);

//CP_DeclareShapeGetter(cpSoftBodyShape, cpVect*, verts);
//CP_DeclareShapeGetter(cpSoftBodyShape, unsigned int, numVerts);
//CP_DeclareShapeGetter(cpSoftBodyShape, unsigned short*, triangs);
//CP_DeclareShapeGetter(cpSoftBodyShape, unsigned int, numTriangs);


cpVect* cpSoftBodyShapeGetVerts(const cpSoftBodyShape *shape);

unsigned int cpSoftBodyShapeGetNumVerts(const cpSoftBodyShape *shape);

unsigned int cpSoftBodyShapeGetNumActiveVerts(const cpSoftBodyShape *shape);

cpTriangle* cpSoftBodyShapeGetTriangles(const cpSoftBodyShape *shape);

unsigned int cpSoftBodyShapeGetNumTriangles(const cpSoftBodyShape *shape);

unsigned int cpSoftBodyShapeGetNumActiveTriangles(const cpSoftBodyShape *shape);

cpIdx cpSoftBodyShapeGetFirstActiveTriangles(const cpSoftBodyShape *shape);

const cpIdx* cpSoftBodyShapeGetActiveTriangles(const cpSoftBodyShape *shape);


void cpSoftBodyShapeSetFilter(cpSoftBodyShape *shape, unsigned int category, unsigned int mask);

void cpSoftBodyShapeSetPointFilter(cpSoftBodyShape *shape, cpIdx nodeId, unsigned int category, unsigned int mask);

void cpSoftBodyShapeSetTriangleFilter(cpSoftBodyShape *shape, cpIdx triagleId, unsigned int category, unsigned int mask);


#endif
