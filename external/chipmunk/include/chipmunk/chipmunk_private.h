/* Copyright (c) 2013 Scott Lembcke and Howling Moon Software
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef CHIPMUNK_PRIVATE_H
#define CHIPMUNK_PRIVATE_H
#ifdef CHIPMUNK_H
#error Cannot include chipmunk_private.h after chipmunk.h.
#endif

#define CP_ALLOW_PRIVATE_ACCESS 1
#include "chipmunk.h"
#include "cpVectorMath.h"

#define CP_HASH_COEF (3344921057ul)
#define CP_HASH_PAIR(A, B) ((cpHashValue)(A)*CP_HASH_COEF ^ (cpHashValue)(B)*CP_HASH_COEF)

// TODO: Eww. Magic numbers.
#define MAGIC_EPSILON 1e-5


//MARK: cpArray

struct cpArray {
	int num, max;
	void **arr;
};

cpArray *cpArrayNew(int size);

void cpArrayFree(cpArray *arr);

void cpArrayPush(cpArray *arr, void *object);
void *cpArrayPop(cpArray *arr);
void cpArrayDeleteObj(cpArray *arr, void *obj);
cpBool cpArrayContains(cpArray *arr, void *ptr);

//void cpArrayFreeEach(cpArray *arr, void (freeFunc)(void*));
void cpArrayFreeEach(cpArray *arr, void (freeFunc)(void*) ,unsigned int sz);

//MARK: cpHashSet

typedef cpBool (*cpHashSetEqlFunc)(void *ptr, void *elt);
typedef void *(*cpHashSetTransFunc)(void *ptr, void *data);

cpHashSet *cpHashSetNew(int size, cpHashSetEqlFunc eqlFunc);
void cpHashSetSetDefaultValue(cpHashSet *set, void *default_value);

void cpHashSetFree(cpHashSet *set);

int cpHashSetCount(cpHashSet *set);
void *cpHashSetInsert(cpHashSet *set, cpHashValue hash, void *ptr, cpHashSetTransFunc trans, void *data);
void *cpHashSetRemove(cpHashSet *set, cpHashValue hash, void *ptr);
void *cpHashSetFind(cpHashSet *set, cpHashValue hash, void *ptr);

typedef void (*cpHashSetIteratorFunc)(void *elt, void *data);
void cpHashSetEach(cpHashSet *set, cpHashSetIteratorFunc func, void *data);


typedef cpBool (*cpHashSetFilterFunc)(void *elt, void *data);
void cpHashSetFilter(cpHashSet *set, cpHashSetFilterFunc func, void *data);



unsigned int cpHashSetGetSize(cpHashSet *set);

cpHashSetBin *cpHashSetGetBin(cpHashSet* set, unsigned int count);



//MARK: Bodies

struct cpBody {
	// Integration functions
	cpBodyVelocityFunc velocity_func;
	cpBodyPositionFunc position_func;
	
	// mass and it's inverse
	cpFloat mass;
	cpFloat mass_inv;
	
	// moment of inertia and it's inverse
	cpFloat inertia;
	cpFloat inertia_inv;
	
	// center of gravity
	cpVect cog;
	
	// position, velocity, force
	cpVect p;
	cpVect v;
	cpVect f;
	
	// Angle, angular velocity, torque (radians)
	cpFloat a;
	cpFloat w;
	cpFloat t;
	
	cpTransform transform;
	
	cpDataPointer userData;
	
	// "pseudo-velocities" used for eliminating overlap.
	// Erin Catto has some papers that talk about what these are.
	cpVect v_bias;
	cpFloat w_bias;
	
	cpSpace *space;
	
	cpShape *shapeList;
	cpArbiter *arbiterList;
	cpConstraint *constraintList;
	
	struct {
		cpBody *root;
		cpBody *next;
		cpFloat idleTime;
	} sleeping;
    
    int tag;			// used by dynamics algorithms
    int island_tag;			// used by dynamics algorithms
    Matrix3 compliance;
    const void* softbody;
};

void cpBodyAddShape(cpBody *body, cpShape *shape);
void cpBodyRemoveShape(cpBody *body, cpShape *shape);

//void cpBodyAccumulateMassForShape(cpBody *body, cpShape *shape);
void cpBodyAccumulateMassFromShapes(cpBody *body);

void cpBodyRemoveConstraint(cpBody *body, cpConstraint *constraint);




//MARK: Bodies


struct cpSoftBody {
	// Integration functions
	cpSoftBodyVelocityFunc velocity_func;
	cpSoftBodyPositionFunc position_func;
	
    cpSoftBodySolveFunc solve_func;
    cpAssembleStiffMatrixFunc assembleMatrix_func;
    
    cpBody collBody;
    
    struct cpMaterial* mat;
    
    
	// mass and it's inverse
	cpFloat mass;
	cpFloat mass_inv;
	
//	// moment of inertia and it's inverse
	cpFloat i;
	cpFloat i_inv;
	
//	// center of gravity
	cpVect cog;
	
	// position, velocity, force
	cpVect* p;
	cpVect* v;
	cpVect* f;
	
//	// Angle, angular velocity, torque (radians)
//	cpFloat a;
//	cpFloat w;
//	cpFloat t;
	
//	cpTransform transform;
	
	cpDataPointer userData;
	
//	// "pseudo-velocities" used for eliminating overlap.
//	// Erin Catto has some papers that talk about what these are.
//	cpVect v_bias;
//	cpFloat w_bias;
	
	cpSpaceSoftBody *space;
	
    cpSoftBodyShape *shape;
	cpShape *shapeList;
	cpArbiter *arbiterList;
	cpConstraint *constraintList;
	
	struct {
		cpSoftBody *root;
		cpSoftBody *next;
		cpFloat idleTime;
	} sleeping;
    
    cpVect gravity;
    
    // precomputed data
    cpFloat timeStep;
    cpFloat timeStep2;
    cpFloat invTimeStep;
    cpFloat Cdt; // damping*timestep
    cpFloat creepdt;
    
    cpFloat yield;
    cpFloat maxYield;
    cpBool isPlastic;
    
    cpFloat toughness;
    cpBool isBrakable;
    
    // solver
    unsigned int nbSolverIteration;
    cpFloat solverTolerance;
    
    unsigned int numConstrainedNodes;
    

    
    struct femBuffers {
        
        struct cpMembuffer* nodes;
        
        struct cpMembuffer* constrNodes;    // idx. size nb contr. nodes
        struct cpMembuffer* externForces;   // cpVect. size nb nodes
        struct cpMembuffer* forces;         // cpVect. size nb nodes
        struct cpMembuffer* stressForces;   // cpVect. size nb nodes
       
        struct cpMembuffer* nodeMasses;     // cpFloat. size nb nodes;
        
        struct cpMembuffer* elasticityMat;  // .size 1;
        struct cpMembuffer* strainMat;      // 3x2|3x2|3x2. size nb elems;
        struct cpMembuffer* Ke;             // size nb elems;
        struct cpMembuffer* undeformedStateVec; // Matrix3. size nb elems;
        struct cpMembuffer* Kx0;                // Vect6. size nb elems;
        struct cpMembuffer* kmap;               // Kmap. size nb elems;
        // not necessary to store
        struct cpMembuffer* elemRotations;      // Matrix2. size nb elems
        
        // plasticity
        struct cpMembuffer* Pe;             // Plasticity Matrix. size nb elems;
        struct cpMembuffer* x0;             // vec6. size nb elems;
        struct cpMembuffer* totalStrain;    // strain. size nb elems;
        struct cpMembuffer* elasticStrain;  // strain. size nb elems;;
        struct cpMembuffer* plasticStrain;  // strain. size nb elems;
        struct cpMembuffer* plasticForce;   // cpVect. size nb elems;
        
        // stress
        struct cpMembuffer* stressTensor; // matrix to compute stress. used for fractures
        
        // stiffness matrix
//        struct cpMembuffer* Ke2StiffnessMatMap ;    // ;
        struct cpMembuffer* stiffnessMat;           // Matrix2. size graph elems;
        
        //    used by solver
        struct cpMembuffer* rhsVect;            // cpVect. size nb nodes
        struct cpMembuffer* newNodeVelVect;     // cpVect. size nb nodes
        struct cpMembuffer* resVect;            // cpVect. size nb nodes
        struct cpMembuffer* directVect;         // cpVect. size nb nodes
        struct cpMembuffer* tempVect;           // cpVect. size nb nodes
        
        // contacts
        struct cpMembuffer* compliance;
        struct cpMembuffer* velocity_bias;
        
//        // fracture
//        struct cpMembuffer* activeElems;
//        struct cpMembuffer* activeNodes;
//        struct cpMembuffer* removedElems;
//        struct cpMembuffer* removedNodes;
        
        // area
        struct cpMembuffer* elemAreas;
    } fem;
        
        
};

struct cpSoftBodyOpt {
	// Integration functions
	cpSoftBodyOptVelocityFunc velocity_func;
	cpSoftBodyOptPositionFunc position_func;
	// solve functions
    cpSoftBodyOptSolveFunc solve_func;
    cpAssembleStiffMatrixOptFunc assembleMatrix_func;
    // callbacks
    cpSoftBodyOptSleepingCallback sleeping_callback;
	cpSoftBodyOptTriangleDestroyCallback triangleDestroy_callback;
    cpSoftBodyOptDestroyCallback destroy_callback;
    
    cpSoftBodyOptCollisionBeginCallback collisionBegin_callback;
    cpSoftBodyOptCollisionSeparateCallback collisionSeparate_callback;
    
	// mass and it's inverse
	cpFloat mass;
	cpFloat mass_inv;
	
//    //	// moment of inertia and it's inverse
//	cpFloat i;
//	cpFloat i_inv;
	
    //	// center of gravity
	cpVect cog;
	
	// position, velocity, force
	cpVect* p;
	cpVect* v;
//	cpVect* f;
	
	cpDataPointer userData;
	
	cpSpaceSoftBody *space;
	
    cpSoftBodyShapeOpt *shape;
	cpShape *shapeList;
	cpArbiter *arbiterList;
	cpConstraint *constraintList;
	
	struct {
		cpSoftBodyOpt *root;
		cpSoftBodyOpt *next;
		cpFloat idleTime;
	} sleeping;
//    cpBool awake;
    
    cpVect gravity;
    
//    cpMaterial material;
    
    
    cpSoftBodySolverParams solverParams;
    
    cpBool isPlastic;
    cpBool isBrakable;

    
    unsigned int numConstrainedNodes;
    
//    notifications
//    cpBool notifyCollision;
    
    // precomputed data
//    cpFloat timeStep2;
//    cpFloat invTimeStep;
//    cpFloat Cdt; // damping*timestep
//    cpFloat creepdt;
//    cpFloat rayleighStiffnessK; // dt*(stiffnessdamping + dt)
//    cpFloat rayleighMassK; // (1+dt*massdamping)

    unsigned int bufferSize;
    
    struct femData
    {
        cpMaterial* materials;
        
        cpVect* const freeNodes;
        
        cpIdx* const constrNodes;    // idx. size nb contr. nodes
        cpVect* const externForces;   // cpVect. size nb nodes
        cpVect* const forces;         // cpVect. size nb nodes
        cpVect* const stressForces;   // cpVect. size nb nodes
        
//        cpFloat* const nodeMasses;     // cpFloat. size nb nodes;
        cpVect* const nodeMasses;     // cpVect. size nb nodes; sse2 parallelization
        
        ElasticityMatrix* const elasticityMat;  // .size 1;
        StrainMatrix* const strainMat;      // 3x2|3x2|3x2. size nb elems;
        ElementStiffnessMatrix* const Ke;             // size nb elems;
        Matrix3* const undeformedStateVec; // Matrix3. size nb elems;
//        cpVect6* const Kx0;                // Vect6. size nb elems;
        Kmap* const kmap;               // Kmap. size nb elems;
        // not necessary to store
        Matrix2* const elemRotations;      // Matrix2. size nb elems
        
        // plasticity
        PlasticityMatrix* const Pe;             // Plasticity Matrix. size nb elems;
        cpVect6* const x0;             // vec6. size nb elems;
        cpVect3* const totalStrain;    // strain. size nb elems;
        cpVect3* const elasticStrain;  // strain. size nb elems;;
        cpVect3* const plasticStrain;  // strain. size nb elems;
//        cpVect* const plasticForce;   // cpVect. size nb elems;
        
        // stress
        cpVect3* const stressTensor; // matrix to compute stress. used for fractures
        
        // stiffness matrix
        Matrix2* stiffnessMat;           // Matrix2. size graph elems;
        
        //    used by solver
        cpVect* const rhsVect;            // cpVect. size nb nodes
        cpVect* const newNodeVelVect;     // cpVect. size nb nodes
        cpVect* const resVect;            // cpVect. size nb nodes
        cpVect* const directVect;         // cpVect. size nb nodes
        cpVect* const tempVect;           // cpVect. size nb nodes
        
        // contact resolution
        Matrix3* const compliance;
        cpVect* const  velocity_bias;
        
        // area
        cpFloat* const elemAreas;
        
    } fem;
    
};

struct cpBeamBody {
	// Integration functions
	cpBeamBodyVelocityFunc velocity_func;
	cpBeamBodyPositionFunc position_func;
	// solve functions
    cpBeamBodySolveFunc solve_func;
    cpAssembleBeamStiffMatrixFunc assembleMatrix_func;
    // callbacks
    cpBeamBodySleepingCallback sleeping_callback;
	cpBeamBodySegmentDestroyCallback segmentDestroy_callback;
    cpBeamBodyDestroyCallback destroy_callback;
    
    cpBeamBodyCollisionBeginCallback collisionBegin_callback;
    cpBeamBodyCollisionSeparateCallback collisionSeparate_callback;
    
	// mass and it's inverse
	cpFloat mass;
	cpFloat mass_inv;
	
    cpFloat I;
    cpFloat I_inv;
    
    //    //	// moment of inertia and it's inverse
    //	cpFloat i;
    //	cpFloat i_inv;
	
    //	// center of gravity
	cpVect cog;
	
	// position, velocity, force
	cpVect* p;
	cpVect* v;
    //	cpVect* f;
	
	cpDataPointer userData;
	
	cpSpaceSoftBody *space;
	
    cpBeamShape *shape;
	cpShape *shapeList;
	cpArbiter *arbiterList;
	cpConstraint *constraintList;
	
	struct {
		cpBeamBody *root;
		cpBeamBody *next;
		cpFloat idleTime;
	} sleeping;
    //    cpBool awake;
    
    cpVect gravity;
    
    cpBeamMaterial material;
    cpSoftBodySolverParams solverParams;
    
    cpBool isPlastic;
    cpBool isBrakable;
    
    
    unsigned int numConstrainedNodes;
    
    unsigned int numBlocks;
    
    //    notifications
    //    cpBool notifyCollision;
    
    // precomputed data
    //    cpFloat timeStep2;
    //    cpFloat invTimeStep;
    //    cpFloat Cdt; // damping*timestep
    //    cpFloat creepdt;
    //    cpFloat rayleighStiffnessK; // dt*(stiffnessdamping + dt)
    //    cpFloat rayleighMassK; // (1+dt*massdamping)
    
    unsigned int bufferSize;
    
    struct femBeamData
    {
        
        cpBeamNode* const freeNodes;
        
        cpIdx* const constrNodes;    // idx. size nb contr. nodes
        cpBeamNode* const externForces;   // cpVect3. size nb nodes
        cpBeamNode* const forces;         // cpVect3. size nb nodes
        cpBeamNode* const stressForces;   // cpVect3. size nb nodes
        
        //        cpFloat* const nodeMasses;     // cpFloat. size nb nodes;
        cpBeamNode* const nodeMasses;     // cpVect. size nb nodes; sse2 parallelization
        
        BeamStiffnessMatrix* const Km;     //           size nb elems;
        BeamElasticityMatrix* const elasticityMat;  // .size 1;
        BeamStrainMatrix* const strainMat;      // 3x2|3x2|3x2. size nb elems;
        BeamStiffnessMatrix* const Ke;             // size nb elems;
        cpBeamElem* const displacement; // Matrix3. size nb elems;
        //        cpVect6* const Kx0;                // Vect6. size nb elems;
        KmapBeam* const kmap;               // Kmap. size nb elems;
        // not necessary to store
        Matrix3* const elemRotations;      // Matrix2. size nb elems
        
        // plasticity
        BeamPlasticityMatrix* const Pe;             // Plasticity Matrix. size nb elems;
        cpBeamElem* const x0;             // vec6. size nb elems;
        cpVect* const totalStrain;    // strain. size nb elems;
        cpVect* const elasticStrain;  // strain. size nb elems;;
        cpVect* const plasticStrain;  // strain. size nb elems;
        //        cpVect* const plasticForce;   // cpVect. size nb elems;
        
        // stress
        cpVect* const stressTensor; // matrix to compute stress. used for fractures
        
        // stiffness matrix
        Matrix3* const stiffnessMat;           // Matrix3. size graph elems;
        Matrix3* const massMat;                // Matrix3. size graph elems;
        
        //    used by solver
        cpBeamNode* const rhsVect;            // cpVect. size nb nodes
        cpBeamNode* const newNodeVelVect;     // cpVect. size nb nodes
        cpBeamNode* const resVect;            // cpVect. size nb nodes
        cpBeamNode* const directVect;         // cpVect. size nb nodes
        cpBeamNode* const tempVect;           // cpVect. size nb nodes
        
        //  cholesky dec block matrix
//        Matrix3* const Ld;              // diagonal: Matrix3. size nb nodes
//        Matrix3* const Lc;        // lower diagonal: Matrix3. size nb nodes
        
        Matrix3* const diagFactBlock;       // diagonal: inv of diagonal factorization. Matrix3. size nb nodes
        Matrix3* const lowerFactBlock;      // upper diagonal: Matrix3. size nb nodes
        
        // contact resolution
        Matrix3* const compliance;
        cpBeamNode* const  velocity_bias;
        
        // area
        cpFloat* const elemAreas;
        
        // num segments per block
        cpIdx* const firstSegmentinBlock;
        cpIdx* const lastSegmentinBlock;;

        
    } fem;
    
};


//void cpBodyAccumulateMassForShape(cpSoftBody *body, cpShape *shape);
void cpSoftBodyAccumulateMassFromShapes(cpSoftBody *body);
void cpSoftBodyOptAccumulateMassFromShapes(cpSoftBodyOpt *body);

void cpSoftBodyRemoveConstraint(cpSoftBody *body, cpConstraint *constraint);
void cpSoftBodyOptRemoveConstraint(cpSoftBodyOpt *body, cpConstraint *constraint);

//MARK: Spatial Index Functions

cpSpatialIndex *cpSpatialIndexInit(cpSpatialIndex *index, cpSpatialIndexClass *klass, cpSpatialIndexBBFunc bbfunc, cpSpatialIndex *staticIndex);


//MARK: Arbiters

enum cpArbiterState {
	// Arbiter is active and its the first collision.
	CP_ARBITER_STATE_FIRST_COLLISION,
	// Arbiter is active and its not the first collision.
	CP_ARBITER_STATE_NORMAL,
	// Collision has been explicitly ignored.
	// Either by returning false from a begin collision handler or calling cpArbiterIgnore().
	CP_ARBITER_STATE_IGNORE,
	// Collison is no longer active. A space will cache an arbiter for up to cpSpace.collisionPersistence more steps.
	CP_ARBITER_STATE_CACHED,
	// Collison arbiter is invalid because one of the shapes was removed.
	CP_ARBITER_STATE_INVALIDATED,
};

struct cpArbiterThread {
	struct cpArbiter *next, *prev;
};


// size 64bytes
// packed to 64 bytes
struct cpContact {
	cpVect r1, r2;
	
// aligned to 16 -------
    //    Fede
    cpVect n;
    cpFloat dist;
    cpFloat bounce; // TODO: look for an alternate bounce solution.
    
// aligned to 16 --------
	cpFloat nMass, tMass;
	
    cpFloat jBias, bias;

//  aligned to 16 -----
	cpFloat jnAcc, jtAcc;
	
    cpHashValue hash;
  
// try to packed to 64 bytes if CP_HASH_VALUE_TYPE not defined
#if !defined CP_HASH_VALUE_TYPE
    #if !CP_64_BITS
        char pad[4];
    #endif
#endif
    
};


struct cpCollisionInfo {
	const cpShape *a, *b;
	cpCollisionID id;
	
	cpVect n;
	
	int count;
	// TODO Should this be a unique struct type?
	struct cpContact *arr;
};

struct cpArbiter {
	cpFloat e;
	cpFloat u;
	cpVect surface_vr;
	
	cpDataPointer data;
	
	const cpShape *a, *b;
	cpBody *body_a, *body_b;
	struct cpArbiterThread thread_a, thread_b;
	
	int count;
    int oldCount;
	struct cpContact *contacts[CP_MAX_CONTACTS_PER_ARBITER];
	cpVect n;
	
	// Regular, wildcard A and wildcard B collision handlers.
	cpCollisionHandler *handler, *handlerA, *handlerB;
	cpBool swapped;
	
	cpTimestamp stamp;
	enum cpArbiterState state;
    
    int island_tag;			// used by dynamics algorithms
    cpBool useContactNormals;
};

cpArbiter* cpArbiterInit(cpArbiter *arb, cpShape *a, cpShape *b);

static inline struct cpArbiterThread *
cpArbiterThreadForBody(cpArbiter *arb, cpBody *body)
{
	return (arb->body_a == body ? &arb->thread_a : &arb->thread_b);
}

void cpArbiterUnthread(cpArbiter *arb);

void cpArbiterUpdate(cpArbiter *arb, struct cpCollisionInfo *info, cpSpace *space);
void cpArbiterPreStep(cpArbiter *arb, cpFloat dt, cpFloat bias, cpFloat slop);
void cpArbiterApplyCachedImpulse(cpArbiter *arb, cpFloat dt_coef);
void cpArbiterApplyImpulse(cpArbiter *arb);

// FEde
void cpArbiterUpdateSB(cpArbiter *arb, struct cpCollisionInfo *info, cpSpace *space);

//MARK: Shapes/Collisions

struct cpShapeMassInfo {
	cpFloat m;
	cpFloat i;
	cpVect cog;
	cpFloat area;
};

typedef enum cpShapeType{
	CP_CIRCLE_SHAPE,
	CP_SEGMENT_SHAPE,
	CP_POLY_SHAPE,
    // fede:: added this shape
    //CP_SOFTBODY_SHAPE,
    CP_TRIANGLE_SHAPE,
    CP_POINT_SHAPE,
    CP_BEAM_SEGMENT_SHAPE,
    CP_BEAM_POINT_SHAPE,
    CP_NUM_SHAPES
	
} cpShapeType;

typedef cpBB (*cpShapeCacheDataImpl)(cpShape *shape, cpTransform transform);
typedef unsigned int (*cpShapeDestroyImpl)(cpShape *shape);
typedef void (*cpShapePointQueryImpl)(const cpShape *shape, cpVect p, cpPointQueryInfo *info);
typedef void (*cpShapeSegmentQueryImpl)(const cpShape *shape, cpVect a, cpVect b, cpFloat radius, cpSegmentQueryInfo *info);

typedef struct cpShapeClass cpShapeClass;

struct cpShapeClass {
	cpShapeType type;
	
	cpShapeCacheDataImpl cacheData;
	cpShapeDestroyImpl destroy;
	cpShapePointQueryImpl pointQuery;
	cpShapeSegmentQueryImpl segmentQuery;
};

struct cpShape {
	const cpShapeClass *klass;
	
	cpSpace *space;
	cpBody *body;
	struct cpShapeMassInfo massInfo;
	cpBB bb;
	
	cpBool sensor;
	
	cpFloat e;
	cpFloat u;
	cpVect surfaceV;

	cpDataPointer userData;
	
	cpCollisionType type;
	cpShapeFilter filter;
	
	cpShape *next;
	cpShape *prev;
	
	cpHashValue hashid;
};

struct cpCircleShape {
	cpShape shape;
	
	cpVect c, tc;
	cpFloat r;
};

struct cpSegmentShape {
	cpShape shape;
	
	cpVect a, b, n;
	cpVect ta, tb, tn;
	cpFloat r;
	
	cpVect a_tangent, b_tangent;
};

struct cpSplittingPlane {
	cpVect v0, n;
};

#define CP_POLY_SHAPE_INLINE_ALLOC 6

struct cpPolyShape {
	cpShape shape;
	
	cpFloat r;
	
	int count;
	// The untransformed planes are appended at the end of the transformed planes.
	struct cpSplittingPlane *planes;
	
	// Allocate a small number of splitting planes internally for simple poly.
	struct cpSplittingPlane _planes[2*CP_POLY_SHAPE_INLINE_ALLOC];
};


struct cpSoftBodyShapeOpt {
    // refactor struct
    struct cpMembuffer buffer;
    unsigned int bufferSize;
    
    unsigned int numNodes;
    unsigned int numTriangles;
    unsigned int graphSize;
    unsigned int numMaterials;
    
    // mesh
    cpVect* const nodes;
    cpTriangle* const triangles;
    
    cpIdx* const elemMaterials;
    
    // graph
    cpIdx* const rows;
    cpIdx* const columns;
    cpIdx* const diagonal;
    
    cpVect* const velocities;
    
    // shapes
    cpTriangleShape* const trianglesShape;
    cpPointShape* const pointsShape;
    
    cpBody* const pointsShapeBody;
    cpBody* const trianglesShapeBody;
    
    cpFloat collisionMargin;
    
    unsigned int numActiveNodes;
    unsigned int numActiveTriangles;
    
    cpIdx firstActiveNode;
    cpIdx firstActiveTriangle;
    
    cpIdx* const activeNodes;
    cpIdx* const activeTriangles;
    
};


struct cpBeamShape {
    // refactor struct
    struct cpMembuffer buffer;
    unsigned int bufferSize;
    
    unsigned int numNodes;
    unsigned int numSegments;
    unsigned int graphSize;
    
    // mesh
    cpBeamNode* const nodes;
    cpSegment* const segments;
    
    // graph
    cpIdx* const rows;
    cpIdx* const columns;
    cpIdx* const diagonal;
    
    cpBeamNode* const velocities;
    
    // shapes
    cpBeamSegmentShape* const segmentsShape;
    cpBeamPointShape* const pointsShape;
   
    cpBody* const pointsShapeBody;
    cpBody* const segmentsShapeBody;
    
    cpFloat collisionMargin;
    
    unsigned int numActiveNodes;
    unsigned int numActiveSegments;
    
    cpIdx firstActiveNode;
    cpIdx firstActiveSegment;
    
    cpIdx* const activeNodes;
    cpIdx* const activeSegments;
    
};



struct cpSoftBodyShape {
    
    struct cpMembuffer* bufferList;
    
    // shapes
    struct cpMembuffer* trianglesShape;
    struct cpMembuffer* pointsShape;
    
    struct cpMembuffer* trianglesShapeBody;
    struct cpMembuffer* pointsShapeBody;
    
    // mesh
    struct cpMembuffer* vertexBuffer;
    struct cpMembuffer* triangleBuffer;
    
    // vel - to improve temporal // update from SoftBody struct
    struct cpMembuffer* velocities;
    
    // graph
    struct cpMembuffer* rows;
    struct cpMembuffer* columns;
    struct cpMembuffer* diagonal;
    
    unsigned int numVertices;
    unsigned int numTriangles;
    
    int numActiveTriangles;
    int numActiveVertices;
    
    cpIdx firstActiveTriangle;
    cpIdx firstActiveVertex;
    
    struct cpMembuffer* activeTriangles;
    struct cpMembuffer* activeVertices;

    
};

struct cpPointShape {
	cpShape shape;
	
	const cpVect* vertices;
    const cpVect* velocities;
    
	cpIdx p;
    
    cpFloat margin;
    
};

struct cpTriangleShape {
	cpShape shape;
	
	const cpVect* vertices;
    const cpVect* velocities;
    
	cpTriangle triangles;

    cpIdx collEdge[2];
    cpFloat bcoord[3];
    
};


struct cpBeamPointShape {
	cpShape shape;
	
	const cpBeamNode* vertices;
    const cpBeamNode* velocities;
    
	cpIdx p;
    
    cpFloat margin;
    
};

struct cpBeamSegmentShape {
	cpShape shape;
	
	const cpBeamNode* nodes;
    const cpBeamNode* velocities;
    
    cpSegment segments;
    
//    cpVect ta, tb, tn;
//	cpVect a_tangent, b_tangent;

//    cpVect n;
    cpFloat r;
	
    
//    cpIdx collEdge[2];
//    cpFloat bcoord[3];
    
};



cpShape *cpShapeInit(cpShape *shape, const cpShapeClass *klass, cpBody *body, struct cpShapeMassInfo massInfo);

static inline cpBool
cpShapeActive(cpShape *shape)
{
	// checks if the shape is added to a shape list.
	// TODO could this just check the space now?
	return (shape->prev || (shape->body && shape->body->shapeList == shape));
}

// Note: This function returns contact points with r1/r2 in absolute coordinates, not body relative.
struct cpCollisionInfo cpCollide(const cpShape *a, const cpShape *b, cpCollisionID id, struct cpContact *contacts);

static inline void
CircleSegmentQuery(cpShape *shape, cpVect center, cpFloat r1, cpVect a, cpVect b, cpFloat r2, cpSegmentQueryInfo *info)
{
	cpVect da = cpvsub(a, center);
	cpVect db = cpvsub(b, center);
	cpFloat rsum = r1 + r2;
	
	cpFloat qa = cpvdot(da, da) - 2.0f*cpvdot(da, db) + cpvdot(db, db);
	cpFloat qb = cpvdot(da, db) - cpvdot(da, da);
	cpFloat det = qb*qb - qa*(cpvdot(da, da) - rsum*rsum);
	
	if(det >= 0.0f){
		cpFloat t = (-qb - cpfsqrt(det))/(qa);
		if(0.0f<= t && t <= 1.0f){
			cpVect n = cpvnormalize(cpvlerp(da, db, t));
			
			info->shape = shape;
			info->point = cpvsub(cpvlerp(a, b, t), cpvmult(n, r2));
			info->normal = n;
			info->alpha = t;
		}
	}
}

static inline cpBool
cpShapeFilterReject(cpShapeFilter a, cpShapeFilter b)
{
	// Reject the collision if:
	return (
		// They are in the same non-zero group.
		(a.group != 0 && a.group == b.group) ||
		// One of the category/mask combinations fails.
		(a.categories & b.mask) == 0 ||
		(b.categories & a.mask) == 0
	);
}

void cpLoopIndexes(const cpVect *verts, int count, int *start, int *end);


//MARK: Constraints
// TODO naming conventions here

typedef void (*cpConstraintPreStepImpl)(cpConstraint *constraint, cpFloat dt);
typedef void (*cpConstraintApplyCachedImpulseImpl)(cpConstraint *constraint, cpFloat dt_coef);
typedef void (*cpConstraintApplyImpulseImpl)(cpConstraint *constraint, cpFloat dt);
typedef cpFloat (*cpConstraintGetImpulseImpl)(cpConstraint *constraint);
typedef unsigned int (*cpConstraintDestroyImpl)(cpConstraint *constraint);

typedef struct cpConstraintClass {
	cpConstraintPreStepImpl preStep;
	cpConstraintApplyCachedImpulseImpl applyCachedImpulse;
	cpConstraintApplyImpulseImpl applyImpulse;
	cpConstraintGetImpulseImpl getImpulse;
    cpConstraintDestroyImpl destroy;
} cpConstraintClass;

struct cpConstraint {
	const cpConstraintClass *klass;
	
	cpSpace *space;
	
	cpBody *a, *b;
	cpConstraint *next_a, *next_b;
	
	cpFloat maxForce;
	cpFloat errorBias;
	cpFloat maxBias;
	
	cpBool collideBodies;
	
	cpConstraintPreSolveFunc preSolve;
	cpConstraintPostSolveFunc postSolve;
	
	cpDataPointer userData;
    
    int island_tag;			// used by dynamics algorithms
};

struct cpPinJoint {
	cpConstraint constraint;
	cpVect anchorA, anchorB;
	cpFloat dist;
	
	cpVect r1, r2;
	cpVect n;
	cpFloat nMass;
	
	cpFloat jnAcc;
	cpFloat bias;
};

struct cpSlideJoint {
	cpConstraint constraint;
	cpVect anchorA, anchorB;
	cpFloat min, max;
	
	cpVect r1, r2;
	cpVect n;
	cpFloat nMass;
	
	cpFloat jnAcc;
	cpFloat bias;
};

struct cpPivotJoint {
	cpConstraint constraint;
	cpVect anchorA, anchorB;
	
	cpVect r1, r2;
	cpMat2x2 k;
	
	cpVect jAcc;
	cpVect bias;
};

struct cpGrooveJoint {
	cpConstraint constraint;
	cpVect grv_n, grv_a, grv_b;
	cpVect  anchorB;
	
	cpVect grv_tn;
	cpFloat clamp;
	cpVect r1, r2;
	cpMat2x2 k;
	
	cpVect jAcc;
	cpVect bias;
};

struct cpDampedSpring {
	cpConstraint constraint;
	cpVect anchorA, anchorB;
	cpFloat restLength;
	cpFloat stiffness;
	cpFloat damping;
	cpDampedSpringForceFunc springForceFunc;
	
	cpFloat target_vrn;
	cpFloat v_coef;
	
	cpVect r1, r2;
	cpFloat nMass;
	cpVect n;
	
	cpFloat jAcc;
};

struct cpDampedRotarySpring {
	cpConstraint constraint;
	cpFloat restAngle;
	cpFloat stiffness;
	cpFloat damping;
	cpDampedRotarySpringTorqueFunc springTorqueFunc;
	
	cpFloat target_wrn;
	cpFloat w_coef;
	
	cpFloat iSum;
	cpFloat jAcc;
};

struct cpRotaryLimitJoint {
	cpConstraint constraint;
	cpFloat min, max;
	
	cpFloat iSum;
		
	cpFloat bias;
	cpFloat jAcc;
};

struct cpRatchetJoint {
	cpConstraint constraint;
	cpFloat angle, phase, ratchet;
	
	cpFloat iSum;
		
	cpFloat bias;
	cpFloat jAcc;
};

struct cpGearJoint {
	cpConstraint constraint;
	cpFloat phase, ratio;
	cpFloat ratio_inv;
	
	cpFloat iSum;
		
	cpFloat bias;
	cpFloat jAcc;
};

struct cpSimpleMotor {
	cpConstraint constraint;
	cpFloat rate;
	
	cpFloat iSum;
		
	cpFloat jAcc;
};

void cpConstraintInit(cpConstraint *constraint, const struct cpConstraintClass *klass, cpBody *a, cpBody *b);

static inline void
cpConstraintActivateBodies(cpConstraint *constraint)
{
	cpBody *a = constraint->a; cpBodyActivate(a);
	cpBody *b = constraint->b; cpBodyActivate(b);
}

static inline cpVect
relative_velocity(cpBody *a, cpBody *b, cpVect r1, cpVect r2){
	cpVect v1_sum = cpvadd(a->CP_PRIVATE(v), cpvmult(cpvperp(r1), a->CP_PRIVATE(w)));
	cpVect v2_sum = cpvadd(b->CP_PRIVATE(v), cpvmult(cpvperp(r2), b->CP_PRIVATE(w)));
	
	return cpvsub(v2_sum, v1_sum);
}

static inline cpFloat
normal_relative_velocity(cpBody *a, cpBody *b, cpVect r1, cpVect r2, cpVect n){
	return cpvdot(relative_velocity(a, b, r1, r2), n);
}

static inline void
apply_impulse(cpBody *body, cpVect j, cpVect r){
    // Fede
    if (body->userData == 0)
    {
        body->CP_PRIVATE(v) = cpvadd(body->CP_PRIVATE(v), cpvmult(j, body->CP_PRIVATE(mass_inv)));
        body->CP_PRIVATE(w) += body->CP_PRIVATE(inertia_inv)*cpvcross(r, j);
    }
    else{
        const cpVect3 J = { j.x, j.y, cpvcross(r, j), 0.0 };
        const cpVect3 dv = multiplyMat3xVect3( &body->compliance, &J );
//        const cpVect dv = multiplyMat2Vect2( body->compliance, &j);
        body->CP_PRIVATE(v.x) = body->CP_PRIVATE(v.x) +  dv.x;
        body->CP_PRIVATE(v.y) = body->CP_PRIVATE(v.y) +  dv.y;
        body->CP_PRIVATE(w) += dv.z;
    }
    
}

static inline void
apply_impulses(cpBody *a , cpBody *b, cpVect r1, cpVect r2, cpVect j)
{
	apply_impulse(a, cpvneg(j), r1);
	apply_impulse(b, j, r2);
}

static inline void
apply_bias_impulse(cpBody *body, cpVect j, cpVect r)
{
    // Fede
    if (body->userData == 0)
    {
        body->CP_PRIVATE(v_bias) = cpvadd(body->CP_PRIVATE(v_bias), cpvmult(j, body->CP_PRIVATE(mass_inv)));
        body->CP_PRIVATE(w_bias) += body->CP_PRIVATE(inertia_inv)*cpvcross(r, j);
    }
    else{
        const cpVect3 __attribute__ ((aligned (16))) J = { j.x, j.y, cpvcross(r, j), 0.0 };
        const cpVect3 __attribute__ ((aligned (16))) dv = multiplyMat3xVect3( &body->compliance, &J );
//        const cpVect dv = multiplyMat2Vect2( body->compliance, &j);
        body->CP_PRIVATE(v_bias.x) = body->CP_PRIVATE(v_bias.x) + dv.x;
        body->CP_PRIVATE(v_bias.y) = body->CP_PRIVATE(v_bias.y) + dv.y;
        body->CP_PRIVATE(w_bias) += dv.z;
    }
}

static inline void
apply_bias_impulses(cpBody *a , cpBody *b, cpVect r1, cpVect r2, cpVect j)
{
	apply_bias_impulse(a, cpvneg(j), r1);
	apply_bias_impulse(b, j, r2);
}

static inline cpFloat
k_scalar_body(cpBody *body, cpVect r, cpVect n)
{
	cpFloat rcn = cpvcross(r, n);
	return body->CP_PRIVATE(mass_inv) + body->CP_PRIVATE(inertia_inv)*rcn*rcn;
}

static inline cpFloat
k_scalar(cpBody *a, cpBody *b, cpVect r1, cpVect r2, cpVect n)
{
	cpFloat value = k_scalar_body(a, r1, n) + k_scalar_body(b, r2, n);
	cpAssertSoft(value != 0.0, "Unsolvable collision or constraint.");
	
	return value;
}

// Fede
static inline cpFloat
k_scalar_n(cpBody *a, cpBody *b, cpVect r1, cpVect r2, cpVect n)
{
    cpFloat value_a, value_b;
    
    if ( a->userData == 0 )
    {
        value_a = k_scalar_body(a, r1, n);
    }
    else{
        value_a = a->mass;
    }
    if ( b->userData == 0 )
    {
        value_b = k_scalar_body(b, r2, n);
    }
    else
    {
        value_b = b->mass;
    }
	cpFloat value = value_a + value_b;
	cpAssertSoft(value != 0.0, "Unsolvable collision or constraint.");
	
	return value;
}

static inline cpFloat
k_scalar_t(cpBody *a, cpBody *b, cpVect r1, cpVect r2, cpVect n)
{
	cpFloat value_a, value_b;
    
    if ( a->userData == 0 )
    {
        value_a = k_scalar_body(a, r1, n);
    }
    else{
        value_a = a->mass_inv;
    }
    if ( b->userData == 0 )
    {
        value_b = k_scalar_body(b, r2, n);
    }
    else
    {
        value_b = b->mass_inv;
    }
	cpFloat value = value_a + value_b;
	cpAssertSoft(value != 0.0, "Unsolvable collision or constraint.");
	
	return value;
}


static inline cpFloat
k_scalar_c(cpBody *a, cpBody *b, cpVect r1, cpVect r2, cpVect n)
{
	cpFloat value_a, value_b;
    
    if ( a->userData == 0 )
    {
        value_a = k_scalar_body(a, r1, n);
    }
    else{
        const Matrix2 compl22 = {
            { {a->compliance.col[0].x, a->compliance.col[0].y} ,
              {a->compliance.col[1].x, a->compliance.col[1].y} }
        };
        value_a = cpvdot( n, multiplyMat2Vect2( &compl22, &n) );
    }
    if ( b->userData == 0 )
    {
        value_b = k_scalar_body(b, r2, n);
    }
    else
    {
        const Matrix2 compl22 = {
            { {b->compliance.col[0].x, b->compliance.col[0].y} ,
              {b->compliance.col[1].x, b->compliance.col[1].y} }
        };
        value_b = cpvdot( n, multiplyMat2Vect2( &compl22, &n) );
    }
	cpFloat value = value_a + value_b;
//	cpAssertSoft(value != 0.0, "Unsolvable collision or constraint.");
	
	return value;
}

static inline cpMat2x2
k_tensor(cpBody *a, cpBody *b, cpVect r1, cpVect r2)
{
	cpFloat m_sum = a->CP_PRIVATE(mass_inv) + b->CP_PRIVATE(mass_inv);
	
	// start with Identity*m_sum
	cpFloat k11 = m_sum, k12 = 0.0f;
	cpFloat k21 = 0.0f,  k22 = m_sum;
	
	// add the influence from r1
	cpFloat a_i_inv = a->CP_PRIVATE(inertia_inv);
	cpFloat r1xsq =  r1.x * r1.x * a_i_inv;
	cpFloat r1ysq =  r1.y * r1.y * a_i_inv;
	cpFloat r1nxy = -r1.x * r1.y * a_i_inv;
	k11 += r1ysq; k12 += r1nxy;
	k21 += r1nxy; k22 += r1xsq;
	
	// add the influnce from r2
	cpFloat b_i_inv = b->CP_PRIVATE(inertia_inv);
	cpFloat r2xsq =  r2.x * r2.x * b_i_inv;
	cpFloat r2ysq =  r2.y * r2.y * b_i_inv;
	cpFloat r2nxy = -r2.x * r2.y * b_i_inv;
	k11 += r2ysq; k12 += r2nxy;
	k21 += r2nxy; k22 += r2xsq;
	
	// invert
	cpFloat det = k11*k22 - k12*k21;
	cpAssertSoft(det != 0.0, "Unsolvable constraint.");
	
	cpFloat det_inv = 1.0f/det;
	return cpMat2x2New(
		 k22*det_inv, -k12*det_inv,
		-k21*det_inv,  k11*det_inv
 	);
}


static inline cpMat2x2
k_tensor_c(cpBody *a, cpBody *b, cpVect r1, cpVect r2)
{
//    cpFloat value_a, value_b;
    cpMat2x2 k = { 0.0, 0.0, 0.0, 0.0 };
    cpFloat a_i_inv, b_i_inv;
    
    if ( a->userData == 0 )
    {
        k.a = a->CP_PRIVATE(mass_inv);
//        k.b = 0.0;
//        k.c = 0.0;
        k.d = a->CP_PRIVATE(mass_inv);
        a_i_inv = a->CP_PRIVATE(inertia_inv);
    }
    else
    {
        k.a = a->compliance.col[0].x;
        k.c = a->compliance.col[0].y;
        k.b = a->compliance.col[1].x;
        k.d = a->compliance.col[1].y;
        a_i_inv = a->compliance.col[2].z;
    }
    
    if ( b->userData == 0 )
    {
        k.a += b->CP_PRIVATE(mass_inv);
        k.d += b->CP_PRIVATE(mass_inv);
        b_i_inv = b->CP_PRIVATE(inertia_inv);
    }
    else
    {
        k.a += b->compliance.col[0].x;
        k.b += b->compliance.col[0].y;
        k.c += b->compliance.col[1].x;
        k.d += b->compliance.col[1].y;
        b_i_inv = b->compliance.col[2].z;
    }
	
	// start with Identity*m_sum
//	cpFloat k11 = m_sum, k12 = 0.0f;
//	cpFloat k21 = 0.0f,  k22 = m_sum;
	
	// add the influence from r1
//	cpFloat a_i_inv = a->CP_PRIVATE(inertia_inv);
	cpFloat r1xsq =  r1.x * r1.x * a_i_inv;
	cpFloat r1ysq =  r1.y * r1.y * a_i_inv;
	cpFloat r1nxy = -r1.x * r1.y * a_i_inv;
    k.a += r1ysq;
    k.b += r1nxy;
    k.c += r1nxy;
    k.d += r1xsq;
    
//	k11 += r1ysq; k12 += r1nxy;
//	k21 += r1nxy; k22 += r1xsq;
	
	// add the influnce from r2
//	cpFloat b_i_inv = b->CP_PRIVATE(inertia_inv);
//	cpFloat r2xsq =  r2.x * r2.x * b_i_inv;
//	cpFloat r2ysq =  r2.y * r2.y * b_i_inv;
//	cpFloat r2nxy = -r2.x * r2.y * b_i_inv;
//    k.a += r2ysq;
//    k.b += r2nxy;
//    k.c += r2nxy;
//    k.d += r2xsq;
    
//	k11 += r2ysq; k12 += r2nxy;
//	k21 += r2nxy; k22 += r2xsq;
	
	// invert
//    cpFloat det = determinantMat2(&k);
//	cpFloat det = k.a*k.d - k.b*k.c;
//	cpAssertSoft(det != 0.0, "Unsolvable constraint.");
	
//	cpFloat det_inv = 1.0f/det;
//    multiplyMat2byScalar( &k, det_inv);
//    k.a *= det_inv;
//    k.b *= det_inv;
//    k.c *= det_inv;
//    k.d *= det_inv;
    
    return k;
//	return cpMat2x2New(
//                       k22*det_inv, -k12*det_inv,
//                       -k21*det_inv,  k11*det_inv
//                       );
}


static inline cpFloat
bias_coef(cpFloat errorBias, cpFloat dt)
{
	return 1.0f - cpfpow(errorBias, dt);
}



// Contact Buffer

struct cpContactBufferHeader {
	cpTimestamp stamp;	
	unsigned int numContacts;
    cpContactBufferHeader *next;
    
#if !CP_64_BITS
    char pad[4];
#endif
};

#define CP_CONTACTS_BUFFER_SIZE ((CP_BUFFER_BYTES - sizeof(cpContactBufferHeader))/sizeof(struct cpContact))
typedef struct cpContactBuffer {
	cpContactBufferHeader header;
	struct cpContact contacts[CP_CONTACTS_BUFFER_SIZE];
} cpContactBuffer;



//MARK: Spaces

struct cpSpace {
	int iterations;
	
	cpVect gravity;
	cpFloat damping;
	
	cpFloat idleSpeedThreshold;
	cpFloat sleepTimeThreshold;
	
	cpFloat collisionSlop;
	cpFloat collisionBias;
	cpTimestamp collisionPersistence;
	
	cpDataPointer userData;
	
	cpTimestamp stamp;
	cpFloat curr_dt;

	cpArray *dynamicBodies;
	cpArray *staticBodies;
	cpArray *rousedBodies;
	cpArray *sleepingComponents;
    
	cpHashValue shapeIDCounter;
	cpSpatialIndex *staticShapes;
	cpSpatialIndex *dynamicShapes;
	
	cpArray *constraints;
	
	cpArray *arbiters;
	cpContactBufferHeader *contactBuffersHead;
	cpHashSet *cachedArbiters;
	cpArray *pooledArbiters;
	
	cpArray *allocatedBuffers;
	unsigned int locked;
	
	cpBool usesWildcards;
	cpHashSet *collisionHandlers;
	cpCollisionHandler defaultHandler;
	
	cpBool skipPostStep;
	cpArray *postStepCallbacks;
	
	cpBody *staticBody;
	cpBody _staticBody;
    
    cpPoolAllocator* allocator;
    
};


struct cpSpaceSoftBody {
	
    struct cpSpace* space;
    
    cpArray *softBodies;
    cpArray *beamBodies;

    cpArray *softBodiesOpt;
    cpArray * sleepingSoftBodyOpt;
    cpArray * sleepingBeamBody;
    
    cpStackAllocator* stack;
    
//    cpPoolAllocator* allocator;
    
    // threading function
    cpSpaceAssembleSoftBodiesFunc assembleSoftBodies;
    cpSpaceSolveSoftBodiesFunc solveSoftBodies;
    cpSpaceUpdateBBShapesFunc updateBBShapes;
    cpSpaceSolveIslandFunc solveIsland;
    cpSpaceIntegratePositionSoftBodiesFunc integratePositonSoftBodies;
    
    cpSpaceAssembleBeamBodiesFunc assembleBeamBodies;
    cpSpaceSolveBeamBodiesFunc solveBeamBodies;
    //    space->updateBBShapes = cpSpaceUpdateBBShapes;
    cpSpaceIntegratePositionBeamBodiesFunc integratePositonBeamBodies;
    
};




#define cpAssertSpaceUnlocked(space) \
	cpAssertHard(!space->locked, \
		"This operation cannot be done safely during a call to cpSpaceStep() or during a query. " \
		"Put these calls into a post-step callback." \
	);

void cpSpaceSetStaticBody(cpSpace *space, cpBody *body);

extern cpCollisionHandler cpCollisionHandlerDoNothing;

void cpSpaceProcessComponents(cpSpace *space, cpFloat dt);

void cpSpaceSoftBodyProcessComponents(cpSpaceSoftBody *space, cpFloat dt);

void cpSpacePushFreshContactBuffer(cpSpace *space);
struct cpContact *cpContactBufferGetArray(cpSpace *space);
void cpSpacePushContacts(cpSpace *space, int count);

typedef struct cpPostStepCallback {
	cpPostStepFunc func;
	void *key;
	void *data;
} cpPostStepCallback;

cpPostStepCallback *cpSpaceGetPostStepCallback(cpSpace *space, void *key);

cpBool cpSpaceArbiterSetFilter(cpArbiter *arb, cpSpace *space);
void cpSpaceFilterArbiters(cpSpace *space, cpBody *body, cpShape *filter);

void cpSpaceActivateBody(cpSpace *space, cpBody *body);
void cpSpaceLock(cpSpace *space);
void cpSpaceUnlock(cpSpace *space, cpBool runPostStep);

void cpSpaceActivateSoftBodyOpt(cpBody *body);
void cpSoftBodyOptConnectSleepingBody(const cpSoftBodyOpt *body);

static inline void
cpSpaceUncacheArbiter(cpSpace *space, cpArbiter *arb)
{
	const cpShape *a = arb->a, *b = arb->b;
	const cpShape *shape_pair[] = {a, b};
	cpHashValue arbHashID = CP_HASH_PAIR((cpHashValue)a, (cpHashValue)b);
	cpHashSetRemove(space->cachedArbiters, arbHashID, shape_pair);
	cpArrayDeleteObj(space->arbiters, arb);
}

static inline cpArray *
cpSpaceArrayForBodyType(cpSpace *space, cpBodyType type)
{
	return (type == CP_BODY_TYPE_STATIC ? space->staticBodies : space->dynamicBodies);
}

void cpShapeUpdateFunc(cpShape *shape, void *unused);
cpCollisionID cpSpaceCollideShapes(cpShape *a, cpShape *b, cpCollisionID id, cpSpace *space);


//MARK: Foreach loops

static inline cpConstraint *
cpConstraintNext(cpConstraint *node, cpBody *body)
{
	return (node->a == body ? node->next_a : node->next_b);
}

//static inline cpConstraint *
//cpSoftBodyConstraintNext(cpConstraint *node, cpSoftBody *body)
//{
//	return (node->a == body ? node->next_a : node->next_b);
//}

#define CP_BODY_FOREACH_CONSTRAINT(bdy, var)\
	for(cpConstraint *var = bdy->constraintList; var; var = cpConstraintNext(var, bdy))

static inline cpArbiter *
cpArbiterNext(cpArbiter *node, cpBody *body)
{
	return (node->body_a == body ? node->thread_a.next : node->thread_b.next);
}

static inline cpBool cpIsSoftBodyOptNode(const cpBody* body)
{
    cpPointShape* pointShape = (cpPointShape*)body->userData;
    if ( pointShape && ( pointShape->shape.klass->type == CP_POINT_SHAPE ) )
    {
        return cpTrue;
    }else{
        return cpFalse;
    }
    
}

void cpSpaceActivateSoftBody(const cpSpaceSoftBody *space, cpSoftBodyOpt *body);

int g_numCollisionTests;

void cpSpaceHashCacheBB(cpHandle *handle);


#define CP_BODY_FOREACH_ARBITER(bdy, var)\
	for(cpArbiter *var = bdy->arbiterList; var; var = cpArbiterNext(var, bdy))

#define CP_BODY_FOREACH_SHAPE(body, var)\
	for(cpShape *var = body->shapeList; var; var = var->next)

#define CP_BODY_FOREACH_COMPONENT(root, var)\
	for(cpBody *var = root; var; var = var->sleeping.next)

#define CP_SOFTBODYOPT_FOREACH_COMPONENT(root, var)\
    for(cpSoftBodyOpt *var = root; var; var = var->sleeping.next)

#define CP_BEAMBODY_FOREACH_COMPONENT(root, var)\
    for(cpBeamBody *var = root; var; var = var->sleeping.next)


#endif
