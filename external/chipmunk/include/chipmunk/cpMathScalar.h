//
//  cpMathScalar.h
//  Chipmunk7
//
//  Created by Federico Spadoni on 30/03/15.
//
//

#ifndef Chipmunk7_cpMathScalar_h
#define Chipmunk7_cpMathScalar_h


static inline cpFloat innProd( const cpVect* v0, const cpVect* v1, unsigned int size)
{
    cpVect temp = {0.0f,0.0f};
    for (unsigned int i=0; i<size; ++i)
    {
        temp.x += v0->x * v1->x;
        temp.y += v0->y * v1->y;
        ++v0;
        ++v1;
    }
    return temp.x + temp.y;
}



/// Returns the length of v.
static inline cpFloat norm3(const cpVect3* v)
{
	return cpfsqrt(dot3(v, v));
}

static inline void multiplyMat2byScalar( Matrix2* m, const cpFloat s)
{
    m->col[0].x *= s;
    m->col[0].y *= s;
    m->col[1].x *= s;
    m->col[1].y *= s;
    return;
}

static inline cpVect multiplyMat2Vect2(const Matrix2* m, const cpVect* v)
{
    cpVect res;
    res.x = m->col[0].x * v->x + m->col[1].x * v->y;
    res.y = m->col[0].y * v->x + m->col[1].y * v->y;
    return res;
}

static inline void multiplyMat2byMat2(Matrix2* res, const Matrix2* m0, const Matrix2* m1)
{
    res->col[0].x = m0->col[0].x * m1->col[0].x + m0->col[1].x * m1->col[0].y;
    res->col[0].y = m0->col[0].y * m1->col[0].x + m0->col[1].y * m1->col[0].y;
    res->col[1].x = m0->col[0].x * m1->col[1].x + m0->col[1].x * m1->col[1].y;
    res->col[1].y = m0->col[0].y * m1->col[1].x + m0->col[1].y * m1->col[1].y;
}

static inline cpVect multiplyTranspMat2Vect2(const Matrix2* m, const cpVect* v)
{
    cpVect res;
    res.x = m->col[0].x * v->x + m->col[0].y * v->y;
    res.y = m->col[1].x * v->x + m->col[1].y * v->y;
    return res;
}

static inline void multiplyMat2Vect2andAdd(cpVect* res, const Matrix2* m, const cpVect* v)
{
    res->x += m->col[0].x * v->x + m->col[1].x * v->y;
    res->y += m->col[0].y * v->x + m->col[1].y * v->y;
}

static inline void multiplyMat2Vect2andSub(cpVect* res, const Matrix2* m, const cpVect* v)
{
    res->x -= m->col[0].x * v->x + m->col[1].x * v->y;
    res->y -= m->col[0].y * v->x + m->col[1].y * v->y;
}

static inline void multiplyMat3Vect3andAdd(cpVect3* res, const Matrix3* m, const cpVect3* v)
{
    res->x += m->col[0].x * v->x + m->col[1].x * v->y + m->col[2].x * v->z;
    res->y += m->col[0].y * v->x + m->col[1].y * v->y + m->col[2].y * v->z;
    res->z += m->col[0].z * v->x + m->col[1].z * v->y + m->col[2].z * v->z;
}

static inline void multiplyMat3Vect3andSub(cpVect3* res, const Matrix3* m, const cpVect3* v)
{
    res->x -= m->col[0].x * v->x + m->col[1].x * v->y + m->col[2].x * v->z;
    res->y -= m->col[0].y * v->x + m->col[1].y * v->y + m->col[2].y * v->z;
    res->z -= m->col[0].z * v->x + m->col[1].z * v->y + m->col[2].z * v->z;
}


static inline cpVect3 addVec3(const cpVect3* v0, const cpVect3* v1)
{
    cpVect3 res = { v0->x + v1->x, v0->y + v1->y, v0->z + v1->z, 0};
    return res;
}

static inline cpVect3 subVec3(const cpVect3* v0, const cpVect3* v1)
{
    cpVect3 res = { v0->x - v1->x, v0->y - v1->y, v0->z - v1->z, 0};
    return res;
}

static inline void addMatrix2byelems(Matrix2* res, const Matrix2* m0, const Matrix2* m1)
{
    res->col[0] = cpvadd(m0->col[0], m1->col[0]);
    res->col[1] = cpvadd(m0->col[1], m1->col[1]);
}

static inline void subMatrix3byelems(Matrix3* res, const Matrix3* m0, const Matrix3* m1)
{
    res->col[0] = subVec3( &m0->col[0], &m1->col[0]);
    res->col[1] = subVec3( &m0->col[1], &m1->col[1]);
    res->col[2] = subVec3( &m0->col[2], &m1->col[2]);
}

static inline cpVect multiplyMat2x3byVect3(const cpMat2x3* m23, const cpVect3* v)
{
    cpVect res = { m23->col[0].x * v->x + m23->col[1].x * v->y + m23->col[2].x * v->z,
        m23->col[0].y * v->x + m23->col[1].y * v->y + m23->col[2].y * v->z };
    return res;
}

static inline void multiplyMat3Vect3(cpVect3* res, const Matrix3* m, const cpVect3* v)
{
    res->x = m->col[0].x * v->x + m->col[1].x * v->y + m->col[2].x * v->z;
    res->y = m->col[0].y * v->x + m->col[1].y * v->y + m->col[2].y * v->z;
    res->z = m->col[0].z * v->x + m->col[1].z * v->y + m->col[2].z * v->z;
}

static inline void multiplyTransposeMat3Vect3(cpVect3* res, const Matrix3* m, const cpVect3* v)
{
    res->x = m->col[0].x * v->x + m->col[0].y * v->y + m->col[0].z * v->z;
    res->y = m->col[1].x * v->x + m->col[1].y * v->y + m->col[1].z * v->z;
    res->z = m->col[2].x * v->x + m->col[2].y * v->y + m->col[2].z * v->z;
}

static inline cpVect3 multiplyMat3xVect3(const Matrix3* m, const cpVect3* v)
{
    cpVect3 res;
    res.x = m->col[0].x * v->x + m->col[1].x * v->y + m->col[2].x * v->z;
    res.y = m->col[0].y * v->x + m->col[1].y * v->y + m->col[2].y * v->z;
    res.z = m->col[0].z * v->x + m->col[1].z * v->y + m->col[2].z * v->z;
    return res;
}

static inline Matrix2 multiplyMat2Mat2Mat2( const Matrix2* m0, const Matrix2* m1, const Matrix2* m2)
{
    // multiply m0 * ( m1 * m2 )
    Matrix2 res;
    res.col[0] = multiplyMat2Vect2(m1, &m2->col[0] );
    res.col[1] = multiplyMat2Vect2(m1, &m2->col[1] );
    res.col[0] = multiplyMat2Vect2(m0, &res.col[0] );
    res.col[1] = multiplyMat2Vect2(m0, &res.col[1] );
    return res;
}

static inline void addMat2ToMat2( Matrix2* sum, const Matrix2* m)
{
    sum->col[0].x += m->col[0].x;
    sum->col[0].y += m->col[0].y;
    sum->col[1].x += m->col[1].x;
    sum->col[1].y += m->col[1].y;
    
}

static inline void addMat3ToMat3( Matrix3* sum, const Matrix3* m)
{
    sum->col[0].x += m->col[0].x;
    sum->col[0].y += m->col[0].y;
    sum->col[0].z += m->col[0].z;
    
    sum->col[1].x += m->col[1].x;
    sum->col[1].y += m->col[1].y;
    sum->col[1].z += m->col[1].z;
    
    sum->col[2].x += m->col[2].x;
    sum->col[2].y += m->col[2].y;
    sum->col[2].z += m->col[2].z;
}

static inline void multiplyMat2Mat2Mat2AddToMat2(Matrix2* sum, const Matrix2* m0, const Matrix2* m1, const Matrix2* m2)
{
    Matrix2 temp;
    
    temp = multiplyMat2Mat2Mat2( m0, m1, m2);
    
    addMat2ToMat2( sum, &temp );
    
}


static inline void multiplyMat3xMat3(Matrix3* res, const Matrix3* m1, const Matrix3* m2)
{
    multiplyMat3Vect3( &res->col[0], m1, &m2->col[0] );
    multiplyMat3Vect3( &res->col[1], m1, &m2->col[1] );
    multiplyMat3Vect3( &res->col[2], m1, &m2->col[2] );
    
    //    res->col[0].x = m1->col[0].x * m2->col[0].x + m1->col[1].x * m2->col[0].y + m1->col[2].x * m2->col[0].z;
    //    res->col[0].y = m1->col[0].y * m2->col[0].x + m1->col[1].y * m2->col[0].y + m1->col[2].y * m2->col[0].z;
    //    res->col[0].z = m1->col[0].z * m2->col[0].x + m1->col[1].z * m2->col[0].y + m1->col[2].z * m2->col[0].z;
    //
    //    res->col[1].x = m1->col[0].x * m2->col[1].x + m1->col[1].x * m2->col[1].y + m1->col[2].x * m2->col[1].z;
    //    res->col[1].y = m1->col[0].y * m2->col[1].x + m1->col[1].y * m2->col[1].y + m1->col[2].y * m2->col[1].z;
    //    res->col[1].z = m1->col[0].z * m2->col[1].x + m1->col[1].z * m2->col[1].y + m1->col[2].z * m2->col[1].z;
    //
    //    res->col[2].x = m1->col[0].x * m2->col[2].x + m1->col[1].x * m2->col[2].y + m1->col[2].x * m2->col[2].z;
    //    res->col[2].y = m1->col[0].y * m2->col[2].x + m1->col[1].y * m2->col[2].y + m1->col[2].y * m2->col[2].z;
    //    res->col[2].z = m1->col[0].z * m2->col[2].x + m1->col[1].z * m2->col[2].y + m1->col[2].z * m2->col[2].z;
    
}

// res = m1t*m2
static inline void multiplyTranposeMat3xMat3(Matrix3* res, const Matrix3* m1,const Matrix3* m2)
{
    multiplyTransposeMat3Vect3( &res->col[0], m1, &m2->col[0] );
    multiplyTransposeMat3Vect3( &res->col[1], m1, &m2->col[1] );
    multiplyTransposeMat3Vect3( &res->col[2], m1, &m2->col[2] );
}

// res = m1*m2t
static inline void multiplyMat3xMat3Tranpose(Matrix3* res, const Matrix3* m1,const Matrix3* m2)
{
    Matrix3 m2t;
    transposeMat3( &m2t, m2);
    multiplyMat3Vect3( &res->col[0], m1, &m2t.col[0] );
    multiplyMat3Vect3( &res->col[1], m1, &m2t.col[1] );
    multiplyMat3Vect3( &res->col[2], m1, &m2t.col[2] );
}

// res = m0*m1*m2
static inline Matrix3 multiplyMat3Mat3Mat3( const Matrix3* m0, const Matrix3* m1, const Matrix3* m2)
{
    // multiply m0 * ( m1 * m2 )
    Matrix3 res;
    res.col[0] = multiplyMat3xVect3(m1, &m2->col[0] );
    res.col[1] = multiplyMat3xVect3(m1, &m2->col[1] );
    res.col[2] = multiplyMat3xVect3(m1, &m2->col[2] );
    
    res.col[0] = multiplyMat3xVect3(m0, &res.col[0] );
    res.col[1] = multiplyMat3xVect3(m0, &res.col[1] );
    res.col[2] = multiplyMat3xVect3(m0, &res.col[2] );
    
//    multiplyMat3Vect3( &res.col[0], m1, &m2->col[0] );
//    multiplyMat3Vect3( &res.col[1], m1, &m2->col[1] );
//    multiplyMat3Vect3( &res.col[2], m1, &m2->col[2] );
    
//    multiplyMat3Vect3( &res.col[0], m0, &res.col[0] );
//    multiplyMat3Vect3( &res.col[1], m0, &res.col[1] );
//    multiplyMat3Vect3( &res.col[2], m0, &res.col[2] );
    
    return res;
}

static inline void multiplyMat3Mat3Mat3AddToMat3(Matrix3* sum, const Matrix3* m0, const Matrix3* m1, const Matrix3* m2)
{
    Matrix3 temp;
    
    temp = multiplyMat3Mat3Mat3( m0, m1, m2);
    
    addMat3ToMat3( sum, &temp );
    
}

static inline void multiplyAddMat3x2byVect2(cpVect3* res, const cpMat3x2* m, const cpVect* v)
{
    res->x += m->col[0].x * v->x + m->col[1].x * v->y;
    res->y += m->col[0].y * v->x + m->col[1].y * v->y;
    res->z += m->col[0].z * v->x + m->col[1].z * v->y;
}

static inline void multiplyAddMat3x3byVect3(cpVect3* res, const Matrix3* m, const cpVect3* v)
{
    res->x += m->col[0].x * v->x + m->col[1].x * v->y + m->col[2].x * v->z;
    res->y += m->col[0].y * v->x + m->col[1].y * v->y + m->col[2].y * v->z;
    res->z += m->col[0].z * v->x + m->col[1].z * v->y + m->col[2].z * v->z;
}

static inline void transposeMat3x2MultiplyByVect3(cpVect* res, const cpMat3x2* m, const cpVect3* v)
{
    const cpVect3* mrow0 = &m->col[0];
    const cpVect3* mrow1 = &m->col[1];
    res->x = mrow0->x * v->x + mrow0->y * v->y + mrow0->z * v->z;
    res->y = mrow1->x * v->x + mrow1->y * v->y + mrow1->z * v->z;
}

static inline void multiplyMat6xVect6(cpVect6* res, const cpMat6* mat, const cpVect6* vect )
{
    res->v[0] = cpvadd( multiplyMat2Vect2(&mat->m[0][0], &vect->v[0]) , cpvadd( multiplyMat2Vect2(&mat->m[1][0], &vect->v[1]) , multiplyMat2Vect2(&mat->m[2][0], &vect->v[2]) ) );
    res->v[1] = cpvadd( multiplyMat2Vect2(&mat->m[0][1], &vect->v[0]) , cpvadd( multiplyMat2Vect2(&mat->m[1][1], &vect->v[1]) , multiplyMat2Vect2(&mat->m[2][1], &vect->v[2]) ) );
    res->v[2] = cpvadd( multiplyMat2Vect2(&mat->m[0][2], &vect->v[0]) , cpvadd( multiplyMat2Vect2(&mat->m[1][2], &vect->v[1])  , multiplyMat2Vect2(&mat->m[2][2], &vect->v[2]) ) );
    
    
}

static inline cpVect multiplyMat2x2byMat2x3byVect3(const Matrix2* m22, const cpMat2x3* m23, const cpVect3* v3 )
{
    cpVect res = multiplyMat2x3byVect3(m23, v3);
    return multiplyMat2Vect2(m22, &res);
}


static inline void transposeMat3x3MultiplyByVect3(cpVect3* res, const Matrix3* m, const cpVect3* v)
{
    const cpVect3* mrow0 = &m->col[0];
    const cpVect3* mrow1 = &m->col[1];
    const cpVect3* mrow2 = &m->col[2];
    res->x = mrow0->x * v->x + mrow0->y * v->y + mrow0->z * v->z;
    res->y = mrow1->x * v->x + mrow1->y * v->y + mrow1->z * v->z;
    res->z = mrow2->x * v->x + mrow2->y * v->y + mrow2->z * v->z;
}

static inline  void matVecProd(cpVect* res, const Matrix2* mat, const cpVect* vec, int nbRows,
                               const cpIdx* cols, const cpIdx* rows )

{
    
    cpIdx nbcols;
    
    while ( nbRows-- > 0 )
    {
        nbcols = *(rows+1) - *rows;
        
        res->x = 0.0;
        res->y = 0.0;
        
        while (nbcols-- > 0)
        {
            multiplyMat2Vect2andAdd(res, mat++, &vec[(*cols++)]);
            //            *res += (*mat++) * vec[(*cols++)];
        }
        ++res;
        ++rows;
    }
    
}


#endif
