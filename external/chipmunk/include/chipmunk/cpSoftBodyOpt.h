//
//  cpSoftBodyOpt.h
//  Chipmunk7
//
//  Created by Federico Spadoni on 17/11/14.
//
//

#ifndef Chipmunk7_cpSoftBodyOpt_h
#define Chipmunk7_cpSoftBodyOpt_h


struct cpMesh;

/// Rigid body velocity update function type.
typedef void (*cpSoftBodyOptVelocityFunc)(cpSoftBodyOpt *body, cpVect gravity, cpFloat damping, cpFloat dt);
/// Rigid body position update function type.
typedef void (*cpSoftBodyOptPositionFunc)(cpSoftBodyOpt *body, cpFloat dt);

typedef cpFloat (*cpAssembleStiffMatrixOptFunc)(cpSoftBodyOpt *body, const cpFloat dt);

typedef cpFloat (*cpSoftBodyOptSolveFunc)(cpSoftBodyOpt* body, const cpFloat dt);

typedef void (*cpSoftBodyOptSleepingCallback)(cpSoftBodyOpt* body, cpDataPointer userData);

typedef void (*cpSoftBodyOptTriangleDestroyCallback)(cpSoftBodyOpt* body, cpIdx triangleIdx, cpDataPointer userData);

typedef void (*cpSoftBodyOptDestroyCallback)(cpSoftBodyOpt* body, cpDataPointer userData);

typedef void (*cpSoftBodyOptCollisionBeginCallback)(cpSoftBodyOpt* body, cpBody* collidingBody, cpArbiter* arb, cpDataPointer userData);

typedef void (*cpSoftBodyOptCollisionSeparateCallback)(cpSoftBodyOpt* body, cpBody* collidingBody, cpArbiter* arb, cpDataPointer userData);



unsigned int cpSoftBodyOptGetBufferSize(const cpSoftBodyOpt* body);

unsigned int cpSoftBodyOptComputeMemorySize(const unsigned int numNodes, const unsigned int numElems, const unsigned int graphSize, const unsigned int numMaterials);

unsigned int cpSoftBodyOptComputeMemorySizeFromMesh(const struct cpMesh *mesh);

//ssize_t cpSoftBodyOptGetMemorySize(const cpSoftBodyOpt *body);

/// Allocate a cpSoftBodyOpt.
cpSoftBodyOpt* cpSoftBodyOptAlloc(void);
/// Initialize a cpSoftBodyOpt.
cpSoftBodyOpt* cpSoftBodyOptInit(cpSoftBodyShapeOpt *shape, const cpMaterial* material, const cpSoftBodySolverParams* params);

cpSoftBodyOpt* cpSoftBodyOptReset(cpSoftBodyOpt *body, const cpMaterial* material, const cpSoftBodySolverParams* params);

/// Allocate and initialize a cpSoftBodyOpt.
cpSoftBodyOpt* cpSoftBodyOptNew(const cpMaterial* material, const cpSoftBodySolverParams* params);


//cpSoftBodyOpt* cpSoftBodyOptInitFromSoftBodyShapeOpt(struct cpSoftBodyShapeOpt* shape, const cpMaterial* material, const cpSoftBodySolverParams* params);

cpSoftBodyOpt* cpSoftBodyOptClone(cpSoftBodyShapeOpt* shape, const cpSoftBodyOpt* other );
//cpSoftBodyOpt* cpSoftBodyOptCopy(cpSoftBodyOpt* other);

//cpSoftBodyOpt* cpSoftBodyOptInitBuffers(struct cpSoftBodyShapeOpt* shape, const cpSoftBodyParams* params);

cpSoftBodyOpt* cpSoftBodyOptCopyBuffers(struct cpSoftBodyOpt* other);


/// Destroy a cpSoftBodyOpt.
void cpSoftBodyOptDestroy(cpSoftBodyOpt *body, cpDataPointer userData);
/// Destroy and free a cpSoftBodyOpt.
void cpSoftBodyOptFree(cpSoftBodyOpt *body);

void cpSoftBodyOptAddShape(cpSoftBodyOpt *body, cpSoftBodyShapeOpt *shape);

cpSoftBodyShapeOpt* cpSoftBodyOptRemoveShape(cpSoftBodyOpt *body );


cpSoftBodyShapeOpt* cpGetSoftBodyOptShape(cpSoftBodyOpt *body);


// Defined in cpSpace.c
/// Wake up a sleeping or idle soft body.
void cpSoftBodyOptActivate(cpSoftBodyOpt *body);
/// Wake up any sleeping or idle soft bodies touching a static body.
void cpSoftBodyOptActivateStatic(cpSoftBodyOpt *body, cpShape *filter);

///// Force a soft body to fall asleep immediately.
//void cpSoftBodyOptSleep(cpSoftBodyOpt *body);
///// Force a soft body to fall asleep immediately along with other bodies in a group.
//void cpSoftBodyOptSleepWithGroup(cpSoftBodyOpt *body, cpSoftBodyOpt *group);

/// Returns true if the  soft body is sleeping.
cpBool cpSoftBodyOptIsSleeping(const cpSoftBodyOpt *body);




/// Get the space this body is added to.
cpSpaceSoftBody* cpSoftBodyOptGetSpace(const cpSoftBodyOpt *body);

/// Get the nodes of the body.
const cpVect* cpSoftBodyOptGetNodes(const cpSoftBodyOpt *body);



/// Get the mass of the body.
cpFloat cpSoftBodyOptGetMass(const cpSoftBodyOpt *body);
/// Set the mass of the body.
void cpSoftBodyOptSetMass(cpSoftBodyOpt *body, cpFloat m);


void cpSoftBodyOptSetBrakable(cpSoftBodyOpt *body, cpBool isBrakable);

void cpSoftBodyOptSetPlasticity(cpSoftBodyOpt *body, cpBool isPlastic);

/// Set the position of a body.
cpVect cpSoftBodyOptGetPosition(const cpSoftBodyOpt *body);
/// Set the position of the body.
void cpSoftBodyOptSetPosition(cpSoftBodyOpt *body, const cpVect pos);

void cpSoftBodyOptTranslate(cpSoftBodyOpt *body, const cpVect translate);

/// Rotate the soft body around the cog.
void cpSoftBodyOptRotateAroundCoG(cpSoftBodyOpt *body, cpFloat a);

/// Mirror the soft body around the cog.
void cpSoftBodyOptReflectAroundCoG(cpSoftBodyOpt *body, cpVect axis);


///// Get the offset of the center of gravity in body local coordinates.
//cpVect cpSoftBodyOptGetCenterOfGravity(const cpSoftBodyOpt *body);
///// Set the offset of the center of gravity in body local coordinates.
//void cpSoftBodyOptSetCenterOfGravity(cpSoftBodyOpt *body, cpVect cog);

/// Get the velocity of the body.
cpVect cpSoftBodyOptGetVelocity(const cpSoftBodyOpt *body);
/// Set the velocity of the body.
void cpSoftBodyOptSetVelocity(cpSoftBodyOpt *body, cpVect velocity);

void cpSoftBodyOptSetNodeVelocity(cpSoftBodyOpt *body, cpVect velocity, cpIdx node);

void cpSoftBodyOptSetVelocityX(cpSoftBodyOpt *body, cpFloat vx);

void cpSoftBodyOptSetVelocityY(cpSoftBodyOpt *body, cpFloat vy);

void cpSoftBodyOptAddVelocity(cpSoftBodyOpt *body, cpVect velocity);

void cpSoftBodyOptSetAngularVelocity(cpSoftBodyOpt *body, cpFloat avel);


void cpSoftBodyOptAddConstraintVelocity(cpSoftBodyOpt *body, cpVect velocity, cpIdx node);

void cpSoftBodyOptRemoveConstraintVelocity(cpSoftBodyOpt *body, cpIdx node );


/// Get the force applied to the body for the next time step.
cpVect cpSoftBodyOptGetForce(const cpSoftBodyOpt *body);
/// Set the force applied to the body for the next time step.
void cpSoftBodyOptSetForce(cpSoftBodyOpt *body, cpVect force);

void cpSoftBodyOptSetGravity(cpSoftBodyOpt *body, cpVect gravity);

cpVect cpSoftBodyOptGetCoG(const cpSoftBodyOpt* body);

/// Get the moment of inertia of the soft body.
cpFloat cpSoftBodyOptGetInertia(const cpSoftBodyOpt *body);


unsigned int cpSoftBodyShapeOptGetNumConstrainedNodes(const cpSoftBodyOpt *body);

const cpIdx* cpSoftBodyShapeOptGetConstrainedNodes(const cpSoftBodyOpt *body);


/// Get the user data pointer assigned to the body.
cpDataPointer cpSoftBodyOptGetUserData(const cpSoftBodyOpt *body);
/// Set the user data pointer assigned to the body.
void cpSoftBodyOptSetUserData(cpSoftBodyOpt *body, cpDataPointer userData);

/// Set the callback used to update a soft body's velocity.
void cpSoftBodyOptSetVelocityUpdateFunc(cpSoftBodyOpt *body, cpSoftBodyOptVelocityFunc velocityFunc);
/// Set the callback used to update a body's position.
/// NOTE: It's not generally recommended to override this.
void cpSoftBodyOptSetPositionUpdateFunc(cpSoftBodyOpt *body, cpSoftBodyOptPositionFunc positionFunc);

void cpSoftBodyOptSetSleepingCallback(cpSoftBodyOpt *body, cpSoftBodyOptSleepingCallback callback);

void cpSoftBodyOptSetTriangleDestroyCallback(cpSoftBodyOpt *body, cpSoftBodyOptTriangleDestroyCallback callback);

void cpSoftBodyOptSetDestroyCallback(cpSoftBodyOpt *body, cpSoftBodyOptDestroyCallback callback);


/// Default velocity integration function..
void cpSoftBodyOptUpdateVelocity(cpSoftBodyOpt *body, cpVect gravity, cpFloat damping, cpFloat dt);
/// Default position integration function.
void cpSoftBodyOptUpdatePosition(cpSoftBodyOpt *body, const cpFloat dt);


void cpSoftBodyOptSetCollisionBeginCallback(cpSoftBodyOpt *body, cpSoftBodyOptCollisionBeginCallback callback);

void cpSoftBodyOptSetCollisionSeparateCallback(cpSoftBodyOpt *body, cpSoftBodyOptCollisionSeparateCallback callback);


//inline void cpSoftBodyOptSleep(cpSoftBodyOpt *body) {}
//
//inline void cpSoftBodyOptDestroyTriangle(cpSoftBodyOpt *body, cpTriangle triangle) {}

//void cpSoftBodyOptDestroy(cpSoftBodyOpt *body);


/// Convert body relative/local coordinates to absolute/world coordinates.
cpVect cpSoftBodyOptLocalToWorld(const cpSoftBodyOpt *body, const cpVect point);
/// Convert body absolute/world coordinates to  relative/local coordinates.
cpVect cpSoftBodyOptWorldToLocal(const cpSoftBodyOpt *body, const cpVect point);


/// Apply a force to a body. Both the force and point are expressed in world coordinates.
void cpSoftBodyOptApplyForceAtWorldPoint(cpSoftBodyOpt *body, cpVect force, cpVect point);
/// Apply a force to a body. Both the force and point are expressed in body local coordinates.
void cpSoftBodyOptApplyForceAtLocalPoint(cpSoftBodyOpt *body, cpVect force, cpVect point);


/// Apply an impulse to a body. Both the impulse and point are expressed in world coordinates.
void cpSoftBodyOptApplyImpulseAtWorldPoint(cpSoftBodyOpt *body, cpVect impulse, cpVect point);
/// Apply an impulse to a body. Both the impulse and point are expressed in body local coordinates.
void cpSoftBodyOptApplyImpulseAtLocalPoint(cpSoftBodyOpt *body, cpVect impulse, cpVect point);



/// Get the amount of kinetic energy contained by the body.
cpFloat cpSoftBodyOptKineticEnergy(const cpSoftBodyOpt *body);

/// Body/shape iterator callback function type.
typedef void (*cpSoftBodyShapeOptIteratorFunc)(cpSoftBodyOpt *body, cpShape *shape, void *data);
/// Call @c func once for each shape attached to @c body and added to the space.
void cpSoftBodyOptEachShape(cpSoftBodyOpt *body, cpSoftBodyShapeOptIteratorFunc func, void *data);


/// Body/constraint iterator callback function type.
typedef void (*cpSoftBodyOptConstraintIteratorFunc)(cpSoftBodyOpt *body, cpConstraint *constraint, void *data);
/// Call @c func once for each constraint attached to @c body and added to the space.
void cpSoftBodyOptEachConstraint(cpSoftBodyOpt *body, cpSoftBodyOptConstraintIteratorFunc func, void *data);


/// Body/arbiter iterator callback function type.
typedef void (*cpSoftBodyOptArbiterIteratorFunc)(cpSoftBodyOpt *body, cpArbiter *arbiter, void *data);
/// Call @c func once for each arbiter that is currently active on the body.
void cpSoftBodyOptEachArbiter(cpSoftBodyOpt *body, cpSoftBodyOptArbiterIteratorFunc func, void *data);


cpFloat cpSoftBodyOptSolve(cpSoftBodyOpt *body, const cpFloat dt);


cpFloat cpSoftBodyOptAssembleStiffnessMatrix(cpSoftBodyOpt *body, const cpFloat dt );

cpFloat cpSoftBodyOptComputeStreesForces(cpSoftBodyOpt *body);

cpFloat computeComplianceOpt(cpSoftBodyOpt* body);

cpFloat cpSoftBodyOptUpdateVertexVels(cpSoftBodyOpt *body);

cpFloat cpSoftBodyOptComputeRhs(cpSoftBodyOpt *body, const cpFloat dt);

cpFloat cpSoftBodyOptComputeLhs(cpSoftBodyOpt *body, const cpFloat dt);

cpFloat cpSoftBodyOptComputeLhsRhs(cpSoftBodyOpt *body, const cpFloat dt);

cpFloat cpSoftBodyOptConjGradSolver(cpSoftBodyOpt *body, const cpFloat dt);



cpVect cpSoftBodyOptGetStressForce(const cpSoftBodyOpt *body, const cpIdx nodeId);

cpVect* cpSoftBodyOptGetStressForces( const cpSoftBodyOpt *body );

cpFloat cpSoftBodyOptGetVertexMass( const cpSoftBodyOpt *body, const cpIdx idx);

//void cpSoftBodyOptSetPosition( cpSoftBodyOpt *body );

void cpSoftBodyOptSetOrientation( cpSoftBodyOpt *body, cpMat2x2 rot );

//void cpSoftBodyOptReset( cpSoftBodyOpt *body );

//void cpSoftBodyOptSetVelocity( cpVect vel, cpSoftBodyOpt *body );

void cpSoftBodyOptAddForce( cpSoftBodyOpt *body, const unsigned short idx, cpVect force);

const Matrix3* cpSoftBodyOptGetCompliance(cpSoftBodyOpt *body, const cpIdx nodeId);


void printVectbufferOpt(const struct cpMembuffer* buf, const char* filename);
void printStiffnessMatrixOpt( const cpSoftBodyOpt *body, const char* filename );


// be careful
void removeDeformedElementsOpt(cpSoftBodyOpt *body, const cpFloat cosAngleFracture, const cpFloat areaFractureMinRatio, const cpFloat areaFractureMaxRatio );

void removeStressedElementOpt(cpSoftBodyOpt *body);

// not used
void computeStressForcesOpt(cpSoftBodyOpt *body );

cpVect cpSoftBodyOptGetCoGvelocity(const cpSoftBodyOpt *body);


///@}


#endif
