//
//  cpPoolAllocator.h
//  Chipmunk7
//
//  Created by Federico Spadoni on 19/11/14.
//
//

#ifndef Chipmunk7_cpPoolAllocator_h
#define Chipmunk7_cpPoolAllocator_h

#include "chipmunk_types.h"
//#include "chipmunk.h"



//static const int poolChunkSize = 16 * 1024;
#define maxPoolSize  32768
#define poolSizes  16
#define chunkArrayIncrement  128



static const int g_pools[poolSizes][2] =
{
//    {size, numAlloc }
	{ 16, 16} ,	// 0
	{ 32, 16} ,	// 1
	{ 64, 16} ,		// 2
	{ 96, 16} ,      // 3
	{ 128, 16} ,	// 4
	{ 192, 64} ,     // 5 // cpSoftBodyShape
	{ 256, 16} ,	// 6  // cpBody around 216
	{ 384, 16} ,	// 7
	{ 512, 64} ,	// 8 // cpSoftBody
	{ 768, 16} ,	// 9
	{ 1024, 16} ,	// 10
	{ 2048, 16} ,	// 11
	{ 4096, 16} ,	// 12
	{ 8192, 16} ,	// 13
    { 16384, 2} ,	// 14
    { 32768, 2}	// 15
};

//unsigned char poolSizeLookup[maxPoolSize + 1];
//bool b2BlockAllocator::s_blockSizeLookupInitialized;

struct Pool;
struct Chunk;
struct UnPooled;

typedef struct Chunk
{
	int poolSize;
	struct Pool* pools;
} Chunk;

typedef struct Pool
{
	struct Pool* next;
} Pool;

typedef struct UnPooled
{
    void* ptr;
} UnPooled;

/// This is a small object allocator used for allocating small
/// objects that persist for more than one time step.
/// See: http://www.codeproject.com/useritems/Small_Block_Allocator.asp
struct cpPoolAllocator
{
    
	Chunk* chunks;
	int chunkCount;
	int chunkSpace;
    
	Pool* freeLists[poolSizes];
    
    
    UnPooled* unPooled;
    int unPooledCount;
    int unPooledSize;
    
//	static int s_poolSizes[poolSizes];
//	static unsigned short s_poolSizeLookup[maxPoolSize + 1];
//	cpBool poolSizeLookupInitialized;
};

cpPoolAllocator* newPoolAllocator(void);

void setAllocator(cpPoolAllocator* allocator);

cpPoolAllocator* getAllocator(void);

//static inline cpPoolAllocator* newPoolAllocator()
//{
//    struct cpPoolAllocator* allocator = (struct cpPoolAllocator*)cpcalloc( 1, sizeof(struct cpPoolAllocator) );
//
//	allocator->chunkSpace = chunkArrayIncrement;
//	allocator->chunkCount = 0;
//	allocator->chunks = (Chunk*)cpcalloc(1, allocator->chunkSpace * sizeof(Chunk));
//	
//	memset(allocator->chunks, 0, allocator->chunkSpace * sizeof(Chunk));
//	memset(allocator->freeLists, 0, sizeof(allocator->freeLists));
//    
//	if (allocator->poolSizeLookupInitialized == false)
//	{
//		int j = 0;
//		for (int i = 1; i <= maxPoolSize; ++i)
//		{
//			cpAssertHard(j < poolSizes, "decrease maxPoolSize or add element in pools array");
//			if (i <= pools[j])
//			{
//				poolSizeLookup[i] = (unsigned char)j;
//			}
//			else
//			{
//				++j;
//				poolSizeLookup[i] = (unsigned char)j;
//			}
//		}
//        
//		allocator->poolSizeLookupInitialized = true;
//	}
//    return allocator;
//}

/// Allocate memory. This will use Alloc if the size is larger than b2_maxBlockSize.
void* allocatePool( unsigned int size);
//{
//    if (size == 0)
//		return NULL;
//    
//	cpAssertHard(0 < size, "allocatePool: size < 0");
//    
//	if (size > maxPoolSize)
//	{
//		return cpcalloc(1, size);
//	}
//    
//	int index = poolSizeLookup[size];
//	cpAssertHard(0 <= index && index < poolSizes,"allocatePool: poolSizeLookup[size] out of pools value");
//    
//	if ((*allocator)->freeLists[index])
//	{
//		Pool* pool = (*allocator)->freeLists[index];
//		(*allocator)->freeLists[index] = pool->next;
//		return pool;
//	}
//	else
//	{
//		if ((*allocator)->chunkCount == (*allocator)->chunkSpace)
//		{
//			Chunk* oldChunks = (*allocator)->chunks;
//			(*allocator)->chunkSpace += chunkArrayIncrement;
//			(*allocator)->chunks = (Chunk*)cpcalloc(1, (*allocator)->chunkSpace * sizeof(Chunk));
//			memcpy((*allocator)->chunks, oldChunks, (*allocator)->chunkCount * sizeof(Chunk));
//			memset((*allocator)->chunks + (*allocator)->chunkCount, 0, chunkArrayIncrement * sizeof(Chunk));
//			cpfree(oldChunks);
//		}
//        
//		Chunk* chunk = (*allocator)->chunks + (*allocator)->chunkCount;
//		chunk->pools = (Pool*)cpcalloc(1,CP_BUFFER_BYTES);
//#if defined(_DEBUG)
//		memset(chunk->blocks, 0xcd, poolChunkSize);
//#endif
//		int poolSize = pools[index];
//		chunk->poolSize = poolSize;
//		int blockCount = CP_BUFFER_BYTES / poolSize;
//		cpAssertHard(blockCount * poolSize <= CP_BUFFER_BYTES, "allocatePool: poolCount * poolSize <= CP_BUFFER_BYTES");
//		for (int i = 0; i < blockCount - 1; ++i)
//		{
//			Pool* block = (Pool*)((unsigned char*)chunk->pools + poolSize * i);
//			Pool* next = (Pool*)((unsigned char*)chunk->pools + poolSize * (i + 1));
//			block->next = next;
//		}
//		Pool* last = (Pool*)((unsigned char*)chunk->pools + poolSize * (blockCount - 1));
//		last->next = NULL;
//        
//		(*allocator)->freeLists[index] = chunk->pools->next;
//		++(*allocator)->chunkCount;
//        
//		return chunk->pools;
//	}
//}

void freePool(void* p, unsigned int size);
//{
//    if (size == 0)
//    {
//        return;
//    }
//    
//    cpAssertHard(0 < size, "freePool: size < 0");
//    
//    if (size > maxPoolSize)
//    {
//        cpfree(p);
//        return;
//    }
//    
//    int index = poolSizeLookup[size];
//    cpAssertHard(0 <= index && index < poolSizes,"freePool: poolSizeLookup[size] out of pools value");
//    
//#ifndef NDEBUG
//    // Verify the memory address and size is valid.
//    int blockSize = pools[index];
//    bool found = false;
//    for (int i = 0; i < (*allocator)->chunkCount; ++i)
//    {
//        Chunk* chunk = (*allocator)->chunks + i;
//        if (chunk->poolSize != blockSize)
//        {
//            cpAssertHard(	(unsigned char*)p + blockSize <= (unsigned char*)chunk->pools ||
//                     (unsigned char*)chunk->pools + CP_BUFFER_BYTES <= (unsigned char*)p, "freePool: pool allocator failed");
//        }
//        else
//        {
//            if ((unsigned char*)chunk->pools <= (unsigned char*)p && (unsigned char*)p + blockSize <= (unsigned char*)chunk->pools + CP_BUFFER_BYTES)
//            {
//                found = true;
//            }
//        }
//    }
//    
//    cpAssertHard(found, "freePool: pool allocator failed");
//    
//    memset(p, 0xfd, blockSize);
//#endif
//    
//    Pool* block = (Pool*)p;
//    block->next = (*allocator)->freeLists[index];
//    (*allocator)->freeLists[index] = block;
//}

/// Free memory. This will use Free if the size is larger than b2_maxBlockSize.
void destroyPoolAllocator(cpPoolAllocator* allocator);
//{
//    
//    for (int i = 0; i < (*allocator)->chunkCount; ++i)
//	{
//		cpfree((*allocator)->chunks[i].pools);
//	}
//    
//	cpfree((*allocator)->chunks);
//    *allocator = NULL;
//}


void clearPoolAllocator(void);
//{
//    for (int i = 0; i < (*allocator)->chunkCount; ++i)
//	{
//		cpfree((*allocator)->chunks[i].pools);
//	}
//    
//	(*allocator)->chunkCount = 0;
//	memset((*allocator)->chunks, 0, (*allocator)->chunkSpace * sizeof(Chunk));
//    
//	memset((*allocator)->freeLists, 0, sizeof((*allocator)->freeLists));
//    
//}

//cpPoolAllocator** getAllocator();
//{
////    static cpPoolAllocator* instance = NULL;
//    
//    // do lock here
//    if(instance == NULL)
//    {
//    	instance = newPoolAllocator();
//
//    }
//    // do unlock
//    
//    return &instance;
//};




#endif
