//
//  chipmunk_fem_utils.h
//  Chipmunk7
//
//  Created by Federico Spadoni on 15/07/14.
//
//

#ifndef Chipmunk7_chipmunk_fem_utils_h
#define Chipmunk7_chipmunk_fem_utils_h


static const int INPUTLINESIZE = 1024;

inline void cpScaleMaterial(struct cpMaterial* material, const cpFloat scale)
{
    material->density /= scale; // [Kg/m^3]
//    cpFloat youngModulus; // [N/m^2]
//    cpFloat poissonRatio; // []
//    cpFloat friction; // [Kg/s] [N*s/m]
//    //    C = (massDamping*M + stiffnessDamping*K)
//    cpFloat stiffnessDamping; // rayleigh stiffness damping 0.1
//    cpFloat massDamping; // rayleigh mass damping 0.1
//    cpFloat yield; // plasticity
//    cpFloat maxYield; // plasticity
//    cpFloat creep; // plasticity
//    cpFloat toughness; // for fractures

}



static inline int idxCompare (const void * a, const void * b)
{
    return ( *(const cpIdx*)a - *(const cpIdx*)b );
}

static inline char *readline(char *string, FILE *infile, char *infilename)
{
    char *result;
    
    /* Search for something that looks like a number. */
    do {
        result = fgets(string, INPUTLINESIZE, infile);
        if (result == (char *) NULL) {
            printf("  Error:  Unexpected end of file in %s.\n", infilename);
            //            triexit(1);
        }
        /* Skip anything that doesn't look like a number, a comment, */
        /*   or the end of a line.                                   */
        while ((*result != '\0') && (*result != '#')
               && (*result != '.') && (*result != '+') && (*result != '-')
               && ((*result < '0') || (*result > '9'))) {
            result++;
        }
        /* If it's a comment or end of line, read another line and try again. */
    } while ((*result == '#') || (*result == '\0'));
    return result;
}


static inline char *findfield(char *string)
{
    char *result;
    
    result = string;
    /* Skip the current field.  Stop upon reaching whitespace. */
    while ((*result != '\0') && (*result != '#')
           && (*result != ' ') && (*result != '\t')) {
        result++;
    }
    /* Now skip the whitespace and anything else that doesn't look like a */
    /*   number, a comment, or the end of a line.                         */
    while ((*result != '\0') && (*result != '#')
           && (*result != '.') && (*result != '+') && (*result != '-')
           && ((*result < '0') || (*result > '9'))) {
        result++;
    }
    /* Check for a comment (prefixed with `#'). */
    if (*result == '#') {
        *result = '\0';
    }
    return result;
}


#endif
