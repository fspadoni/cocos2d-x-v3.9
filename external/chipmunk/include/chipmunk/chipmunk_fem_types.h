//
//  chipmunk_fem_types.h
//  Chipmunk7
//
//  Created by Federico Spadoni on 04/07/14.
//
//

#ifndef Chipmunk7_chipmunk_fem_types_h
#define Chipmunk7_chipmunk_fem_types_h


//#define cpcalloc( A ,B )  allocatePool( (unsigned int)A * (unsigned int)B )
//#define cpfree freePool

static const unsigned long _SimdAligned = 16;

static inline void* getNextAlignedMemory(const void* ptr, const unsigned long alignment)
{
    return (void *)(( (unsigned long)(ptr) + alignment - 1) & ~ (alignment - 1) );
}

static inline void* getNextSimdAlignedMemory(const void* ptr)
{
    return (void *)(( (unsigned long)(ptr) + _SimdAligned - 1) & ~ (_SimdAligned - 1) );
}

typedef unsigned short cpIdx;


enum SOFTBODY_COLLISION_TYPES {
    SB_SHAPES = 0,
	SB_POINT = 1 << 0,
    SB_BEAM_POINT = 1 << 1,
	SB_TRIANGLE = 1 << 2,
    SB_BEAM_SEGMENT = 1 << 3
};


enum CONSTRIANT_TYPES {
    CT_PERMANENT = 0,
	CT_TEMPORARY = 1 << 0,
    CT_LOCKED = 1 << 1
};


// from ODE
enum {
//    dContactMu2	  = 0x001,      /**< Use axis dependent friction */
    dContactAxisDep = 0x001,      /**< Same as above */
//    dContactFDir1	  = 0x002,      /**< Use FDir for the first friction value */
    dContactBounce  = 0x004,      /**< Restore collision energy anti-parallel to the normal */
    dContactSoftERP = 0x008,      /**< Don't use global erp for penetration reduction */
    dContactSoftCFM = 0x010,      /**< Don't use global cfm for penetration constraint */
    dContactMotion1 = 0x020,      /**< Use a non-zero target velocity for the constraint */
//    dContactMotion2 = 0x040,
    dContactMotionN = 0x080,
    dContactSlip1	  = 0x100,      /**< Force-dependent slip. */
    dContactSlip2	  = 0x200,
    dContactRolling = 0x400,      /**< Rolling/Angular friction */
    
    dContactApprox0   = 0x0000,
    dContactApprox1_1 = 0x1000,
    dContactApprox1_2 = 0x2000,
    dContactApprox1_N = 0x4000,   /**< For rolling friction */
    dContactApprox1   = 0x7000
};


typedef struct ContactSurfaceParameters {
    /* must always be defined */
    int   mode;
    cpFloat mu;
    
    /* only defined if the corresponding flag is set in mode */
    cpFloat mu2;
    cpFloat rho;                    /**< Rolling friction */
    cpFloat rho2;
    cpFloat rhoN;                   /**< Spinning friction */
    cpFloat bounce;                 /**< Coefficient of restitution */
    cpFloat bounce_vel;             /**< Bouncing threshold */
    cpFloat soft_erp;
    cpFloat soft_cfm;
    cpFloat motion1,motion2,motionN;
    cpFloat slip1,slip2;
} ContactSurfaceParameters;



struct cpContactParams {

	ContactSurfaceParameters surface;
	cpFloat depth; // penetration depth

};

typedef struct cpSoftBodyContact {
    cpIdx p;
    cpFloat bias;
    cpVect n;
} cpSoftBodyContact;

// used by SoftBodyOpt
typedef struct cpMaterial {
    // material
    cpFloat density; // [Kg/m^3]
    cpFloat youngModulus; // [N/m^2]
    cpFloat poissonRatio; // []
    cpFloat friction; // [Kg/s] [N*s/m]
//    C = (massDamping*M + stiffnessDamping*K)
    cpFloat stiffnessDamping; // rayleigh stiffness damping 0.1
    cpFloat massDamping; // rayleigh mass damping 0.1
    cpFloat contactFriction;
    cpFloat contactElasticity;
    cpFloat yield; // plasticity
    cpFloat maxYield; // plasticity
    cpFloat creep; // plasticity
    cpFloat toughness; // for fractures
    cpFloat cosAngleFracture; // for fractures
    cpFloat areaFractureMinRatio; // for fractures
    cpFloat areaFractureMaxRatio; // for fractures

} cpMaterial;

typedef struct cpBeamMaterial {

    // material
    cpFloat radius; // [m2]
//    cpFloat inertia; // []
    cpFloat density; // [Kg/m^3]
    cpVect youngModulus; // [N/m^2]
//    cpFloat poissonRatio; // []
    cpFloat friction; // [Kg/s] [N*s/m]
    //    C = (massDamping*M + stiffnessDamping*K)
    cpFloat stiffnessDamping; // rayleigh stiffness damping 0.1
    cpFloat massDamping; // rayleigh mass damping 0.1
    cpFloat contactFriction;
    cpFloat contactElasticity;
    cpFloat yield; // plasticity
    cpFloat maxYield; // plasticity
    cpFloat creep; // plasticity
    cpFloat axialToughness; // for fractures
    cpFloat transverseToughness; // for fractures
    cpFloat cosAngleFracture; // for fractures
    cpFloat areaFractureMinRatio; // for fractures
    cpFloat areaFractureMaxRatio; // for fractures
    
} cpBeamMaterial;

// used by SoftBodyOpt
typedef struct cpSoftBodySolverParams {
    
//    cpFloat timeStep; //[s]
    cpVect position;
    unsigned int nbSolverIteration;
    cpFloat solverTolerance;
    
    const char* constrNodesFile;
    
    unsigned int numConstrainedNodes;
    cpIdx* constrainedNodes;
    
} cpSoftBodySolverParams;

// used by SoftBody
typedef struct cpSoftBodyParams {
    
    cpFloat timeStep; //[s]
    cpVect position;
    cpMat2x2 orientation;
    unsigned int nbSolverIteration;
    cpFloat solverTolerance;
    
    const char* constrNodesFile;
    const struct cpMaterial* material;
} cpSoftBodyParams;

typedef struct cpTriangle
{
    cpIdx n[3];
} cpTriangle;

typedef struct cpSegment
{
    cpIdx n[2];
} cpSegment;

typedef struct cpBeamNode
{
//    cpVect v; // vertex
//    cpVect t; // orientation
    cpFloat x;
    cpFloat y;
    cpFloat a;
    
    union{
        cpFloat _unused;
        unsigned int _mask;
    };
} cpBeamNode;

typedef struct cpVect3
{
    float x,y,z, _unused;
} cpVect3;

typedef struct cpVect6
{
    cpVect v[3];
} cpVect6;

typedef struct cpBeamElem
{
    cpBeamNode n[2];
} cpBeamElem;

// a 2x2 col major matrix
typedef struct Matrix2
{
    cpVect col[2];
    
} Matrix2;


typedef struct cpMat3x2
{
    struct cpVect3 col[2];
} cpMat3x2;

typedef struct cpMat2x3
{
    cpVect col[3];
} cpMat2x3;


typedef struct Matrix3
{
    struct cpVect3 col[3];
} Matrix3;


typedef struct Matrix3 ElasticityMatrix;
typedef struct Matrix2 BeamElasticityMatrix;

//{
//    
//    struct cpVect3 col[3];
//    
//} ElasticityMat;


typedef struct StrainMatrix
{
    cpMat3x2 m[3];
} StrainMatrix;


typedef struct PlasticityMatrix
{
    cpMat2x3 m[3];
} PlasticityMatrix;

typedef struct BeamPlasticityMatrix
{
    cpMat2x3 m[2];
} BeamPlasticityMatrix;

typedef struct cpMat6
{
    struct Matrix2 m[3][3];
} cpMat6;
typedef struct cpMat6 ElementStiffnessMatrix;


typedef struct BeamStrainMatrix
{
    cpMat2x3 m[2];
} BeamStrainMatrix;

typedef struct cpBeamStiffnessMatrix
{
    struct Matrix3 m[2][2];
} cpBeamStiffnessMatrix;
typedef struct cpBeamStiffnessMatrix BeamStiffnessMatrix;

typedef struct Kmap
{
    cpIdx ij[3][3];
} Kmap;


typedef struct KmapBeam
{
    cpIdx ij[2][2];
} KmapBeam;

/// Convenience constructor for cpVect structs.
static inline Matrix2 cpMatrixFromFloat( const cpFloat r0c0, const cpFloat r0c1, const cpFloat r1c0, const cpFloat r1c1)
{
    Matrix2 dest;
    dest.col[0].x = r0c0;
    dest.col[0].y = r1c0;
    dest.col[1].x = r0c1;
    dest.col[1].y = r1c1;
    return dest;
}

/// Convenience constructor for Matrix from cpVect structs.
static inline Matrix2 cpMatrixFromVect( const cpVect* c0, const cpVect* c1)
{
    Matrix2 dest;
    dest.col[0] = *c0;
    dest.col[1] = *c1;
    return dest;
}

/// Convenience constructor for Matrix from Matrix2 structs.
static inline Matrix2 cpMatrixCopy( const Matrix2* src )
{
    return cpMatrixFromVect(&src->col[0], &src->col[1]);
}

static inline cpFloat Matrix2_det( const Matrix2* mat )
{
    
    return (mat->col[0].x * mat->col[1].y) - (mat->col[1].x * mat->col[0].y);
}


static inline cpFloat cpAreaForTriangle(const cpVect* p0, const cpVect* p1, const cpVect* p2 )
{
    // not fast, optimize it
    Matrix2 a0 = cpMatrixFromVect( p1, p2 );
    Matrix2 a1 = cpMatrixFromVect( p2, p0 );
    Matrix2 a2 = cpMatrixFromVect( p0, p1 );
    
    return ( Matrix2_det(&a0) + Matrix2_det(&a1) + Matrix2_det(&a2) ) / 2.0;

}



#endif
