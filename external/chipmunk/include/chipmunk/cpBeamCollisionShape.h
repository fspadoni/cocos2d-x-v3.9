//
//  cpBeamCollisionShape.h
//  Chipmunk7
//
//  Created by Federico Spadoni on 13/04/15.
//
//

#ifndef Chipmunk7_cpBeamCollisionShape_h
#define Chipmunk7_cpBeamCollisionShape_h


struct cpPolyLineBuffer;
struct cpMembuffer;


unsigned int cpBeamShapeComputeMemorySize(const unsigned int numNodes, const unsigned int numElems, const unsigned int graphSize);

unsigned int cpBeamShapeGetBufferSize(const cpBeamShape* shape);


/// Allocate a Segment shape.
cpBeamShape* cpBeamShapeAlloc(void);

cpBeamShape* cpBeamShapeInitFromMeshBuffer( struct cpPolyLineBuffer* mesh, const cpFloat collisionMargin);

cpBeamShape* cpBeamShapeCopy( cpBeamShape* other, struct cpMembuffer* buffer);

//cpBeamShape* cpBeamShapeInitFromFile(cpBeamShape *shape, char* filename);

//cpPointShape * cpPointShapeInit(cpPointShape *shape, cpBody *body, const cpIdx idx, const cpFloat margin, const cpVect* verts, const cpVect* velocities);
//
///// Initialize a Segment shape.
//cpSegmentShape* cpSegmentShapeInit(cpSegmentShape *shape, cpBody *body, const cpSegment* triang, const cpVect* verts, const cpVect* velocities);

/// Initialize a Segment shape.
cpBeamShape* cpBeamShapeInit(cpBeamShape *circle, cpBeamBody *body, cpBeamNode* nodes, unsigned int numVertices,  cpIdx *segments, unsigned int numSegments );

void cpBeamShapeAddToSpace(cpBeamShape* shape, cpSpace* space );


/// Allocate and initialize a Segment shape.
cpShape* cpBeamShapeNew(cpBeamBody *body, cpBeamNode* nodes, unsigned int numVertices,
                               cpIdx *segments, unsigned int numSegments );


void cpBeamShapeRemove(cpSpace* space, cpBeamShape *shape);

void cpBeamShapeDelete(cpBeamShape *shape);


unsigned int cpBeamShapeGetMemSize(cpBeamShape *shape);

//CP_DeclareShapeGetter(cpSoftBodyShape, cpVect*, verts);
//CP_DeclareShapeGetter(cpSoftBodyShape, unsigned int, numVerts);
//CP_DeclareShapeGetter(cpSoftBodyShape, unsigned short*, triangs);
//CP_DeclareShapeGetter(cpSoftBodyShape, unsigned int, numTriangs);


cpBeamNode* cpBeamShapeGetVerts(const cpBeamShape *shape);

unsigned int cpBeamShapeGetNumVerts(const cpBeamShape *shape);

unsigned int cpBeamShapeGetNumActiveVerts(const cpBeamShape *shape);

cpIdx* cpBeamShapeGetSegments(const cpBeamShape *shape);

unsigned int cpBeamShapeGetNumSegments(const cpBeamShape *shape);

unsigned int cpBeamShapeGetNumActiveSegments(const cpBeamShape *shape);

cpIdx cpBeamShapeGetFirstActiveSegments(const cpBeamShape *shape);

const cpIdx* cpBeamShapeGetActiveSegments(const cpBeamShape *shape);


void cpBeamShapeSetFilter(cpBeamShape *shape, unsigned int category, unsigned int mask);

void cpBeamShapeSetPointFilter(cpBeamShape *shape, cpIdx nodeId, unsigned int category, unsigned int mask);

void cpBeamShapeSetSegmentFilter(cpBeamShape *shape, cpIdx triagleId, unsigned int category, unsigned int mask);



#endif
