//
//  cpBeamBody.h
//  Chipmunk7
//
//  Created by Federico Spadoni on 13/04/15.
//
//

#ifndef Chipmunk7_cpBeamBody_h
#define Chipmunk7_cpBeamBody_h

struct cpPolyLine;

/// Rigid body velocity update function type.
typedef void (*cpBeamBodyVelocityFunc)(cpBeamBody *body, cpVect gravity, cpFloat damping, cpFloat dt);
/// Rigid body position update function type.
typedef void (*cpBeamBodyPositionFunc)(cpBeamBody *body, cpFloat dt);

typedef cpFloat (*cpAssembleBeamStiffMatrixFunc)(cpBeamBody *body, const cpFloat dt);

typedef cpFloat (*cpBeamBodySolveFunc)(cpBeamBody* body, const cpFloat dt);

typedef void (*cpBeamBodySleepingCallback)(cpBeamBody* body, cpDataPointer userData);

typedef void (*cpBeamBodySegmentDestroyCallback)(cpBeamBody* body, cpIdx segmIdx, cpDataPointer userData);

typedef void (*cpBeamBodyDestroyCallback)(cpBeamBody* body, cpDataPointer userData);

typedef void (*cpBeamBodyCollisionBeginCallback)(cpBeamBody* body, cpBody* collidingBody, cpArbiter* arb, cpDataPointer userData);

typedef void (*cpBeamBodyCollisionSeparateCallback)(cpBeamBody* body, cpBody* collidingBody, cpArbiter* arb, cpDataPointer userData);



unsigned int cpBeamBodyGetBufferSize(const cpBeamBody* body);

unsigned int cpBeamBodyComputeMemorySize(const unsigned int numNodes, const unsigned int numElems, const unsigned int graphSize);

unsigned int cpBeamBodyComputeMemorySizeFromPolyLine(const struct cpPolyLine *poly);

//ssize_t cpBeamBodyGetMemorySize(const cpBeamBody *body);

/// Allocate a cpBeamBody.
cpBeamBody* cpBeamBodyAlloc(void);
/// Initialize a cpBeamBody.
cpBeamBody* cpBeamBodyInit(cpBeamShape *shape, const cpBeamMaterial* material, const cpSoftBodySolverParams* params);

cpBeamBody* cpBeamBodyReset(cpBeamBody *body, const cpBeamMaterial* material, const cpSoftBodySolverParams* params);

/// Allocate and initialize a cpBeamBody.
cpBeamBody* cpBeamBodyNew(const cpBeamMaterial* material, const cpSoftBodySolverParams* params);


//cpBeamBody* cpBeamBodyInitFromSoftBodyShapeOpt(struct cpBeamShape* shape, const cpMaterial* material, const cpSoftBodySolverParams* params);

cpBeamBody* cpBeamBodyClone(cpBeamShape* shape, const cpBeamBody* other );
//cpBeamBody* cpBeamBodyCopy(cpBeamBody* other);

//cpBeamBody* cpBeamBodyInitBuffers(struct cpBeamShape* shape, const cpSoftBodyParams* params);

cpBeamBody* cpBeamBodyCopyBuffers(struct cpBeamBody* other);


/// Destroy a cpBeamBody.
void cpBeamBodyDestroy(cpBeamBody *body, cpDataPointer userData);
/// Destroy and free a cpBeamBody.
void cpBeamBodyFree(cpBeamBody *body);

void cpBeamBodyAddShape(cpBeamBody *body, cpBeamShape *shape);

cpBeamShape* cpBeamBodyRemoveShape(cpBeamBody *body );


cpBeamShape* cpGetBeamShape(cpBeamBody *body);


// Defined in cpSpace.c
/// Wake up a sleeping or idle soft body.
void cpBeamBodyActivate(cpBeamBody *body);
/// Wake up any sleeping or idle soft bodies touching a static body.
void cpBeamBodyActivateStatic(cpBeamBody *body, cpShape *filter);

///// Force a soft body to fall asleep immediately.
//void cpBeamBodySleep(cpBeamBody *body);
///// Force a soft body to fall asleep immediately along with other bodies in a group.
//void cpBeamBodySleepWithGroup(cpBeamBody *body, cpBeamBody *group);

/// Returns true if the  soft body is sleeping.
cpBool cpBeamBodyIsSleeping(const cpBeamBody *body);




/// Get the space this body is added to.
cpSpaceSoftBody* cpBeamBodyGetSpace(const cpBeamBody *body);

/// Get the nodes of the body.
const cpBeamNode* cpBeamBodyGetNodes(const cpBeamBody *body);



/// Get the mass of the body.
cpFloat cpBeamBodyGetMass(const cpBeamBody *body);
/// Set the mass of the body.
void cpBeamBodySetMass(cpBeamBody *body, cpFloat m);



/// Set the position of a body.
cpVect cpBeamBodyGetPosition(const cpBeamBody *body);
/// Set the position of the body.
void cpBeamBodySetPosition(cpBeamBody *body, const cpVect pos);

void cpBeamBodyTranslate(cpBeamBody *body, const cpVect translate);

/// Rotate the soft body around the cog.
void cpBeamBodyRotateAroundCoG(cpBeamBody *body, cpFloat a);

/// Mirror the soft body around the cog.
void cpBeamBodyReflectAroundCoG(cpBeamBody *body, cpVect axis);


///// Get the offset of the center of gravity in body local coordinates.
//cpVect cpBeamBodyGetCenterOfGravity(const cpBeamBody *body);
///// Set the offset of the center of gravity in body local coordinates.
//void cpBeamBodySetCenterOfGravity(cpBeamBody *body, cpVect cog);

/// Get the velocity of the body.
cpVect cpBeamBodyGetVelocity(const cpBeamBody *body);
/// Set the velocity of the body.
void cpBeamBodySetVelocity(cpBeamBody *body, cpVect velocity);

void cpBeamBodySetNodeVelocity(cpBeamBody *body, cpVect velocity, cpIdx node);

void cpBeamBodySetVelocityX(cpBeamBody *body, cpFloat vx);

void cpBeamBodySetVelocityY(cpBeamBody *body, cpFloat vy);

void cpBeamBodyAddVelocity(cpBeamBody *body, cpVect velocity);

void cpBeamBodySetAngularVelocity(cpBeamBody *body, cpFloat avel);


void cpBeamBodyAddConstraintVelocity(cpBeamBody *body, cpVect velocity, cpIdx node);

void cpBeamBodyRemoveConstraintVelocity(cpBeamBody *body, cpIdx node);


/// Get the force applied to the body for the next time step.
cpVect cpBeamBodyGetForce(const cpBeamBody *body);
/// Set the force applied to the body for the next time step.
void cpBeamBodySetForce(cpBeamBody *body, cpVect force);

void cpBeamBodySetGravity(cpBeamBody *body, cpVect gravity);

cpVect cpBeamBodyGetCoG(const cpBeamBody* body);

/// Get the moment of inertia of the soft body.
cpFloat cpBeamBodyGetInertia(const cpBeamBody *body);


unsigned int cpBeamShapeGetNumConstrainedNodes(const cpBeamBody *body);

const cpIdx* cpBeamShapeGetConstrainedNodes(const cpBeamBody *body);


/// Get the user data pointer assigned to the body.
cpDataPointer cpBeamBodyGetUserData(const cpBeamBody *body);
/// Set the user data pointer assigned to the body.
void cpBeamBodySetUserData(cpBeamBody *body, cpDataPointer userData);

/// Set the callback used to update a soft body's velocity.
void cpBeamBodySetVelocityUpdateFunc(cpBeamBody *body, cpBeamBodyVelocityFunc velocityFunc);
/// Set the callback used to update a body's position.
/// NOTE: It's not generally recommended to override this.
void cpBeamBodySetPositionUpdateFunc(cpBeamBody *body, cpBeamBodyPositionFunc positionFunc);

void cpBeamBodySetSleepingCallback(cpBeamBody *body, cpBeamBodySleepingCallback callback);

void cpBeamBodySetSegmentDestroyCallback(cpBeamBody *body, cpBeamBodySegmentDestroyCallback callback);

void cpBeamBodySetDestroyCallback(cpBeamBody *body, cpBeamBodyDestroyCallback callback);


/// Default velocity integration function..
void cpBeamBodyUpdateVelocity(cpBeamBody *body, cpVect gravity, cpFloat damping, cpFloat dt);
/// Default position integration function.
void cpBeamBodyUpdatePosition(cpBeamBody *body, const cpFloat dt);


void cpBeamBodySetCollisionBeginCallback(cpBeamBody *body, cpBeamBodyCollisionBeginCallback callback);

void cpBeamBodySetCollisionSeparateCallback(cpBeamBody *body, cpBeamBodyCollisionSeparateCallback callback);


//inline void cpBeamBodySleep(cpBeamBody *body) {}
//
//inline void cpBeamBodyDestroyTriangle(cpBeamBody *body, cpTriangle triangle) {}

//void cpBeamBodyDestroy(cpBeamBody *body);


/// Convert body relative/local coordinates to absolute/world coordinates.
cpVect cpBeamBodyLocalToWorld(const cpBeamBody *body, const cpVect point);
/// Convert body absolute/world coordinates to  relative/local coordinates.
cpVect cpBeamBodyWorldToLocal(const cpBeamBody *body, const cpVect point);


/// Apply a force to a body. Both the force and point are expressed in world coordinates.
void cpBeamBodyApplyForceAtWorldPoint(cpBeamBody *body, cpVect force, cpVect point);
/// Apply a force to a body. Both the force and point are expressed in body local coordinates.
void cpBeamBodyApplyForceAtLocalPoint(cpBeamBody *body, cpVect force, cpVect point);


/// Apply an impulse to a body. Both the impulse and point are expressed in world coordinates.
void cpBeamBodyApplyImpulseAtWorldPoint(cpBeamBody *body, cpVect impulse, cpVect point);
/// Apply an impulse to a body. Both the impulse and point are expressed in body local coordinates.
void cpBeamBodyApplyImpulseAtLocalPoint(cpBeamBody *body, cpVect impulse, cpVect point);



/// Get the amount of kinetic energy contained by the body.
cpFloat cpBeamBodyKineticEnergy(const cpBeamBody *body);

/// Body/shape iterator callback function type.
typedef void (*cpBeamShapeIteratorFunc)(cpBeamBody *body, cpShape *shape, void *data);
/// Call @c func once for each shape attached to @c body and added to the space.
void cpBeamBodyEachShape(cpBeamBody *body, cpBeamShapeIteratorFunc func, void *data);


/// Body/constraint iterator callback function type.
typedef void (*cpBeamBodyConstraintIteratorFunc)(cpBeamBody *body, cpConstraint *constraint, void *data);
/// Call @c func once for each constraint attached to @c body and added to the space.
void cpBeamBodyEachConstraint(cpBeamBody *body, cpBeamBodyConstraintIteratorFunc func, void *data);


/// Body/arbiter iterator callback function type.
typedef void (*cpBeamBodyArbiterIteratorFunc)(cpBeamBody *body, cpArbiter *arbiter, void *data);
/// Call @c func once for each arbiter that is currently active on the body.
void cpBeamBodyEachArbiter(cpBeamBody *body, cpBeamBodyArbiterIteratorFunc func, void *data);


cpFloat cpBeamBodySolve(cpBeamBody *body, const cpFloat dt);


cpFloat cpBeamBodyAssembleStiffnessMatrix(cpBeamBody *body, const cpFloat dt );

cpFloat cpBeamBodyComputeStreesForces(cpBeamBody *body);

cpFloat computeBeamCompliance(cpBeamBody* body);

cpFloat cpBeamBodyUpdateVertexVels(cpBeamBody *body);

cpFloat cpBeamBodyComputeRhs(cpBeamBody *body, const cpFloat dt);

cpFloat cpBeamBodyComputeLhs(cpBeamBody *body, const cpFloat dt);

cpFloat cpBeamBodyConjGradSolver(cpBeamBody *body, const cpFloat dt);

cpFloat cpBeamBodyConjGradSolver2(cpBeamBody *body, const cpFloat dt, cpIdx firstNode, unsigned int numNodes );

cpFloat cpBeamBodyBlockTridiagSolver(cpBeamBody *body, const cpFloat dt );

cpFloat cpBeamBodyBlockTridiagSolver2(cpBeamBody *body, const cpFloat dt, cpIdx firstNode, unsigned int numNodes );



cpBeamNode cpBeamBodyGetStressForce(const cpBeamBody *body, const cpIdx nodeId);

cpBeamNode* cpBeamBodyGetStressForces( const cpBeamBody *body );

cpBeamNode cpBeamBodyGetVertexMass( const cpBeamBody *body, const cpIdx idx);

//void cpBeamBodySetPosition( cpBeamBody *body );

void cpBeamBodySetOrientation( cpBeamBody *body, cpMat2x2 rot );

//void cpBeamBodyReset( cpBeamBody *body );

//void cpBeamBodySetVelocity( cpVect vel, cpBeamBody *body );

void cpBeamBodyAddForce( cpBeamBody *body, const unsigned short idx, cpVect force);

void cpBeamBodyGetCompliance(cpBeamBody *body, const cpIdx nodeId, Matrix3* compliance);



void printBeamVector(const cpVect3 *v, const int size, const char* filename);

void printBeamStiffnessMatrix( const cpBeamBody *body, const Matrix3* mat, const char* filename );


// be careful
void removeDeformedBeam(cpBeamBody *body, const cpFloat cosAngleFracture, const cpFloat areaFractureMinRatio, const cpFloat areaFractureMaxRatio );

void removeStressedBeam(cpBeamBody *body);

void removeBeam(cpBeamBody *body, cpIdx segmIdx);

// not used
void computeBeamStressForces(cpBeamBody *body );

cpVect cpBeamBodyGetCoGvelocity(const cpBeamBody *body);




#endif
