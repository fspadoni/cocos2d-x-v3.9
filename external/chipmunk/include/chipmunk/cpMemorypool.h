//
//  cpMemorypool.h
//  Chipmunk7
//
//  Created by Federico Spadoni on 28/06/14.
//
//

#ifndef Chipmunk7_cpMemorypool_h
#define Chipmunk7_cpMemorypool_h

//#include "chipmunk_private.h"

/*****************************************************************************/
/*                                                                           */
/*  poolinit()   Initialize a pool of memory for allocation of items.        */
/*                                                                           */
/*  This routine initializes the machinery for allocating items.  A `pool'   */
/*  is created whose records have size at least `bytecount'.  Items will be  */
/*  allocated in `itemcount'-item blocks.  Each item is assumed to be a      */
/*  collection of words, and either pointers or floating-point values are    */
/*  assumed to be the "primary" word type.  (The "primary" word type is used */
/*  to determine alignment of items.)  If `alignment' isn't zero, all items  */
/*  will be `alignment'-byte aligned in memory.  `alignment' must be either  */
/*  a multiple or a factor of the primary word size; powers of two are safe. */
/*  `alignment' is normally used to create a few unused bits at the bottom   */
/*  of each item's pointer, in which information may be stored.              */
/*                                                                           */
/*  Don't change this routine unless you understand it.                      */
/*                                                                           */
/*****************************************************************************/

#include "chipmunk_types.h"
#include "chipmunk.h"
#include "cpPoolAllocator.h"

#include <string.h>

//stack allocator config
static const unsigned int StackSize = 64*1024;
static const unsigned int MaxStackChunks = 4*1024;


struct cpMembuffer {
    
    void* data;
    unsigned int mSizeInBytes;
    unsigned int mCount;
    unsigned int mDataSize;
    
    struct cpMembuffer* next;
};

static inline struct cpMembuffer*  allocateBuffer(struct cpMembuffer** blist, unsigned int dataSize, unsigned int count)
{
    struct cpMembuffer* b = (struct cpMembuffer*)allocatePool(sizeof(struct cpMembuffer) );
    
    b->data = allocatePool( count * dataSize);
    b->mDataSize = dataSize;
    b->mCount = count;
    b->mSizeInBytes = dataSize*count;
   
    if ( *blist == NULL )
    {
        *blist = b;
        (*blist)->next = NULL;
        
    }
    else
    {
        b->next = *blist;
        *blist = b;
    }

    return b;
}

static inline void copyBuffer(struct cpMembuffer* dest, struct cpMembuffer* src)
{
    cpAssertHard(dest->mSizeInBytes == src->mSizeInBytes, "copy buffers of different size");
    memcpy(dest->data, src->data, dest->mSizeInBytes);
}

static inline struct cpMembuffer* newBufferCopy(struct cpMembuffer** blist, struct cpMembuffer* src)
{
    
    struct cpMembuffer* dest = (struct cpMembuffer*)allocatePool( sizeof(struct cpMembuffer) );
    
    dest->data = allocatePool( src->mCount * src->mDataSize);
    memcpy( dest->data, src->data, src->mSizeInBytes);
    dest->mDataSize = src->mDataSize;
    dest->mCount = src->mCount;
    dest->mSizeInBytes = src->mSizeInBytes;
    
    if ( *blist == NULL )
    {
        *blist = dest;
        (*blist)->next = NULL;
        
    }
    else
    {
        dest->next = *blist;
        *blist = dest;
    }
    
    return dest;
}

static inline void resetBuffer(struct cpMembuffer* buf)
{
    memset(buf->data, 0, buf->mSizeInBytes);
}

static inline void setBuffer(struct cpMembuffer* buf, int val)
{
    memset(buf->data, val, buf->mSizeInBytes);
}



typedef struct cpStackChunk
{
	char* data;
	unsigned int size;
	cpBool usedMalloc;
} cpStackChunk;


struct cpStackAllocator
{
    unsigned int Size; // = 100 * 1024;
    const unsigned int MaxChunks; // = 32;
    
//    enum {
//        Size = 100 * 1024,
//        MaxChunks = 32
//    } config;
    
	char* data;//[Size];
	unsigned int index;
    
	unsigned int allocation;
	unsigned int maxAllocation;
    
	cpStackChunk* chunks;//[MaxChunks];
	unsigned int chunkCount;
};


static inline cpStackAllocator* newStackAllocator(const unsigned int size, const unsigned int maxChunks)
{
//    struct cpStackAllocator* stack = (struct cpStackAllocator*)cpcalloc( 1, sizeof(struct cpStackAllocator) );
    struct cpStackAllocator* stack = (struct cpStackAllocator*)allocatePool( sizeof(struct cpStackAllocator) );
   
    // init const
    stack->Size = size;
    *(unsigned int *)&stack->MaxChunks = maxChunks;
    
	stack->index = 0;
	stack->allocation = 0;
	stack->maxAllocation = 0;
	stack->chunkCount = 0;
    
//    stack->data = (char*)cpcalloc( 1, size );
    stack->data = (char*)allocatePool( size );
//    stack->chunks = (cpStackChunk*)cpcalloc( maxChunks, sizeof(struct cpStackChunk) );
    stack->chunks = (cpStackChunk*)allocatePool( maxChunks * sizeof(struct cpStackChunk) );
    return stack;
}

static inline void deleteStackAllocator(cpStackAllocator** stack )
{
//    cpfree((*stack)->chunks);
    freePool((*stack)->chunks, (*stack)->MaxChunks * sizeof(struct cpStackChunk));
//    cpfree((*stack)->data);
    freePool((*stack)->data, (*stack)->Size);
//    cpfree(*stack);
    freePool(*stack, sizeof(struct cpStackAllocator));
    *stack = 0;
}



static inline void* allocateStack(cpStackAllocator** stack, unsigned int size)
{
    cpAssertHard( (*stack)->chunkCount < (*stack)->MaxChunks, "stack allocator si full");
    
	cpStackChunk* chunk = (*stack)->chunks + (*stack)->chunkCount;
	chunk->size = size;
	if ((*stack)->index + size > (*stack)->Size)
	{
//		chunk->data = (char*)cpcalloc(1, size);
        chunk->data = (char*)allocatePool( size);
//        (*stack)->Size += size;
		chunk->usedMalloc = cpTrue;
	}
	else
	{
		chunk->data = (*stack)->data + (*stack)->index;
		chunk->usedMalloc = cpFalse;
		(*stack)->index += size;
	}
    
	(*stack)->allocation += size;
	(*stack)->maxAllocation = ((*stack)->maxAllocation > (*stack)->allocation) ? (*stack)->maxAllocation : (*stack)->allocation;
	++(*stack)->chunkCount;
    
    // reset memory
//    memset(chunk->data, 0, chunk->size);
    
	return chunk->data;
}

static inline void freeStack(cpStackAllocator** stack, void* p)
{
    cpAssertHard( (*stack)->chunkCount > 0, "stack is empty");
	cpStackChunk* chunk = (cpStackChunk*)p; // (*stack)->chunks + (*stack)->chunkCount - 1;
//	cpAssertHard(p == chunk, "stack deallocation failed");
	if (chunk->usedMalloc)
	{
//        cpfree(chunk->data);
        freePool(chunk->data, chunk->size );
        
	}
	else
	{
		(*stack)->index -= chunk->size;
	}
	(*stack)->allocation -= chunk->size;
	--(*stack)->chunkCount;
    
	p = NULL;
}


static inline void resetStackAllocator(cpStackAllocator** stack )
{
    
    while ( (*stack)->chunkCount > 0)

    {
        freeStack( stack, (*stack)->chunks + (*stack)->chunkCount - 1 );
        
    }
}

inline unsigned int getMaxStackAllocation(const cpStackAllocator* stack)
{
    return stack->maxAllocation;
}


#endif
