//
//  cpPointTriangleShape.h
//  Chipmunk7
//
//  Created by Federico Spadoni on 14/11/14.
//
//

#ifndef __Chipmunk7__cpPointTriangleShape__
#define __Chipmunk7__cpPointTriangleShape__


cpPointShape * cpPointShapeInit(cpPointShape *shape, cpBody *body, const cpIdx idx, const cpFloat margin, const cpVect* verts, const cpVect* velocities);

/// Initialize a triangle shape.
cpTriangleShape* cpTriangleShapeInit(cpTriangleShape *shape, cpBody *body, const cpTriangle* triang, const cpVect* verts, const cpVect* velocities);

cpBeamPointShape * cpBeamPointShapeInit(cpBeamPointShape *shape, cpBody *body, const cpIdx idx, const cpFloat margin, const cpBeamNode* verts, const cpBeamNode* velocities);

cpBeamSegmentShape* cpBeamSegmentShapeInit(cpBeamSegmentShape *shape, cpBody *body, const cpSegment* seg, const cpBeamNode* nodes, float r, const cpBeamNode* velocities);



cpIdx cpPointShapeGetIndex(cpPointShape *shape);

cpTriangle cpPointShapeGetTriangle(cpTriangleShape *shape);

cpIdx cpBeamPointShapeGetIndex(cpBeamPointShape *shape);

cpSegment cpBeamPointShapeGetSegment(cpBeamSegmentShape *shape);


#endif /* defined(__Chipmunk7__cpPointTriangleShape__) */
