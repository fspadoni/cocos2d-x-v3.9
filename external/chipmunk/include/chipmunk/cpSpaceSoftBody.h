//
//  cpSpaceSoftBody.h
//  Chipmunk7
//
//  Created by Federico Spadoni on 12/07/14.
//
//

#ifndef Chipmunk7_cpSpaceSoftBody_h
#define Chipmunk7_cpSpaceSoftBody_h

typedef struct cpSpaceStats
{
    unsigned long stamps;
    int numNodes;
    int numElements;
    int numArbiters;
    int numContacts;
    int numCollisionTests;
    cpFloat assemble;
    cpFloat solve;
    cpFloat broadphase;
    cpFloat narrowphase;
    cpFloat graphs;
    cpFloat contacts;
    cpFloat time;
    
} cpSpaceStats;


// threading functions
typedef void (*cpSpaceAssembleSoftBodiesFunc)(cpSoftBodyOpt**bodies, int num, const cpFloat dt );
typedef void (*cpSpaceSolveSoftBodiesFunc)(cpSoftBodyOpt**bodies, int num, const cpFloat dt );
typedef void (*cpSpaceUpdateBBShapesFunc)(cpSpaceSoftBody* space, cpSpatialIndex *dynamicShapes);
typedef void (*cpSpaceSolveIslandFunc)(cpContactIsland* islands, int num, const cpFloat dt);
typedef void (*cpSpaceIntegratePositionSoftBodiesFunc)(cpSoftBodyOpt**bodies, int num, const cpFloat dt );

typedef void (*cpSpaceAssembleBeamBodiesFunc)(cpBeamBody**bodies, int num, const cpFloat dt );
typedef void (*cpSpaceSolveBeamBodiesFunc)(cpBeamBody**bodies, int num, const cpFloat dt );
//typedef void (*cpSpaceUpdateBBShapesFunc)(cpSpaceSoftBody* space, cpSpatialIndex *dynamicShapes);
//typedef void (*cpSpaceSolveIslandFunc)(cpContactIsland* islands, int num, const cpFloat dt);
typedef void (*cpSpaceIntegratePositionBeamBodiesFunc)(cpBeamBody**bodies, int num, const cpFloat dt );


/// Allocate a cpSpace.
cpSpaceSoftBody* cpSpaceSoftBodyAlloc(void);
/// Initialize a cpSpace.
cpSpaceSoftBody* cpSpaceSoftBodyInit(cpSpaceSoftBody *space, cpFloat dim, int count);
/// Allocate and initialize a cpSpace.
cpSpaceSoftBody* cpSpaceSoftBodyNew(cpFloat dim, int count);

/// Destroy a cpSpace.
void cpSpaceSoftBodyDestroy(cpSpaceSoftBody *space);
/// Destroy and free a cpSpace.
void cpSpaceSoftBodyFree(cpSpaceSoftBody *space);



void cpSpaceSoftBodyStep(cpSpaceSoftBody *space, cpFloat dt);

void cpSpaceSoftBodyStep2(cpSpaceSoftBody *space, cpFloat dt );

void cpSpaceSoftBodyStep2Stats(cpSpaceSoftBody *space, cpFloat dt, cpSpaceStats* stats );

void cpSpaceSoftBodyStepThredingStats(cpSpaceSoftBody *space, cpFloat dt, cpSpaceStats* stats );


void cpSpaceSolveIsland(cpSpaceSoftBody *space, cpContactIsland *island);


cpSpace* cpGetSpace(cpSpaceSoftBody *space);


/// Add a rigid body to the simulation.
cpSoftBody* cpSpaceAddSoftBody(cpSpaceSoftBody *space, cpSoftBody *body);

/// Add a rigid body to the simulation.
cpSoftBodyOpt* cpSpaceAddSoftBodyOpt(cpSpaceSoftBody *space, cpSoftBodyOpt *body);

/// Add a rigid body to the simulation.
cpBeamBody* cpSpaceAddBeamBody(cpSpaceSoftBody *space, cpBeamBody *body);


/// Remove a rigid body from the simulation.
void cpSpaceRemoveSoftBody(cpSpaceSoftBody *space, cpSoftBody *body);

void cpSpaceRemoveSoftBodyOpt(cpSpaceSoftBody *space, cpSoftBodyOpt *body);

void cpSpaceRemoveBeamBody(cpSpaceSoftBody *space, cpBeamBody *body);


void cpSpaceSoftBodySetGravity(cpSpaceSoftBody *space, cpVect gravity);

cpBool cpSpaceContainsSoftBody(cpSpaceSoftBody *space, cpSoftBody *body);

cpBool cpSpaceContainsSoftBodyOpt(cpSpaceSoftBody *space, cpSoftBodyOpt *body);

cpBool cpSpaceContainsBeamBody(cpSpaceSoftBody *space, cpBeamBody *body);


///// Space/body iterator callback function type.
typedef void (*cpSpaceSoftBodyIteratorFunc)(cpSoftBody *body, void *data);

/// Call @c func for each body in the space.
void cpSpaceEachSoftBody(cpSpaceSoftBody *space, cpSpaceSoftBodyIteratorFunc func, void *data);

///// Space/body iterator callback function type.
typedef void (*cpSpaceSoftBodyOptIteratorFunc)(cpSoftBodyOpt *body, void *data);

typedef void (*cpSpaceBeamBodyIteratorFunc)(cpBeamBody *body, void *data);

/// Call @c func for each body in the space.
void cpSpaceEachSoftBodyOpt(cpSpaceSoftBody *space, cpSpaceSoftBodyOptIteratorFunc func, void *data);

void cpSpaceEachActiveSoftBodyOpt(cpSpaceSoftBody *space, cpSpaceSoftBodyOptIteratorFunc func, void *data);

void cpSpaceEachSleepingSoftBodyOpt(cpSpaceSoftBody *space, cpSpaceSoftBodyOptIteratorFunc func, void *data);


void cpSpaceEachBeamBody(cpSpaceSoftBody *space, cpSpaceBeamBodyIteratorFunc func, void *data);

void cpSpaceEachActiveBeamBody(cpSpaceSoftBody *space, cpSpaceBeamBodyIteratorFunc func, void *data);

void cpSpaceEachSleepingBeamBody(cpSpaceSoftBody *space, cpSpaceBeamBodyIteratorFunc func, void *data);


cpCollisionHandler* cpSpaceGetSoftBodyVsSoftBodyCollisionHandler(cpSpaceSoftBody *space);

cpCollisionHandler* cpSpaceGetRigidBodyVsSoftBodyCollisionHandler(cpSpaceSoftBody *space);



// threading functions callbacks
void cpSpaceSetAssembleSoftBodiesCallback(cpSpaceSoftBody *space, cpSpaceAssembleSoftBodiesFunc func);

void cpSpaceSetSolveSoftBodiesCallback(cpSpaceSoftBody *space, cpSpaceSolveSoftBodiesFunc func);

void cpSpaceSetUpdateBBShapesCallback(cpSpaceSoftBody *space, cpSpaceUpdateBBShapesFunc func);

void cpSpaceSetSolveIslandCallback(cpSpaceSoftBody *space, cpSpaceSolveIslandFunc func);

void cpSpaceSetIntegrateSoftBodiesCallback(cpSpaceSoftBody *space, cpSpaceIntegratePositionSoftBodiesFunc func);

#endif
