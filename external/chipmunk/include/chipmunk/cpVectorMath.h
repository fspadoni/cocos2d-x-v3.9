//
//  cpVectorMath.h
//  Chipmunk7
//
//  Created by Federico Spadoni on 25/06/14.
//
//

#ifndef Chipmunk7_cpVectorMath_h
#define Chipmunk7_cpVectorMath_h

#include "chipmunk_types.h"

#include "chipmunk_fem_types.h"

#include "cpVect.h"

#include <stdio.h>

#if ( (defined _M_IX86) || (defined _M_X64) ||  (defined __i386__) || defined( __x86_64__))
    #define CP_MATH_SSE2 1
#elif (defined _M_ARM || defined __ARM_NEON__)
    #define CP_MATH_NEON 1
#else
    #define CP_MATH_SCALAR
#endif



//typedef struct
//{
//    cpFloat r1;
//    cpFloat r2;
//} // __attribute__ ( (packed))
//mat_col2;
//
//
//typedef struct
//{
//    mat_col2 c1;
//    mat_col2 c2;
//    
//} //__attribute__ ( (packed))
//ne10_mat2x2f_t;   // a 2x2 matrix

static const Matrix2 zeroMatrix2 = {
    .col[0] = { 0.0, 0.0},
    .col[1] = {0.0, 0.0},
};

static const Matrix2 identityMatrix2 = {
    .col[0] = { 1.0, 0.0},
    .col[1] = {0.0, 1.0},
};

static const Matrix3 zeroMatrix3 = {
    .col[0] = {0.0, 0.0, 0.0, 0.0},
    .col[1] = {0.0, 0.0, 0.0, 0.0},
    .col[2] = {0.0, 0.0, 0.0, 0.0},
};
    
static inline cpBool SolveQuadratic(cpFloat a, cpFloat b, cpFloat c, cpFloat* minT, cpFloat* maxT)
{
	if (a == 0.0 && b == 0.0)
	{
		*minT = *maxT = 0.0f;
		return cpTrue;
	}
    
	cpFloat discriminant = b*b - 4.0*a*c;
    
	if (discriminant < 0.0f)
	{
        *minT = *maxT = 0.0f;
		return cpFalse;
	}
    
	// numerical receipes 5.6 (this method ensures numerical accuracy is preserved)
//    cpFloat signb = (b < 0.0f) ? -1.0 : 1.0;
    const cpFloat signb = signbit(b) ? -1.0 : 1.0;
	cpFloat t = -0.5 * (b + signb * cpfsqrt(discriminant) );
	*minT = t / a;
	*maxT = c / t;
    
	if (*minT > *maxT)
	{
        cpFloat temp = *minT;
		*minT = *maxT;
        *maxT = temp;
	}
    
	return cpTrue;
}



static inline void computeStressTensorEigenV(const cpVect3* stressTensor, cpFloat* e1, cpFloat* e2)
{
	// solve the characteristic polynomial
	cpFloat a = 1.0f;
    cpFloat b = -(stressTensor->x + stressTensor->y);
    cpFloat c = stressTensor->x * stressTensor->y - stressTensor->z * stressTensor->z;
    
    SolveQuadratic(a, b, c, e1, e2);
}


/// Subtract two vectors.
static inline void subVect2( cpVect* v1, const cpVect* v2)
{
	v1->x -= v2->x;
    v1->y -= v2->y;
}

//static inline cpFloat innProd( const cpVect* v0, const cpVect* v1, unsigned int size)
//{
//    cpVect temp = {0.0f,0.0f};
//    for (unsigned int i=0; i<size; ++i)
//    {
//        temp.x += v0->x * v1->x;
//        temp.y += v0->y * v1->y;
//        ++v0;
//        ++v1;
//    }
//    return temp.x + temp.y;
//}


static inline void printVect3(const cpVect3* vec, const char* filename)
{
    
    FILE * file = fopen( filename, "w");
    
	if(!file)
        return;
    
    fprintf(file, "%f \n", vec->x );
    fprintf(file, "%f \n", vec->y );
    fprintf(file, "%f \n", vec->z );
    
    fclose(file);
}

static inline void printVect6(const cpVect6* vec, const char* filename)
{
    
    FILE * file = fopen( filename, "w");
    
	if(!file)
        return;
    
    fprintf(file, "%f \n", vec->v[0].x );
    fprintf(file, "%f \n", vec->v[0].y );
    fprintf(file, "%f \n", vec->v[1].x );
    fprintf(file, "%f \n", vec->v[1].y );
    fprintf(file, "%f \n", vec->v[2].x );
    fprintf(file, "%f \n", vec->v[2].y );
    
    fclose(file);
}



static inline cpVect3 cross3(const cpVect3* a, const cpVect3* b )
{
    cpVect3 res = { (a->y*b->z - a->z*b->y), (a->z*b->x - a->x*b->z), (a->x*b->y - a->y*b->x) , 0.0};
    return res;
}

static inline cpFloat dot3( const cpVect3* a, const cpVect3* b )
{
    return a->x*b->x + a->y*b->y + a->z*b->z;
}

/// Returns the length of v.
//static inline cpFloat norm3(const cpVect3* v)
//{
//	return cpfsqrt(dot3(v, v));
//}

static inline Matrix2 inverseMatrix2(const Matrix2* m)
{
    const float invdet = 1.0 / (m->col[0].x*m->col[1].y - m->col[0].y*m->col[1].x);
    
    Matrix2 invm;
    invm.col[0].x =  m->col[1].y * invdet;
    invm.col[0].y = -m->col[0].y * invdet;
    invm.col[1].x = -m->col[1].x * invdet;
    invm.col[1].y =  m->col[0].x * invdet;
    
    return invm;
}


static inline Matrix3 inverseMatrix3(const Matrix3* m)
{
    
    cpVect3 tmp0 = cross3( &m->col[1], &m->col[2]);
    cpVect3 tmp1 = cross3( &m->col[2], &m->col[0]);
    cpVect3 tmp2 = cross3( &m->col[0], &m->col[1]);
    cpFloat detinv = ( 1.0 / dot3( &m->col[2], &tmp2 ) );
    
    Matrix3 inv;
    inv.col[0].x = tmp0.x * detinv;
    inv.col[0].y = tmp1.x * detinv;
    inv.col[0].z = tmp2.x * detinv;
    inv.col[1].x = tmp0.y * detinv;
    inv.col[1].y = tmp1.y * detinv;
    inv.col[1].z = tmp2.y * detinv;
    inv.col[2].x = tmp0.z * detinv;
    inv.col[2].y = tmp1.z * detinv;
    inv.col[2].z = tmp2.z * detinv;
    
    return inv;
    
}

static inline void invMatrix3(Matrix3* m_inv, const Matrix3* m)
{    
    cpVect3 CP_ALIGN(16) tmp0 = cross3( &m->col[1], &m->col[2]);
    cpVect3 CP_ALIGN(16) tmp1 = cross3( &m->col[2], &m->col[0]);
    cpVect3 CP_ALIGN(16) tmp2 = cross3( &m->col[0], &m->col[1]);
    cpFloat detinv = ( 1.0 / dot3( &m->col[2], &tmp2 ) );
    
    m_inv->col[0].x = tmp0.x * detinv;
    m_inv->col[0].y = tmp1.x * detinv;
    m_inv->col[0].z = tmp2.x * detinv;
    m_inv->col[1].x = tmp0.y * detinv;
    m_inv->col[1].y = tmp1.y * detinv;
    m_inv->col[1].z = tmp2.y * detinv;
    m_inv->col[2].x = tmp0.z * detinv;
    m_inv->col[2].y = tmp1.z * detinv;
    m_inv->col[2].z = tmp2.z * detinv;
    
}

static inline cpFloat determinantMat2(const Matrix2* m )
{
    return m->col[0].x * m->col[1].y - m->col[1].x * m->col[0].y;
}

static inline void transposeMat2(Matrix2* t, const Matrix2* m )
{
    t->col[0].x = m->col[0].x;
    t->col[0].y = m->col[1].x;
    t->col[1].x = m->col[0].y;
    t->col[1].y = m->col[1].y;
}

static inline void transposeMat3(Matrix3* t, const Matrix3* m )
{
    t->col[0].x = m->col[0].x;
    t->col[0].y = m->col[1].x;
    t->col[0].z = m->col[2].x;
    t->col[1].x = m->col[0].y;
    t->col[1].y = m->col[1].y;
    t->col[1].z = m->col[2].y;
    t->col[2].x = m->col[0].z;
    t->col[2].y = m->col[1].z;
    t->col[2].z = m->col[2].z;

}


//mt_inv = inv(mt)
static inline void  invMatrix3transpose(Matrix3* mt_inv, const Matrix3* m)
{
    cpVect3 tmp0 = cross3( &m->col[1], &m->col[2]);
    cpVect3 tmp1 = cross3( &m->col[2], &m->col[0]);
    cpVect3 tmp2 = cross3( &m->col[0], &m->col[1]);
    cpFloat detinv = ( 1.0 / dot3( &m->col[2], &tmp2 ) );
    
    mt_inv->col[0].x = tmp0.x * detinv;
    mt_inv->col[0].y = tmp0.y * detinv;
    mt_inv->col[0].z = tmp0.z * detinv;
    mt_inv->col[1].x = tmp1.x * detinv;
    mt_inv->col[1].y = tmp1.y * detinv;
    mt_inv->col[1].z = tmp1.z * detinv;
    mt_inv->col[2].x = tmp2.x * detinv;
    mt_inv->col[2].y = tmp2.y * detinv;
    mt_inv->col[2].z = tmp2.z * detinv;

}

static inline void cholMatrix3(Matrix3* lower, const Matrix3* a)
{
    
    cpFloat l11 = cpfsqrt(a->col[0].x);
    lower->col[0].x = l11;
    lower->col[0].y = a->col[0].y / l11;
    lower->col[0].z = a->col[0].z / l11;
    
    cpFloat l22 = cpfsqrt( a->col[1].y - lower->col[0].y*lower->col[0].y);
    lower->col[1].x = 0;
    lower->col[1].y = l22;
    lower->col[1].z = ( a->col[1].z - lower->col[0].y*lower->col[0].z ) / l22;
    
    lower->col[2].x = 0;
    lower->col[2].y = 0;
    lower->col[2].z = cpfsqrt( a->col[2].z - lower->col[0].z*lower->col[0].z - lower->col[1].z*lower->col[1].z );
    
}

//static inline cpVect multiplyMat2Vect2(const Matrix2* m, const cpVect* v)
//{
//    cpVect res;
//    res.x = m->col[0].x * v->x + m->col[1].x * v->y;
//    res.y = m->col[0].y * v->x + m->col[1].y * v->y;
//    return res;
//}

//static inline void multiplyMat2byMat2(Matrix2* res, const Matrix2* m0, const Matrix2* m1)
//{
//    res->col[0].x = m0->col[0].x * m1->col[0].x + m0->col[1].x * m1->col[0].y;
//    res->col[0].y = m0->col[0].y * m1->col[0].x + m0->col[1].y * m1->col[0].y;
//    res->col[1].x = m0->col[0].x * m1->col[1].x + m0->col[1].x * m1->col[1].y;
//    res->col[1].y = m0->col[0].y * m1->col[1].x + m0->col[1].y * m1->col[1].y;
//}


//static inline cpVect multiplyTranspMat2Vect2(const Matrix2* m, const cpVect* v)
//{
//    cpVect res;
//    res.x = m->col[0].x * v->x + m->col[0].y * v->y;
//    res.y = m->col[1].x * v->x + m->col[1].y * v->y;
//    return res;
//}
//
//static inline void multiplyMat2Vect2andAdd(cpVect* res, const Matrix2* m, const cpVect* v)
//{
//    res->x += m->col[0].x * v->x + m->col[1].x * v->y;
//    res->y += m->col[0].y * v->x + m->col[1].y * v->y;
//}
//
//static inline void multiplyMat2Vect2andSub(cpVect* res, const Matrix2* m, const cpVect* v)
//{
//    res->x -= m->col[0].x * v->x + m->col[1].x * v->y;
//    res->y -= m->col[0].y * v->x + m->col[1].y * v->y;
//}
//
//
//static inline void addMatrix2byelems(Matrix2* res, const Matrix2* m0, const Matrix2* m1)
//{
//    res->col[0] = cpvadd(m0->col[0], m1->col[0]);
//    res->col[1] = cpvadd(m0->col[1], m1->col[1]);
//}

//static inline cpVect multiplyMat2x3byVect3(const cpMat2x3* m23, const cpVect3* v)
//{
//    cpVect res = { m23->col[0].x * v->x + m23->col[1].x * v->y + m23->col[2].x * v->z,
//        m23->col[0].y * v->x + m23->col[1].y * v->y + m23->col[2].y * v->z };
//    return res;
//}
//
//static inline void multiplyMat3Vect3(cpVect3* res, const Matrix3* m, const cpVect3* v)
//{
//    res->x = m->col[0].x * v->x + m->col[1].x * v->y + m->col[2].x * v->z;
//    res->y = m->col[0].y * v->x + m->col[1].y * v->y + m->col[2].y * v->z;
//    res->z = m->col[0].z * v->x + m->col[1].z * v->y + m->col[2].z * v->z;
//}
//
//static inline Matrix2 multiplyMat2Mat2Mat2( const Matrix2* m0, const Matrix2* m1, const Matrix2* m2)
//{
//    // multiply m0 * ( m1 * m2 )
//    Matrix2 res;
//    res.col[0] = multiplyMat2Vect2(m1, &m2->col[0] );
//    res.col[1] = multiplyMat2Vect2(m1, &m2->col[1] );
//    res.col[0] = multiplyMat2Vect2(m0, &res.col[0] );
//    res.col[1] = multiplyMat2Vect2(m0, &res.col[1] );
//    return res;
//}
//
//static inline void addMat2ToMat2( Matrix2* sum, const Matrix2* m)
//{
//    sum->col[0].x += m->col[0].x;
//    sum->col[0].y += m->col[0].y;
//    sum->col[1].x += m->col[1].x;
//    sum->col[1].y += m->col[1].y;
//    
//}


//static inline void multiplyMat3xMat3(Matrix3* res, const Matrix3* m1, const Matrix3* m2)
//{
//    multiplyMat3Vect3( &res->col[0], m1, &m2->col[0] );
//    multiplyMat3Vect3( &res->col[1], m1, &m2->col[1] );
//    multiplyMat3Vect3( &res->col[2], m1, &m2->col[2] );
//}

//static inline void multiplyAddMat3x2byVect2(cpVect3* res, const cpMat3x2* m, const cpVect* v)
//{
//    res->x += m->col[0].x * v->x + m->col[1].x * v->y;
//    res->y += m->col[0].y * v->x + m->col[1].y * v->y;
//    res->z += m->col[0].z * v->x + m->col[1].z * v->y;
//}
//
//static inline void transposeMat3x2MultiplyByVect3(cpVect* res, const cpMat3x2* m, const cpVect3* v)
//{
//    const cpVect3* mrow0 = &m->col[0];
//    const cpVect3* mrow1 = &m->col[1];
//    res->x = mrow0->x * v->x + mrow0->y * v->y + mrow0->z * v->z;
//    res->y = mrow1->x * v->x + mrow1->y * v->y + mrow1->z * v->z;
//}

static inline void printMatrix2(const Matrix2* m, const char* filename)
{
    
    FILE * file = fopen( filename, "w");
    
	if(!file)
        return;
    
    fprintf(file, "%f %f \n", m->col[0].x , m->col[1].x );
    fprintf(file, "%f %f \n", m->col[0].y , m->col[1].y );
    
    fclose(file);
}

static inline void printMatrix3x2(const cpMat3x2* m, const char* filename)
{
    
    FILE * file = fopen( filename, "w");
    
	if(!file)
        return;
    
    fprintf(file, "%f %f \n", m->col[0].x , m->col[1].x );
    fprintf(file, "%f %f \n", m->col[0].y , m->col[1].y );
    fprintf(file, "%f %f \n", m->col[0].z , m->col[1].z );
    
    fclose(file);
}


static inline void printMatrix3(const Matrix3* m, const char* filename)
{
    
    FILE * file = fopen( filename, "w");
    
	if(!file)
        return;
    
    fprintf(file, "%f %f %f \n", m->col[0].x , m->col[1].x, m->col[2].x );
    fprintf(file, "%f %f %f \n", m->col[0].y , m->col[1].y, m->col[2].y );
    fprintf(file, "%f %f %f \n", m->col[0].z , m->col[1].z, m->col[2].z );
    
    fclose(file);
}


//static inline void multiplyMat6xVect6(cpVect6* res, const cpMat6* mat, const cpVect6* vect )
//{
//    res->v[0] = cpvadd( multiplyMat2Vect2(&mat->m[0][0], &vect->v[0]) , cpvadd( multiplyMat2Vect2(&mat->m[1][0], &vect->v[1]) , multiplyMat2Vect2(&mat->m[2][0], &vect->v[2]) ) );
//    res->v[1] = cpvadd( multiplyMat2Vect2(&mat->m[0][1], &vect->v[0]) , cpvadd( multiplyMat2Vect2(&mat->m[1][1], &vect->v[1]) , multiplyMat2Vect2(&mat->m[2][1], &vect->v[2]) ) );
//    res->v[2] = cpvadd( multiplyMat2Vect2(&mat->m[0][2], &vect->v[0]) , cpvadd( multiplyMat2Vect2(&mat->m[1][2], &vect->v[1])  , multiplyMat2Vect2(&mat->m[2][2], &vect->v[2]) ) );
//    
//
//}
//
//static inline cpVect multiplyMat2x2byMat2x3byVect3(const Matrix2* m22, const cpMat2x3* m23, const cpVect3* v3 )
//{
//    cpVect res = multiplyMat2x3byVect3(m23, v3);
//    return multiplyMat2Vect2(m22, &res);
//}



static inline void polarDecomposition(Matrix2* res, const Matrix2* m)
{

//    cpFloat det = determinantMat2(m);
    if ( determinantMat2(m) > 0 )
    {
//        compute Q transpose
        res->col[0].x = m->col[0].x + m->col[1].y;
        res->col[0].y = m->col[0].y - m->col[1].x;
        res->col[1].x = m->col[1].x - m->col[0].y;
        res->col[1].y = m->col[1].y + m->col[0].x;
        
    }
    else{
        // triangle is inverted
        res->col[0].x = m->col[0].x - m->col[1].y;
        res->col[0].y = m->col[0].y + m->col[1].x;
        res->col[1].x = m->col[1].x + m->col[0].y;
        res->col[1].y = m->col[1].y - m->col[0].x;

    }
    
    cpFloat s = 1.0 / cpvlength( res->col[0] );
    
    res->col[0].x *= s;
    res->col[0].y *= s;
    res->col[1].x *= s;
    res->col[1].y *= s;

}

//static inline  void matVecProd(cpVect* res, const Matrix2* mat, const cpVect* vec, int nbRows,
//                          const cpIdx* cols, const cpIdx* rows )
//
//{
//    
//    cpIdx nbcols;
//    
//    while ( nbRows-- > 0 )
//    {
//        nbcols = *(rows+1) - *rows;
//        
//        res->x = 0.0;
//        res->y = 0.0;
//        
//        while (nbcols-- > 0)
//        {
//            multiplyMat2Vect2andAdd(res, mat++, &vec[(*cols++)]);
////            *res += (*mat++) * vec[(*cols++)];
//        }
//        ++res;
//        ++rows;
//    }
//    
//}


#if (defined CP_MATH_SSE2)
#include "cpMathSSE2.h"
#elif (defined CP_MATH_NEON)
#include "cpMathNeon.h"
#elif (defined CP_MATH_SCALAR)
#include "cpMathScalar.h"
#endif

//#define CP_MATH_SIMD (CP_MATH_SSE2 || CP_MATH_NEON )
//#define CP_MATH_SCALAR !CP_MATH_SIMD


#endif
