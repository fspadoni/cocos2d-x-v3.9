//
//  cpMathNeon.h
//  Chipmunk7
//
//  Created by Federico Spadoni on 30/03/15.
//
//

#ifndef Chipmunk7_cpMathNeon_h
#define Chipmunk7_cpMathNeon_h

#include <arm_neon.h>


static inline cpFloat innProd( const cpVect* v0, const cpVect* v1, unsigned int size)
{
    cpVect temp = {0.0f,0.0f};
    for (unsigned int i=0; i<size; ++i)
    {
        temp.x += v0->x * v1->x;
        temp.y += v0->y * v1->y;
        ++v0;
        ++v1;
    }
    return temp.x + temp.y;
}



/// Returns the length of v.
static inline cpFloat norm3(const cpVect3* v)
{
	return cpfsqrt(dot3(v, v));
}


static inline cpVect multiplyMat2Vect2(const Matrix2* m, const cpVect* v)
{
    cpVect res;
    res.x = m->col[0].x * v->x + m->col[1].x * v->y;
    res.y = m->col[0].y * v->x + m->col[1].y * v->y;
    return res;
}

static inline void multiplyMat2byMat2(Matrix2* res, const Matrix2* m0, const Matrix2* m1)
{
    res->col[0].x = m0->col[0].x * m1->col[0].x + m0->col[1].x * m1->col[0].y;
    res->col[0].y = m0->col[0].y * m1->col[0].x + m0->col[1].y * m1->col[0].y;
    res->col[1].x = m0->col[0].x * m1->col[1].x + m0->col[1].x * m1->col[1].y;
    res->col[1].y = m0->col[0].y * m1->col[1].x + m0->col[1].y * m1->col[1].y;
}

static inline cpVect multiplyTranspMat2Vect2(const Matrix2* m, const cpVect* v)
{
    cpVect res;
    res.x = m->col[0].x * v->x + m->col[0].y * v->y;
    res.y = m->col[1].x * v->x + m->col[1].y * v->y;
    return res;
}

static inline void multiplyMat2Vect2andAdd(cpVect* res, const Matrix2* m, const cpVect* v)
{
    res->x += m->col[0].x * v->x + m->col[1].x * v->y;
    res->y += m->col[0].y * v->x + m->col[1].y * v->y;
}

static inline void multiplyMat2Vect2andSub(cpVect* res, const Matrix2* m, const cpVect* v)
{
    res->x -= m->col[0].x * v->x + m->col[1].x * v->y;
    res->y -= m->col[0].y * v->x + m->col[1].y * v->y;
}


static inline void addMatrix2byelems(Matrix2* res, const Matrix2* m0, const Matrix2* m1)
{
    res->col[0] = cpvadd(m0->col[0], m1->col[0]);
    res->col[1] = cpvadd(m0->col[1], m1->col[1]);
}


//res = res + v1
static inline void addVec3byVec3( const cpVect3* res, const cpVect3* v )
{
    vst1q_f32( (float*)res, vaddq_f32( vld1q_f32((float *)res), vld1q_f32((float *) v) ) );
    return;
}


static inline cpVect multiplyMat2x3byVect3(const cpMat2x3* m23, const cpVect3* v)
{
    cpVect res = { m23->col[0].x * v->x + m23->col[1].x * v->y + m23->col[2].x * v->z,
        m23->col[0].y * v->x + m23->col[1].y * v->y + m23->col[2].y * v->z };
    return res;
}

static inline void multiplyMat3Vect3(cpVect3* res, const Matrix3* m, const cpVect3* v)
{
//    res->x = m->col[0].x * v->x + m->col[1].x * v->y + m->col[2].x * v->z;
//    res->y = m->col[0].y * v->x + m->col[1].y * v->y + m->col[2].y * v->z;
//    res->z = m->col[0].z * v->x + m->col[1].z * v->y + m->col[2].z * v->z;
    
    const float32x4_t col0 = vld1q_f32((float *) &m->col[0]);
    const float32x4_t col1 = vld1q_f32((float *) &m->col[1]);
    const float32x4_t col2 = vld1q_f32((float *) &m->col[2]);
    
//    float __attribute__ ((aligned (16))) _vx[4] = { v->x, v->x, v->x, v->x };
    float CP_ALIGN(16) _vx[4] = { v->x, v->x, v->x, v->x };
    const float32x4_t vx = vld1q_f32( _vx );
    
    float CP_ALIGN(16) _vy[4] = { v->y, v->y, v->y, v->y };
    const float32x4_t vy = vld1q_f32( _vy );
    
    float CP_ALIGN(16) _vz[4] = { v->z, v->z, v->z, v->z };
    const float32x4_t vz = vld1q_f32( _vz );
    
    
    vst1q_f32( (float*)res, vaddq_f32( vmulq_f32(col0, vx), vaddq_f32( vmulq_f32(col2, vz), vmulq_f32(col1, vy) ) ) );
    
    return;
}


static inline cpVect3 multiplyMat3xVect3(const Matrix3* m, const cpVect3* v)
{
    //    res->x = m->col[0].x * v->x + m->col[1].x * v->y + m->col[2].x * v->z;
    //    res->y = m->col[0].y * v->x + m->col[1].y * v->y + m->col[2].y * v->z;
    //    res->z = m->col[0].z * v->x + m->col[1].z * v->y + m->col[2].z * v->z;
    
    cpVect3 res;
    
    const float32x4_t col0 = vld1q_f32((float *) &m->col[0]);
    const float32x4_t col1 = vld1q_f32((float *) &m->col[1]);
    const float32x4_t col2 = vld1q_f32((float *) &m->col[2]);
    
    //    float __attribute__ ((aligned (16))) _vx[4] = { v->x, v->x, v->x, v->x };
    float CP_ALIGN(16) _vx[4] = { v->x, v->x, v->x, v->x };
    const float32x4_t vx = vld1q_f32( _vx );
    
    float CP_ALIGN(16) _vy[4] = { v->y, v->y, v->y, v->y };
    const float32x4_t vy = vld1q_f32( _vy );
    
    float CP_ALIGN(16) _vz[4] = { v->z, v->z, v->z, v->z };
    const float32x4_t vz = vld1q_f32( _vz );
    
    
    vst1q_f32( (float*)&res, vaddq_f32( vmulq_f32(col0, vx), vaddq_f32( vmulq_f32(col2, vz), vmulq_f32(col1, vy) ) ) );
    
    return res;
}

static inline Matrix2 multiplyMat2Mat2Mat2( const Matrix2* m0, const Matrix2* m1, const Matrix2* m2)
{
    // multiply m0 * ( m1 * m2 )
    Matrix2 res;
    res.col[0] = multiplyMat2Vect2(m1, &m2->col[0] );
    res.col[1] = multiplyMat2Vect2(m1, &m2->col[1] );
    res.col[0] = multiplyMat2Vect2(m0, &res.col[0] );
    res.col[1] = multiplyMat2Vect2(m0, &res.col[1] );
    return res;
}

static inline void addMat2ToMat2( Matrix2* sum, const Matrix2* m)
{
//    sum->col[0].x += m->col[0].x;
//    sum->col[0].y += m->col[0].y;
//    sum->col[1].x += m->col[1].x;
//    sum->col[1].y += m->col[1].y;
    
    vst1q_f32( (float*)sum, vaddq_f32( vld1q_f32((float *)sum), vld1q_f32((float *) m) ) );
    
}




static inline void multiplyMat3xMat3(Matrix3* res, const Matrix3* m1, const Matrix3* m2)
{
    multiplyMat3Vect3( &res->col[0], m1, &m2->col[0] );
    multiplyMat3Vect3( &res->col[1], m1, &m2->col[1] );
    multiplyMat3Vect3( &res->col[2], m1, &m2->col[2] );
    
    //    res->col[0].x = m1->col[0].x * m2->col[0].x + m1->col[1].x * m2->col[0].y + m1->col[2].x * m2->col[0].z;
    //    res->col[0].y = m1->col[0].y * m2->col[0].x + m1->col[1].y * m2->col[0].y + m1->col[2].y * m2->col[0].z;
    //    res->col[0].z = m1->col[0].z * m2->col[0].x + m1->col[1].z * m2->col[0].y + m1->col[2].z * m2->col[0].z;
    //
    //    res->col[1].x = m1->col[0].x * m2->col[1].x + m1->col[1].x * m2->col[1].y + m1->col[2].x * m2->col[1].z;
    //    res->col[1].y = m1->col[0].y * m2->col[1].x + m1->col[1].y * m2->col[1].y + m1->col[2].y * m2->col[1].z;
    //    res->col[1].z = m1->col[0].z * m2->col[1].x + m1->col[1].z * m2->col[1].y + m1->col[2].z * m2->col[1].z;
    //
    //    res->col[2].x = m1->col[0].x * m2->col[2].x + m1->col[1].x * m2->col[2].y + m1->col[2].x * m2->col[2].z;
    //    res->col[2].y = m1->col[0].y * m2->col[2].x + m1->col[1].y * m2->col[2].y + m1->col[2].y * m2->col[2].z;
    //    res->col[2].z = m1->col[0].z * m2->col[2].x + m1->col[1].z * m2->col[2].y + m1->col[2].z * m2->col[2].z;
    
}



static inline void multiplyAddMat3x2byVect2(cpVect3* res, const cpMat3x2* m, const cpVect* v)
{
//    res->x += m->col[0].x * v->x + m->col[1].x * v->y;
//    res->y += m->col[0].y * v->x + m->col[1].y * v->y;
//    res->z += m->col[0].z * v->x + m->col[1].z * v->y;
    
    const float32x4_t col0 = vld1q_f32((float *) &m->col[0]);
    const float32x4_t col1 = vld1q_f32((float *) &m->col[1]);
    float32x4_t mRes = vld1q_f32((float *) res);
    
    float CP_ALIGN(16) _vx[4] = { v->x, v->x, v->x, v->x };
    const float32x4_t vx = vld1q_f32( _vx );
    
    float CP_ALIGN(16) _vy[4] = { v->y, v->y, v->y, v->y };
    const float32x4_t vy = vld1q_f32( _vy );
    
    vst1q_f32( (float*)res, vaddq_f32( mRes, vaddq_f32( vmulq_f32(col0, vx), vmulq_f32(col1, vy) ) ) );
    
    return;
}

static inline void transposeMat3x2MultiplyByVect3(cpVect* res, const cpMat3x2* m, const cpVect3* v)
{
    const cpVect3* mrow0 = &m->col[0];
    const cpVect3* mrow1 = &m->col[1];
    res->x = mrow0->x * v->x + mrow0->y * v->y + mrow0->z * v->z;
    res->y = mrow1->x * v->x + mrow1->y * v->y + mrow1->z * v->z;
}

static inline void multiplyMat6xVect6(cpVect6* res, const cpMat6* mat, const cpVect6* vect )
{
    res->v[0] = cpvadd( multiplyMat2Vect2(&mat->m[0][0], &vect->v[0]) , cpvadd( multiplyMat2Vect2(&mat->m[1][0], &vect->v[1]) , multiplyMat2Vect2(&mat->m[2][0], &vect->v[2]) ) );
    res->v[1] = cpvadd( multiplyMat2Vect2(&mat->m[0][1], &vect->v[0]) , cpvadd( multiplyMat2Vect2(&mat->m[1][1], &vect->v[1]) , multiplyMat2Vect2(&mat->m[2][1], &vect->v[2]) ) );
    res->v[2] = cpvadd( multiplyMat2Vect2(&mat->m[0][2], &vect->v[0]) , cpvadd( multiplyMat2Vect2(&mat->m[1][2], &vect->v[1])  , multiplyMat2Vect2(&mat->m[2][2], &vect->v[2]) ) );
    
    
}

static inline cpVect multiplyMat2x2byMat2x3byVect3(const Matrix2* m22, const cpMat2x3* m23, const cpVect3* v3 )
{
    cpVect res = multiplyMat2x3byVect3(m23, v3);
    return multiplyMat2Vect2(m22, &res);
}


static inline  void matVecProd(cpVect* res, const Matrix2* mat, const cpVect* vec, int nbRows,
                               const cpIdx* cols, const cpIdx* rows )

{
    
    cpIdx nbcols;
    
    while ( nbRows-- > 0 )
    {
        nbcols = *(rows+1) - *rows;
        
        res->x = 0.0;
        res->y = 0.0;
        
        while (nbcols-- > 0)
        {
            multiplyMat2Vect2andAdd(res, mat++, &vec[(*cols++)]);
            //            *res += (*mat++) * vec[(*cols++)];
        }
        ++res;
        ++rows;
    }
    
}


//noen from here

static inline void multiplyElemByElem( const cpVect* v0, const cpVect* v1, cpVect* res, unsigned int size)
{
    
    unsigned int numIter = (size + 1) / 2;
    
    while ( numIter-- )
    {
        vst1q_f32( (float*)res, vmulq_f32( vld1q_f32((float *) v0), vld1q_f32((float *) v1) ) );
        v0 += 2;
        v1 += 2;
        res += 2;
    }
    
    return;
}

//res = v0 + v1
static inline void multiplyElemByElem3( const cpVect3* v0, const cpVect3* v1, cpVect3* res, unsigned int size)
{
    
    while ( size-- )
    {
        vst1q_f32( (float*)res, vmulq_f32( vld1q_f32((float *) v0), vld1q_f32((float *) v1) ) );
        ++v0;
        ++v1;
        ++res;
    }
    
    return;
}

//res += v0 * v1
static inline void multiplyElemByElemAddElem( const cpVect* v0, const cpVect* v1, cpVect* res, unsigned int size)
{
    
    unsigned int numIter = (size + 1) / 2;
    
    while ( numIter-- )
    {
        vst1q_f32( (float*)res, vmlaq_f32( vld1q_f32((float *) res), vld1q_f32((float *) v0), vld1q_f32((float *) v1) ) );
        
        v0 += 2;
        v1 += 2;
        res += 2;
    }

    return;
}

//res = a * v0
static inline void multiplyElemByScalar( const cpVect* v0, const float a, cpVect* res, unsigned int size)
{
    
    unsigned int numIter = (size + 1) / 2;
    
    float CP_ALIGN(16) data[4] = { a, a, a, a };
    const float32x4_t scalar = vld1q_f32(data);
    
    while ( numIter-- )
    {
        vst1q_f32( (float*)res, vmulq_f32( vld1q_f32((float *) v0), scalar) );
        v0 += 2;
        res += 2;
    }
    
    return;
}


//res = a * v0
static inline void multiplyElemByScalar3( const cpVect3* v0, const float a, cpVect3* res, unsigned int size)
{
    //    unsigned int numIter = (size + 1) / 2;
    
    float CP_ALIGN(16) data[4] = { a, a, a, a };
    const float32x4_t scalar = vld1q_f32(data);
    
    while ( size-- )
    {
        vst1q_f32( (float*)res, vmulq_f32( vld1q_f32((float *) v0), scalar) );
        ++v0;
        ++res;
    }
    
    return;
}


//res = a * v0 + v1
static inline void multiplyElemByScalarAddElem(const cpVect* v0, const cpVect* a, const cpVect* v1, cpVect* res, unsigned int size)
{
    unsigned int numIter = (size + 1) / 2;
    
    float CP_ALIGN(16) data[4] = { a->x, a->y, a->x, a->y };
    const float32x4_t scalar = vld1q_f32(data);
    
    while ( numIter-- )
    {
//        vst1q_f32( (float*)res, vaddq_f32( vmulq_f32( vld1q_f32((float *) v0), scalar), vld1q_f32((float *) v1) ) );
//        test mult and add
        vst1q_f32( (float*)res, vmlaq_f32( vld1q_f32((float *) v1), vld1q_f32((float *) v0), scalar ) );
        
        v0 += 2;
        v1 += 2;
        res += 2;
    }
    
    return;
}

//res = v0 + v1
static inline void addElemByElem( const cpVect* v0, const cpVect* v1, cpVect* res, unsigned int size)
{
    unsigned int numIter = (size + 1) / 2;
    
    while ( numIter-- )
    {
        vst1q_f32( (float*)res, vaddq_f32(vld1q_f32((float *) v0), vld1q_f32((float *) v1)) );
        v0 += 2;
        v1 += 2;
        res += 2;
    }

    return;
}

//res = v0 + v1
static inline void addElemByElem3( const cpVect3* v0, const cpVect3* v1, cpVect3* res, unsigned int size)
{
    
    while ( size-- )
    {
        vst1q_f32( (float*)res, vaddq_f32(vld1q_f32((float *) v0), vld1q_f32((float *) v1)) );
        ++v0;
        ++v1;
        ++res;
    }
    
    return;
}

//res = v0 - v1
static inline void subElemByElem( const cpVect* v0, const cpVect* v1, cpVect* res, unsigned int size)
{
    unsigned int numIter = (size + 1) / 2;
    
    while ( numIter-- )
    {
        vst1q_f32( (float*)res, vsubq_f32(vld1q_f32((float *) v0), vld1q_f32((float *) v1)) );
        v0 += 2;
        v1 += 2;
        res += 2;
    }

    return;
}

//res = v0 - v1
static inline void subElemByElem3( const cpVect3* v0, const cpVect3* v1, cpVect3* res, unsigned int size)
{
    
    while ( size-- )
    {
        vst1q_f32( (float*)res, vsubq_f32(vld1q_f32((float *) v0), vld1q_f32((float *) v1)) );
        ++v0;
        ++v1;
        ++res;
    }
    
    return;
}

static inline cpVect3 subVec3(const cpVect3* v0, const cpVect3* v1)
{
    //    cpVect3 res = { v0->x - v1->x, v0->y - v1->y, v0->z - v1->z, 0};
    
    cpVect3 CP_ALIGN(16) res;
    
    vst1q_f32( (float*)&res, vsubq_f32( vld1q_f32((float *)v0), vld1q_f32((float *)v1) ) );
    
    return res;
}

static inline void subMatrix3byelems(Matrix3* res, const Matrix3* m0, const Matrix3* m1)
{
    //    res->col[0] = subVec3( &m0->col[0], &m1->col[0]);
    //    res->col[1] = subVec3( &m0->col[1], &m1->col[1]);
    //    res->col[2] = subVec3( &m0->col[2], &m1->col[2]);
    
    vst1q_f32( (float*)&res->col[0], vsubq_f32( vld1q_f32((float *)&m0->col[0]), vld1q_f32((float *)&m1->col[0])) );
    vst1q_f32( (float*)&res->col[1], vsubq_f32( vld1q_f32((float *)&m0->col[1]), vld1q_f32((float *)&m1->col[1])) );
    vst1q_f32( (float*)&res->col[2], vsubq_f32( vld1q_f32((float *)&m0->col[2]), vld1q_f32((float *)&m1->col[2])) );
    
}


static inline void multiplyMat2Mat2Mat2AddToMat2(Matrix2* sum, const Matrix2* m0, const Matrix2* m1, const Matrix2* m2)
{
    
    //    multiply m0 * ( m1 * m2 )
    const float32x4_t vm0 = vld1q_f32((float *) m0);
    const float32x4_t vm1 = vld1q_f32((float *) m1);
    const float32x4_t vm2 = vld1q_f32((float *) m2);
    float32x4_t vmsum = vld1q_f32((float *) sum);
    
    
    float32x4_t t1 = __builtin_shuffle(vm1, vm1, (uint32x4_t){0, 1, 0, 1} );   // t1 = [a11, a21, a11, a21]
    float32x4_t t2 = __builtin_shuffle( vm2, vm2, (uint32x4_t){0, 0, 2, 2} );  // t2 = [b11, b11, b12, b12]
    float32x4_t t3 = __builtin_shuffle(vm1, vm1, (uint32x4_t){2, 3, 2, 3} );   // t3 = [a12, a22, a12, a22]
    float32x4_t t4 = __builtin_shuffle( vm2, vm2, (uint32x4_t){1, 1, 3, 3} ); // t4 = [b21, b21, b22, b22]
//    t1 = [a11 * b11, a21 * b11, a11 * b12, a21 * b12]
//    t2 = [a12 * b21, a22 * b21, a12 * b22, a22 * b22]
    

    // vmlaq_f32(a,b,c) = a+ b*c
    //  vmlaq_f32( (t1*t2), t3, t4 )
    float32x4_t tempRes = vmlaq_f32( vmulq_f32(t1, t2), t3, t4);
//    float32x4_t tempRes = vaddq_f32( vmulq_f32(t1, t2), vmulq_f32(t3, t4) );  // the result
    
    t1 = __builtin_shuffle(vm0, vm0, (uint32x4_t){0, 1, 0, 1} );   // t1 = [a11, a21, a11, a21]
    t2 = __builtin_shuffle( tempRes, tempRes, (uint32x4_t){0, 0, 2, 2} );  // t2 = [b11, b11, b12, b12]
    t3 = __builtin_shuffle(vm0, vm0, (uint32x4_t){2, 3, 2, 3} );   // t3 = [a12, a22, a12, a22]
    t4 = __builtin_shuffle( tempRes, tempRes, (uint32x4_t){1, 1, 3, 3} ); // t4 = [b21, b21, b22, b22]

    
//    add to sum
// vmlaq_f32(a,b,c) = a+ b*c
//  vmlaq_f32( (t1*t2), t3, t4 )
    vmsum = vaddq_f32( vmsum, vmlaq_f32( vmulq_f32(t1, t2), t3, t4) );
//    vmsum = vaddq_f32( vmsum, vaddq_f32( vmulq_f32(t1, t2), vmulq_f32(t3, t4) ) );
    
    vst1q_f32( (float*)sum, vmsum );
    
}




// res = m0*m1*m2
static inline void multiplyMat3Mat3Mat3( Matrix3* res, const Matrix3* m0, const Matrix3* m1, const Matrix3* m2)
{
    // multiply m0 * ( m1 * m2 )
//    Matrix3 res;
    
    multiplyMat3Vect3( &res->col[0], m1, &m2->col[0] );
    multiplyMat3Vect3( &res->col[1], m1, &m2->col[1] );
    multiplyMat3Vect3( &res->col[2], m1, &m2->col[2] );
    
    multiplyMat3Vect3( &res->col[0], m0, &res->col[0] );
    multiplyMat3Vect3( &res->col[1], m0, &res->col[1] );
    multiplyMat3Vect3( &res->col[2], m0, &res->col[2] );
    
    
//    res.col[0] = multiplyMat3xVect3(m1, &m2->col[0] );
//    res.col[1] = multiplyMat3xVect3(m1, &m2->col[1] );
//    res.col[2] = multiplyMat3xVect3(m1, &m2->col[2] );
//    
//    res.col[0] = multiplyMat3xVect3(m0, &res.col[0] );
//    res.col[1] = multiplyMat3xVect3(m0, &res.col[1] );
//    res.col[2] = multiplyMat3xVect3(m0, &res.col[2] );
    
    return;
}


//res += M*scalar
static inline void multiplyMat3byScalarAndAdd(Matrix3* res, Matrix3* m, float s)
{
    float CP_ALIGN(16) data[4] = { s, s, s, 0.0 };
    const float32x4_t scalar = vld1q_f32(data);

    
    vst1q_f32((float*)&res->col[0],vmlaq_f32( vld1q_f32((float *)&res->col[0]), vld1q_f32((float *)&m->col[0]), scalar) );
    vst1q_f32((float*)&res->col[1],vmlaq_f32( vld1q_f32((float *)&res->col[1]), vld1q_f32((float *)&m->col[1]), scalar) );
    vst1q_f32((float*)&res->col[2],vmlaq_f32( vld1q_f32((float *)&res->col[2]), vld1q_f32((float *)&m->col[2]), scalar) );
    
}

static inline void addMat3ToMat3( Matrix3* sum, const Matrix3* m)
{
    vst1q_f32( (float*)&sum->col[0], vaddq_f32(vld1q_f32((float *)&sum->col[0]), vld1q_f32((float *)&m->col[0])) );
    vst1q_f32( (float*)&sum->col[1], vaddq_f32(vld1q_f32((float *)&sum->col[1]), vld1q_f32((float *)&m->col[1])) );
    vst1q_f32( (float*)&sum->col[2], vaddq_f32(vld1q_f32((float *)&sum->col[2]), vld1q_f32((float *)&m->col[2])) );
    
//    sum->col[0].x += m->col[0].x;
//    sum->col[0].y += m->col[0].y;
//    sum->col[0].z += m->col[0].z;
//    
//    sum->col[1].x += m->col[1].x;
//    sum->col[1].y += m->col[1].y;
//    sum->col[1].z += m->col[1].z;
//    
//    sum->col[2].x += m->col[2].x;
//    sum->col[2].y += m->col[2].y;
//    sum->col[2].z += m->col[2].z;
}

static inline void multiplyMat3byScalar(Matrix3* m, float s)
{
    float CP_ALIGN(16) data[4] = { s, s, s, 0.0 };
    const float32x4_t scalar = vld1q_f32(data);
    
    vst1q_f32( (float*)&m->col[0], vmulq_f32( vld1q_f32((float *)&m->col[0]), scalar ) );
    vst1q_f32( (float*)&m->col[1], vmulq_f32( vld1q_f32((float *)&m->col[1]), scalar ) );
    vst1q_f32( (float*)&m->col[2], vmulq_f32( vld1q_f32((float *)&m->col[2]), scalar ) );
}

static inline void multiplyMat3Vect3andAdd(cpVect3* res, const Matrix3* m, const cpVect3* v)
{
    
    const float32x4_t col0 = vld1q_f32((float *) &m->col[0]);
    const float32x4_t col1 = vld1q_f32((float *) &m->col[1]);
    const float32x4_t col2 = vld1q_f32((float *) &m->col[2]);
    
    float CP_ALIGN(16) _mvx[4] = { v->x, v->x, v->x, v->x };
    const float32x4_t mvx = vld1q_f32(_mvx);

    float CP_ALIGN(16) _mvy[4] = { v->y, v->y, v->y, v->y };
    const float32x4_t mvy = vld1q_f32(_mvy);
    
    float CP_ALIGN(16) _mvz[4] = { v->z, v->z, v->z, v->z };
    const float32x4_t mvz = vld1q_f32(_mvz);
    
    
    
    vst1q_f32( (float*)res,  vaddq_f32( vld1q_f32((float *) res),
                                           vaddq_f32( vmulq_f32(col0, mvx), vaddq_f32( vmulq_f32(col1, mvy) ,  vmulq_f32(col2, mvz)) ) ) );
    return;
}

static inline void multiplyMat3Vect3andSub(cpVect3* res, const Matrix3* m, const cpVect3* v)
{
    const float32x4_t col0 = vld1q_f32((float *) &m->col[0]);
    const float32x4_t col1 = vld1q_f32((float *) &m->col[1]);
    const float32x4_t col2 = vld1q_f32((float *) &m->col[2]);
    
    float CP_ALIGN(16) _mvx[4] = { v->x, v->x, v->x, v->x };
    const float32x4_t mvx = vld1q_f32(_mvx);
    
    float CP_ALIGN(16) _mvy[4] = { v->y, v->y, v->y, v->y };
    const float32x4_t mvy = vld1q_f32(_mvy);
    
    float CP_ALIGN(16) _mvz[4] = { v->z, v->z, v->z, v->z };
    const float32x4_t mvz = vld1q_f32(_mvz);
    
    
    
    vst1q_f32( (float*)res,  vsubq_f32( vld1q_f32((float *) res),
                                       vaddq_f32( vmulq_f32(col0, mvx), vaddq_f32( vmulq_f32(col1, mvy) ,  vmulq_f32(col2, mvz)) ) ) );
    return;
}

// res = m1*m2t
static inline void multiplyMat3xMat3Tranpose(Matrix3* res, const Matrix3* m1,const Matrix3* m2)
{
    Matrix3 CP_ALIGN(16) m2t;
    transposeMat3( &m2t, m2);
    
    multiplyMat3Vect3( &res->col[0], m1, &m2t.col[0] );
    multiplyMat3Vect3( &res->col[1], m1, &m2t.col[1] );
    multiplyMat3Vect3( &res->col[2], m1, &m2t.col[2] );
}

static inline void multiplyMat3Mat3Mat3AddToMat3(Matrix3* sum, const Matrix3* m0, const Matrix3* m1, const Matrix3* m2)
{
    Matrix3 CP_ALIGN(16) temp;
    
    multiplyMat3Mat3Mat3( &temp, m0, m1, m2);
    
    addMat3ToMat3( sum, &temp );
    
}

#endif
