//
//  CCPhysicsSoftBody.h
//  cocos2d_libs
//
//  Created by Federico Spadoni on 11/11/14.
//
//

#ifndef cocos2d_libs_CCPhysicsSoftBody_h
#define cocos2d_libs_CCPhysicsSoftBody_h


#include "physics/CCPhysicsObject.h"
#include "base/ccTypes.h"
#include "physics-utils/CCMemoryBuffer.h"
#include "physics/CCTriangularMesh.h"
#include "physics-utils/CCPageMemoryPool.h"
#include "math/Vec2.h"



struct cpSoftBodyShapeOpt;
struct cpSoftBodyOpt;
struct cpVect;
//struct cpIdx;
struct cpArbiter;
struct cpBody;


NS_CC_BEGIN


class PhysicsWorld;
class SoftBodyMaterial;
class SoftBodyDrawNode;

struct V2F_T2F;
struct V2F_C4F;

template <typename T> class BufferIterator;


class CC_DLL PhysicsSoftBody : public PhysicsObject
{
public:
    
    static PhysicsSoftBody* createFromTriangularMesh(TriangularMesh* mesh, SoftBodyMaterial* material);
    
    static PhysicsSoftBody* create(const unsigned int numNodes, const unsigned int numElems, const unsigned int graphSize, const unsigned int numMaterials );

    static PhysicsSoftBody* clone(const PhysicsSoftBody* other);
    
    void destroy();
    
    void setData(void* data);
    
    void setDrawNode(SoftBodyDrawNode* drawNode) {_drawNode = drawNode;}
    
    SoftBodyDrawNode* getDrawNode()  { return _drawNode;}

    
    void fillDisplayBuffer(V2F_T2F* vertexBuffer);
    
    void fillDisplayWireFrameBuffer(V2F_C4F* vertexBuffer);
    
    void fillDisplayWireFrameBuffer(V2F_C4F_T2F* vertexBuffer);
    
    
    /** get the sprite the body set to. */
//    inline const Node* getNode() const { return _node; }
//    inline void setNode( Node* node) { _node = node; }
    
    /** remove the body from the world it added to */
    void removeFromWorld();

    inline bool isEnabled() const { return _enabled; }
    /**
     * set the enable value.
     * if the body it isn't enabled, it will not has simulation by world
     */
    void setEnable(bool enable);

    /** Get the body's tag. */
    inline int getTag() const { return _tag; }
    
    /** set the body's tag. */
    inline void setTag(int tag) { _tag = tag; }
    
    
    inline SoftBodyMaterial* getMaterials() const { return _materials; }
    
    inline TriangularMesh* getTriangularMesh() const { return _mesh; }
    
    cpSoftBodyShapeOpt* getSoftBodyShapeOpt() const { return _shape; }
    
    cpSoftBodyOpt* getSoftBodyOpt() const { return _softbody; }
    
    
    void setPlasticity(bool isPlastic);
    void setBrakable(bool isBrakable);
    

    unsigned int getNumActiveTriangles(void) const;
    
    const cpVect* getNodes() const;
    const unsigned short* getTriangles() const;
    unsigned short getFirstActiveTriangleIdx() const;
    const unsigned short* getActiveTriangles() const;
    
    unsigned int getNumConstrainedNodes(void) const;
    const unsigned short* getConstrainedNodes() const;
    
    
    
    void setPosition(const Vec2& pos);
    
    Vec2 getPosition(void);
    
    
    void translate(const Vec2& pos);
    void translate(const float x, const float y);
    
    void rotate(const float a);

    void setVelocity(const Vec2& vel);
    
    void setVelocity(const float x, const float y);
    
    void setNodeVelocity(const Vec2& vel, unsigned short nodeId);
    
    void setVelocityX(const float vx);
    
    void setVelocityY(const float vy);
    
    void addVelocity(const Vec2& vel);
    
    void addVelocity(const float x, const float y);
    
    void setAngularVelocity(const float avel);
    
    
    void setGravity(const Vec2& gravity);
    
    void setGravity(const float x, const float y);

//    collision filter
    void setCollisionFilter(unsigned int category, unsigned int mask = ~(unsigned int)0u/*CP_ALL_CATEGORIES*/ );
    
    void setPointCollisionFilter(unsigned short nodeId, unsigned int category, unsigned int mask = ~(unsigned int)0u/*CP_ALL_CATEGORIES*/);
    
    void setTriangleCollisionFilter(unsigned short triagleId, unsigned int category, unsigned int mask = ~(unsigned int)0u/*CP_ALL_CATEGORIES*/);
    
//    void setSleepCallback(const std::function<void(PhysicsSoftBody*)>& callback ) { _sleepBeginCallback = callback; }
//    
//    void setBreakElementCallback(const std::function<void(PhysicsSoftBody*, const unsigned short)>& callback ) { _breakElementCallback = callback; }
//    
//    void setBreakAllCallback(const std::function<void(PhysicsSoftBody*)>& callback ) { _breakAllCallback = callback; }
    
    void notifyBreakElement(bool active);
    
    void notifyBreak(bool active);
    
    void notifySleep(bool active);
    
    void notifyBeginCollision(bool active);
    
    void notifySeparateCollision(bool active);
    
    
    
    virtual void sleepBegin() {}
    
    virtual void breakElement(const unsigned short index) {}
    
    virtual void breakAllElement() {}
    
    virtual void collisionBegin( cpBody* collidingBody, cpArbiter* arb ) {}
    
    virtual void collisionSeparate(cpBody* collidingBody, cpArbiter* arb) {}
    
    
    virtual Vec3 getActionPosition(void) override;
    
    virtual void setActionPosition(const Vec3& position) override;
    
    virtual Vec3 getActionRotation(void) override;
    
    virtual void setActionRotation(const Vec3& rotation) override;
    
    virtual void stopAction() override;
    
    
//    inline void setSleepingCallback(const Vec2& pos) { cpSoftBodyOptSetSleepingCallback(_softbody, sleepingBody)
//    template<typename T, PhysicsSoftBody* softbody = this>
//    class TriangleIterator
//    {
//    public:
//        const T* begin() const;
//        const T* end() const;
//
//    private:
//        cpIdx* activeTriangle;
//        cpIdx* firstTriangle;
//    };
    
protected:
    
    PhysicsSoftBody();
    virtual ~PhysicsSoftBody();
    
    virtual bool init(TriangularMesh* mesh, SoftBodyMaterial* material);
    
    virtual bool init(const unsigned int numNodes, const unsigned int numElems, const unsigned int graphSize, const unsigned int numMaterials);

    bool copy(const PhysicsSoftBody* other);


//    PhysicsSoftBody& operator= (const PhysicsSoftBody& other);
 
private:
    
    PoolBufferSharedPtr<unsigned char> _data;
    
    unsigned int _numNodes;
    unsigned int _numElems;
    unsigned int _graphSize;
    unsigned int _numMaterials;
    
    SoftBodyMaterial* _materials;
    TriangularMesh* _mesh;
    
    cpSoftBodyShapeOpt* _shape;
    cpSoftBodyOpt* _softbody;
    
//    Node* _node;
    SoftBodyDrawNode* _drawNode;
    bool _enabled;
    
    Color4F _color;
    
    Vec2 _initialCoG;
    float _currentRotation;
    
    int _tag;
    
    bool _destoryMark;
    
//    std::function<void(PhysicsSoftBody*)> _sleepBeginCallback;
//    std::function<void(PhysicsSoftBody*, const unsigned short)> _breakElementCallback;
//    std::function<void(PhysicsSoftBody*)> _breakAllCallback;
    
    // all friends class
    friend class PhysicsWorld;
//    friend class PhysicsWorldInfo;
    friend class PhysicsSoftBodyCallback;
    
    template<typename T>
    friend class BufferIterator;
    
};


//class created only to make the code easy to understand
//completley unsafe. use caution.
template <class Type>
class TriangleBufferIterator
{
public:
    TriangleBufferIterator(int size, const Type first, const Type* next)
    : _size(size)
    , _next(*next)
    , _first(first)
    , _counter(size)
    , _iterator(next + first)
    {
    }
    
    template <typename T>
    inline T* begin(T* ptr)
    {
        return (_size > 0 ) ? ptr + _first : nullptr;
    }
    
    template <typename T>
    inline T* getNext(T* ptr)
    {
        return ptr + *_iterator;
    }
    
    inline int next(void)
    {
        CCASSERT(_counter>0, "TriangleBufferIterator reading out of boundary");
        _iterator += *_iterator;
        return --_counter;
    }
    
    inline void reset(void)
    {
        _counter = _size;
        _iterator = &_next + _first;
    }
    
    inline const TriangleBufferIterator& operator++()
    {
        CCASSERT(_counter>0, "TriangleBufferIterator reading out of boundary");
        _iterator += *_iterator;
        return *this;
    }
    
    inline void operator++(int)
    {
        CCASSERT(_counter>0, "TriangleBufferIterator reading out of boundary");
        _iterator += *_iterator;
        return;
    }
    
private:
    
    const int _size;
    const Type _next;
    const Type _first;
    int _counter;
    const Type* _iterator;
};



//template <typename T>
//class Ptr
//{
//    template <typename T>
//    class Iterator
//    {
//    public:
//        Iterator(T* current)
//        : _current(current)
//        ,
//        {}
//        
//        inline const Iterator<T>& operator++()
//        {
//            _current = current->next; return *this;
//        }
//        
//        inline T operator*()
//        {
//            return *_current;
//        }
//        
//        inline T* operator ->()
//        {
//            return _current;
//        }
//        
//    private:
//        T* _current;
//        const cpIdx* _next;
//    };
//    
//    typedef PtrIterator<T>  iterator;
//
// 
//    inline iterator begin()
//    {
//        return _data + _first;
//    }
//
//
//    
//
//    
//private:
//    T* _data;
//    
//    const int& _size;
//    const cpIdx& _next;
//    const cpIdx _first;
//    int _counter;
//    const cpIdx* _iterator;
//};


NS_CC_END



#endif
