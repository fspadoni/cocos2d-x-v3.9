//
//  CCPhysicsBeamBody.inl
//  cocos2d_libs
//
//  Created by Federico Spadoni on 06/10/16.
//
//

//#ifndef __cocos2d_libs__CCPhysicsBeamBody_inl__
//#define __cocos2d_libs__CCPhysicsBeamBody_inl__

#include "physics-utils/CCPhysicsHelper.h"


NS_CC_BEGIN


unsigned int PhysicsBeamBody::getNumActiveSegments(void) const {return cpBeamShapeGetNumActiveSegments(_shape);}

unsigned int PhysicsBeamBody::getNumNodes(void) const {return cpBeamShapeGetNumVerts(_shape);}

const cpVect3* PhysicsBeamBody::getNodes() const {return (cpVect3*)cpBeamShapeGetVerts(_shape);}

const cpIdx* PhysicsBeamBody::getSegments() const {return cpBeamShapeGetSegments(_shape);}

cpIdx PhysicsBeamBody::getFirstActiveSegmentIdx() const {return cpBeamShapeGetFirstActiveSegments(_shape);}

const cpIdx* PhysicsBeamBody::getActiveSegments() const {return cpBeamShapeGetActiveSegments(_shape);}

unsigned int PhysicsBeamBody::getNumConstrainedNodes(void) const {return cpBeamShapeGetNumConstrainedNodes(_beambody);}

const cpIdx* PhysicsBeamBody::getConstrainedNodes() const {return cpBeamShapeGetConstrainedNodes(_beambody);}

void PhysicsBeamBody::setPosition(const Vec2& pos) { cpBeamBodySetPosition(_beambody, {pos.x,pos.y} ); }

Vec2 PhysicsBeamBody::getPosition(void) { cpVect pos = cpBeamBodyGetCoG(_beambody); return Vec2(pos.x,pos.y); }

void PhysicsBeamBody::translate(const Vec2& pos) { cpBeamBodyTranslate(_beambody, {pos.x,pos.y} ); }

void PhysicsBeamBody::translate(const float x, const float y)
{ cpBeamBodyTranslate(_beambody, { PhysicsHelper::float2cpfloat(x), PhysicsHelper::float2cpfloat(y) } ); }

void PhysicsBeamBody::rotate(const float a) { cpBeamBodyRotateAroundCoG(_beambody, a); }

void PhysicsBeamBody::setVelocity(const Vec2& vel) { cpBeamBodySetVelocity( _beambody, {vel.x, vel.y} ); }

void PhysicsBeamBody::setNodeVelocity(const Vec2& vel, unsigned short nodeId)
{ cpBeamBodySetNodeVelocity( _beambody, {vel.x, vel.y}, nodeId ); }

void PhysicsBeamBody::setVelocity(const float x, const float y)
{ cpBeamBodySetVelocity( _beambody, { PhysicsHelper::float2cpfloat(x), PhysicsHelper::float2cpfloat(y) } ); }

void PhysicsBeamBody::setVelocityX(const float vx)
{ cpBeamBodySetVelocityX( _beambody, PhysicsHelper::float2cpfloat(vx) ); }

void PhysicsBeamBody::setVelocityY(const float vy)
{ cpBeamBodySetVelocityY( _beambody, PhysicsHelper::float2cpfloat(vy) ); }

void PhysicsBeamBody::addVelocity(const Vec2& vel) { cpBeamBodyAddVelocity( _beambody, {vel.x, vel.y} ); }

void PhysicsBeamBody::addVelocity(const float x, const float y)
{ cpBeamBodyAddVelocity( _beambody, { PhysicsHelper::float2cpfloat(x), PhysicsHelper::float2cpfloat(y) } ); }

void PhysicsBeamBody::setAngularVelocity(const float avel)
{ cpBeamBodySetAngularVelocity( _beambody, PhysicsHelper::float2cpfloat(avel) ); }


void PhysicsBeamBody::setGravity(const Vec2& gravity) { cpBeamBodySetGravity( _beambody, {gravity.x, gravity.y} ); }

void PhysicsBeamBody::setGravity(const float x, const float y)
{ cpBeamBodySetGravity( _beambody, { PhysicsHelper::float2cpfloat(x), PhysicsHelper::float2cpfloat(y) } ); }

//    collision filter
void PhysicsBeamBody::setCollisionFilter(unsigned int category, unsigned int mask )
{ cpBeamShapeSetFilter(_shape, category, mask); }

void PhysicsBeamBody::setPointCollisionFilter(unsigned short nodeId, unsigned int category, unsigned int mask )
{ cpBeamShapeSetPointFilter(_shape, nodeId, category, mask); }

void PhysicsBeamBody::setTriangleCollisionFilter(unsigned short triagleId, unsigned int category, unsigned int mask )
{ cpBeamShapeSetSegmentFilter(_shape, triagleId, category, mask); }

void PhysicsBeamBody::addConstraintVelocity(unsigned short nodeId, const Vec2& velocity )
{ cpBeamBodyAddConstraintVelocity( _beambody,  {velocity.x, velocity.y}, nodeId); }

void PhysicsBeamBody::removeConstraintVelocity(unsigned short nodeId)
{ cpBeamBodyRemoveConstraintVelocity(_beambody, nodeId); }

NS_CC_END

//#endif /* __cocos2d_libs__CCPhysicsBeamBody_inl__ */
