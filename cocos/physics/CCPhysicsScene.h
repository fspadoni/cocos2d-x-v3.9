//
//  CCPhysicsScene.h
//  cocos2d_libs
//
//  Created by Federico Spadoni on 03/12/14.
//
//

#ifndef __cocos2d_libs__CCPhysicsScene__
#define __cocos2d_libs__CCPhysicsScene__

#include "2d/CCScene.h"
#include "base/ccConfig.h"
//#include "extensions/ExtensionExport.h"

#if CC_USE_PHYSICS

#include "physics-thread/CCTask.h"

#ifndef PLATFORM_SUPPORTS_TBB
#define PLATFORM_SUPPORTS_TBB 0
#endif

#if PLATFORM_SUPPORTS_TBB
namespace tbb {
    class task_scheduler_init;
    class task;
}
#endif

NS_CC_BEGIN

// forward declaration
class PhysicsSoftBody;
class PhysicsBeamBody;
class PhysicsRigidBody;

class CC_DLL PhysicsScene : public Scene
{
public:
    
    static const char* _EVENT_SIM_LOOP_COMPLETION;
    
    enum _update_rate
    {
        _sim_update_rate_levels = 4,
        
        _sim_update_rate_min = 60,
        _sim_update_rate_low = 90,
        _sim_update_rate_mid = 120,
        _sim_update_rate_high = 150,
        
    };
    static const int _sim_update_rates[4];
    
    
    /** creates a new Scene object */
    static PhysicsScene *create(float hashCellSize, float hashCellCount);
    
    
    virtual void updateThreadSafe(float delta) {}
    
    
    /**
     * Stops all running actions and schedulers
     */
    virtual void cleanup() override;
    
    
    /** creates a new Scene object with a predefined Size */
//    static PhysicsScene *createWithSize(const Size& size);
    
    // Overrides
    virtual PhysicsScene *getScene() const override;
    
    
    virtual std::string getDescription() const override;
    

    const Task::Status* getSimLoopStatus() const { return &_simLoopStatus;}
    
    float GetHashCellSize() const { return _hashCellSize; }
    float GetHashCellCount() const { return _hashCellCount; }

    
//    virtual void update(float delta) override;
    virtual void stepPhysicsAndNavigation(float deltaTime) override;
    
    
    void simulate(float delta);
    
    void resetPhysicsWorld();
    
    void addSoftBodyToPhysicsWorld(PhysicsSoftBody* softbody);
    
    void addBeamBodyToPhysicsWorld(PhysicsBeamBody* beambody);
    
    void addRigidBodyToPhysicsWorld(PhysicsRigidBody* body);
    
    bool isSimulationPaused() { return _simulationPaused; }
    
    void simulationPause() { _simulationPaused = true; }
//
    void simulationResume() { _simulationPaused = false; }
    
       /** render the scene */
//    void render(Renderer* renderer);
    
    
CC_CONSTRUCTOR_ACCESS:
    PhysicsScene(float hashCellSize, float hashCellCount);
    virtual ~PhysicsScene();
    
    virtual bool initWithPhysics() override;
    
    
//    void onProjectionChanged(EventCustom* event);
    
    
private:
    CC_DISALLOW_COPY_AND_ASSIGN(PhysicsScene);
    
    static const int computeUpdateRate(int frameRate);
    
    void adjustUpdateRate(float delta);
    
    

    Task::Status _simLoopStatus;
    
    
    float _hashCellSize;
    float _hashCellCount;
    
    int _frames;
    float _accumDt;
    
    bool _simulationPaused;
    
    static int _current_update_rate;
    
#if PLATFORM_SUPPORTS_TBB
    tbb::task_scheduler_init* _tbbScheduler;
    tbb::task* _rootTask;
#endif
    

};

#endif // CC_USE_PHYSICS

NS_CC_END


#endif /* defined(__cocos2d_libs__CCPhysicsScene__) */
