/****************************************************************************
 Copyright (c) 2013 Chukong Technologies Inc.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include "physics/CCPhysicsWorld.h"
#if CC_USE_PHYSICS
#include <algorithm>
#include <climits>

#include "chipmunk.h"
#include "CCPhysicsBody.h"
#include "CCPhysicsShape.h"
#include "CCPhysicsContact.h"
#include "CCPhysicsJoint.h"
#include "CCPhysicsContact.h"
#include "physics-utils/CCPhysicsHelper.h"
#include "physics/CCPhysicsSoftBody.h"
#include "physics/CCPhysicsBeamBody.h"
#include "physics/CCPhysicsRigidBody.h"
#include "physics-utils/CCPageMemoryPool.h"
#include "physics/CCPhysicsScene.h"

#include "2d/CCDrawNode.h"
#include "2d/CCScene.h"
#include "base/CCDirector.h"
#include "base/CCEventDispatcher.h"
#include "base/CCEventCustom.h"

#include "2d/CCLabel.h"


NS_CC_BEGIN
const float PHYSICS_INFINITY = INFINITY;
extern const char* PHYSICSCONTACT_EVENT_NAME;
extern std::unordered_map<cpShape*, PhysicsShape*> s_physicsShapeMap;

const int PhysicsWorld::DEBUGDRAW_NONE = 0x00;
const int PhysicsWorld::DEBUGDRAW_SHAPE = 0x01;
const int PhysicsWorld::DEBUGDRAW_JOINT = 0x02;
const int PhysicsWorld::DEBUGDRAW_CONTACT = 0x04;
const int PhysicsWorld::DEBUGDRAW_STATISTICS = 0x08;
const int PhysicsWorld::DEBUGDRAW_ALL = DEBUGDRAW_SHAPE | DEBUGDRAW_JOINT | DEBUGDRAW_CONTACT | DEBUGDRAW_STATISTICS;

namespace
{
    typedef struct RayCastCallbackInfo
    {
        PhysicsWorld* world;
        PhysicsRayCastCallbackFunc func;
        Vec2 p1;
        Vec2 p2;
        void* data;
    }RayCastCallbackInfo;
    
    typedef struct RectQueryCallbackInfo
    {
        PhysicsWorld* world;
        PhysicsQueryRectCallbackFunc func;
        void* data;
    }RectQueryCallbackInfo;
    
    typedef struct PointQueryCallbackInfo
    {
        PhysicsWorld* world;
        PhysicsQueryPointCallbackFunc func;
        void* data;
    }PointQueryCallbackInfo;
}

class PhysicsWorldCallback
{
public:
    static int collisionBeginCallbackFunc(cpArbiter *arb, struct cpSpace *space, PhysicsWorld *world);
    static int collisionPreSolveCallbackFunc(cpArbiter *arb, cpSpace *space, PhysicsWorld *world);
    static void collisionPostSolveCallbackFunc(cpArbiter *arb, cpSpace *space, PhysicsWorld *world);
    static void collisionSeparateCallbackFunc(cpArbiter *arb, cpSpace *space, PhysicsWorld *world);
    static void rayCastCallbackFunc(cpShape *shape, cpVect t, cpVect n, RayCastCallbackInfo *info);
    static void queryRectCallbackFunc(cpShape *shape, RectQueryCallbackInfo *info);
    static void queryPointFunc(cpShape *shape, cpFloat distance, cpVect point, PointQueryCallbackInfo *info);
    static void getShapesAtPointFunc(cpShape *shape, cpFloat distance, cpVect point, Vector<PhysicsShape*>* arr);
    
public:
    static bool continues;
};

bool PhysicsWorldCallback::continues = true;

int PhysicsWorldCallback::collisionBeginCallbackFunc(cpArbiter *arb, struct cpSpace *space, PhysicsWorld *world)
{
    CP_ARBITER_GET_SHAPES(arb, a, b);
    
    auto ita = s_physicsShapeMap.find(a);
    auto itb = s_physicsShapeMap.find(b);
//    CC_ASSERT(ita != s_physicsShapeMap.end() && itb != s_physicsShapeMap.end());
    
    //    Fede
    if ( ita == s_physicsShapeMap.end() || itb == s_physicsShapeMap.end() )
    {
        return 1;
    }
    
    return 1;
    
    auto contact = PhysicsContact::construct(ita->second, itb->second);
//    arb->data = contact;
    cpArbiterSetUserData(arb, contact);
    contact->_contactInfo = arb;
    
    return world->collisionBeginCallback(*contact);
}

int PhysicsWorldCallback::collisionPreSolveCallbackFunc(cpArbiter *arb, cpSpace *space, PhysicsWorld *world)
{
    return world->collisionPreSolveCallback(*static_cast<PhysicsContact*>(cpArbiterGetUserData(arb)));
}

void PhysicsWorldCallback::collisionPostSolveCallbackFunc(cpArbiter *arb, cpSpace *space, PhysicsWorld *world)
{
    world->collisionPostSolveCallback(*static_cast<PhysicsContact*>(cpArbiterGetUserData(arb)));
}

void PhysicsWorldCallback::collisionSeparateCallbackFunc(cpArbiter *arb, cpSpace *space, PhysicsWorld *world)
{
    return;
    
    PhysicsContact* contact = static_cast<PhysicsContact*>(cpArbiterGetUserData(arb));
    
    world->collisionSeparateCallback(*contact);
    
    delete contact;
}

void PhysicsWorldCallback::rayCastCallbackFunc(cpShape *shape, cpVect t, cpVect n, RayCastCallbackInfo *info)
{
    if (!PhysicsWorldCallback::continues)
    {
        return;
    }
    
    auto it = s_physicsShapeMap.find(shape);
    CC_ASSERT(it != s_physicsShapeMap.end());
    
    PhysicsRayCastInfo callbackInfo =
    {
        it->second,
        info->p1,
        info->p2,
        Vec2(t.x, t.y),
        Vec2(n.x, n.y),
        0,
//        Vec2(info->p1.x+(info->p2.x-info->p1.x)*t, info->p1.y+(info->p2.y-info->p1.y)*t),
//        Vec2(n.x, n.y),
//        (float)t,
    };
    
    PhysicsWorldCallback::continues = info->func(*info->world, callbackInfo, info->data);
}

void PhysicsWorldCallback::queryRectCallbackFunc(cpShape *shape, RectQueryCallbackInfo *info)
{
    auto it = s_physicsShapeMap.find(shape);
    
//    CC_ASSERT(it != s_physicsShapeMap.end());
    
    if (!PhysicsWorldCallback::continues)
    {
        return;
    }
    
    PhysicsShape* physicsShape =   ( it != s_physicsShapeMap.end() ) ?  it->second :  nullptr;
    
    PhysicsWorldCallback::continues = info->func(*info->world, *physicsShape, info->data);
}

void PhysicsWorldCallback::getShapesAtPointFunc(cpShape *shape, cpFloat distance, cpVect point, Vector<PhysicsShape*>* arr)
{
    auto it = s_physicsShapeMap.find(shape);
    
    CC_ASSERT(it != s_physicsShapeMap.end());
    
    arr->pushBack(it->second);
}

void PhysicsWorldCallback::queryPointFunc(cpShape *shape, cpFloat distance, cpVect point, PointQueryCallbackInfo *info)
{
    auto it = s_physicsShapeMap.find(shape);
    
    CC_ASSERT(it != s_physicsShapeMap.end());
    
    PhysicsWorldCallback::continues = info->func(*info->world, *it->second, info->data);
}


class PhysicsCollisionCallback
{
public:
    static int collisionBeginCallbackFunc(cpArbiter *arb, struct cpSpace *space, PhysicsWorld *world);
    static int collisionPreSolveCallbackFunc(cpArbiter *arb, cpSpace *space, PhysicsWorld *world);
    static void collisionPostSolveCallbackFunc(cpArbiter *arb, cpSpace *space, PhysicsWorld *world);
    static void collisionSeparateCallbackFunc(cpArbiter *arb, cpSpace *space, PhysicsWorld *world);
    
public:
    static bool continues;
};

int PhysicsCollisionCallback::collisionBeginCallbackFunc(cpArbiter *arb, struct cpSpace *space, PhysicsWorld *world)
{
    CP_ARBITER_GET_SHAPES(arb, a, b);
    PhysicsRigidBody* bodyA = static_cast<PhysicsRigidBody*>(cpShapeGetUserData(a));
    PhysicsRigidBody* bodyB = static_cast<PhysicsRigidBody*>(cpShapeGetUserData(b));
    
    if ( bodyA->notifyBeginCollision())
        bodyA->collisionBegin( bodyB, arb);
    
    if ( bodyB->notifyBeginCollision())
        bodyB->collisionBegin( bodyA, arb);
    
    return 0;
}

int PhysicsCollisionCallback::collisionPreSolveCallbackFunc(cpArbiter *arb, cpSpace *space, PhysicsWorld *world)
{
    return 1;
}

void PhysicsCollisionCallback::collisionPostSolveCallbackFunc(cpArbiter *arb, cpSpace *space, PhysicsWorld *world)
{
    return;
}

void PhysicsCollisionCallback::collisionSeparateCallbackFunc(cpArbiter *arb, cpSpace *space, PhysicsWorld *world)
{
    CP_ARBITER_GET_SHAPES(arb, a, b);
    PhysicsRigidBody* bodyA = static_cast<PhysicsRigidBody*>(cpShapeGetUserData(a));
    PhysicsRigidBody* bodyB = static_cast<PhysicsRigidBody*>(cpShapeGetUserData(b));
    
    if ( bodyA->notifySeparateCollision())
        bodyA->collisionSeparate( bodyB, arb);
    
    if ( bodyB->notifySeparateCollision())
        bodyB->collisionSeparate( bodyA, arb);

}


void PhysicsWorld::debugDraw(float delta)
{
    static float stats_interval = 0.0;
    
//    if (_debugDraw == nullptr)
//    {
//        _debugDraw = new (std::nothrow) PhysicsDebugDraw(*this);
//    }
    
    if (_debugDraw && !_bodies.empty())
    {
        if (_debugDraw->begin())
        {
            if (_debugDrawMask & DEBUGDRAW_SHAPE)
            {
                for (Ref* obj : _bodies)
                {
                    PhysicsBody* body = dynamic_cast<PhysicsBody*>(obj);
                    
                    if (!body->isEnabled())
                    {
                        continue;
                    }
                    
                    for (auto& shape : body->getShapes())
                    {
                        _debugDraw->drawShape(*dynamic_cast<PhysicsShape*>(shape));
                    }
                }
            }
            
            if (_debugDrawMask & DEBUGDRAW_JOINT)
            {
                for (auto joint : _joints)
                {
                    _debugDraw->drawJoint(*joint);
                }
            }
            
            _debugDraw->end();
        }
    }
    
    if (_debugDrawMask & DEBUGDRAW_STATISTICS)
    {
        stats_interval += delta;
        
        if ( stats_interval > 0.5)
        {
            stats_interval = 0.0;
            _physicsStats.updateRelativeTime();
            
            _debugDraw->drawStatistics( &_physicsStats );
            
            _physicsStats.reset();
            
        }
    }
}

int PhysicsWorld::collisionBeginCallback(PhysicsContact& contact)
{
    bool ret = true;
    
    PhysicsShape* shapeA = contact.getShapeA();
    PhysicsShape* shapeB = contact.getShapeB();
    PhysicsBody* bodyA = shapeA->getBody();
    PhysicsBody* bodyB = shapeB->getBody();
    std::vector<PhysicsJoint*> jointsA = bodyA->getJoints();
    
    // check the joint is collision enable or not
    for (PhysicsJoint* joint : jointsA)
    {
        if (std::find(_joints.begin(), _joints.end(), joint) == _joints.end())
        {
            continue;
        }
        
        if (!joint->isCollisionEnabled())
        {
            PhysicsBody* body = joint->getBodyA() == bodyA ? joint->getBodyB() : joint->getBodyA();
            
            if (body == bodyB)
            {
                contact.setNotificationEnable(false);
                return false;
            }
        }
    }
    
    // bitmask check
    if ((shapeA->getCategoryBitmask() & shapeB->getContactTestBitmask()) == 0
        || (shapeA->getContactTestBitmask() & shapeB->getCategoryBitmask()) == 0)
    {
        contact.setNotificationEnable(false);
    }
    
    if (shapeA->getGroup() != 0 && shapeA->getGroup() == shapeB->getGroup())
    {
        ret = shapeA->getGroup() > 0;
    }
    else
    {
        if ((shapeA->getCategoryBitmask() & shapeB->getCollisionBitmask()) == 0
            || (shapeB->getCategoryBitmask() & shapeA->getCollisionBitmask()) == 0)
        {
            ret = false;
        }
    }
    
    if (contact.isNotificationEnabled())
    {
        contact.setEventCode(PhysicsContact::EventCode::BEGIN);
        contact.setWorld(this);
        _eventDispatcher->dispatchEvent(&contact);
    }
    
    return ret ? contact.resetResult() : false;
}

int PhysicsWorld::collisionPreSolveCallback(PhysicsContact& contact)
{
//    Fede
    return true;
    
    if (!contact.isNotificationEnabled())
    {
        return true;
    }
    
    contact.setEventCode(PhysicsContact::EventCode::PRESOLVE);
    contact.setWorld(this);
    _eventDispatcher->dispatchEvent(&contact);
    
    return contact.resetResult();
}

void PhysicsWorld::collisionPostSolveCallback(PhysicsContact& contact)
{
//    Fede
    return ;
    
    if (!contact.isNotificationEnabled())
    {
        return;
    }
    
    contact.setEventCode(PhysicsContact::EventCode::POSTSOLVE);
    contact.setWorld(this);
    _eventDispatcher->dispatchEvent(&contact);
}

void PhysicsWorld::collisionSeparateCallback(PhysicsContact& contact)
{
//    Fede
    return ;
    
    if (!contact.isNotificationEnabled())
    {
        return;
    }
    
    contact.setEventCode(PhysicsContact::EventCode::SEPARATE);
    contact.setWorld(this);
    _eventDispatcher->dispatchEvent(&contact);
}

void PhysicsWorld::rayCast(PhysicsRayCastCallbackFunc func, const Vec2& point1, const Vec2& point2, void* data)
{
    CCASSERT(func != nullptr, "func shouldn't be nullptr");
    
    if (func != nullptr)
    {
        if (!_delayAddBodies.empty() || !_delayRemoveBodies.empty())
        {
            updateBodies();
        }
        RayCastCallbackInfo info = { this, func, point1, point2, data };
        
        PhysicsWorldCallback::continues = true;
        cpSpaceSegmentQuery(cpGetSpace(_cpSpace),
                            PhysicsHelper::point2cpv(point1),
                            PhysicsHelper::point2cpv(point2),
                            0.0,
                            CP_SHAPE_FILTER_ALL,
                            (cpSpaceSegmentQueryFunc)PhysicsWorldCallback::rayCastCallbackFunc,
                            &info);
    }
}

void PhysicsWorld::queryRect(PhysicsQueryRectCallbackFunc func, const Rect& rect, unsigned int category, unsigned int mask, void* data)
{
    CCASSERT(func != nullptr, "func shouldn't be nullptr");
    
    if (func != nullptr)
    {
        if (!_delayAddBodies.empty() || !_delayRemoveBodies.empty())
        {
            updateBodies();
        }
        RectQueryCallbackInfo info = {this, func, data};
        
        const cpShapeFilter filter = {CP_NO_GROUP, category, mask};
        
        PhysicsWorldCallback::continues = true;
        cpSpaceBBQuery(cpGetSpace(_cpSpace),
                       PhysicsHelper::rect2cpbb(rect),
                       filter,
                       (cpSpaceBBQueryFunc)PhysicsWorldCallback::queryRectCallbackFunc,
                       &info);
    }
}

void PhysicsWorld::queryPoint(PhysicsQueryPointCallbackFunc func, const Vec2& point, void* data)
{
    CCASSERT(func != nullptr, "func shouldn't be nullptr");
    
    if (func != nullptr)
    {
        if (!_delayAddBodies.empty() || !_delayRemoveBodies.empty())
        {
            updateBodies();
        }
        PointQueryCallbackInfo info = {this, func, data};
        
        PhysicsWorldCallback::continues = true;
        cpSpacePointQuery(cpGetSpace(_cpSpace),
                                 PhysicsHelper::point2cpv(point),
                                 0,
                                 CP_SHAPE_FILTER_ALL,
                                 (cpSpacePointQueryFunc)PhysicsWorldCallback::queryPointFunc,
                                 &info);
    }
}



PhysicsShape* PhysicsWorld::queryPointNearest( const Vec2& point, const float& maxDistance, unsigned int category, unsigned int mask, cpPointQueryInfo* datainfo )
{
    
//    if (func != nullptr)
    {
        if (!_delayAddBodies.empty() || !_delayRemoveBodies.empty())
        {
            updateBodies();
        }
        
//        cpPointQueryInfo info = {};
        
        
        const cpShapeFilter filter = {CP_NO_GROUP, category, mask};
        
        cpShape* shape = cpSpacePointQueryNearest( cpGetSpace(_cpSpace),
                                 PhysicsHelper::point2cpv(point),
                                 maxDistance,
                                 filter,
                                 datainfo);
        
        if ( s_physicsShapeMap.size() > 0)
        {
            auto it = s_physicsShapeMap.find(shape);
            if (it != s_physicsShapeMap.end())
            {
                return it->second;
            }
            
//            s_physicsShapeMap.find(shape);
//            return shape == nullptr ? nullptr : s_physicsShapeMap.find(shape)->second;
        }
        
        return nullptr;
    }
}

Vector<PhysicsShape*> PhysicsWorld::getShapes(const Vec2& point) const
{
    Vector<PhysicsShape*> arr;
    cpSpacePointQuery(cpGetSpace(_cpSpace),
                             PhysicsHelper::point2cpv(point),
                             0,
                             CP_SHAPE_FILTER_ALL,
                             (cpSpacePointQueryFunc)PhysicsWorldCallback::getShapesAtPointFunc,
                             &arr);
    
    return arr;
}

PhysicsShape* PhysicsWorld::getShape(const Vec2& point) const
{
    cpShape* shape = cpSpacePointQueryNearest(cpGetSpace(_cpSpace),
                                    PhysicsHelper::point2cpv(point),
                                    0,
                                    CP_SHAPE_FILTER_ALL,
                                    nullptr);
    
    return shape == nullptr ? nullptr : s_physicsShapeMap.find(shape)->second;
}

bool PhysicsWorld::init(Scene& scene)
{
    do
    {
        float hashCellSize = 16;
        float hashCellCount = 10000;
        
        if (PhysicsScene* physicsScene = dynamic_cast<PhysicsScene*>(&scene) )
        {
            hashCellSize =  physicsScene->GetHashCellSize();
            hashCellCount = physicsScene->GetHashCellCount();
        }
        
        _scene = &scene;
        
        _cpSpace = cpSpaceSoftBodyNew(hashCellSize, hashCellCount);
        
        CC_BREAK_IF(_cpSpace == nullptr);
        
        cpSpaceSetGravity(cpGetSpace(_cpSpace), PhysicsHelper::point2cpv(_gravity));
        
//        cpCollisionHandler* collisionHandler = cpSpaceAddDefaultCollisionHandler( cpGetSpace(_cpSpace) );
        cpCollisionHandler* collisionHandler = cpSpaceAddWildcardHandler(cpGetSpace(_cpSpace), 0 /*SB_SHAPES*/);
        
        collisionHandler->beginFunc = (cpCollisionBeginFunc)PhysicsWorldCallback::collisionBeginCallbackFunc;
        collisionHandler->preSolveFunc = (cpCollisionPreSolveFunc)PhysicsWorldCallback::collisionPreSolveCallbackFunc;
        collisionHandler->postSolveFunc = (cpCollisionPostSolveFunc)PhysicsWorldCallback::collisionPostSolveCallbackFunc;
        collisionHandler->separateFunc = (cpCollisionSeparateFunc)PhysicsWorldCallback::collisionSeparateCallbackFunc;
        collisionHandler->userData = this;
        
//        cpSpaceSetDefaultCollisionHandler(_cpSpace,
//                                          (cpCollisionBeginFunc)PhysicsWorldCallback::collisionBeginCallbackFunc,
//                                          (cpCollisionPreSolveFunc)PhysicsWorldCallback::collisionPreSolveCallbackFunc,
//                                          (cpCollisionPostSolveFunc)PhysicsWorldCallback::collisionPostSolveCallbackFunc,
//                                          (cpCollisionSeparateFunc)PhysicsWorldCallback::collisionSeparateCallbackFunc,
//                                          this);
        
        
        
//        Fede: add code here
        if (_debugDraw == nullptr)
        {
            _debugDraw = new (std::nothrow) PhysicsDebugDraw(*this);
        }
        
        
        return true;
    } while (false);
    
    return false;
}


void PhysicsWorld::addSoftBody(PhysicsSoftBody* body)
{
    CCASSERT(body != nullptr, "the body can not be nullptr");
    
    if (body->getWorld() == this)
    {
        return;
    }
    if (body->getWorld() != nullptr)
    {
        body->removeFromWorld();
    }
    
    addSoftBodyOrDelay(body);
    _softbodies.pushBack(body);
    body->_world = this;
}

void PhysicsWorld::addSoftBodyOrDelay(PhysicsSoftBody* body)
{
    auto removeBodyIter = _delayRemoveSoftBodies.find(body);
    if (removeBodyIter != _delayRemoveSoftBodies.end())
    {
        _delayRemoveSoftBodies.erase(removeBodyIter);
        return;
    }
    
    if ( cpSpaceIsLocked(cpGetSpace(_cpSpace)) )
    {
        if (_delayAddSoftBodies.find(body) == _delayRemoveSoftBodies.end())
        {
            _delayRemoveSoftBodies.pushBack(body);
//            _delayDirty = true;
        }
    }
    else
    {
        doAddSoftBody(body);
    }
    
}

void PhysicsWorld::doAddSoftBody(PhysicsSoftBody* body)
{
    if (body->isEnabled())
    {
        if (!cpSpaceContainsSoftBodyOpt(_cpSpace, body->getSoftBodyOpt() ))
        {
            cpSpaceAddSoftBodyOpt(_cpSpace, body->getSoftBodyOpt() );
        }
    }
}

void PhysicsWorld::addBeamBody(PhysicsBeamBody* body)
{
    CCASSERT(body != nullptr, "the body can not be nullptr");
    
    if (body->getWorld() == this)
    {
        return;
    }
    if (body->getWorld() != nullptr)
    {
        body->removeFromWorld();
    }
    
    addBeamBodyOrDelay(body);
    _beambodies.pushBack(body);
    body->_world = this;
}

void PhysicsWorld::addBeamBodyOrDelay(PhysicsBeamBody* body)
{
    auto removeBodyIter = _delayRemoveBeamBodies.find(body);
    if (removeBodyIter != _delayRemoveBeamBodies.end())
    {
        _delayRemoveBeamBodies.erase(removeBodyIter);
        return;
    }
    
    if ( cpSpaceIsLocked(cpGetSpace(_cpSpace)) )
    {
        if (_delayAddBeamBodies.find(body) == _delayRemoveBeamBodies.end())
        {
            _delayRemoveBeamBodies.pushBack(body);
            //            _delayDirty = true;
        }
    }
    else
    {
        doAddBeamBody(body);
    }
    
}

void PhysicsWorld::doAddBeamBody(PhysicsBeamBody* body)
{
    if (body->isEnabled())
    {
        if (!cpSpaceContainsBeamBody(_cpSpace, body->getBeamBody() ))
        {
            cpSpaceAddBeamBody(_cpSpace, body->getBeamBody() );
        }
    }
}


void PhysicsWorld::removeSoftBody(PhysicsSoftBody* body, bool destroy)
{
    if (body->getWorld() != this)
    {
        CCLOG("Physics Warnning: this body doesn't belong to this world");
        return;
    }
    
    //    Fede
    body->_destoryMark = destroy;
    
    removeSoftBodyOrDelay(body);
    body->_world = nullptr;
    _softbodies.eraseObject(body);
}

void PhysicsWorld::removeSoftBody(int tag, bool destroy)
{
    for (auto& body : _softbodies)
    {
        if (body->getTag() == tag)
        {
            removeSoftBody(body, destroy);
            return;
        }
    }
}

void PhysicsWorld::removeAllSoftBodies()
{
    for (auto& child : _softbodies)
    {
        removeSoftBodyOrDelay(child);
        child->_world = nullptr;
    }
    
    _softbodies.clear();
}

void PhysicsWorld::removeSoftBodyOrDelay(PhysicsSoftBody* body)
{
    if (_delayAddSoftBodies.getIndex(body) != CC_INVALID_INDEX)
    {
        _delayAddSoftBodies.eraseObject(body);
        return;
    }
    
    if ( cpSpaceIsLocked(cpGetSpace(_cpSpace)) )
    {
        if (_delayRemoveSoftBodies.getIndex(body) == CC_INVALID_INDEX)
        {
            _delayRemoveSoftBodies.pushBack(body);
//            _delayDirty = true;
        }
    }else
    {
        doRemoveSoftBody(body);
    }
}

void PhysicsWorld::doRemoveSoftBody(PhysicsSoftBody* body)
{
    CCASSERT(body != nullptr, "the body can not be nullptr");
    
    // remove body
    if ( cpSpaceContainsSoftBodyOpt(_cpSpace, body->getSoftBodyOpt() ))
    {
        cpSpaceRemoveSoftBodyOpt(_cpSpace, body->getSoftBodyOpt() );
    }
    
    //    Fede
    if ( body->_destoryMark )
    {
        //        body->destroy();
        //        CC_SAFE_DELETE(body);
    }
}


void PhysicsWorld::removeBeamBody(PhysicsBeamBody* body, bool destroy)
{
    if (body->getWorld() != this)
    {
        CCLOG("Physics Warnning: this body doesn't belong to this world");
        return;
    }
    
    //    Fede
    body->_destoryMark = destroy;
    
    removeBeamBodyOrDelay(body);
    body->_world = nullptr;
    _beambodies.eraseObject(body);
}

void PhysicsWorld::removeBeamBody(int tag, bool destroy)
{
    for (auto& body : _beambodies)
    {
        if (body->getTag() == tag)
        {
            removeBeamBody(body, destroy);
            return;
        }
    }
}

void PhysicsWorld::removeAllBeamBodies()
{
    for (auto& child : _beambodies)
    {
        removeBeamBodyOrDelay(child);
        child->_world = nullptr;
    }
    
    _beambodies.clear();
}

void PhysicsWorld::removeBeamBodyOrDelay(PhysicsBeamBody* body)
{
    if (_delayAddBeamBodies.getIndex(body) != CC_INVALID_INDEX)
    {
        _delayAddBeamBodies.eraseObject(body);
        return;
    }
    
    if ( cpSpaceIsLocked(cpGetSpace(_cpSpace)) )
    {
        if (_delayRemoveBeamBodies.getIndex(body) == CC_INVALID_INDEX)
        {
            _delayRemoveBeamBodies.pushBack(body);
            //            _delayDirty = true;
        }
    }else
    {
        doRemoveBeamBody(body);
    }
}

void PhysicsWorld::doRemoveBeamBody(PhysicsBeamBody* body)
{
    CCASSERT(body != nullptr, "the body can not be nullptr");
    
    // remove body
    if ( cpSpaceContainsBeamBody(_cpSpace, body->getBeamBody() ))
    {
        cpSpaceRemoveBeamBody(_cpSpace, body->getBeamBody() );
    }
    
    //    Fede
    if ( body->_destoryMark )
    {
        //        body->destroy();
        //        CC_SAFE_DELETE(body);
    }
}


void PhysicsWorld::addBody(PhysicsBody* body)
{
    CCASSERT(body != nullptr, "the body can not be nullptr");
    
    if (body->getWorld() == this)
    {
        return;
    }
    
    if (body->getWorld() != nullptr)
    {
        body->removeFromWorld();
    }
    
    addBodyOrDelay(body);
    _bodies.pushBack(body);
    body->_world = this;
}

void PhysicsWorld::doAddBody(PhysicsBody* body)
{
    if (body->isEnabled())
    {
        // add body to space
//        if (body->isDynamic() && !cpSpaceContainsBody(cpGetSpace(_cpSpace), body->_cpBody))
        if ( !cpSpaceContainsBody(cpGetSpace(_cpSpace), body->_cpBody))
        {
            cpSpaceAddBody(cpGetSpace(_cpSpace), body->_cpBody);
        }
        
        // add shapes to space
        for (auto& shape : body->getShapes())
        {
            addShape(dynamic_cast<PhysicsShape*>(shape));
        }
    }
}

void PhysicsWorld::addBodyOrDelay(PhysicsBody* body)
{
    auto removeBodyIter = _delayRemoveBodies.find(body);
    if (removeBodyIter != _delayRemoveBodies.end())
    {
        _delayRemoveBodies.erase(removeBodyIter);
        return;
    }
    
    if (_delayAddBodies.find(body) == _delayAddBodies.end())
    {
        _delayAddBodies.pushBack(body);
    }
}

void PhysicsWorld::updateBodies()
{
    if (cpSpaceIsLocked(cpGetSpace(_cpSpace)))
    {
        return;
    }
    
    // issue #4944, contact callback will be invoked when add/remove body, _delayAddBodies maybe changed, so we need make a copy.
    auto addCopy = _delayAddBodies;
    _delayAddBodies.clear();
    for (auto& body : addCopy)
    {
        doAddBody(body);
    }
    
    auto removeCopy = _delayRemoveBodies;
    _delayRemoveBodies.clear();
    for (auto& body : removeCopy)
    {
        doRemoveBody(body);
    }
}

void PhysicsWorld::removeBody(int tag, bool destroy)
{
    for (auto& body : _bodies)
    {
        if (body->getTag() == tag)
        {
            removeBody(body, destroy);
            return;
        }
    }
}

void PhysicsWorld::removeBody(PhysicsBody* body, bool destroy)
{
    if (body->getWorld() != this)
    {
        CCLOG("Physics Warning: this body doesn't belong to this world");
        return;
    }
    
//    Fede
    body->_destoryMark = destroy;
    
    // destroy the body's joints
    auto removeCopy = body->_joints;
    for (auto joint : removeCopy)
    {
        removeJoint(joint, true);
    }
    body->_joints.clear();
    
    removeBodyOrDelay(body);
    _bodies.eraseObject(body);
    body->_world = nullptr;
}

void PhysicsWorld::removeBodyOrDelay(PhysicsBody* body)
{
    if (_delayAddBodies.getIndex(body) != CC_INVALID_INDEX)
    {
        _delayAddBodies.eraseObject(body);
        return;
    }
    
    if (cpSpaceIsLocked(cpGetSpace(_cpSpace)))
    {
        if (_delayRemoveBodies.getIndex(body) == CC_INVALID_INDEX)
        {
            _delayRemoveBodies.pushBack(body);
        }
    }else
    {
        doRemoveBody(body);
    }
}

void PhysicsWorld::removeJoint(PhysicsJoint* joint, bool destroy)
{
    if (joint)
    {
        if (joint->getWorld() != this && destroy)
        {
            CCLOG("physics warning: the joint is not in this world, it won't be destroyed until the body it connects is destroyed");
            return;
        }

        joint->_destoryMark = destroy;

        bool removedFromDelayAdd = false;
        auto it = std::find(_delayAddJoints.begin(), _delayAddJoints.end(), joint);
        if (it != _delayAddJoints.end())
        {
            _delayAddJoints.erase(it);
            removedFromDelayAdd = true;
        }

        if (cpSpaceIsLocked(cpGetSpace(_cpSpace)))
        {
            if (removedFromDelayAdd)
                return;
            if (std::find(_delayRemoveJoints.rbegin(), _delayRemoveJoints.rend(), joint) == _delayRemoveJoints.rend())
            {
                _delayRemoveJoints.push_back(joint);
            }
        }
        else
        {
            doRemoveJoint(joint);
        }
    }
}

void PhysicsWorld::updateJoints()
{
    if (cpSpaceIsLocked(cpGetSpace(_cpSpace)))
    {
        return;
    }
    
    for (auto joint : _delayAddJoints)
    {
        joint->_world = this;
        if (joint->initJoint())
        {
            _joints.push_back(joint);
        }
        else
        {
            delete joint;
        }
    }
    _delayAddJoints.clear();

    for (auto joint : _delayRemoveJoints)
    {
        doRemoveJoint(joint);
    }
    _delayRemoveJoints.clear();
}

void PhysicsWorld::removeShape(PhysicsShape* shape)
{
    if (shape)
    {
        for (auto cps : shape->_cpShapes)
        {
            if (cpSpaceContainsShape(cpGetSpace(_cpSpace), cps))
            {
                cpSpaceRemoveShape(cpGetSpace(_cpSpace), cps);
            }
        }
    }
}

void PhysicsWorld::addJoint(PhysicsJoint* joint)
{
    if (joint)
    {
        CCASSERT(joint->getWorld() == nullptr, "Can not add joint already add to other world!");

        joint->_world = this;
        auto it = std::find(_delayRemoveJoints.begin(), _delayRemoveJoints.end(), joint);
        if (it != _delayRemoveJoints.end())
        {
            _delayRemoveJoints.erase(it);
            return;
        }

        if (std::find(_delayAddJoints.begin(), _delayAddJoints.end(), joint) == _delayAddJoints.end())
        {
            _delayAddJoints.push_back(joint);
        }
    }
}

void PhysicsWorld::removeAllJoints(bool destroy)
{
    auto removeCopy = _joints;
    for (auto joint : removeCopy)
    {
        removeJoint(joint, destroy);
    }
}

void PhysicsWorld::addShape(PhysicsShape* physicsShape)
{
    if (physicsShape)
    {
        for (auto shape : physicsShape->_cpShapes)
        {
            cpSpaceAddShape(cpGetSpace(_cpSpace), shape);
        }
    }
}

void PhysicsWorld::doRemoveBody(PhysicsBody* body)
{
    CCASSERT(body != nullptr, "the body can not be nullptr");
    
    // remove shapes
    for (auto& shape : body->getShapes())
    {
        removeShape(shape);
    }
    
    // remove body
    if (cpSpaceContainsBody(cpGetSpace(_cpSpace), body->_cpBody))
    {
        cpSpaceRemoveBody(cpGetSpace(_cpSpace), body->_cpBody);
    }
//    Fede
    if ( body->_destoryMark )
    {
//        body->destroy();
//        CC_SAFE_DELETE(body);
    }
}

void PhysicsWorld::doRemoveJoint(PhysicsJoint* joint)
{
    for (auto constraint : joint->_cpConstraints)
    {
        cpSpaceRemoveConstraint(cpGetSpace(_cpSpace), constraint);

    }
    _joints.remove(joint);
    joint->_world = nullptr;

    if (joint->getBodyA())
    {
        joint->getBodyA()->removeJoint(joint);
    }

    if (joint->getBodyB())
    {
        joint->getBodyB()->removeJoint(joint);
    }

    if (joint->_destoryMark)
    {
        //        Fede: add this to free the pool
//        joint->destroy();
        delete joint;
    }
}

void PhysicsWorld::removeAllBodies()
{
    for (auto& child : _bodies)
    {
        removeBodyOrDelay(child);
        child->_world = nullptr;
    }
    
    _bodies.clear();
}


void PhysicsWorld::addRigidBody(PhysicsRigidBody* body)
{
    if ( body->getCPBody() != nullptr)
    {
        cpSpaceAddBody(cpGetSpace(_cpSpace), body->getCPBody() );
        const std::set<cpShape*>& shapes = body->getCPShapes();
        for ( auto shape : shapes )
        {
            cpSpaceAddShape(cpGetSpace(_cpSpace), shape);
        }
        
        _rigidbodies.pushBack(body);
    }
    else
    {
        addStaticBody(body);
    }
    
}

void PhysicsWorld::addStaticBody(PhysicsRigidBody* body)
{
    if ( body->getCPBody() == nullptr)
    {
        cpBody* staticBody = cpSpaceGetStaticBody( cpGetSpace(_cpSpace) );
        const std::set<cpShape*>& shapes = body->getCPShapes();
        for ( auto shape : shapes )
        {
            cpBodySetPosition(staticBody, cpv(body->getPosition().x, body->getPosition().y) );
            cpShapeSetBody(shape, staticBody);
            cpSpaceAddShape(cpGetSpace(_cpSpace), shape );
        }
        
        _staticbodies.pushBack(body);
    }
    else
    {
        addRigidBody(body);
    }
}


void PhysicsWorld::removeRigidBody(PhysicsRigidBody* body, bool destroy )
{
    cpSpaceRemoveBody(cpGetSpace(_cpSpace), body->getCPBody() );
    const std::set<cpShape*>& shapes = body->getCPShapes();
    for ( auto shape : shapes )
    {
        cpSpaceRemoveShape(cpGetSpace(_cpSpace), shape);
    }

    
    _rigidbodies.eraseObject(body);
    
    if ( destroy )
    {
        
    }
}

void PhysicsWorld::removeRigidBody(int tag, bool destroy )
{
    for (auto& body : _rigidbodies)
    {
        if (body->getTag() == tag)
        {
            removeRigidBody(body, destroy);
            return;
        }
    }
}

void PhysicsWorld::removeAllRigidBodies()
{
    for ( auto body : _rigidbodies )
    {
        cpSpaceRemoveBody(cpGetSpace(_cpSpace), body->getCPBody() );
        const std::set<cpShape*>& shapes = body->getCPShapes();
        for ( auto shape : shapes )
        {
            cpSpaceRemoveShape(cpGetSpace(_cpSpace), shape);
        }
    }
    _rigidbodies.clear();
}


void PhysicsWorld::removeStaticBody(PhysicsRigidBody* body, bool destroy )
{
    const std::set<cpShape*>& shapes = body->getCPShapes();
    for ( auto shape : shapes )
    {
        cpSpaceRemoveShape(cpGetSpace(_cpSpace), shape );
    }
    
    _staticbodies.eraseObject(body);
}

void PhysicsWorld::removeStaticBody(int tag, bool destroy )
{
    for (auto& body : _staticbodies)
    {
        if (body->getTag() == tag)
        {
            removeStaticBody(body, destroy);
            return;
        }
    }
}

void PhysicsWorld::removeAllStaticBodies()
{
    for ( auto body : _staticbodies )
    {
        const std::set<cpShape*>& shapes = body->getCPShapes();
        for ( auto shape : shapes )
        {
            cpSpaceRemoveShape(cpGetSpace(_cpSpace), shape );
        }
    }
    _staticbodies.clear();
}


void PhysicsWorld::removeAllSoftBodiesActions()
{
    for (auto& child : _softbodies)
    {
        child->stopAllActions();
    }
}

void PhysicsWorld::removeAllBeamBodiesActions()
{
    for (auto& child : _beambodies)
    {
        child->stopAllActions();
    }
}

void PhysicsWorld::removeAllRigidBodiesActions()
{
    for (auto& child : _rigidbodies)
    {
        child->stopAllActions();
    }
}


void PhysicsWorld::setDebugDrawMask(int mask)
{
    if (mask == DEBUGDRAW_NONE)
    {
        if (mask == DEBUGDRAW_NONE)
        {
            if ( _debugDraw && _debugDraw->_physicsLabel )
                _scene->removeChild(_debugDraw->_physicsLabel);
            
//            CC_SAFE_DELETE(_debugDraw);
        }
    }
    else if ( mask & DEBUGDRAW_STATISTICS )
    {
        _scene->addChild(_debugDraw->_physicsLabel);
    }
    
    _debugDrawMask = mask;
}

const Vector<PhysicsBody*>& PhysicsWorld::getAllBodies() const
{
    return _bodies;
}

PhysicsBody* PhysicsWorld::getBody(int tag) const
{
    for (auto& body : _bodies)
    {
        if (body->getTag() == tag)
        {
            return body;
        }
    }
    
    return nullptr;
}

void PhysicsWorld::setGravity(const Vec2& gravity)
{
    _gravity = gravity;
    cpSpaceSetGravity(cpGetSpace(_cpSpace), PhysicsHelper::point2cpv(gravity));
}

void PhysicsWorld::setSubsteps(int steps)
{
    if(steps > 0)
    {
        _substeps = steps;
        
        _dt = Director::getInstance()->getAnimationInterval() / _substeps;
        _invdt = 1.0 / _dt;
        
//        _physicsStats.setSubSteps(steps);
        
//        if (steps > 1)
//        {
//          _updateRate = 1;
//        }
    }
}

void PhysicsWorld::step(float delta)
{
    if (_autoStep)
    {
        CCLOG("Physics Warning: You need to close auto step( setAutoStep(false) ) first");
    }
    else
    {
        update(delta, true);
    }
}

void PhysicsWorld::update(float delta, bool userCall/* = false*/)
{
    static const double anim_dt = Director::getInstance()->getAnimationInterval();
    
//  Fede:moved this before adding object because of the static object problem
    auto sceneToWorldTransform = _scene->getNodeToParentTransform();
    beforeSimulation(_scene, sceneToWorldTransform, 1.f, 1.f, 0.f);
    
    if(!_delayAddBodies.empty())
    {
        updateBodies();
    }
    else if (!_delayRemoveBodies.empty())
    {
        updateBodies();
    }
    
    

    if (!_delayAddJoints.empty() || !_delayRemoveJoints.empty())
    {
        updateJoints();
    }
    
//    if (delta < FLT_EPSILON)
//    {
//        return;
//    }
//    
//    if (userCall)
//    {
//        cpSpaceStep(_cpSpace, delta);
//        for (auto& body : _bodies)
//        {
//            body->update(delta);
//        }
//    }
//    else
//    {
//        _updateTime += delta;
//        if (++_updateRateCount >= _updateRate)
        {
//            const float dt = anim_dt / _substeps; // _updateTime * _speed / _substeps;
//            for (int i = 0; i < _substeps; ++i)
//            {
//                cpSpaceStep(_cpSpace, dt);
//                for (auto& body : _bodies)
//                {
//                    body->update(dt);
//                }
//            }
            
            if (_debugDrawMask & DEBUGDRAW_STATISTICS )
            {
                cpSpaceStats stats;
                for (int i = 0; i < _substeps; ++i)
                {
                    cpSpaceSoftBodyStep2Stats(_cpSpace, _dt, &stats);
                    
                    for (auto& body : _bodies)
                    {
                        body->update(anim_dt);
                    }
                    
                    _physicsStats.updateTime(&stats);
                }
            }
            else
            {
                
                for (int i = 0; i < _substeps; ++i)
                {
                    cpSpaceSoftBodyStep2Stats(_cpSpace, _dt, nullptr);
                    
                    for (auto& body : _bodies)
                    {
                        body->update(anim_dt);
                    }
                    
                }
            }
            
//            _updateRateCount = 0;
//            _updateTime = 0.0f;
        }
//    }
    
    if (_debugDrawMask != DEBUGDRAW_NONE)
    {
        debugDraw(delta);
    }

    // Update physics position, should loop as the same sequence as node tree.
    // PhysicsWorld::afterSimulation() will depend on the sequence.
    afterSimulation(_scene, sceneToWorldTransform, 0.f);
}


void PhysicsWorld::reset()
{
    cleanup();
    
    removeAllJoints();
    removeAllSoftBodies();
    removeAllBeamBodies();
    removeAllRigidBodies();
    removeAllStaticBodies();
    removeAllBodies();
    
}

void PhysicsWorld::cleanup()
{
    
    // actions
    removeAllSoftBodiesActions();
    removeAllBeamBodiesActions();
    removeAllRigidBodiesActions();

//    _physicsWorld->stopAllActions();
//    _physicsWorld->unscheduleAllCallbacks();
    
}

void PhysicsWorld::setCollisionBias(const float collBias)
{
    cpSpaceSetCollisionBias(cpGetSpace(_cpSpace), PhysicsHelper::float2cpfloat(collBias));
}

void PhysicsWorld::setCollisionSlop(const float collSlop)
{
    cpSpaceSetCollisionSlop(cpGetSpace(_cpSpace), PhysicsHelper::float2cpfloat(collSlop));
}

void PhysicsWorld::setIterations(const unsigned int iterations)
{
    cpSpaceSetIterations(cpGetSpace(_cpSpace), iterations);
}
void PhysicsWorld::setSleepTimeThreshold(const float timeThreshold)
{
    cpSpaceSetSleepTimeThreshold(cpGetSpace(_cpSpace), PhysicsHelper::float2cpfloat(timeThreshold));
}
void PhysicsWorld::setIdleSpeedThreshold(const float speedThreshold)
{
    cpSpaceSetIdleSpeedThreshold(cpGetSpace(_cpSpace), PhysicsHelper::float2cpfloat(speedThreshold));
}


PhysicsWorld* PhysicsWorld::construct(Scene& scene)
{
    PhysicsWorld * world = new (std::nothrow) PhysicsWorld();
    if (world && world->init(scene))
    {
//        world->_scene = scene;
        world->_eventDispatcher = scene.getEventDispatcher();
        return world;
    }

    CC_SAFE_DELETE(world);
    return nullptr;
}



int  PhysicsWorld::_debugDrawMask = DEBUGDRAW_NONE;


PhysicsWorld::PhysicsWorld()
: _gravity(Vec2(0.0f, -98.0f))
, _speed(1.0f)
, _updateRate(1)
, _updateRateCount(0)
, _updateTime(0.0f)
, _substeps(1)
, _cpSpace(nullptr)
, _updateBodyTransform(false)
, _scene(nullptr)
, _autoStep(true)
, _debugDraw(nullptr)
//, _debugDrawMask(DEBUGDRAW_NONE)
, _eventDispatcher(nullptr)
{
    _dt = Director::getInstance()->getAnimationInterval() / _substeps;
    _invdt = 1.0 / _dt;
    
}

PhysicsWorld::~PhysicsWorld()
{
//    removeAllJoints(true);
    for (auto joint : _joints)
    {
        joint->_world = nullptr;
    }
    _joints.clear();
    
//    removeAllBodies();
    for (auto& body : _bodies)
    {
        body->_world = nullptr;
    }
    _bodies.clear();
    
    for (auto& body : _softbodies)
    {
        body->_world = nullptr;
    }
    _softbodies.clear();
    
    
    if (_cpSpace)
    {
        cpSpaceSoftBodyFree(_cpSpace);
    }
    
    CC_SAFE_DELETE(_debugDraw);
    
}


//cpBody* PhysicsWorld::getStaticPhysicsBody() const 
//{
//    cpSpaceGetStaticBody( cpGetSpace(_cpSpace) );
//}

void PhysicsWorld::beforeSimulation(Node *node, const Mat4& parentToWorldTransform, float nodeParentScaleX, float nodeParentScaleY, float parentRotation)
{
    auto scaleX = nodeParentScaleX * node->getScaleX();
    auto scaleY = nodeParentScaleY * node->getScaleY();
    auto rotation = parentRotation + node->getRotation();

    auto nodeToWorldTransform = parentToWorldTransform * node->getNodeToParentTransform();

    auto physicsBody = node->getPhysicsBody();
    if (physicsBody)
    {
        physicsBody->beforeSimulation(parentToWorldTransform, nodeToWorldTransform, scaleX, scaleY, rotation);
    }

    for (auto child : node->getChildren())
        beforeSimulation(child, nodeToWorldTransform, scaleX, scaleY, rotation);
}

void PhysicsWorld::afterSimulation(Node *node, const Mat4& parentToWorldTransform, float parentRotation)
{
    auto nodeToWorldTransform = parentToWorldTransform * node->getNodeToParentTransform();
    auto nodeRotation = parentRotation + node->getRotation();

    auto physicsBody = node->getPhysicsBody();
    if (physicsBody)
    {
        physicsBody->afterSimulation(parentToWorldTransform, parentRotation);
    }

    for (auto child : node->getChildren())
        afterSimulation(child, nodeToWorldTransform, nodeRotation);
}


PhysicsDebugDraw::PhysicsDebugDraw(PhysicsWorld& world)
: _drawNode(nullptr)
, _world(world)
, _physicsLabel(nullptr)
{
    _drawNode = DrawNode::create();
//    Director::getInstance()->getRunningScene()->addChild(_drawNode);
    _world.getScene().addChild(_drawNode);
    
    createStatsLabel();
}

PhysicsDebugDraw::~PhysicsDebugDraw()
{
    _drawNode->removeFromParent();
    _drawNode = nullptr;
}

bool PhysicsDebugDraw::begin()
{
    _drawNode->clear();
    return true;
}

void PhysicsDebugDraw::end()
{
}

void PhysicsDebugDraw::drawShape(PhysicsShape& shape)
{
    const Color4F fillColor(1.0f, 0.0f, 0.0f, 0.3f);
    const Color4F outlineColor(1.0f, 0.0f, 0.0f, 1.0f);
    
    for (auto it = shape._cpShapes.begin(); it != shape._cpShapes.end(); ++it)
    {
        cpShape *subShape = *it;
        
//        switch ((*it)->klass_private->type)
        {
            if ( cpShapeIsCircle(subShape) )
            {
                
                float radius = PhysicsHelper::cpfloat2float(cpCircleShapeGetRadius(subShape));
                Vec2 centre = PhysicsHelper::cpv2point(cpBodyGetPosition(cpShapeGetBody(subShape)));
                Vec2 offset = PhysicsHelper::cpv2point(cpCircleShapeGetOffset(subShape));
                Vec2 rotation(PhysicsHelper::cpv2point(cpBodyGetRotation(cpShapeGetBody(subShape))));
		              centre += offset.rotate(rotation);
                
                static const int CIRCLE_SEG_NUM = 12;
                Vec2 seg[CIRCLE_SEG_NUM] = {};
                
                for (int i = 0; i < CIRCLE_SEG_NUM; ++i)
                {
                    float angle = (float)i * M_PI / (float)CIRCLE_SEG_NUM * 2.0f;
                    Vec2 d(radius * cosf(angle), radius * sinf(angle));
                    seg[i] = centre + d;
                }
                _drawNode->drawPolygon(seg, CIRCLE_SEG_NUM, fillColor, 1, outlineColor);
//                break;
            }
            else if ( cpShapeIsSegment(subShape) ) //case CP_SEGMENT_SHAPE:
            {
//                cpSegmentShape *seg = (cpSegmentShape *)subShape;
                _drawNode->drawSegment(PhysicsHelper::cpv2point(cpSegmentShapeGetTransformedA(subShape)),
                                       PhysicsHelper::cpv2point(cpSegmentShapeGetTransformedB(subShape)),
                                       PhysicsHelper::cpfloat2float(cpSegmentShapeGetRadius(subShape)==0 ? 1 : cpSegmentShapeGetRadius(subShape)), outlineColor);
//                break;
            }
            else if (cpShapeIsPoly(subShape)) //case CP_POLY_SHAPE:
            {
//                cpPolyShape* poly = (cpPolyShape*)subShape;
                int num = cpPolyShapeGetCount(subShape);
                Vec2* seg = new (std::nothrow) Vec2[num];
                cpVect vert;
                
                for (int i=0; i<num; ++i)
                {
                    vert = cpPolyShapeGetTrasnformedVert(subShape,i);
                    PhysicsHelper::cpvs2points(&vert, &seg[i], 1);
                }
                
                _drawNode->drawPolygon(seg, num, fillColor, 1.0f, outlineColor);
                
                delete[] seg;
//                break;
            }
//            default:
//                break;
        }
    }
}

void PhysicsDebugDraw::drawJoint(PhysicsJoint& joint)
{
    const Color4F lineColor(0.0f, 0.0f, 1.0f, 1.0f);
    const Color4F jointPointColor(0.0f, 1.0f, 0.0f, 1.0f);
    
    for (auto it = joint._cpConstraints.begin(); it != joint._cpConstraints.end(); ++it)
    {
        cpConstraint *constraint = *it;
        
        
        cpBody *body_a = cpConstraintGetBodyA(constraint);
        cpBody *body_b = cpConstraintGetBodyB(constraint);
        
//        const cpConstraintClass *klass = constraint->klass_private;
        if( cpConstraintIsPinJoint(constraint) )
        {
//            cpPinJoint *subJoint = (cpPinJoint *)constraint;
            
            cpVect a = cpvadd(cpBodyGetPosition(body_a), cpvrotate(cpPinJointGetAnchorA(constraint), cpBodyGetRotation(body_a)));
            cpVect b = cpvadd(cpBodyGetPosition(body_b), cpvrotate(cpPinJointGetAnchorB(constraint), cpBodyGetRotation(body_b)));
            
            _drawNode->drawSegment(PhysicsHelper::cpv2point(a), PhysicsHelper::cpv2point(b), 1, lineColor);
            _drawNode->drawDot(PhysicsHelper::cpv2point(a), 2, jointPointColor);
            _drawNode->drawDot(PhysicsHelper::cpv2point(b), 2, jointPointColor);
        }
        else if(cpConstraintIsSlideJoint(constraint))
        {
//            cpSlideJoint *subJoint = (cpSlideJoint *)constraint;
            
            cpVect a = cpvadd(cpBodyGetPosition(body_a), cpvrotate(cpSlideJointGetAnchorA(constraint), cpBodyGetRotation(body_a)));
            cpVect b = cpvadd(cpBodyGetPosition(body_b), cpvrotate(cpSlideJointGetAnchorB(constraint), cpBodyGetRotation(body_b)));
            
            _drawNode->drawSegment(PhysicsHelper::cpv2point(a), PhysicsHelper::cpv2point(b), 1, lineColor);
            _drawNode->drawDot(PhysicsHelper::cpv2point(a), 2, jointPointColor);
            _drawNode->drawDot(PhysicsHelper::cpv2point(b), 2, jointPointColor);
        }
        else if(cpConstraintIsPivotJoint(constraint))
        {
            //            cpPivotJoint *subJoint = (cpPivotJoint *)constraint;
            
            cpVect a = cpvadd(cpBodyGetPosition(body_a), cpvrotate(cpPivotJointGetAnchorA(constraint), cpBodyGetRotation(body_a)));
            cpVect b = cpvadd(cpBodyGetPosition(body_b), cpvrotate(cpPivotJointGetAnchorB(constraint), cpBodyGetRotation(body_b)));
            
            _drawNode->drawDot(PhysicsHelper::cpv2point(a), 2, jointPointColor);
            _drawNode->drawDot(PhysicsHelper::cpv2point(b), 2, jointPointColor);
        }
        else if(cpConstraintIsGrooveJoint(constraint))
        {
            //            cpGrooveJoint *subJoint = (cpGrooveJoint *)constraint;
            
            cpVect a = cpvadd(cpBodyGetPosition(body_a), cpvrotate(cpGrooveJointGetGrooveA(constraint), cpBodyGetRotation(body_a)));
            cpVect b = cpvadd(cpBodyGetPosition(body_a), cpvrotate(cpGrooveJointGetGrooveB(constraint), cpBodyGetRotation(body_a)));
            cpVect c = cpvadd(cpBodyGetPosition(body_b), cpvrotate(cpGrooveJointGetAnchorB(constraint), cpBodyGetRotation(body_b)));
            
            _drawNode->drawSegment(PhysicsHelper::cpv2point(a), PhysicsHelper::cpv2point(b), 1, lineColor);
            _drawNode->drawDot(PhysicsHelper::cpv2point(c), 2, jointPointColor);
        }
        else if(cpConstraintIsDampedSpring(constraint))
        {
            //            cpDampedSpring *subJoint = (cpDampedSpring *)constraint;
            
            cpVect a = cpvadd(cpBodyGetPosition(body_a), cpvrotate(cpDampedSpringGetAnchorA(constraint), cpBodyGetRotation(body_a)));
            cpVect b = cpvadd(cpBodyGetPosition(body_b), cpvrotate(cpDampedSpringGetAnchorB(constraint), cpBodyGetRotation(body_b)));
            
            _drawNode->drawSegment(PhysicsHelper::cpv2point(a), PhysicsHelper::cpv2point(b), 1, lineColor);
            _drawNode->drawDot(PhysicsHelper::cpv2point(a), 2, jointPointColor);
            _drawNode->drawDot(PhysicsHelper::cpv2point(b), 2, jointPointColor);
        }

    }
}

void PhysicsDebugDraw::drawContact()
{
    
}

void PhysicsDebugDraw::drawStatistics(const PhysicsStatistics* physicsStats)
{
    
    char buffer[256];
    sprintf(buffer, " nodes: %d  elements: %d \n arbiters: %d   contacts: %d \n collisionTests: %d \n assemble: %.1f / %.1f%% \n solve: %.1f / %.1f%% \n broad: %.1f / %.1f%% \n narrow: %.1f / %.1f%% \n graphs: %.1f / %.1f%% \n contacts: %.1f / %.1f%% \n time: %.1f",
            physicsStats->_numNodes, physicsStats->_numElements,
            physicsStats->_numArbiters, physicsStats->_numContacts,
            physicsStats->_numCollisionTests,
            physicsStats->_assemble[0], physicsStats->_assemble[1],
            physicsStats->_solve[0], physicsStats->_solve[1],
            physicsStats->_broadphase[0], physicsStats->_broadphase[1],
            physicsStats->_narrowphase[0], physicsStats->_narrowphase[1],
            physicsStats->_graphs[0], physicsStats->_graphs[1],
            physicsStats->_contacts[0], physicsStats->_contacts[1],
            physicsStats->_time[0]
            );
    
    _physicsLabel->setString(buffer);
    
}

void PhysicsDebugDraw::createStatsLabel()
{
    std::string physString = "nodes/triangles: 0 / 0 \n assemble: 00.0 / 00.0 \n solve: 00.0 / 00.0\n broad: 00.0 / 00.0\n narrow: 00.0 / 00.0 \n graphs: 00.0 / 00.0 \n contacts: 00.0 / 00.0 \n time 00.0f";
    
    if (_physicsLabel)
    {
        physString = _physicsLabel->getString();
        CC_SAFE_RELEASE_NULL(_physicsLabel);
    }
    
    
    //    cocos2d::Texture2D* texture = Director::getInstance()->getTextureCache()->getTextureForKey("/cc_fps_images");
    
    _physicsLabel = Label::createWithTTF("fonts/arial.ttf", physString);
    _physicsLabel->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    _physicsLabel->retain();
    
    /*
     We want to use an image which is stored in the file named ccFPSImage.c
     for any design resolutions and all resource resolutions.
     
     To achieve this, we need to ignore 'contentScaleFactor' in 'AtlasNode' and 'LabelAtlas'.
     So I added a new method called 'setIgnoreContentScaleFactor' for 'AtlasNode',
     this is not exposed to game developers, it's only used for displaying FPS now.
     */
    //    float scaleFactor = 1.6 / CC_CONTENT_SCALE_FACTOR();
    //
    //    _physicsLabel = LabelAtlas::create();
    //    _physicsLabel->retain();
    //    _physicsLabel->setIgnoreContentScaleFactor(true);
    //    _physicsLabel->initWithString(physString, texture, 12, 32 , '.');
    _physicsLabel->setScale(1.0);
    
    
    //    Texture2D::setDefaultAlphaPixelFormat(currentFormat);
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 visibleOrigin = Director::getInstance()->getVisibleOrigin();
    
    const int height_spacing = 24 / CC_CONTENT_SCALE_FACTOR();
    //    _drawnVerticesLabel->setPosition(Vec2(0, height_spacing*2) + CC_DIRECTOR_STATS_POSITION);
    //    _drawnBatchesLabel->setPosition(Vec2(0, height_spacing*1) + CC_DIRECTOR_STATS_POSITION);
    _physicsLabel->setPosition(Vec2(0, -height_spacing)+ visibleOrigin + Vec2(0, visibleSize.height) );
    
    //    _physicsLabel->setPosition(Vec2(VisibleRect::left().x, VisibleRect::top().y -height_spacing );
    
    Vec2 leftTop(visibleOrigin.x, visibleOrigin.y+visibleSize.height);
    
    _physicsLabel->setPosition(leftTop);
}


NS_CC_END

#endif // CC_USE_PHYSICS
