//
//  CCPhysicsRigidShape.cpp
//  cocos2d_libs
//
//  Created by Federico Spadoni on 19/02/16.
//
//

#include "CCPhysicsRigidShape.h"
#include "physics-utils/CCPhysicsHelper.h"

#include "chipmunk.h"


NS_CC_BEGIN


cpShape* PhysicsRigidShape::createCircle(float radius, float elasticity, float friction)
{
    auto shape = cpCircleShapeNew(nullptr, radius, cpvzero);
    cpShapeSetElasticity(shape, elasticity );
	cpShapeSetFriction(shape, friction );
    return shape;
}



cpShape* PhysicsRigidShape::createPoly(const Vec2* points, int count, float elasticity, float friction )
{
//    auto vecs = new cpVect[count];
    cpVect *vecs = (cpVect *)allocatePool(count*sizeof(cpVect));
    PhysicsHelper::points2cpvs(points, vecs, count);
    cpShape* shape = cpPolyShapeNew(nullptr, count, vecs, cpTransformIdentity, 0.0);
    freePool(vecs, count*sizeof(cpVect));
    
    cpShapeSetElasticity(shape, elasticity );
	cpShapeSetFriction(shape, friction );
    
    return shape;
}


cpShape* PhysicsRigidShape::createBox(const Size& size, float elasticity, float friction)
{
//    auto wh = PhysicsHelper::size2cpv(size);
//    cpVect vec[4] =
//    {
//        {-wh.x/2.0f, -wh.y/2.0f}, {-wh.x/2.0f, wh.y/2.0f}, {wh.x/2.0f, wh.y/2.0f}, {wh.x/2.0f, -wh.y/2.0f}
//    };
    
//    auto shape = cpPolyShapeNew(nullptr, 4, vec, cpTransformIdentity, 0.0);
    auto shape = cpBoxShapeNew(nullptr, size.width, size.height, 0.0 );
    cpShapeSetElasticity(shape, elasticity );
	cpShapeSetFriction(shape, friction );
    return shape;

}


cpShape* PhysicsRigidShape::createEdgeSegmnt(const Vec2& a, const Vec2& b, float border, float elasticity, float friction )
{
    cpShape* shape = cpSegmentShapeNew(nullptr,
                                   PhysicsHelper::point2cpv(a),
                                   PhysicsHelper::point2cpv(b),
                                   border);
    
    cpShapeSetElasticity(shape, elasticity );
	cpShapeSetFriction(shape, friction );
    return shape;
}

std::vector<cpShape*> PhysicsRigidShape::createEdgePoly(const Vec2* points, int count,  float border, float elasticity, float friction)
{

    std::vector<cpShape*> shapes;
    shapes.reserve(count);
    
    cpVect *vec = (cpVect *)allocatePool(count*sizeof(cpVect));
    PhysicsHelper::points2cpvs(points, vec, count);
    
    int i = 0;
    for (; i < count; ++i)
    {
        auto shape = cpSegmentShapeNew(nullptr, vec[i], vec[(i + 1) % count], border);
        
        CC_BREAK_IF(shape == nullptr);
        
        cpShapeSetElasticity(shape, elasticity );
        cpShapeSetFriction(shape, friction );
        
        shapes.push_back(shape);
    }
    
    freePool(vec, count*sizeof(cpVect));
    
    return shapes;
}

std::vector<cpShape*> PhysicsRigidShape::createEdgeBox(const Size& size, float border, float elasticity, float friction)
{
    std::vector<cpShape*> shapes;
    shapes.reserve(4);
    
    cpVect vec[4] = {};
    vec[0] = PhysicsHelper::point2cpv(Vec2(-size.width/2, -size.height/2 ));
    vec[1] = PhysicsHelper::point2cpv(Vec2(+size.width/2, -size.height/2 ));
    vec[2] = PhysicsHelper::point2cpv(Vec2(+size.width/2, +size.height/2 ));
    vec[3] = PhysicsHelper::point2cpv(Vec2(-size.width/2, +size.height/2 ));
    
    int i = 0;
    for (; i < 4; ++i)
    {
        //            auto shape = cpSegmentShapeNew(s_sharedBody, vec[i], vec[(i + 1) % 4], border);
        auto shape = cpSegmentShapeNew(nullptr, vec[i], vec[(i + 1) % 4], border);
        CC_BREAK_IF(shape == nullptr);
        
        cpShapeSetElasticity(shape, elasticity );
        cpShapeSetFriction(shape, friction );
        
        shapes.push_back(shape);
    }
    
    return shapes;
}


std::vector<cpShape*> PhysicsRigidShape::createEdgeChain(const Vec2* points, int count, float border, float elasticity, float friction)
{
    std::vector<cpShape*> shapes;
    shapes.reserve(4);
    
    cpVect *vec = (cpVect *)allocatePool(count*sizeof(cpVect));
    PhysicsHelper::points2cpvs(points, vec, count);
    
    int i = 0;
    for (; i < count - 1; ++i)
    {
        //            auto shape = cpSegmentShapeNew(s_sharedBody, vec[i], vec[i + 1], border);
        auto shape = cpSegmentShapeNew(nullptr, vec[i], vec[i + 1], border);
        CC_BREAK_IF(shape == nullptr);
        cpShapeSetElasticity(shape, elasticity );
        cpShapeSetFriction(shape, friction );
        
        shapes.push_back(shape);
    }

    freePool(vec, count*sizeof(cpVect));
    
    return shapes;
}


NS_CC_END