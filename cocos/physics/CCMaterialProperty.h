//
//  CCMaterialProperty.h
//  cocos2d_libs
//
//  Created by Federico Spadoni on 10/11/14.
//
//

#ifndef cocos2d_libs_CCMaterialProperty_h
#define cocos2d_libs_CCMaterialProperty_h


#include "base/CCRef.h"
#include "base/ccTypes.h"
//#include "base/CCVector.h"

#include "physics-utils/CCMemoryBuffer.h"
#include "math/Vec2.h"

#include <vector>

NS_CC_BEGIN

class CC_DLL SoftBodyMaterial : public cocos2d::Ref
{
    
    class Material
    {
    public:
        
        std::string _name;
        
        double _density; // [Kg/m^3]
        
        double _youngModulus;	// [N/m^2]
        
        double _poissonRatio; // []
        
        double _friction;
        
        double _stiffnessDamping; // rayleigh stiffness damping
        
        double _massDamping; // rayleigh mass damping
        
        double _contactFriction;
        
        double _contactElasticity;
        
        double _toughness;
        
        double _yield;
        
        double _maxYield;
        
        double _creep;
        
        double _cosAngleFracture;
        
        double _areaFractureMinRatio; // 0 < x < 1
        
        double _areaFractureMaxRatio; // x > 1
        
        
        Material();
        
    };
    
public:
    
    static SoftBodyMaterial* create();
    
    static SoftBodyMaterial* createFromFile(const std::string& filename);
    
    const std::string& getTextureFileName() const { return _textureName; }
    
    const std::string& getName() const {return _name;}
    
    const std::vector<Material>& getMaterials() const { return _materials; }
    
    
protected:
    
    SoftBodyMaterial(const std::string& name = "");
    virtual ~SoftBodyMaterial();
    
    bool init();
    bool initFromFile(const std::string& filename);
    
    bool loadFromFile(const std::string& filename);
    
    void setColor(const std::string& color);
    
    void setColorFromRGBA(const std::string& color);
    
private:
    
    std::string _name;
    
    std::string _textureName;
    
    Color4F _color;
    

    
    std::vector<Material> _materials;
    
    
    friend class PhysicsSoftBody;
    
};


class CC_DLL BeamBodyMaterial : public cocos2d::Ref
{
public:
    
    static BeamBodyMaterial* create();
    
    static BeamBodyMaterial* createFromFile(const std::string& filename);
    
    const std::string& getTextureFileName() const { return _textureName; }
    
    const std::string& getName() const {return _name;}
    
    bool isTextureRepeat() const {return _textureRepeat;}
    
protected:
    
    BeamBodyMaterial(const std::string& name = "");
    virtual ~BeamBodyMaterial();
    
    bool init();
    bool initFromFile(const std::string& filename);
    
    bool loadFromFile(const std::string& filename);
    
    void setColor(const std::string& color);
    
    void setColorFromRGBA(const std::string& color);
    
private:
    
    std::string _name;
    
    std::string _textureName;
    
    Color4F _color;
    
    
    double _radius;
    
    double _density; // [Kg/m^3]
    
    double _youngModulusAxial;// [N/m^2]
    
    double _youngModulusTransverse; // [N/m^2]
    
    double _friction;
    
    double _stiffnessDamping; // rayleigh stiffness damping
    
    double _massDamping; // rayleigh mass damping
    
    double _contactFriction;
    
    double _contactElasticity;
    
    double _axialToughness;
    
    double _transverseToughness;
    
    double _yield;
    
    double _maxYield;
    
    double _creep;
    

    bool _textureRepeat;
    
    
    friend class PhysicsBeamBody;
    
};


NS_CC_END

#endif
