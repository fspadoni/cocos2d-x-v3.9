//
//  CCPhysicsSoftBody.cpp
//  cocos2d_libs
//
//  Created by Federico Spadoni on 11/11/14.
//
//
#include "physics/CCPhysicsSoftBody.h"
#include "physics/CCPhysicsSoftBody.inl"

#include "physics/CCMaterialProperty.h"
#include "physics/CCPhysicsWorld.h"
#include "base/ccTypes.h"

#include "physics-render/CCSoftBodyDrawNode.h"
#include "physics/CCPhysicsType.h"

#include "chipmunk.h"


NS_CC_BEGIN

class PhysicsSoftBodyCallback
{
public:
    static void sleepCallback(cpSoftBodyOpt* body, PhysicsSoftBody* softbody);
    static void breakElementCallback(cpSoftBodyOpt* body, cpIdx triangleIdx, PhysicsSoftBody* softbody);
    static void breakAllElementCallback(cpSoftBodyOpt* body, PhysicsSoftBody* softbody);
    
    static void collisionBeginCallback(cpSoftBodyOpt* body, cpBody* collidingBody, cpArbiter* arb, void*);
    static void collisionSeparateCallback(cpSoftBodyOpt* body, cpBody* collidingBody, cpArbiter* arb, void*);

};

void PhysicsSoftBodyCallback::sleepCallback(cpSoftBodyOpt* body, PhysicsSoftBody* softbody)
{
//    if ( softbody->_sleepBeginCallback )
        softbody->sleepBegin();
}

void PhysicsSoftBodyCallback::breakElementCallback(cpSoftBodyOpt* body, cpIdx triangleIdx, PhysicsSoftBody* softbody)
{
//    if ( softbody->_breakElementCallback )
        softbody->breakElement(triangleIdx);
}

void PhysicsSoftBodyCallback::breakAllElementCallback(cpSoftBodyOpt* body, PhysicsSoftBody* softbody)
{
//    if ( softbody->breakAllCallback )
    {
        
        softbody->breakAllElement();
        
        if (softbody->getDrawNode() )
        {
            softbody->getDrawNode()->removeSoftBody(softbody);
        }
        
        softbody->destroy();
//        softbody->removeFromWorld();
    }
}

void PhysicsSoftBodyCallback::collisionBeginCallback(cpSoftBodyOpt* body, cpBody* collidingBody, cpArbiter* arb, void*)
{
    PhysicsSoftBody* softbody =  static_cast<PhysicsSoftBody*>(cpSoftBodyOptGetUserData(body));
    
    softbody->collisionBegin(collidingBody, arb);
}


void PhysicsSoftBodyCallback::collisionSeparateCallback(cpSoftBodyOpt* body, cpBody* collidingBody, cpArbiter* arb, void*)
{
    PhysicsSoftBody* softbody =  static_cast<PhysicsSoftBody*>(cpSoftBodyOptGetUserData(body));
    
    softbody->collisionSeparate(collidingBody, arb);
}



PhysicsSoftBody* PhysicsSoftBody::createFromTriangularMesh(TriangularMesh* mesh, SoftBodyMaterial* material)
{
    PhysicsSoftBody* softbody = new (std::nothrow) PhysicsSoftBody();
    
    if (softbody && softbody->init(mesh, material) )
    {
        softbody->autorelease();
    }
    else{
        CC_SAFE_RELEASE(softbody);
        return nullptr;
    }
    
    return softbody;
}

PhysicsSoftBody* PhysicsSoftBody::create(const unsigned int numNodes, const unsigned int numElems, const unsigned int graphSize, const unsigned int numMaterials)
{
    PhysicsSoftBody* softbody = new (std::nothrow) PhysicsSoftBody();
    
    if (softbody && softbody->init(numNodes, numElems, graphSize, numMaterials) )
    {
        softbody->autorelease();
    }
    else{
        CC_SAFE_RELEASE(softbody);
        return nullptr;
    }
    
    return softbody;
}

PhysicsSoftBody* PhysicsSoftBody::clone(const PhysicsSoftBody* other)
{
    PhysicsSoftBody* softbody = new (std::nothrow) PhysicsSoftBody();
    
    if (softbody && softbody->copy(other) )
    {
        softbody->autorelease();
    }
    else{
        CC_SAFE_RELEASE(softbody);
        return nullptr;
    }
    
    return softbody;
}



PhysicsSoftBody::PhysicsSoftBody()
: PhysicsObject()
, _materials(nullptr)
, _mesh(nullptr)
, _shape(nullptr)
, _softbody(nullptr)
//, _node(nullptr)
, _enabled(true)
, _drawNode(nullptr)
//, _sleepBeginCallback(nullptr)
//, _breakElementCallback(nullptr)
//, _breakAllCallback(nullptr)
//, _initialCoG()
, _currentRotation(0.0)
, _tag(0)
, _color(Color4F::GREEN)
{
//    CCLOG("%p PhysicsSoftBody::create", this);
}

PhysicsSoftBody::~PhysicsSoftBody()
{
//    if (_softbody)
//    {
//        if ( cpSpaceSoftBody* world = cpSoftBodyOptGetSpace(_softbody) )
//        {
//            cpSpaceRemoveSoftBodyOpt(world, _softbody );
//        }
//        cpSoftBodyOptFree( _softbody );
//        _softbody = nullptr;
//    }
//    _softbody = nullptr;
    
    if (_mesh != nullptr )
    {
        _mesh->release();
        _mesh = nullptr;
    }
    
    if (_materials != nullptr )
    {
        _materials->release();
        _materials = nullptr;
    }
 
//    CCLOG("%p PhysicsSoftBody::~PhysicsSoftBody", this);
}


void PhysicsSoftBody::destroy()
{
    if (_softbody)
    {
        removeFromWorld();
        
        if ( cpSpaceSoftBody* world = cpSoftBodyOptGetSpace(_softbody) )
        {
            cpSpaceRemoveSoftBodyOpt(world, _softbody );
        }
        cpSoftBodyOptFree( _softbody );
        
        
//        _softbody = nullptr;
    }
}

bool PhysicsSoftBody::init(TriangularMesh* mesh, SoftBodyMaterial* materials)
{
//    CCLOG("%p PhysicsSoftBody::init", this);
    
    _numNodes = mesh->getNumNodes();
    _numElems = mesh->getNumTriangles();
    _graphSize = mesh->getGraphSize();
    _numMaterials = mesh->getNumMaterials();
    
    if ( mesh->getNumMaterials() == 0 )
    {
        _numMaterials = 1;
    }
        
    ssize_t memSize = cpSoftBodyShapeOptComputeMemorySize(_numNodes, _numElems, _graphSize);
    memSize += cpSoftBodyOptComputeMemorySize(_numNodes, _numElems, _graphSize, _numMaterials);

    _data = PoolBuffer<unsigned char>::create(memSize);
    
    cpMeshBuffer meshbuf = cpMeshBufferInitFromBuffer(_numNodes, _numElems, _graphSize ,_numMaterials, _data->lockForRead<void>(), _data->getSizeInBytes());
    
    // fill cpMeshBuffer struct
    const Vec2* source = mesh->_nodes->lockForRead();
    cpVect* dest = meshbuf.vertices;
    for (int i=0; i<_numNodes; ++i)
    {
        dest->x = source->x;
        dest->y = source->y;
        ++dest;
        ++source;
    }
    mesh->_nodes->unlock();
    
    memcpy(meshbuf.triangles, mesh->_triangles->lockForRead<void>(), mesh->_triangles->getSizeInBytes());
    memcpy(meshbuf.rows, mesh->_rows->lockForRead<void>(), mesh->_rows->getSizeInBytes());
    memcpy(meshbuf.columns, mesh->_columns->lockForRead<void>(), mesh->_columns->getSizeInBytes());
    memcpy(meshbuf.diagonal, mesh->_diagonal->lockForRead<void>(), mesh->_diagonal->getSizeInBytes());

//    create shape
    this->_shape = cpSoftBodyShapeOptInitFromMeshBuffer( &meshbuf, 4.0 );
    
    this->_mesh = mesh;
    _mesh->retain();
    
    this->_materials = materials;
    _materials->retain();
    
//    materials names check!!!
    
    
    PoolBufferSharedPtr<cpMaterial> materialTempBuffer;
    
    
    if ( mesh->getNumMaterials() == 0 )
    {
//      no materials define in the .ele file. set all elemenents the same material (first in the materials)
        
        materialTempBuffer =  PoolBuffer<cpMaterial>::create();
        cpMaterial* materialbuf = materialTempBuffer->lockForWrite();
        
//      fill the materials in the right order        
        auto material = materials->getMaterials()[0];
        
        materialbuf->density = static_cast<cpFloat>(material._density);
        materialbuf->youngModulus = static_cast<cpFloat>(material._youngModulus);
        materialbuf->poissonRatio = static_cast<cpFloat>(material._poissonRatio);
        materialbuf->friction = static_cast<cpFloat>(material._friction);
        materialbuf->stiffnessDamping = static_cast<cpFloat>(material._stiffnessDamping);
        materialbuf->massDamping = static_cast<cpFloat>(material._massDamping);
        materialbuf->contactFriction = static_cast<cpFloat>(material._contactFriction);
        materialbuf->contactElasticity = static_cast<cpFloat>(material._contactElasticity);
        materialbuf->yield = static_cast<cpFloat>(material._yield);
        materialbuf->maxYield = static_cast<cpFloat>(material._maxYield);
        materialbuf->creep = static_cast<cpFloat>(material._creep);
        materialbuf->toughness = static_cast<cpFloat>(material._toughness);
        materialbuf->cosAngleFracture = static_cast<cpFloat>(material._cosAngleFracture);
        materialbuf->areaFractureMinRatio = static_cast<cpFloat>(material._areaFractureMinRatio);
        materialbuf->areaFractureMaxRatio = static_cast<cpFloat>(material._areaFractureMaxRatio);
        
        
        materialTempBuffer->unlock();
        
    }
    else
    {
        
        const std::map<std::string, unsigned int>& meshMaterials = mesh->getMaterials();
        
        for ( auto&& material : materials->getMaterials() )
        {
            CCASSERT( meshMaterials.find(material._name) != meshMaterials.end(), "FATAL: material name don't match ");
            
        }
        
        materialTempBuffer =  PoolBuffer<cpMaterial>::create(_numMaterials);
        cpMaterial* materialbuf = materialTempBuffer->lockForWrite();
        
        //    fill the materials in the right order
        for ( auto&& material : materials->getMaterials() )
        {
            const std::map<std::string, unsigned int>::const_iterator matIter = meshMaterials.find(material._name);
            
            if ( matIter !=  meshMaterials.end() )
            {
                const unsigned int matIndex = matIter->second;
                materialbuf[matIndex].density = static_cast<cpFloat>(material._density);
                materialbuf[matIndex].youngModulus = static_cast<cpFloat>(material._youngModulus);
                materialbuf[matIndex].poissonRatio = static_cast<cpFloat>(material._poissonRatio);
                materialbuf[matIndex].friction = static_cast<cpFloat>(material._friction);
                materialbuf[matIndex].stiffnessDamping = static_cast<cpFloat>(material._stiffnessDamping);
                materialbuf[matIndex].massDamping = static_cast<cpFloat>(material._massDamping);
                materialbuf[matIndex].contactFriction = static_cast<cpFloat>(material._contactFriction);
                materialbuf[matIndex].contactElasticity = static_cast<cpFloat>(material._contactElasticity);
                materialbuf[matIndex].yield = static_cast<cpFloat>(material._yield);
                materialbuf[matIndex].maxYield = static_cast<cpFloat>(material._maxYield);
                materialbuf[matIndex].creep = static_cast<cpFloat>(material._creep);
                materialbuf[matIndex].toughness = static_cast<cpFloat>(material._toughness);
                materialbuf[matIndex].cosAngleFracture = static_cast<cpFloat>(material._cosAngleFracture);
                materialbuf[matIndex].areaFractureMinRatio = static_cast<cpFloat>(material._areaFractureMinRatio);
                materialbuf[matIndex].areaFractureMaxRatio = static_cast<cpFloat>(material._areaFractureMaxRatio);
            }
        }
        
        materialTempBuffer->unlock();
    }
    
//    cpMaterial mat = {
//        .density = static_cast<cpFloat>(material->_density),
//        .youngModulus = static_cast<cpFloat>(material->_youngModulus),
//        .poissonRatio = static_cast<cpFloat>(material->_poissonRatio),
//        .friction = static_cast<cpFloat>(material->_friction),
//        .stiffnessDamping = static_cast<cpFloat>(material->_stiffnessDamping),
//        .massDamping = static_cast<cpFloat>(material->_massDamping),
//        .contactFriction = static_cast<cpFloat>(material->_contactFriction),
//        .contactElasticity = static_cast<cpFloat>(material->_contactElasticity),
//        .yield = static_cast<cpFloat>(material->_yield),
//        .maxYield = static_cast<cpFloat>(material->_maxYield),
//        .creep = static_cast<cpFloat>(material->_creep),
//        .toughness = static_cast<cpFloat>(material->_toughness),
//        .cosAngleFracture = static_cast<cpFloat>(material->_cosAngleFracture),
//        .areaFractureMinRatio = static_cast<cpFloat>(material->_areaFractureMinRatio),
//        .areaFractureMaxRatio = static_cast<cpFloat>(material->_areaFractureMaxRatio)
//    };

    
    cpSoftBodySolverParams params = {
        .position = cpvzero,
        .nbSolverIteration = 10,
        .solverTolerance = 1e-7,
        .constrNodesFile = 0,
    };
    
    if ( mesh->getNumConstrainedNodes() )
    {
        params.numConstrainedNodes = mesh->getNumConstrainedNodes();
        params.constrainedNodes = mesh->_constrainedNodes->lockForWrite();
    }
    
//    create softbody
    this->_softbody = cpSoftBodyOptInit(_shape, materialTempBuffer->lockForRead(), &params);
    cpSoftBodyOptSetUserData(_softbody, this);
    
//    activate plastic deformation and brakes
    setPlasticity(true);
    setBrakable(true);
    
    
    // callbacks
    cpSoftBodyOptSetSleepingCallback(_softbody, (cpSoftBodyOptSleepingCallback)PhysicsSoftBodyCallback::sleepCallback );
    cpSoftBodyOptSetTriangleDestroyCallback(_softbody, (cpSoftBodyOptTriangleDestroyCallback)PhysicsSoftBodyCallback::breakElementCallback);
    cpSoftBodyOptSetDestroyCallback(_softbody, (cpSoftBodyOptDestroyCallback)PhysicsSoftBodyCallback::breakAllElementCallback);
    
    
    _color = materials->_color;
    
    return true;
}

bool PhysicsSoftBody::init(const unsigned int numNodes, const unsigned int numElems, const unsigned graphSize, const unsigned int numMaterials)
{
    ssize_t memSize = cpSoftBodyShapeOptComputeMemorySize(numNodes, numElems, graphSize);
    memSize += cpSoftBodyOptComputeMemorySize(numNodes, numElems, graphSize, numMaterials);
    
    
    return true;
}


bool PhysicsSoftBody::copy(const PhysicsSoftBody* other)
{
    if (other == nullptr)
        return false;
    
     _data = PoolBuffer<unsigned char>::clone(other->_data.get());
    
    _numNodes = other->_numNodes;
    _numElems = other->_numElems;
    _graphSize = other->_graphSize;
    _numMaterials = other->_numMaterials;
    
    this->_mesh = other->_mesh;
    _mesh->retain();
    
    this->_materials = other->_materials;
    _materials->retain();
    
    cpMeshBuffer meshbuf = cpMeshBufferInitFromBuffer(_numNodes, _numElems, _graphSize ,_numMaterials, _data->lockForRead<void>(), _data->getSizeInBytes());
    
    _shape = cpSoftBodyShapeOptCopy( other->_shape, &meshbuf.buffer );

    //    create softbody
    this->_softbody = cpSoftBodyOptClone(_shape, other->_softbody);
    cpSoftBodyOptSetUserData(_softbody, this);
    
    // callbacks
//    cpSoftBodyOptSetSleepingCallback(_softbody, (cpSoftBodyOptSleepingCallback)PhysicsSoftBodyCallback::sleepCallback );
    cpSoftBodyOptSetTriangleDestroyCallback(_softbody, (cpSoftBodyOptTriangleDestroyCallback)PhysicsSoftBodyCallback::breakElementCallback);
    cpSoftBodyOptSetDestroyCallback(_softbody, (cpSoftBodyOptDestroyCallback)PhysicsSoftBodyCallback::breakAllElementCallback);
    
    
    _color = other->_color;
    
    return true;
}

void PhysicsSoftBody::removeFromWorld()
{
    if (_world)
    {
        _world->removeSoftBody(this);
    }
}


void PhysicsSoftBody::setEnable(bool enable)
{
    if (_enabled != enable)
    {
        _enabled = enable;
        
        if (_world)
        {
            if (enable)
            {
                _world->addSoftBodyOrDelay(this);
            }else
            {
                _world->removeSoftBodyOrDelay(this);
            }
        }
    }
}

void PhysicsSoftBody::setData(void* data)
{
    
    
    
}

void PhysicsSoftBody::notifyBreakElement(bool active)
{
    if ( active )
    {
        cpSoftBodyOptSetTriangleDestroyCallback(_softbody, (cpSoftBodyOptTriangleDestroyCallback)PhysicsSoftBodyCallback::breakElementCallback);

    }
    else
    {
        cpSoftBodyOptSetTriangleDestroyCallback(_softbody, nullptr);

    }
}

void PhysicsSoftBody::notifyBreak(bool active)
{
    if ( active )
    {
        cpSoftBodyOptSetDestroyCallback(_softbody, (cpSoftBodyOptDestroyCallback)PhysicsSoftBodyCallback::breakAllElementCallback);
        
    }
    else
    {
        cpSoftBodyOptSetDestroyCallback(_softbody,nullptr);
        
    }
}

void PhysicsSoftBody::notifySleep(bool active)
{
    if ( active )
    {
        cpSoftBodyOptSetSleepingCallback(_softbody, (cpSoftBodyOptSleepingCallback)PhysicsSoftBodyCallback::sleepCallback );
    }
    else
    {
        cpSoftBodyOptSetSleepingCallback(_softbody, nullptr );
    }
}

void PhysicsSoftBody::notifyBeginCollision(bool active)
{
    if ( active )
    {
        cpSoftBodyOptSetCollisionBeginCallback( _softbody, (cpSoftBodyOptCollisionBeginCallback)PhysicsSoftBodyCallback::collisionBeginCallback );
    }
    else
    {
        cpSoftBodyOptSetCollisionBeginCallback( _softbody, nullptr);
    }
}

void PhysicsSoftBody::notifySeparateCollision(bool active)
{
    if ( active )
    {
        cpSoftBodyOptSetCollisionSeparateCallback( _softbody, (cpSoftBodyOptCollisionSeparateCallback)PhysicsSoftBodyCallback::collisionSeparateCallback );
    }
    else
    {
        cpSoftBodyOptSetCollisionSeparateCallback( _softbody, nullptr);
    }
}


Vec3 PhysicsSoftBody::getActionPosition(void)
{
//    if ( unsigned int numConstrNodes = getNumConstrainedNodes() )
//    {
//        cpVect position = {0.0,0.0};
//        
//        for (int i=0; i<getNumConstrainedNodes(); ++i )
//        {
//            cpvadd( position, getNodes()[getConstrainedNodes()[i]] );
//        }
//        
//        return Vec3(position.x / numConstrNodes, position.y / numConstrNodes, 0.0);
//    }
    const Vec2 pos = getPosition();
    return Vec3(pos.x, pos.y, 0.0);
}

void PhysicsSoftBody::setActionPosition(const Vec3& targetPosition)
{
    static const float freq = _world->getFrequency()*_world->getSubsteps();
    
    if ( unsigned int numConstrNodes = getNumConstrainedNodes() )
    {
        Vec2 vel = (Vec2(targetPosition.x, targetPosition.y) - getPosition()) * freq;
        
        for (int i=0; i<numConstrNodes; ++i )
        {
            setNodeVelocity( vel, getConstrainedNodes()[i] );
        }
    }
}

Vec3 PhysicsSoftBody::getActionRotation(void)
{
    Vec3 ret = Vec3::ZERO;
    
    if ( unsigned int numConstrNodes = getNumConstrainedNodes() )
    {
        cpVect cog = cpSoftBodyOptGetCoG(_softbody);
        _initialCoG = PhysicsHelper::cpv2point(cog );
        
        const cpVect R = cpvsub(getNodes()[getConstrainedNodes()[0]], cog );
        ret.x = _currentRotation = MATH_RAD_TO_DEG( cpvtoangle(R) );
    }
    else
    {
        
    }
    
    return ret;
}

void PhysicsSoftBody::setActionRotation(const Vec3& targetRotation)
{
    static const float freq = _world->getFrequency()/_world->getSubsteps();
    
    if ( unsigned int numConstrNodes = getNumConstrainedNodes() )
    {
//        const cpVect R = cpvsub(getNodes()[getConstrainedNodes()[0]], cpSoftBodyOptGetCoG(_softbody) );
        
        const cpVect diffCoG = cpvsub( PhysicsHelper::point2cpv(_initialCoG), cpSoftBodyOptGetCoG(_softbody));
        const cpVect diffCoGvel = cpvmult( diffCoG, freq );
        
        float angVel = MATH_DEG_TO_RAD( (targetRotation.x - _currentRotation ) * freq );
        
        const cpIdx* constrNodes = getConstrainedNodes();
        const cpVect* nodes = getNodes();
//        Vec2 vel = (Vec2(targetRotation.x, targetRotation.y) - getPosition()) * freq;
        
        for (int i=0; i<numConstrNodes; ++i )
        {
            cpSoftBodyOptSetNodeVelocity( _softbody, cpvadd( diffCoGvel,
                cpvmult(
                        cpvperp(
                                cpvsub( nodes[constrNodes[i]],
                                       cpSoftBodyOptGetCoG(_softbody)) ),
                        angVel )
                        ),
                constrNodes[i]  );
        }

        _currentRotation = targetRotation.x;
    }
}


void PhysicsSoftBody::stopAction()
{
    if ( unsigned int numConstrNodes = getNumConstrainedNodes() )
    {
        for (int i=0; i<numConstrNodes; ++i )
        {
            setNodeVelocity( Vec2::ZERO, getConstrainedNodes()[i] );
        }
    }
}

void PhysicsSoftBody::fillDisplayBuffer(V2F_T2F* vertexBuffer)
{
    int size = getNumActiveTriangles();
    const cpIdx first = getFirstActiveTriangleIdx();
    const cpIdx* next = getActiveTriangles();
    
    TriangleBufferIterator<cpIdx> iterator(size, first, next);
    
    const cpVect* vertices = getNodes();
    
    const Tex2F* texCoordsBuffer = _mesh->_texCoords->lockForRead();
    
    V2F_T2F* vertexBufferIter = vertexBuffer;
    
    if ( const cpTriangle* triangles = iterator.begin( reinterpret_cast<const cpTriangle*>(getTriangles()) ) )
    {
        do
        {
            const cpVect& v0 = vertices[triangles->n[0]];
            const cpVect& v1 = vertices[triangles->n[1]];
            const cpVect& v2 = vertices[triangles->n[2]];
            
            const Tex2F& texCoords0 = texCoordsBuffer[triangles->n[0]];
            const Tex2F& texCoords1 = texCoordsBuffer[triangles->n[1]];
            const Tex2F& texCoords2 = texCoordsBuffer[triangles->n[2]];
            
            vertexBufferIter->vertices.set(v0.x, v0.y);
            //        vertexBufferIter->colors = _color;
            vertexBufferIter->texCoords.u = texCoords0.u;
            vertexBufferIter->texCoords.v = texCoords0.v;
            ++vertexBufferIter;
            
            vertexBufferIter->vertices.set(v1.x, v1.y);
            //        vertexBufferIter->colors = _color;
            vertexBufferIter->texCoords.u = texCoords1.u;
            vertexBufferIter->texCoords.v = texCoords1.v;
            ++vertexBufferIter;
            
            vertexBufferIter->vertices.set(v2.x, v2.y);
            //        vertexBufferIter->colors = _color;
            vertexBufferIter->texCoords.u = texCoords2.u;
            vertexBufferIter->texCoords.v = texCoords2.v;
            ++vertexBufferIter;
            
            
            triangles = iterator.getNext(triangles);
        } while ( iterator.next() );
    }
    
    _mesh->_texCoords->unlock();
    
    
//    const cpVect* vertices = getNodes();
//    unsigned int numTriangles = getNumActiveTriangles();
//    const cpIdx* triangles = getTriangles();
//    const cpIdx firstActiveTriangle = getFirstActiveTriangleIdx();
//    const cpIdx* activeTriangles = getActiveTriangles();
//
//    
//    const cpIdx* nextTriangs = (const cpIdx*)activeTriangles + firstActiveTriangle;
//    
//    const cpTriangle* triangIter = (cpTriangle*)triangles + firstActiveTriangle;
//    
//    V2F_C4F_T2F* vertexBufferIter = vertexBuffer;
//    
//    for (unsigned int i=0; i<numTriangles; ++i)
//    {
//        const cpVect& v0 = vertices[triangIter->n[0]];
//        const cpVect& v1 = vertices[triangIter->n[1]];
//        const cpVect& v2 = vertices[triangIter->n[2]];
//        
//        vertexBufferIter->vertices.set(v0.x, v0.y);
//        vertexBufferIter->colors = Color4F::GREEN;
//        vertexBufferIter->texCoords.u = 0.0;
//        vertexBufferIter->texCoords.v = 0.0;
//
//        ++vertexBufferIter;
//        
//        vertexBufferIter->vertices.set(v1.x, v1.y);
//        vertexBufferIter->colors = Color4F::GREEN;
//        vertexBufferIter->texCoords.u = 0.0;
//        vertexBufferIter->texCoords.v = 0.0;
//        ++vertexBufferIter;
//        
//        vertexBufferIter->vertices.set(v2.x, v2.y);
//        vertexBufferIter->colors = Color4F::GREEN;
//        vertexBufferIter->texCoords.u = 0.0;
//        vertexBufferIter->texCoords.v = 0.0;
//        ++vertexBufferIter;
//        
//        triangIter += *nextTriangs;
//        nextTriangs += *nextTriangs;
//    }

}

void PhysicsSoftBody::fillDisplayWireFrameBuffer(V2F_C4F* vertexBuffer)
{
    TriangleBufferIterator<cpIdx> iterator(getNumActiveTriangles(), getFirstActiveTriangleIdx(), getActiveTriangles());
    
    
    const cpVect* vertices = getNodes();
    
    V2F_C4F* vertexBufferIter = vertexBuffer;
    
    if ( const cpTriangle* triangles = iterator.begin( reinterpret_cast<const cpTriangle*>(getTriangles()) ) )
    {
        do
        {
            const cpVect& v0 = vertices[triangles->n[0]];
            const cpVect& v1 = vertices[triangles->n[1]];
            const cpVect& v2 = vertices[triangles->n[2]];
            
            
            vertexBufferIter->vertices.set(v0.x, v0.y);
            vertexBufferIter->colors = _color;
            ++vertexBufferIter;
            
            vertexBufferIter->vertices.set(v1.x, v1.y);
            vertexBufferIter->colors = _color;
            ++vertexBufferIter;
            
            vertexBufferIter->vertices.set(v1.x, v1.y);
            vertexBufferIter->colors = _color;
            ++vertexBufferIter;
            
            vertexBufferIter->vertices.set(v2.x, v2.y);
            vertexBufferIter->colors = _color;
            ++vertexBufferIter;
            
            vertexBufferIter->vertices.set(v2.x, v2.y);
            vertexBufferIter->colors = _color;
            ++vertexBufferIter;
            
            vertexBufferIter->vertices.set(v0.x, v0.y);
            vertexBufferIter->colors = _color;
            ++vertexBufferIter;
            
            triangles = iterator.getNext(triangles);
        } while ( iterator.next() );
    }
    
}




NS_CC_END