//
//  CCPhysicsBeamBody.h
//  cocos2d_libs
//
//  Created by Federico Spadoni on 24/01/16.
//
//

#ifndef __cocos2d_libs__CCPhysicsBeamBody__
#define __cocos2d_libs__CCPhysicsBeamBody__


#include "physics/CCPhysicsObject.h"
#include "base/ccTypes.h"
#include "physics-utils/CCMemoryBuffer.h"
#include "physics/CCPolyLine.h"
#include "physics-utils/CCPageMemoryPool.h"
#include "math/Vec2.h"

//#include "CCPhysicsHelper.h"


// chipmunk forward decl
struct cpBeamShape;
struct cpBeamBody;
struct cpVect3;
struct cpBody;
struct cpArbiter;


NS_CC_BEGIN


class PhysicsWorld;
class BeamBodyMaterial;
class BeamBodyDrawNode;
class PolyLine;


struct V2F_C4F;
struct V2F_T2F;
struct V2F_N2F_T2F_C4F;



template <typename T> class BufferIterator;



class CC_DLL PhysicsBeamBody : public PhysicsObject
{
public:
    
    static PhysicsBeamBody* createFromPolyLine(PolyLine* polyLine, BeamBodyMaterial* material);
    
    static PhysicsBeamBody* create(const unsigned int numNodes, const unsigned int numElems, const unsigned int graphSize);
    
    static PhysicsBeamBody* clone(const PhysicsBeamBody* other);
    
    void destroy();
    
    void setData(void* data);
    
//    void setDrawNode(BeamBodyDrawNode* drawNode) {_drawNode = drawNode;}
//    
//    BeamBodyDrawNode* getDrawNode()  { return _drawNode;}
    
    
    
    
    void fillDisplayBuffer(V2F_T2F* vertexBuffer);
    
//    void fillDisplayBuffer(V2F_N2F_C4B* vertexBuffer);
    
    void fillDisplayBuffer(V2F_N2F_T2F_C4F* vertexBuffer);
    
    
    void fillDisplayWireFrameBuffer(V2F_C4F* vertexBuffer);
    

    
    
    /** remove the body from the world it added to */
    void removeFromWorld();
    
    inline bool isEnabled() const { return _enabled; }
    /**
     * set the enable value.
     * if the body it isn't enabled, it will not has simulation by world
     */
    void setEnable(bool enable);
    
    /** Get the body's tag. */
    inline int getTag() const { return _tag; }
    
    /** set the body's tag. */
    inline void setTag(int tag) { _tag = tag; }
    
    
    inline const BeamBodyMaterial* getMaterial() const { return _material; }
    
    inline cpBeamShape* getBeamShape() const { return _shape; }
    
    inline cpBeamBody* getBeamBody() const { return _beambody; }
    

    //    inline function
    unsigned int getNumActiveSegments(void) const;
    
    unsigned int getNumNodes(void) const;
    
    const cpVect3* getNodes() const;
    
    const unsigned short* getSegments() const;
    
    unsigned short getFirstActiveSegmentIdx() const;
    
    const unsigned short* getActiveSegments() const;
    
    unsigned int getNumConstrainedNodes(void) const;
    
    const unsigned short* getConstrainedNodes() const;
    
    void setPosition(const Vec2& pos);
    
    Vec2 getPosition(void);
    
    void translate(const Vec2& pos);
    
    void translate(const float x, const float y);
    
    void rotate(const float a);
    
    void setVelocity(const Vec2& vel);
    
    void setNodeVelocity(const Vec2& vel, unsigned short nodeId);
    
    void setVelocity(const float x, const float y);
    
    void setVelocityX(const float vx);
    
    void setVelocityY(const float vy);
    
    void addVelocity(const Vec2& vel);
    
    void addVelocity(const float x, const float y);
    
    void setAngularVelocity(const float avel);
    
    void setGravity(const Vec2& gravity);
    
    void setGravity(const float x, const float y);
    
    //    collision filter
    void setCollisionFilter(unsigned int category, unsigned int mask = ~(unsigned int)0u/*CP_ALL_CATEGORIES*/);
    
    void setPointCollisionFilter(unsigned short nodeId, unsigned int category, unsigned int mask = ~(unsigned int)0u/*CP_ALL_CATEGORIES*/);
    
    void setTriangleCollisionFilter(unsigned short triagleId, unsigned int category, unsigned int mask = ~(unsigned int)0u/*CP_ALL_CATEGORIES*/);

    void addConstraintVelocity(unsigned short nodeId, const Vec2& velocity );
    
    void removeConstraintVelocity(unsigned short nodeId);
    
    
    
    void notifyBreakElement(bool active);
    
    void notifyBreak(bool active);
    
    void notifySleep(bool active);
    
    void notifyBeginCollision(bool active);
    
    void notifySeparateCollision(bool active);
    
    
    virtual void sleepBegin() {}
    
    virtual void breakElement(const unsigned short index) {}
    
    virtual void breakAllElement() {}
    
    virtual void collisionBegin( cpBody* collidingBody, cpArbiter* arb ) {}
    
    virtual void collisionSeparate(cpBody* collidingBody, cpArbiter* arb) {}
    

    
protected:
    
    PhysicsBeamBody();
    virtual ~PhysicsBeamBody();
    
    virtual bool init(PolyLine* polyLine, BeamBodyMaterial* material);
    
    virtual bool init(const unsigned int numNodes, const unsigned int numElems, const unsigned intgraphSize);
    
    bool copy(const PhysicsBeamBody* other);
    
    
    //    PhysicsBeamBody& operator= (const PhysicsBeamBody& other);
    
private:
    
    PoolBufferSharedPtr<unsigned char> _data;
    
    unsigned int _numNodes;
    unsigned int _numElems;
    unsigned int _graphSize;
    
    BeamBodyMaterial* _material;
    PolyLine* _polyLine;
    
    cpBeamShape* _shape;
    cpBeamBody* _beambody;
    

//    BeamBodyDrawNode* _drawNode;
    bool _enabled;
    
    Color4F _color;
    
    int _tag;
    
    bool _destoryMark;
    
    //    std::function<void(PhysicsBeamBody*)> _sleepBeginCallback;
    //    std::function<void(PhysicsBeamBody*, const unsigned short)> _breakElementCallback;
    //    std::function<void(PhysicsBeamBody*)> _breakAllCallback;
    
    // all friends class
    friend class PhysicsWorld;
    //    friend class PhysicsWorldInfo;
    friend class PhysicsBeamBodyCallback;
    
    template<typename T>
    friend class BufferIterator;
    
};


//class created only to make the code easy to understand
//completley unsafe. use caution.
template <class Type>
class SegmentBufferIterator
{
public:
    SegmentBufferIterator(int size, const Type first, const Type* next)
    : _size(size)
    , _next(*next)
    , _first(first)
    , _counter(size)
    , _iterator(next + first)
    {
    }
    
    template <typename T>
    inline T* begin(T* ptr)
    {
        return (_size > 0 ) ? ptr + _first : nullptr;
    }
    
    template <typename T>
    inline T* getNext(T* ptr)
    {
        return ptr + *_iterator;
    }
    
    inline int next(void)
    {
        CCASSERT(_counter>0, "TriangleBufferIterator reading out of boundary");
        _iterator += *_iterator;
        return --_counter;
    }
    
    inline void reset(void)
    {
        _counter = _size;
        _iterator = &_next + _first;
    }
    
    inline const SegmentBufferIterator& operator++()
    {
        CCASSERT(_counter>0, "TriangleBufferIterator reading out of boundary");
        _iterator += *_iterator;
        return *this;
    }
    
    inline void operator++(int)
    {
        CCASSERT(_counter>0, "TriangleBufferIterator reading out of boundary");
        _iterator += *_iterator;
        return;
    }
    
private:
    
    const int _size;
    const Type _next;
    const Type _first;
    int _counter;
    const Type* _iterator;
};



NS_CC_END


#endif /* defined(__cocos2d_libs__CCPhysicsBeamBody__) */
