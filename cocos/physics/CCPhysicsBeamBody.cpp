//
//  CCPhysicsBeamBody.cpp
//  cocos2d_libs
//
//  Created by Federico Spadoni on 24/01/16.
//
//

#include "CCPhysicsBeamBody.h"
#include "CCPhysicsBeamBody.inl"
#include "physics/CCMaterialProperty.h"
#include "physics/CCPhysicsWorld.h"
#include "base/ccTypes.h"

#include "physics-render/CCBeamBodyDrawNode.h"
#include "physics/CCPhysicsType.h"

#include "chipmunk.h"


NS_CC_BEGIN

class PhysicsBeamBodyCallback
{
public:
    static void sleepCallback(cpBeamBody* body, PhysicsBeamBody* beambody);
    static void breakElementCallback(cpBeamBody* body, cpIdx triangleIdx, PhysicsBeamBody* beambody);
    static void breakAllElementCallback(cpBeamBody* body, PhysicsBeamBody* beambody);
    
    static void collisionBeginCallback(cpBeamBody* body, cpBody* collidingBody, cpArbiter* arb, void*);
    static void collisionSeparateCallback(cpBeamBody* body, cpBody* collidingBody, cpArbiter* arb, void*);
    
};

void PhysicsBeamBodyCallback::sleepCallback(cpBeamBody* body, PhysicsBeamBody* beambody)
{
    //    if ( beambody->_sleepBeginCallback )
    beambody->sleepBegin();
}

void PhysicsBeamBodyCallback::breakElementCallback(cpBeamBody* body, cpIdx triangleIdx, PhysicsBeamBody* beambody)
{
    //    if ( beambody->_breakElementCallback )
    beambody->breakElement(triangleIdx);
}

void PhysicsBeamBodyCallback::breakAllElementCallback(cpBeamBody* body, PhysicsBeamBody* beambody)
{
    //    if ( beambody->breakAllCallback )
    {
        
        beambody->breakAllElement();
        
//        if (beambody->getDrawNode() )
//        {
//            beambody->getDrawNode()->removeBeamBody(beambody);
//        }
        
        beambody->destroy();
        beambody = nullptr;
        
        //        beambody->removeFromWorld();
    }
}

void PhysicsBeamBodyCallback::collisionBeginCallback(cpBeamBody* body, cpBody* collidingBody, cpArbiter* arb, void*)
{
    PhysicsBeamBody* beambody =  static_cast<PhysicsBeamBody*>(cpBeamBodyGetUserData(body));
    
    beambody->collisionBegin(collidingBody, arb);
}


void PhysicsBeamBodyCallback::collisionSeparateCallback(cpBeamBody* body, cpBody* collidingBody, cpArbiter* arb, void*)
{
    PhysicsBeamBody* beambody =  static_cast<PhysicsBeamBody*>(cpBeamBodyGetUserData(body));
    
    beambody->collisionSeparate(collidingBody, arb);
}



PhysicsBeamBody* PhysicsBeamBody::createFromPolyLine(PolyLine* mesh, BeamBodyMaterial* material)
{
    PhysicsBeamBody* beambody = new (std::nothrow) PhysicsBeamBody();
    
    if (beambody && beambody->init(mesh, material) )
    {
        beambody->autorelease();
    }
    else{
        CC_SAFE_RELEASE(beambody);
        return nullptr;
    }
    
    return beambody;
}

PhysicsBeamBody* PhysicsBeamBody::create(const unsigned int numNodes, const unsigned int numElems, const unsigned int graphSize)
{
    PhysicsBeamBody* beambody = new (std::nothrow) PhysicsBeamBody();
    
    if (beambody && beambody->init(numNodes, numElems, graphSize) )
    {
        beambody->autorelease();
    }
    else{
        CC_SAFE_RELEASE(beambody);
        return nullptr;
    }
    
    return beambody;
}

PhysicsBeamBody* PhysicsBeamBody::clone(const PhysicsBeamBody* other)
{
    PhysicsBeamBody* beambody = new (std::nothrow) PhysicsBeamBody();
    
    if (beambody && beambody->copy(other) )
    {
        beambody->autorelease();
    }
    else{
        CC_SAFE_RELEASE(beambody);
        return nullptr;
    }
    
    return beambody;
}



PhysicsBeamBody::PhysicsBeamBody()
: PhysicsObject()
, _material(nullptr)
, _polyLine(nullptr)
, _shape(nullptr)
, _beambody(nullptr)
, _enabled(true)
//, _drawNode(nullptr)
//, _sleepBeginCallback(nullptr)
//, _breakElementCallback(nullptr)
//, _breakAllCallback(nullptr)
, _tag(0)
, _color(Color4F::GREEN)
{
    
}

PhysicsBeamBody::~PhysicsBeamBody()
{
    //    if (_beambody)
    //    {
    //        if ( cpSpaceSoftBody* world = cpBeamBodyGetSpace(_beambody) )
    //        {
    //            cpSpaceRemoveSoftBodyOpt(world, _beambody );
    //        }
    //        cpBeamBodyFree( _beambody );
    //        _beambody = nullptr;
    //    }
    //    _beambody = nullptr;
    
    if (_polyLine)
        _polyLine->release();
    
    if (_material)
        _material->release();

}


void PhysicsBeamBody::destroy()
{
    if (_beambody)
    {
        removeFromWorld();
        
        if ( cpSpaceSoftBody* world = cpBeamBodyGetSpace(_beambody) )
        {
            cpSpaceRemoveBeamBody(world, _beambody );
        }
        cpBeamBodyFree( _beambody );
        
        
        //        _beambody = nullptr;
    }
}

bool PhysicsBeamBody::init( PolyLine* mesh, BeamBodyMaterial* material)
{
    _numNodes = mesh->getNumNodes();
    _numElems = mesh->getNumSegments();
    _graphSize = mesh->getGraphSize();
    
    ssize_t memSize = cpBeamShapeComputeMemorySize(_numNodes, _numElems, _graphSize);
    memSize += cpBeamBodyComputeMemorySize(_numNodes, _numElems, _graphSize);
    
    _data = PoolBuffer<unsigned char>::create(memSize);
    
    cpPolyLineBuffer meshbuf = cpPolyLineBufferInitFromBuffer(_numNodes, _numElems, _graphSize ,_data->lockForRead<void>(), _data->getSizeInBytes());
    
    // fill cpMeshBuffer struct
    const Vec3* source = mesh->_nodes->lockForRead();
    cpVect3* dest = (cpVect3*)meshbuf.nodes;
    for (int i=0; i<_numNodes; ++i)
    {
        dest->x = source->x;
        dest->y = source->y;
        dest->z = source->z;
        dest->_unused = 0;
        ++dest;
        ++source;
    }
    mesh->_nodes->unlock();
    
//    memcpy(meshbuf.nodes, mesh->_nodes->lockForRead<void>(), mesh->_nodes->getSizeInBytes());
    
    memcpy(meshbuf.segments, mesh->_segments->lockForRead<void>(), mesh->_segments->getSizeInBytes());
    memcpy(meshbuf.rows, mesh->_rows->lockForRead<void>(), mesh->_rows->getSizeInBytes());
    memcpy(meshbuf.columns, mesh->_columns->lockForRead<void>(), mesh->_columns->getSizeInBytes());
    memcpy(meshbuf.diagonal, mesh->_diagonal->lockForRead<void>(), mesh->_diagonal->getSizeInBytes());
    
    //    create shape
    this->_shape = cpBeamShapeInitFromMeshBuffer( &meshbuf, 4.0 );
    
    this->_polyLine = mesh;
    _polyLine->retain();
    
    this->_material = material;
    _material->retain();
    
    cpBeamMaterial mat = {
        
        .radius = static_cast<cpFloat>(material->_radius),
        .density = static_cast<cpFloat>(material->_density),
        .youngModulus= {static_cast<cpFloat>(material->_youngModulusAxial),static_cast<cpFloat>(material->_youngModulusTransverse) },
        .friction = static_cast<cpFloat>(material->_friction),
        .stiffnessDamping = static_cast<cpFloat>(material->_stiffnessDamping),
        .massDamping = static_cast<cpFloat>(material->_massDamping),
        .contactFriction = static_cast<cpFloat>(material->_contactFriction),
        .contactElasticity = static_cast<cpFloat>(material->_contactElasticity),
        .yield = static_cast<cpFloat>(material->_yield),
        .maxYield = static_cast<cpFloat>(material->_maxYield),
        .creep = static_cast<cpFloat>(material->_creep),
        .axialToughness = static_cast<cpFloat>(material->_axialToughness),
        .transverseToughness = static_cast<cpFloat>(material->_transverseToughness),
//        not used
        .cosAngleFracture = 0,
        .areaFractureMinRatio = 0,
        .areaFractureMaxRatio = 0
    };
    
    
    cpSoftBodySolverParams params = {
        .position = cpvzero,
        .nbSolverIteration = 10,
        .solverTolerance = 1e-7,
        .constrNodesFile = 0,
    };
    
    if ( mesh->getNumConstrainedNodes() )
    {
        params.numConstrainedNodes = mesh->getNumConstrainedNodes();
        params.constrainedNodes = mesh->_constrainedNodes->lockForWrite();
    }
    
    //    create beambody
    this->_beambody = cpBeamBodyInit(_shape, &mat, &params);
    cpBeamBodySetUserData(_beambody, this);
    
    // callbacks
    cpBeamBodySetSleepingCallback(_beambody, (cpBeamBodySleepingCallback)PhysicsBeamBodyCallback::sleepCallback );
    cpBeamBodySetSegmentDestroyCallback(_beambody, (cpBeamBodySegmentDestroyCallback)PhysicsBeamBodyCallback::breakElementCallback);
    cpBeamBodySetDestroyCallback(_beambody, (cpBeamBodyDestroyCallback)PhysicsBeamBodyCallback::breakAllElementCallback);
    
    
    _color = material->_color;
    
    return true;
}

bool PhysicsBeamBody::init(const unsigned int numNodes, const unsigned int numElems, const unsigned graphSize)
{
    ssize_t memSize = cpSoftBodyShapeOptComputeMemorySize(numNodes, numElems, graphSize);
    memSize += cpBeamBodyComputeMemorySize(numNodes, numElems, graphSize);
    
    
    return true;
}


bool PhysicsBeamBody::copy(const PhysicsBeamBody* other)
{
    if (other == nullptr)
        return false;
    
    _data = PoolBuffer<unsigned char>::clone(other->_data.get());
    
    _numNodes = other->_numNodes;
    _numElems = other->_numElems;
    _graphSize = other->_graphSize;
    
    this->_polyLine = other->_polyLine;
    _polyLine->retain();
    
    this->_material = other->_material;
    _material->retain();
    
    cpPolyLineBuffer meshbuf = cpPolyLineBufferInitFromBuffer(_numNodes, _numElems, _graphSize ,_data->lockForRead<void>(), _data->getSizeInBytes());

    _shape = cpBeamShapeCopy( other->_shape, &meshbuf.buffer );
    
    //    create beambody
    this->_beambody = cpBeamBodyClone(_shape, other->_beambody);
    cpBeamBodySetUserData(_beambody, this);
    
    // callbacks
    //    cpBeamBodySetSleepingCallback(_beambody, (cpBeamBodySleepingCallback)PhysicsBeamBodyCallback::sleepCallback );
    cpBeamBodySetSegmentDestroyCallback(_beambody, (cpBeamBodySegmentDestroyCallback)PhysicsBeamBodyCallback::breakElementCallback);
    cpBeamBodySetDestroyCallback(_beambody, (cpBeamBodyDestroyCallback)PhysicsBeamBodyCallback::breakAllElementCallback);
    
    
    _color = other->_color;
    
    return true;
}

void PhysicsBeamBody::removeFromWorld()
{
    if (_world)
    {
        _world->removeBeamBody(this);
    }
}


void PhysicsBeamBody::setEnable(bool enable)
{
    if (_enabled != enable)
    {
        _enabled = enable;
        
        if (_world)
        {
            if (enable)
            {
                _world->addBeamBodyOrDelay(this);
            }else
            {
                _world->removeBeamBodyOrDelay(this);
            }
        }
    }
}

void PhysicsBeamBody::setData(void* data)
{
    
    
    
}

void PhysicsBeamBody::notifyBreakElement(bool active)
{
    if ( active )
    {
        cpBeamBodySetSegmentDestroyCallback(_beambody, (cpBeamBodySegmentDestroyCallback)PhysicsBeamBodyCallback::breakElementCallback);
        
    }
    else
    {
        cpBeamBodySetSegmentDestroyCallback(_beambody, nullptr);
        
    }
}

void PhysicsBeamBody::notifyBreak(bool active)
{
    if ( active )
    {
        cpBeamBodySetDestroyCallback(_beambody, (cpBeamBodyDestroyCallback)PhysicsBeamBodyCallback::breakAllElementCallback);
        
    }
    else
    {
        cpBeamBodySetDestroyCallback(_beambody,nullptr);
        
    }
}

void PhysicsBeamBody::notifySleep(bool active)
{
    if ( active )
    {
        cpBeamBodySetSleepingCallback(_beambody, (cpBeamBodySleepingCallback)PhysicsBeamBodyCallback::sleepCallback );
    }
    else
    {
        cpBeamBodySetSleepingCallback(_beambody, nullptr );
    }
}

void PhysicsBeamBody::notifyBeginCollision(bool active)
{
    if ( active )
    {
        cpBeamBodySetCollisionBeginCallback( _beambody, (cpBeamBodyCollisionBeginCallback)PhysicsBeamBodyCallback::collisionBeginCallback );
    }
    else
    {
        cpBeamBodySetCollisionBeginCallback( _beambody, nullptr);
    }
}

void PhysicsBeamBody::notifySeparateCollision(bool active)
{
    if ( active )
    {
        cpBeamBodySetCollisionSeparateCallback( _beambody, (cpBeamBodyCollisionSeparateCallback)PhysicsBeamBodyCallback::collisionSeparateCallback );
    }
    else
    {
        cpBeamBodySetCollisionSeparateCallback( _beambody, nullptr);
    }
}



void PhysicsBeamBody::fillDisplayBuffer(V2F_T2F* vertexBuffer)
{
    int size = getNumActiveSegments();
    const cpIdx first = getFirstActiveSegmentIdx();
    const cpIdx* next = getActiveSegments();
    
    SegmentBufferIterator<cpIdx> iterator(size, first, next);
    
    const cpVect3* vertices = getNodes();
    
    const Tex2F* texCoordsBuffer = _polyLine->_texCoords->lockForRead();
    
    V2F_T2F* vertexBufferIter = vertexBuffer;
    
    if ( const cpSegment* segments = iterator.begin( reinterpret_cast<const cpSegment*>(getSegments()) ) )
    {
        do
        {
            const cpVect3& v0 = vertices[segments->n[0]];
            const cpVect3& v1 = vertices[segments->n[1]];
            
            const Tex2F& texCoords0 = texCoordsBuffer[segments->n[0]];
            const Tex2F& texCoords1 = texCoordsBuffer[segments->n[1]];
            
            const Vec2 normal0 = 2.0 * PhysicsHelper::cpv2point( cpvforangle( v0.z ) );
            
//            vertexBufferIter->vertices.set( v0.x + normal0.x, v0.y + normal0.y);//  Vec2(v0.x, v0.y) +  set;//   );
////        vertexBufferIter->colors = _color;
//            vertexBufferIter->texCoords.u = texCoords0.u;
//            vertexBufferIter->texCoords.v = texCoords0.v;
//            ++vertexBufferIter;
//            
//            vertexBufferIter->vertices.set( v0.x - normal0.x, v0.y - normal0.y);//  Vec2(v0.x, v0.y) +  set;//   );
////        vertexBufferIter->colors = _color;
//            vertexBufferIter->texCoords.u = texCoords0.u;
//            vertexBufferIter->texCoords.v = texCoords0.v;
//            ++vertexBufferIter;
//            
//            
//            const Vec2 normal1 = 2.0 * PhysicsHelper::cpv2point( cpvforangle( v1.z ) );
//            
//            vertexBufferIter->vertices.set(v1.x + normal1.x, v1.y + normal1.y);
////        vertexBufferIter->colors = _color;
//            vertexBufferIter->texCoords.u = texCoords1.u;
//            vertexBufferIter->texCoords.v = texCoords1.v;
//            ++vertexBufferIter;
//            
//            vertexBufferIter->vertices.set(v1.x - normal1.x, v1.y - normal1.y);
////        vertexBufferIter->colors = _color;
//            vertexBufferIter->texCoords.u = texCoords1.u;
//            vertexBufferIter->texCoords.v = texCoords1.v;
//            ++vertexBufferIter;
            
            
            
            
            segments = iterator.getNext(segments);
        } while ( iterator.next() );
    }
    
    _polyLine->_texCoords->unlock();
    
}



void PhysicsBeamBody::fillDisplayWireFrameBuffer(V2F_C4F* vertexBuffer)
{
    SegmentBufferIterator<cpIdx> iterator(getNumActiveSegments(), getFirstActiveSegmentIdx(), getActiveSegments());
    
    
    const cpVect3* vertices = getNodes();
    
    V2F_C4F* vertexBufferIter = vertexBuffer;
    
    if ( const cpSegment* segments = iterator.begin( reinterpret_cast<const cpSegment*>(getSegments()) ) )
    {
        do
        {
            const cpVect3& v0 = vertices[segments->n[0]];
            const cpVect3& v1 = vertices[segments->n[1]];
            
            
            vertexBufferIter->vertices.set(v0.x, v0.y);
            vertexBufferIter->colors = _color;
            ++vertexBufferIter;
            
            vertexBufferIter->vertices.set(v1.x, v1.y);
            vertexBufferIter->colors = _color;
            ++vertexBufferIter;
            
            segments = iterator.getNext(segments);
        } while ( iterator.next() );
    }
    
}


void PhysicsBeamBody::fillDisplayBuffer(V2F_N2F_T2F_C4F* vertexBuffer)
{
    static const float pi_h = 3.14159265358979323846 / 2.0;
    
    SegmentBufferIterator<cpIdx> iterator(getNumActiveSegments(), getFirstActiveSegmentIdx(), getActiveSegments());
    
    
    const cpVect3* vertices = getNodes();
    
    V2F_N2F_T2F_C4F* vertexBufferIter = vertexBuffer;
    
    const Tex2F* texCoordsBuffer = _polyLine->_texCoords->lockForRead();
    
    if ( const cpSegment* segments = iterator.begin( reinterpret_cast<const cpSegment*>(getSegments()) ) )
    {
        do
        {
            const cpVect3& v0 = vertices[segments->n[0]];
            const cpVect3& v1 = vertices[segments->n[1]];
            
            const Tex2F& texCoord0 = texCoordsBuffer[segments->n[0]];
            const Tex2F& texCoord1 = texCoordsBuffer[segments->n[1]];
            
            
            vertexBufferIter->vertices.set(v0.x, v0.y);
            vertexBufferIter->normals.set(v0.z + pi_h, 0.0);
            vertexBufferIter->texCoords.u = texCoord0.u;
            vertexBufferIter->texCoords.v = 1.0f;
            vertexBufferIter->colors = _color;
            ++vertexBufferIter;
            
            vertexBufferIter->vertices.set(v0.x, v0.y);
            vertexBufferIter->normals.set( v0.z - pi_h, 0.0);
            vertexBufferIter->texCoords.u = texCoord0.u;
            vertexBufferIter->texCoords.v = 0.0f;
            vertexBufferIter->colors = _color;
            ++vertexBufferIter;
            
            vertexBufferIter->vertices.set(v1.x, v1.y);
            vertexBufferIter->normals.set(v1.z + pi_h, 0.0);
            vertexBufferIter->texCoords.u = texCoord1.u;
            vertexBufferIter->texCoords.v = 1.0f;
            vertexBufferIter->colors = _color;
            ++vertexBufferIter;
            
            
//            second triangle
            vertexBufferIter->vertices.set(v1.x, v1.y);
            vertexBufferIter->normals.set(v1.z + pi_h, 0.0);
            vertexBufferIter->texCoords.u = texCoord1.u;
            vertexBufferIter->texCoords.v = 1.0f;
            vertexBufferIter->colors = _color;
            ++vertexBufferIter;
            
            vertexBufferIter->vertices.set(v0.x, v0.y);
            vertexBufferIter->normals.set( v0.z - pi_h, 0.0);
            vertexBufferIter->texCoords.u = texCoord0.u;
            vertexBufferIter->texCoords.v = 0.0f;
            vertexBufferIter->colors = _color;
            ++vertexBufferIter;

            vertexBufferIter->vertices.set(v1.x, v1.y);
            vertexBufferIter->normals.set(v1.z - pi_h, 0.0);
            vertexBufferIter->texCoords.u = texCoord1.u;
            vertexBufferIter->texCoords.v = 0.0f;
            vertexBufferIter->colors = _color;
            ++vertexBufferIter;
            
            segments = iterator.getNext(segments);
        } while ( iterator.next() );
    }
    
}



NS_CC_END