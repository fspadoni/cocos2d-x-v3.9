//
//  CCMaterialProperty.cpp
//  cocos2d_libs
//
//  Created by Federico Spadoni on 10/11/14.
//
//

#include "CCMaterialProperty.h"
#include "platform/CCFileUtils.h"
#include "tinyxml2/tinyxml2.h"

#include <sstream>

NS_CC_BEGIN


SoftBodyMaterial* SoftBodyMaterial::create()
{
    SoftBodyMaterial* material = new (std::nothrow) SoftBodyMaterial();
    
    if (material && material->init() )
    {
        material->autorelease();
    }
    else{
        CC_SAFE_RELEASE(material);
        return nullptr;
    }
    
    return material;
}

SoftBodyMaterial* SoftBodyMaterial::createFromFile(const std::string& filename)
{
    SoftBodyMaterial* material = new (std::nothrow) SoftBodyMaterial();
    
    if (material && material->initFromFile(filename) )
    {
        material->autorelease();
    }
    else{
        CC_SAFE_RELEASE(material);
        return nullptr;
    }
    
    return material;
}

SoftBodyMaterial::SoftBodyMaterial(const std::string& name)
: _name(name)

{
    
}

SoftBodyMaterial::~SoftBodyMaterial()
{
    
}

SoftBodyMaterial::Material::Material()
: _density(1.0) // [Kg/m^3]
, _youngModulus(1e6) // [N/m^2]
, _poissonRatio(0.33) // []
, _friction(0.5)
, _contactFriction(4.0)
, _contactElasticity(0.0)
, _toughness(0.0)
, _yield(0.0)
, _maxYield(0.0)
, _creep(0.0)
, _cosAngleFracture(0)
, _areaFractureMinRatio(1.0)
, _areaFractureMaxRatio(0.0)
{
}

bool SoftBodyMaterial::init()
{
    
    return true;
}

bool SoftBodyMaterial::initFromFile(const std::string& filename)
{
    std::string fullpath = FileUtils::getInstance()->fullPathForFilename(filename);
    if (fullpath.size() == 0)
    {
        return false;
    }
    // Read content from file
    // xml read
    ssize_t filesize;
    unsigned char *data = FileUtils::getInstance()->getFileData(fullpath, "rb", &filesize);
    std::string content((const char*)data,filesize);
    
    delete [] data;
    
    // xml parse
    tinyxml2::XMLDocument* document = new tinyxml2::XMLDocument();
    document->Parse(content.c_str());
    
    const tinyxml2::XMLElement* rootElement = document->RootElement();// Root
    CCLOG("rootElement name = %s", rootElement->Name());
    
//    if (strcmp("PHYSICS_MATERIALS", rootElement->Name()) == 0)
    {
        if ( const tinyxml2::XMLAttribute* attribute = rootElement->FindAttribute("name") )
        {
            this->_name = attribute->Value();
        }
        
        if ( const tinyxml2::XMLAttribute* attribute = rootElement->FindAttribute("color") )
        {
            std::string color = attribute->Value();
            setColor(color);
        }
        
        if ( const tinyxml2::XMLAttribute* attribute = rootElement->FindAttribute("rgba") )
        {
            std::string color = attribute->Value();
            setColorFromRGBA(color);
        }
        
        if ( const tinyxml2::XMLAttribute* attribute = rootElement->FindAttribute("texture") )
        {
            this->_textureName = attribute->Value();
        }
    }
    
    const tinyxml2::XMLElement* element = rootElement->FirstChildElement();
    
    bool createEnabled = false;
    std::string rootType = "";
    
    while (element)
    {
        CCLOG("material name = %s", element->Name());
        
        if (strcmp("MATERIAL", element->Name()) == 0)
        {
            Material material;
            
            if ( const tinyxml2::XMLAttribute* attribute = element->FindAttribute("name") )
            {
                material._name = attribute->Value();
            }
//            else
//            {
//                material._name = "default";
//            }
            
//            if ( const tinyxml2::XMLAttribute* attribute = element->FindAttribute("color") )
//            {
//                std::string color = attribute->Value();
//                setColor(color);
//            }

//            if ( const tinyxml2::XMLAttribute* attribute = element->FindAttribute("rgba") )
//            {
//                std::string color = attribute->Value();
//                setColorFromRGBA(color);
//            }
            
//            if ( const tinyxml2::XMLAttribute* attribute = element->FindAttribute("texture") )
//            {
//                this->_textureName = attribute->Value();
//                
//            }
            
            
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("density") )
            {
                material._density = atof( child->FirstChild()->Value() );
            }
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("youngModulus") )
            {
                material._youngModulus = atof( child->FirstChild()->Value() );
            }
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("poissonRatio") )
            {
                material._poissonRatio = atof( child->FirstChild()->Value() );
            }
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("friction") )
            {
                material._friction = atof( child->FirstChild()->Value() );
            }
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("stiffnessDamping") )
            {
                material._stiffnessDamping = atof( child->FirstChild()->Value() );
            }
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("massDamping") )
            {
                material._massDamping = atof( child->FirstChild()->Value() );
            }
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("contactFriction") )
            {
                material._contactFriction = atof( child->FirstChild()->Value() );
            }
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("contactElasticity") )
            {
                material._contactElasticity = atof( child->FirstChild()->Value() );
            }
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("toughness") )
            {
                material._toughness = atof( child->FirstChild()->Value() );
            }
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("yield") )
            {
                material._yield = atof( child->FirstChild()->Value() );
            }
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("maxYield") )
            {
                material._maxYield = atof( child->FirstChild()->Value() );
            }
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("creep") )
            {
                material._creep = atof( child->FirstChild()->Value() );
            }
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("angleFracture") )
            {
                material._cosAngleFracture = atof( child->FirstChild()->Value() );
            }
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("areaFractureMinRatio") )
            {
                material._areaFractureMinRatio = atof( child->FirstChild()->Value() );
            }
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("areaFractureMaxRatio") )
            {
                material._areaFractureMaxRatio = atof( child->FirstChild()->Value() );
            }
            
            _materials.push_back(material);
        }
        
        element = element->NextSiblingElement();
    }
    
    
    delete document;
    
    return true;

}

void SoftBodyMaterial::setColor(const std::string& color)
{
    if ( !color.empty() )
    {
        if ( color.find("white", 0) !=std::string::npos )
        {
            _color = Color4F::WHITE;
        }
        else if ( color.find("yellow", 0) !=std::string::npos )
        {
            _color = Color4F::YELLOW;
        }
        else if ( color.find("blue", 0) !=std::string::npos )
        {
            _color = Color4F::BLUE;
        }
        else if ( color.find("green", 0) !=std::string::npos )
        {
            _color = Color4F::GREEN;
        }
        else if ( color.find("red", 0) !=std::string::npos )
        {
            _color = Color4F::RED;
        }
        else if ( color.find("magenta", 0) !=std::string::npos )
        {
            _color = Color4F::MAGENTA;
        }
        else if ( color.find("black", 0) !=std::string::npos )
        {
            _color = Color4F::BLACK;
        }
        else if ( color.find("orange", 0) !=std::string::npos )
        {
            _color = Color4F::ORANGE;
        }
        else if ( color.find("gray", 0) !=std::string::npos )
        {
            _color = Color4F::GRAY;
        }
    }

}

void SoftBodyMaterial::setColorFromRGBA(const std::string& color)
{
    if ( !color.empty() )
    {
        std::istringstream ss(color);
        ss >> _color.r >> _color.g >> _color.b >> _color.a;
        return;
    }
}



//
//bool SoftBodyMaterial::loadFromXML(const tinyxml2::XMLElement *animationElement)
//{
//    ActionTimeline* action = ActionTimeline::create();
//    CCLOG("animationElement name = %s", animationElement->Name());
//    
//    // ActionTimeline
//    const tinyxml2::XMLAttribute* attribute = animationElement->FirstAttribute();
//    
//    // attibutes
//    while (attribute)
//    {
//        std::string name = attribute->Name();
//        std::string value = attribute->Value();
//        
//        if (name == "Duration")
//        {
//            action->setDuration(atoi(value.c_str()));
//        }
//        else if (name == "Speed")
//        {
//            action->setTimeSpeed(atof(value.c_str()));
//        }
//        
//        attribute = attribute->Next();
//    }
//    
//    // all Timeline
//    const tinyxml2::XMLElement* timelineElement = animationElement->FirstChildElement();
//    while (timelineElement)
//    {
//        Timeline* timeline = loadTimelineFromXML(timelineElement);
//        if (timeline)
//        {
//            action->addTimeline(timeline);
//        }
//        
//        timelineElement = timelineElement->NextSiblingElement();
//    }
//    
//    return action;
//}


BeamBodyMaterial* BeamBodyMaterial::create()
{
    BeamBodyMaterial* material = new (std::nothrow) BeamBodyMaterial();
    
    if (material && material->init() )
    {
        material->autorelease();
    }
    else{
        CC_SAFE_RELEASE(material);
        return nullptr;
    }
    
    return material;
}

BeamBodyMaterial* BeamBodyMaterial::createFromFile(const std::string& filename)
{
    BeamBodyMaterial* material = new (std::nothrow) BeamBodyMaterial();
    
    if (material && material->initFromFile(filename) )
    {
        material->autorelease();
    }
    else{
        CC_SAFE_RELEASE(material);
        return nullptr;
    }
    
    return material;
}

BeamBodyMaterial::BeamBodyMaterial(const std::string& name)
: _name(name)
, _radius(2.0)
, _density(1.0) // [Kg/m^3]
, _youngModulusAxial(1e6) // [N/m^2]
, _youngModulusTransverse(5e6) // [N/m^2]
, _friction(0.5)
, _stiffnessDamping(0.025)
, _massDamping(0.025)
, _contactFriction(4.0)
, _contactElasticity(0.0)
, _axialToughness(0.0)
, _transverseToughness(0.0)
, _yield(0.0)
, _maxYield(0.0)
, _creep(0.0)
, _textureRepeat(false)
{
    
}

BeamBodyMaterial::~BeamBodyMaterial()
{
    
}

bool BeamBodyMaterial::init()
{
    
    return true;
}

bool BeamBodyMaterial::initFromFile(const std::string& filename)
{
    std::string fullpath = FileUtils::getInstance()->fullPathForFilename(filename);
    if (fullpath.size() == 0)
    {
        return false;
    }
    // Read content from file
    // xml read
    ssize_t filesize;
    unsigned char *data = FileUtils::getInstance()->getFileData(fullpath, "rb", &filesize);
    std::string content((const char*)data,filesize);
    
    delete [] data;
    
    // xml parse
    tinyxml2::XMLDocument* document = new tinyxml2::XMLDocument();
    document->Parse(content.c_str());
    
    const tinyxml2::XMLElement* rootElement = document->RootElement();// Root
    CCLOG("rootElement name = %s", rootElement->Name());
    
    const tinyxml2::XMLElement* element = rootElement->FirstChildElement();
    
    bool createEnabled = false;
    std::string rootType = "";
    
    while (element)
    {
        CCLOG("material name = %s", element->Name());
        
        if (strcmp("MATERIAL", element->Name()) == 0)
        {
            if ( const tinyxml2::XMLAttribute* attribute = element->FindAttribute("name") )
            {
                this->_name = attribute->Value();
            }
            
            if ( const tinyxml2::XMLAttribute* attribute = element->FindAttribute("color") )
            {
                std::string color = attribute->Value();
                setColor(color);
            }
            
            if ( const tinyxml2::XMLAttribute* attribute = element->FindAttribute("rgba") )
            {
                std::string color = attribute->Value();
                setColorFromRGBA(color);
            }
            
            if ( const tinyxml2::XMLAttribute* attribute = element->FindAttribute("texture") )
            {
                this->_textureName = attribute->Value();
                
            }
            
            if ( const tinyxml2::XMLAttribute* attribute = element->FindAttribute("repeat") )
            {
                this->_textureRepeat = attribute->Value();
                
            }
            
            
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("radius") )
            {
                this->_radius = atof( child->FirstChild()->Value() );
            }
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("density") )
            {
                this->_density = atof( child->FirstChild()->Value() );
            }
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("youngModulusAxial") )
            {
                this->_youngModulusAxial = atof( child->FirstChild()->Value() );
            }
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("youngModulusTransverse") )
            {
                this->_youngModulusTransverse = atof( child->FirstChild()->Value() );
            }
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("friction") )
            {
                this->_friction = atof( child->FirstChild()->Value() );
            }
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("stiffnessDamping") )
            {
                this->_stiffnessDamping = atof( child->FirstChild()->Value() );
            }
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("massDamping") )
            {
                this->_massDamping = atof( child->FirstChild()->Value() );
            }
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("contactFriction") )
            {
                this->_contactFriction = atof( child->FirstChild()->Value() );
            }
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("contactElasticity") )
            {
                this->_contactElasticity = atof( child->FirstChild()->Value() );
            }
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("axialToughness") )
            {
                this->_axialToughness = atof( child->FirstChild()->Value() );
            }
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("transverseToughness") )
            {
                this->_transverseToughness = atof( child->FirstChild()->Value() );
            }
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("yield") )
            {
                this->_yield = atof( child->FirstChild()->Value() );
            }
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("maxYield") )
            {
                this->_maxYield = atof( child->FirstChild()->Value() );
            }
            if ( const tinyxml2::XMLElement* child = element->FirstChildElement("creep") )
            {
                this->_creep = atof( child->FirstChild()->Value() );
            }
            
            
        }
        
        element = element->NextSiblingElement();
    }
    
    
    delete document;
    
    return true;
    
}

void BeamBodyMaterial::setColor(const std::string& color)
{
    if ( !color.empty() )
    {
        if ( color.find("white", 0) !=std::string::npos )
        {
            _color = Color4F::WHITE;
        }
        else if ( color.find("yellow", 0) !=std::string::npos )
        {
            _color = Color4F::YELLOW;
        }
        else if ( color.find("blue", 0) !=std::string::npos )
        {
            _color = Color4F::BLUE;
        }
        else if ( color.find("green", 0) !=std::string::npos )
        {
            _color = Color4F::GREEN;
        }
        else if ( color.find("red", 0) !=std::string::npos )
        {
            _color = Color4F::RED;
        }
        else if ( color.find("magenta", 0) !=std::string::npos )
        {
            _color = Color4F::MAGENTA;
        }
        else if ( color.find("black", 0) !=std::string::npos )
        {
            _color = Color4F::BLACK;
        }
        else if ( color.find("orange", 0) !=std::string::npos )
        {
            _color = Color4F::ORANGE;
        }
        else if ( color.find("gray", 0) !=std::string::npos )
        {
            _color = Color4F::GRAY;
        }
    }
    
}

void BeamBodyMaterial::setColorFromRGBA(const std::string& color)
{
    if ( !color.empty() )
    {
        std::istringstream ss(color);
        ss >> _color.r >> _color.g >> _color.b >> _color.a;
        return;
    }
}



NS_CC_END