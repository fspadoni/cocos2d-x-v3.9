//
//  CCPhysicsSoftBody.inl
//  cocos2d_libs
//
//  Created by Federico Spadoni on 05/10/16.
//
//
#ifndef cocos2d_libs_CCPhysicsSoftBody_inl__
#define cocos2d_libs_CCPhysicsSoftBody_inl__

#include "physics-utils/CCPhysicsHelper.h"


NS_CC_BEGIN


void PhysicsSoftBody::setPlasticity(bool isPlastic) { cpSoftBodyOptSetPlasticity(_softbody, isPlastic); }

void PhysicsSoftBody::setBrakable(bool isBrakable) { cpSoftBodyOptSetBrakable(_softbody, isBrakable); }

unsigned int PhysicsSoftBody::getNumActiveTriangles(void) const {return cpSoftBodyShapeOptGetNumActiveTriangles(_shape);}

const cpVect* PhysicsSoftBody::getNodes() const {return cpSoftBodyShapeOptGetVerts(_shape);}

const cpIdx* PhysicsSoftBody::getTriangles() const {return cpSoftBodyShapeOptGetTriangles(_shape);}

cpIdx PhysicsSoftBody::getFirstActiveTriangleIdx() const {return cpSoftBodyShapeOptGetFirstActiveTriangles(_shape);}

const cpIdx* PhysicsSoftBody::getActiveTriangles() const {return cpSoftBodyShapeOptGetActiveTriangles(_shape);}

unsigned int PhysicsSoftBody::getNumConstrainedNodes(void) const {return cpSoftBodyShapeOptGetNumConstrainedNodes(_softbody);}

const cpIdx* PhysicsSoftBody::getConstrainedNodes() const {return cpSoftBodyShapeOptGetConstrainedNodes(_softbody);}

void PhysicsSoftBody::setPosition(const Vec2& pos) { cpSoftBodyOptSetPosition(_softbody, {pos.x,pos.y} ); }

Vec2 PhysicsSoftBody::getPosition(void) { cpVect pos = cpSoftBodyOptGetCoG(_softbody); return Vec2(pos.x,pos.y); }

void PhysicsSoftBody::translate(const Vec2& pos) { cpSoftBodyOptTranslate(_softbody, {pos.x,pos.y} ); }

void PhysicsSoftBody::translate(const float x, const float y) { cpSoftBodyOptTranslate(_softbody, { PhysicsHelper::float2cpfloat(x), PhysicsHelper::float2cpfloat(y) } ); }

void PhysicsSoftBody::rotate(const float a) { cpSoftBodyOptRotateAroundCoG(_softbody, a); }

void PhysicsSoftBody::setVelocity(const Vec2& vel) { cpSoftBodyOptSetVelocity( _softbody, {vel.x, vel.y} ); }

void PhysicsSoftBody::setVelocity(const float x, const float y)
{ cpSoftBodyOptSetVelocity( _softbody, { PhysicsHelper::float2cpfloat(x), PhysicsHelper::float2cpfloat(y) } ); }

void PhysicsSoftBody::setNodeVelocity(const Vec2& vel, unsigned short nodeId)
{ cpSoftBodyOptSetNodeVelocity( _softbody, {vel.x, vel.y}, nodeId ); }

void PhysicsSoftBody::setVelocityX(const float vx)
{ cpSoftBodyOptSetVelocityX( _softbody, PhysicsHelper::float2cpfloat(vx) ); }

void PhysicsSoftBody::setVelocityY(const float vy)
{ cpSoftBodyOptSetVelocityY( _softbody, PhysicsHelper::float2cpfloat(vy) ); }

void PhysicsSoftBody::addVelocity(const Vec2& vel)
{ cpSoftBodyOptAddVelocity( _softbody, {vel.x, vel.y} ); }

void PhysicsSoftBody::addVelocity(const float x, const float y)
{ cpSoftBodyOptAddVelocity( _softbody, { PhysicsHelper::float2cpfloat(x), PhysicsHelper::float2cpfloat(y) } ); }

void PhysicsSoftBody::setAngularVelocity(const float avel)
{ cpSoftBodyOptSetAngularVelocity( _softbody, PhysicsHelper::float2cpfloat(avel) ); }

void PhysicsSoftBody::setGravity(const Vec2& gravity)
{ cpSoftBodyOptSetGravity( _softbody, {gravity.x, gravity.y} ); }

void PhysicsSoftBody::setGravity(const float x, const float y)
{ cpSoftBodyOptSetGravity( _softbody, { PhysicsHelper::float2cpfloat(x), PhysicsHelper::float2cpfloat(y) } ); }

//    collision filter
void PhysicsSoftBody::setCollisionFilter(unsigned int category, unsigned int mask )
{ cpSoftBodyShapeOptSetFilter(_shape, category, mask); }

void PhysicsSoftBody::setPointCollisionFilter(unsigned short nodeId, unsigned int category, unsigned int mask )
{ cpSoftBodyShapeOptSetPointFilter(_shape, nodeId, category, mask); }

void PhysicsSoftBody::setTriangleCollisionFilter(unsigned short triagleId, unsigned int category, unsigned int mask )
{ cpSoftBodyShapeOptSetTriangleFilter(_shape, triagleId, category, mask); }


NS_CC_END


#endif /* #ifndef cocos2d_libs_CCPhysicsSoftBody_inl__ */
