//
//  TriangularMesh.h
//  cocos2d_libs
//
//  Created by Federico Spadoni on 09/11/14.
//
//

#ifndef cocos2d_libs_TriangularMesh_h
#define cocos2d_libs_TriangularMesh_h

#include "base/CCRef.h"
#include "base/ccTypes.h"

#include "physics-utils/CCMemoryBuffer.h"
#include "math/Vec2.h"

NS_CC_BEGIN




class CC_DLL TriangularMesh : public cocos2d::Ref
{
public:
    
    static TriangularMesh* create();
    
    static TriangularMesh* createFromFile(const std::string& filename, bool isZeroBasedIndex = true );
    
    enum{
        _input_line_size = 1024
    };
    
    inline unsigned int getNumNodes() const {return _numNodes;}
    inline unsigned int getNumTriangles() const {return _numTriangles;}
    inline unsigned int getGraphSize() const {return _graphSize;}
    inline unsigned int getNumMaterials() const {return _numMaterials;}
    inline unsigned int getNumConstrainedNodes() const { return _numConstrainedNodes; }
    inline unsigned int getNumMarkedPoints() const { return _numMarkedPoints; }
    
    inline const std::map<std::string, unsigned int>& getMaterials() const { return _materials; }
    
    inline const Vec2* getMarkedPoints() { return _markedPoints->lockForRead(); }
    
    void setScale(const float scale);
    
protected:
    
    TriangularMesh();
    virtual ~TriangularMesh();
    
    bool init();
    bool initFromFile(const std::string& filename, bool isZeroBasedIndex);

    
    bool loadNodesFromFile(const std::string& filename);
    bool loadElementsFromFile(const std::string& filename, bool isZeroBasedIndex);
    bool loadGraphFromFile(const std::string& filename, bool isZeroBasedIndex);
    bool loadConstrainedNodesFromFile(const std::string& filename);
    bool loadMarkedPointsFromFile(const std::string& filename);
    
    
    unsigned int _numNodes;
    unsigned int _numTriangles;
    unsigned int _graphSize;
    unsigned int _numMaterials;
    unsigned int _numConstrainedNodes;
    unsigned int _numMarkedPoints;
    
    BufferSharedPtr<Vec2> _nodes;
    BufferSharedPtr<unsigned short> _triangles;
    
    BufferSharedPtr<unsigned short> _elemMaterials;
    
    // graph
    BufferSharedPtr<unsigned short> _rows;
    BufferSharedPtr<unsigned short> _columns;
    BufferSharedPtr<unsigned short> _diagonal;
    BufferSharedPtr<unsigned short> _constrainedNodes;
    
    BufferSharedPtr<Tex2F> _texCoords;
    
    std::map<std::string, unsigned int> _materials;
    
    BufferSharedPtr<Vec2> _markedPoints;
    
    float _scale;
    
    int alignment;
    
private:
    
    
    friend class PhysicsSoftBody;
};

NS_CC_END

#endif
