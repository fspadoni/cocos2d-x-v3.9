//
//  TriangularMesh.cpp
//  cocos2d_libs
//
//  Created by Federico Spadoni on 09/11/14.
//
//

#include "CCTriangularMesh.h"
#include "platform/CCFileUtils.h"

#include <sstream>      // std::istringstream
#include <cstring>

NS_CC_BEGIN


TriangularMesh* TriangularMesh::create()
{
    TriangularMesh* triMesh = new (std::nothrow) TriangularMesh();
    
    if (triMesh && triMesh->init() )
    {
        triMesh->autorelease();
    }
    else{
        CC_SAFE_RELEASE(triMesh);
        return nullptr;
    }
    
    return triMesh;
}

TriangularMesh* TriangularMesh::createFromFile(const std::string& filename, bool isZeroBasedIndex)
{
    TriangularMesh* triMesh = new (std::nothrow) TriangularMesh();
    
    if (triMesh && triMesh->initFromFile(filename, isZeroBasedIndex) )
    {
        triMesh->autorelease();
    }
    else{
        CC_SAFE_RELEASE(triMesh);
        return nullptr;
    }
    
    return triMesh;
}



TriangularMesh::TriangularMesh()
: _scale(1.0)
, _numNodes(0)
, _numTriangles(0)
, _graphSize(0)
, _numMaterials(0)
, _numConstrainedNodes(0)
, _numMarkedPoints(0)
{
    
}

TriangularMesh::~TriangularMesh()
{
    return;
}

bool TriangularMesh::init()
{
    
    return true;
}

bool TriangularMesh::initFromFile(const std::string& filename, bool isZeroBasedIndex)
{
    
//    std::string fullpath = FileUtils::getInstance()->fullPathForFilename(filename);
    if (filename.size() != 0)
    {
        if ( loadNodesFromFile(filename+".node") == false && loadNodesFromFile(filename+".1.node") == false)
        {
            return false;
        }
        if ( loadElementsFromFile(filename+".ele", isZeroBasedIndex) == false && loadElementsFromFile(filename+".1.ele", isZeroBasedIndex) == false )
        {
            return false;
        }
        if (loadGraphFromFile(filename+".graph", isZeroBasedIndex) == false && loadGraphFromFile(filename+".1.graph", isZeroBasedIndex) == false )
        {
            return false;
        }
//        look for constrained nodes if any
        loadConstrainedNodesFromFile(filename+".constr");
        loadConstrainedNodesFromFile(filename+".1.constr");
        
        loadMarkedPointsFromFile(filename+".marked");
    }
    
    return true;
}

void TriangularMesh::setScale(const float scale)
{
    _scale = scale;
    
    Vec2* vertex = _nodes->lockForWrite();
    
    if ( vertex != nullptr)
    {
       int n =  _nodes->getSize();
        while (n--)
        {
            vertex->scale( _scale );
            ++vertex;
        }
        
    }
    _nodes->unlock();
    
    
    if ( _numMarkedPoints > 0 )
    {
        Vec2* points = _markedPoints->lockForWrite();
        
        if ( points != nullptr)
        {
            int n =  _markedPoints->getSize();
            while (n--)
            {
                points->scale( _scale );
                ++points;
            }
            
        }
        _markedPoints->unlock();
    }
}

bool TriangularMesh::loadNodesFromFile(const std::string& _filename)
{
    FileUtils* utils = FileUtils::getInstance();
    std::string filename = utils->fullPathForFilename(_filename);
    
    if (filename.size() == 0)
        return false;
    
    unsigned char* filebuf = nullptr;
    ssize_t fileSize = 0;
    
//    std::string filestring = (char*)utils->getFileData(filename.c_str(), "rb", &fileSize);
    unsigned char *data = FileUtils::getInstance()->getFileData(filename.c_str(), "rb", &fileSize);
    std::string filestring((const char*)data,fileSize);
    
    delete [] data;
    
    if (!utils->isFileExist(filename) )
        return false;

    std::istringstream filestream(filestring);
    std::string line;
    
//    read header
    while ( std::getline(filestream,line) )
    {
        if (line.empty())
            continue;
        if ( std::strcmp(&line.front(), "#") == 0 )
            continue;
        
        std::istringstream lstream(line);

        lstream >> this->_numNodes;
        
        break;
    }
    
    this->_nodes = Buffer<Vec2>::create(_numNodes);
    Vec2* vertex = _nodes->lockForWrite();
    
    this->_texCoords = Buffer<Tex2F>::create(_numNodes);
    Tex2F* texcoords = _texCoords->lockForWrite();
    
    int counter = 0;
    //    read nodes
    while ( std::getline(filestream,line) && counter!=this->_numNodes )
    {
        if ( std::strcmp(&line.front(), "#") == 0  || line.empty())
            continue;
        
        std::istringstream lstream(line);
        
        lstream >> counter >> vertex->x  >> vertex->y >> texcoords->u >> texcoords->v;
        ++texcoords;
        ++vertex;
    }
    _texCoords->unlock();
    _nodes->unlock();
    
    delete filebuf;
    
 
//    texcoords = _texCoords->lockForWrite();
//   
//    const Vec2* cvertex = _nodes->lockForRead();
//    
//    counter = _numNodes;
//    while (counter--)
//    {
//        texcoords->u = cvertex->x;
//        texcoords->v = cvertex->y;
//        ++cvertex;
//        ++texcoords;
//    }
//    _texCoords->unlock();
//    _nodes->unlock();
    
    
    return true;
}


bool TriangularMesh::loadElementsFromFile(const std::string& _filename, bool isZeroBasedIndex)
{
    FileUtils* utils = FileUtils::getInstance();
    std::string filename = utils->fullPathForFilename(_filename);
    
    if (filename.size() == 0)
        return false;
    
    unsigned char* filebuf = nullptr;
    ssize_t fileSize = 0;
    
//    std::string filestring = (char*)utils->getFileData(filename.c_str(), "rb", &fileSize);
    unsigned char *data = FileUtils::getInstance()->getFileData(filename.c_str(), "rb", &fileSize);
    std::string filestring((const char*)data,fileSize);
    
    delete [] data;

    
    if (!utils->isFileExist(filename) )
        return false;
    
    std::istringstream filestream(filestring);
    std::string line;
    
    int dummy =0;
    unsigned int numMaterials = 0;
    
    //    read header
    while ( std::getline(filestream,line) )
    {
        if (line.empty())
            continue;
        
        if ( std::strcmp(&line.front(), "#") == 0  || line.empty())
            continue;
        
        std::istringstream lstream(line);
        
        lstream >> this->_numTriangles >> dummy >> numMaterials;
        
        break;
    }
    
    this->_triangles = Buffer<unsigned short>::create(_numTriangles*3);
    unsigned short* triang = _triangles->lockForWrite();
    
    int counter = 0;
    unsigned short v0,v1,v2;

    if ( numMaterials > 0 )
    {
        this->_elemMaterials = Buffer<unsigned short>::create(_numTriangles);
        unsigned short* elemMaterial = _elemMaterials->lockForWrite();
        
//        _materials.reserve(numMaterials);
//        reuse numMaterials
        numMaterials = 0;
//        unsigned int materialCounter = 0;
        
        std::string materialName;
//      read nodes
        while ( std::getline(filestream,line) && counter!=this->_numTriangles )
        {
            if (line.empty())
                continue;
            
            if ( std::strcmp(&line.front(), "#") == 0  || line.empty())
                continue;
            
            std::istringstream lstream(line);
            
            lstream >> counter >> v0  >> v1 >> v2 >> materialName;
            
            if ( isZeroBasedIndex )
            {
                *triang++ = v0;
                *triang++ = v1;
                *triang++ = v2;
            }
            else
            {
                *triang++ = --v0;
                *triang++ = --v1;
                *triang++ = --v2;
            }
            
            if ( _materials.find(materialName) == _materials.end() )
            {
                _materials[materialName] = numMaterials++;
                
            }

            *elemMaterial = _materials[materialName];
            ++elemMaterial;
        }
        
        _elemMaterials->unlock();
        this->_numMaterials = numMaterials;

    }
    else
    {
//      read nodes
        while ( std::getline(filestream,line) && counter!=this->_numTriangles )
        {
            if (line.empty())
                continue;
            
            if ( std::strcmp(&line.front(), "#") == 0  || line.empty())
                continue;
            
            std::istringstream lstream(line);
            
            lstream >> counter >> v0  >> v1 >> v2;
            
            if ( isZeroBasedIndex )
            {
                *triang++ = v0;
                *triang++ = v1;
                *triang++ = v2;
            }
            else
            {
                *triang++ = --v0;
                *triang++ = --v1;
                *triang++ = --v2;
            }
        }
        

        this->_elemMaterials = Buffer<unsigned short>::create(_numTriangles);
        this->_elemMaterials.reset();
    }
    
    _triangles->unlock();
    
    delete filebuf;
    
    return true;
}



static int ushortCompare(const void * a, const void * b)
{
    return ( *(unsigned short*)a - *(unsigned short*)b );
}

static int uintCompare(const void * a, const void * b)
{
    return ( *(unsigned int*)a - *(unsigned int*)b );
}

static int intCompare (const void * a, const void * b)
{
    return ( *(int*)a - *(int*)b );
}

bool TriangularMesh::loadGraphFromFile(const std::string& _filename, bool isZeroBasedIndex)
{
    FileUtils* utils = FileUtils::getInstance();
    std::string filename = utils->fullPathForFilename(_filename);
    
    if (filename.size() == 0)
        return false;
    
    unsigned char* filebuf = nullptr;
    ssize_t fileSize = 0;
    
//    std::string filestring = (char*)utils->getFileData(filename.c_str(), "rb", &fileSize);
    unsigned char *data = FileUtils::getInstance()->getFileData(filename.c_str(), "rb", &fileSize);
    std::string filestring((const char*)data,fileSize);
    
    delete [] data;

    
    if (!utils->isFileExist(filename) )
        return false;
    
    std::istringstream filestream(filestring);
    std::string line;
    
    int numVerts,numEdges;
    //    read header
    while ( std::getline(filestream,line) )
    {
        if (line.empty())
            continue;
        
        if ( std::strcmp(&line.front(), "#") == 0  || line.empty())
            continue;
        
        std::istringstream lstream(line);
        
        lstream >> numVerts>> numEdges >> this->_graphSize;
        
        break;
    }
    
//    add diagonal node
    this->_graphSize +=  _numNodes;
    
    this->_rows = Buffer<unsigned short>::create(_numNodes+1);
    unsigned short* rows = _rows->lockForWrite();
    
//    this->_columns = Buffer<unsigned short>::create(_graphSize); // add diag verts
    BufferSharedPtr<int> tempColumns = Buffer<int>::create(_graphSize); // add diag verts
    int* columns = tempColumns->lockForWrite();
    
    int counter = 0;
    unsigned short idx = 0;
    //    read nodes
    while ( std::getline(filestream,line) && counter<this->_numNodes )
    {
        if ( std::strcmp(&line.front(), "#") == 0  || line.empty())
            continue;
        
        *rows++ = idx;
        
        *columns++ = counter; // add diag verts
        ++counter;
        ++idx;
        
        std::istringstream lstream(line);
        
        if ( isZeroBasedIndex )
        {
            while ( lstream >> *columns )
            {
                ++columns;
                ++idx;
            }
        }
        else
        {
            while ( lstream >> *columns )
            {
                --*columns;
                ++columns;
                ++idx;
            }
        }
    
    }
    *rows = idx;
    
    _rows->unlock();
    tempColumns->unlock();
    
    
    this->_diagonal = Buffer<unsigned short>::create(_numNodes); // add diag verts
    unsigned short* diag = _diagonal->lockForWrite();
    
    const unsigned short* c_rows = _rows->lockForRead();
    columns = tempColumns->lockForWrite();
    
    
    idx = 0;
    for ( unsigned int r=0; r<_numNodes; ++r )
    {
        const size_t nb = c_rows[r+1] - c_rows[r];
        int* first = columns + c_rows[r];
        std::qsort((void*)first, nb, sizeof(int), intCompare);
        
        const int* p = &columns[c_rows[r]];
        
        for ( int c = 0; c<nb; ++c  )
        {
            if ( p[c] == r )
            {
                *diag++ = idx + c;
                break;
            }
        }
        
        idx += nb;
        
    }
    _diagonal->unlock();
    _rows->unlock();
    tempColumns->unlock();
    
    this->_columns = Buffer<unsigned short>::create(_graphSize); // add diag verts
    unsigned short* dataColumns = _columns->lockForWrite();
    columns = tempColumns->lockForWrite();
    
    for ( unsigned int i=0; i<_graphSize; ++i )
    {
        *dataColumns++ = *columns++;
    }
    _columns->unlock();
    tempColumns->unlock();
    
//    _columns->copyData(*tempColumns);
    
    delete filebuf;
    
    return true;
}


bool TriangularMesh::loadConstrainedNodesFromFile(const std::string& _filename)
{
    FileUtils* utils = FileUtils::getInstance();
    std::string filename = utils->fullPathForFilename(_filename);
    
    if (filename.size() == 0)
        return false;
    
    if (!utils->isFileExist(filename) )
        return false;
    
    CCLOG("constrained nodes file found");
    
    unsigned char* filebuf = nullptr;
    ssize_t fileSize = 0;
    
    //    std::string filestring = (char*)utils->getFileData(filename.c_str(), "rb", &fileSize);
    unsigned char *data = FileUtils::getInstance()->getFileData(filename.c_str(), "rb", &fileSize);
    std::string filestring((const char*)data,fileSize);
    
    delete [] data;
    
    std::istringstream filestream(filestring);
    std::string line;
    
    //    read header
    while ( std::getline(filestream,line) )
    {
        if ( std::strcmp(&line.front(), "#") == 0  || line.empty())
            continue;
        
        std::istringstream lstream(line);
        
        lstream >> this->_numConstrainedNodes;
        
        break;
    }
    
    if ( _numConstrainedNodes > 0 )
    {
        this->_constrainedNodes = Buffer<unsigned short>::create(_numConstrainedNodes);
        unsigned short* constrNodes = _constrainedNodes->lockForWrite();
        
        int counter = 0;
        unsigned short constrNodesIdx;
        
        //    read nodes
        while ( std::getline(filestream,line) && counter!=this->_numConstrainedNodes )
        {
            if ( std::strcmp(&line.front(), "#") == 0  || line.empty())
                continue;
            
            std::istringstream lstream(line);
            
            lstream >> constrNodesIdx;
            
            *constrNodes++ = constrNodesIdx;
            ++counter;
        }
        _constrainedNodes->unlock();
    }
    
    delete filebuf;
    
    return true;
}

bool TriangularMesh::loadMarkedPointsFromFile(const std::string& _filename)
{
    
    FileUtils* utils = FileUtils::getInstance();
    std::string filename = utils->fullPathForFilename(_filename);
    
    if (filename.size() == 0)
        return false;
    
    if (!utils->isFileExist(filename) )
        return false;
    
    unsigned char* filebuf = nullptr;
    ssize_t fileSize = 0;
    
    //    std::string filestring = (char*)utils->getFileData(filename.c_str(), "rb", &fileSize);
    unsigned char *data = FileUtils::getInstance()->getFileData(filename.c_str(), "rb", &fileSize);
    std::string filestring((const char*)data,fileSize);
    
    delete [] data;
    
    if (!utils->isFileExist(filename) )
        return false;
    
    std::istringstream filestream(filestring);
    std::string line;
    
    //    read header
    while ( std::getline(filestream,line) )
    {
        if (line.empty())
            continue;
        if ( std::strcmp(&line.front(), "#") == 0 )
            continue;
        
        std::istringstream lstream(line);
        
        lstream >> this->_numMarkedPoints;
        
        break;
    }
    
    this->_markedPoints = Buffer<Vec2>::create(_numMarkedPoints);
    Vec2* points = _markedPoints->lockForWrite();
    
    int counter = 0;
//    read points
    while ( std::getline(filestream,line) && counter!=this->_numMarkedPoints )
    {
        if ( std::strcmp(&line.front(), "#") == 0  || line.empty())
            continue;
        
        std::istringstream lstream(line);
        
        lstream >> counter >> points->x  >> points->y;
        ++points;
    }

    _markedPoints->unlock();
    
    delete filebuf;

    
    return true;
}


NS_CC_END

