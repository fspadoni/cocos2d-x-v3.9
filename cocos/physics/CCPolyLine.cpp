//
//  CCPolyLine.cpp
//  cocos2d_libs
//
//  Created by Federico Spadoni on 24/01/16.
//
//

#include "CCPolyLine.h"

#include "platform/CCFileUtils.h"

#include <sstream>      // std::istringstream
#include <cstring>


NS_CC_BEGIN


PolyLine* PolyLine::create()
{
    PolyLine* polyLine = new (std::nothrow) PolyLine();
    
    if (polyLine && polyLine->init() )
    {
        polyLine->autorelease();
    }
    else{
        CC_SAFE_RELEASE(polyLine);
        return nullptr;
    }
    
    return polyLine;
}

PolyLine* PolyLine::createFromFile(const std::string& filename)
{
    PolyLine* polyLine = new (std::nothrow) PolyLine();
    
    if (polyLine && polyLine->initFromFile(filename) )
    {
        polyLine->autorelease();
    }
    else{
        CC_SAFE_RELEASE(polyLine);
        return nullptr;
    }
    
    return polyLine;
}



PolyLine::PolyLine()
: _scale(1.0)
, _numNodes(0)
, _numSegments(0)
, _graphSize(0)
, _numConstrainedNodes(0)
{
    
}

PolyLine::~PolyLine()
{
    return;
}

bool PolyLine::init()
{
    
    return true;
}

bool PolyLine::initFromFile(const std::string& filename)
{
    
    //    std::string fullpath = FileUtils::getInstance()->fullPathForFilename(filename);
    if (filename.size() != 0)
    {
        if ( loadNodesFromFile(filename) == false && loadNodesFromFile(filename+".beam.node") == false)
        {
            return false;
        }
        if ( loadElementsFromFile(filename) == false && loadElementsFromFile(filename+".1.ele") == false )
        {
            return false;
        }
        if (loadGraphFromFile(filename) == false && loadGraphFromFile(filename+".1.graph") == false )
        {
            return false;
        }
        //        look for constrained nodes if any
        loadConstrainedNodesFromFile(filename+".beam.constr");
        loadConstrainedNodesFromFile(filename+".constr");
    }
    
    return true;
}

void PolyLine::setScale(const float scale)
{
    _scale = scale;
    
    Vec3* vertex = _nodes->lockForWrite();
    
    if ( vertex != nullptr)
    {
        int n =  _nodes->getSize();
        while (n--)
        {
            vertex->scale( _scale );
            ++vertex;
        }
        
    }
    _nodes->unlock();
}

bool PolyLine::loadNodesFromFile(const std::string& _filename)
{
    FileUtils* utils = FileUtils::getInstance();
    std::string filename = utils->fullPathForFilename(_filename);
    unsigned char* filebuf = nullptr;
    ssize_t fileSize = 0;
    
    //    std::string filestring = (char*)utils->getFileData(filename.c_str(), "rb", &fileSize);
    unsigned char *data = FileUtils::getInstance()->getFileData(filename.c_str(), "rb", &fileSize);
    std::string filestring((const char*)data,fileSize);
    
    delete [] data;
    
    if (!utils->isFileExist(filename) )
        return false;
    
    std::istringstream filestream(filestring);
    std::string line;
    
    //    read header
    while ( std::getline(filestream,line) )
    {
        if (line.empty())
            continue;
        if ( std::strcmp(&line.front(), "#") == 0 )
            continue;
        
        std::istringstream lstream(line);
        
        lstream >> this->_numNodes;
        
        break;
    }
    
    this->_nodes = Buffer<Vec3>::create(_numNodes);
    Vec3* vertex = _nodes->lockForWrite();
    
    this->_texCoords = Buffer<Tex2F>::create(_numNodes);
    Tex2F* texcoords = _texCoords->lockForWrite();
    
    int counter = 0;
    //    read nodes
    while ( std::getline(filestream,line) && counter!=this->_numNodes )
    {
        if ( std::strcmp(&line.front(), "#") == 0  || line.empty())
            continue;
        
        std::istringstream lstream(line);
        
        lstream >> counter >> vertex->x  >> vertex->y >> vertex->z >> texcoords->u >> texcoords->v;
        ++texcoords;
        ++vertex;
    }
    _texCoords->unlock();
    _nodes->unlock();
    
    delete filebuf;
    
    
    //    this->_texCoords = Buffer<Tex2F>::create(_numNodes);
//    texcoords = _texCoords->lockForWrite();
//    
//    const Vec3* cvertex = _nodes->lockForRead();
//    
//    counter = _numNodes;
//    while (counter--)
//    {
//        texcoords->u = cvertex->x;
//        texcoords->v = cvertex->y;
//        ++cvertex;
//        ++texcoords;
//    }
//    _texCoords->unlock();
//    _nodes->unlock();
    
    
    return true;
}


bool PolyLine::loadElementsFromFile(const std::string& _filename)
{
    
    _numSegments = _numNodes - 1;
    
    this->_segments = Buffer<unsigned short>::create(_numSegments*2);
    unsigned short* segm = _segments->lockForWrite();
    
    for (int i=0; i<_numSegments; ++i)
    {
        *segm++ = i;
        *segm++ = i+1;
    }
    _segments->unlock();
    
    return true;
    
}



static int ushortCompare(const void * a, const void * b)
{
    return ( *(unsigned short*)a - *(unsigned short*)b );
}

static int uintCompare(const void * a, const void * b)
{
    return ( *(unsigned int*)a - *(unsigned int*)b );
}

static int intCompare (const void * a, const void * b)
{
    return ( *(int*)a - *(int*)b );
}

bool PolyLine::loadGraphFromFile(const std::string& _filename)
{
    
    _graphSize = 4;
    
    if ( _numNodes > 1)
    {
        _graphSize += 3 * (_numNodes-2);
    }
    
    this->_rows = Buffer<unsigned short>::create(_numNodes+1);
    unsigned short* rows = _rows->lockForWrite();
    
    this->_columns = Buffer<unsigned short>::create(_graphSize); // add diag verts
    unsigned short* columns = _columns->lockForWrite();

    this->_diagonal = Buffer<unsigned short>::create(_numNodes); // add diag verts
    unsigned short* diag = _diagonal->lockForWrite();
    
    
    //        first node
    unsigned short idx = 0;
    *rows++ = idx;
    *columns++ = 0;
    *columns++ = 1;
    *diag++ = idx;
    idx += 2;
    
    for (unsigned int i = 1; i < _numNodes-1; ++i)
    {
        *rows++ = idx;
        *columns++ = i-1;
        *columns++ = i;
        *columns++ = i+1;
        *diag++ = idx+1;
        idx += 3;
    }
    
    //        last node
    *rows++ = idx;
    *columns++ = _numNodes - 2;
    *columns++ = _numNodes - 1;
    *diag++ = idx+1;
    idx += 2;
    *rows++ = idx;
    

    return true;

}


bool PolyLine::loadConstrainedNodesFromFile(const std::string& _filename)
{
    FileUtils* utils = FileUtils::getInstance();
    std::string filename = utils->fullPathForFilename(_filename);
    
    if (!utils->isFileExist(filename) )
        return false;
    
    CCLOG("constrained nodes file found");
    
    unsigned char* filebuf = nullptr;
    ssize_t fileSize = 0;
    
    //    std::string filestring = (char*)utils->getFileData(filename.c_str(), "rb", &fileSize);
    unsigned char *data = FileUtils::getInstance()->getFileData(filename.c_str(), "rb", &fileSize);
    std::string filestring((const char*)data,fileSize);
    
    delete [] data;
    
    std::istringstream filestream(filestring);
    std::string line;
    
    //    read header
    while ( std::getline(filestream,line) )
    {
        if ( std::strcmp(&line.front(), "#") == 0  || line.empty())
            continue;
        
        std::istringstream lstream(line);
        
        lstream >> this->_numConstrainedNodes;
        
        break;
    }
    
    this->_constrainedNodes = Buffer<unsigned short>::create(_numConstrainedNodes);
    unsigned short* constrNodes = _constrainedNodes->lockForWrite();
    
    int counter = 0;
    unsigned short constrNodesIdx;
    
    //    read nodes
    while ( std::getline(filestream,line) && counter!=this->_numConstrainedNodes )
    {
        if ( std::strcmp(&line.front(), "#") == 0  || line.empty())
            continue;
        
        std::istringstream lstream(line);
        
        lstream >> constrNodesIdx;
        
        *constrNodes++ = constrNodesIdx;
        ++counter;
    }
    _constrainedNodes->unlock();
    
    delete filebuf;
    
    return true;
}


NS_CC_END
