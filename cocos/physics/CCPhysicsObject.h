//
//  CCPhysicsObject.h
//  cocos2d_libs
//
//  Created by Federico Spadoni on 02/03/16.
//
//

#ifndef __cocos2d_libs__CCPhysicsObject__
#define __cocos2d_libs__CCPhysicsObject__

#include "base/CCRef.h"
#include "math/Vec3.h"

NS_CC_BEGIN

class PhysicsActionManager;
class PhysicsAction;
class PhysicsWorld;


class CC_DLL PhysicsObject : public Ref
{
public:

    /**
     * Sets the ActionManager object that is used by all actions.
     *
     * @warning If you set a new ActionManager, then previously created actions will be removed.
     *
     * @param actionManager     A ActionManager object that is used by all actions.
     */
    virtual void setActionManager(PhysicsActionManager* actionManager);
    /**
     * Gets the ActionManager object that is used by all actions.
     * @see setActionManager(ActionManager*)
     * @return A ActionManager object.
     */
    virtual PhysicsActionManager* getActionManager() { return _actionManager; }
    virtual const PhysicsActionManager* getActionManager() const { return _actionManager; }
    
    /**
     * Executes an action, and returns the action that is executed.
     *
     * This node becomes the action's target. Refer to Action::getTarget().
     * @warning Actions don't retain their target.
     *
     * @param action An Action pointer.
     */
    virtual PhysicsAction* runAction(PhysicsAction* action);
    
    /**
     * Stops and removes all actions from the running action list .
     */
    void stopAllActions();
    
    /**
     * Stops and removes an action from the running action list.
     *
     * @param action    The action object to be removed.
     */
    void stopAction(PhysicsAction* action);
    
    /**
     * Removes an action from the running action list by its tag.
     *
     * @param tag   A tag that indicates the action to be removed.
     */
    void stopActionByTag(int tag);
    
    /**
     * Removes all actions from the running action list by its tag.
     *
     * @param tag   A tag that indicates the action to be removed.
     */
    void stopAllActionsByTag(int tag);
    
    /**
     * Removes all actions from the running action list by its flags.
     *
     * @param flags   A flag field that removes actions based on bitwise AND.
     */
    void stopActionsByFlags(unsigned int flags);
    
    /**
     * Gets an action from the running action list by its tag.
     *
     * @see `setTag(int)`, `getTag()`.
     *
     * @return The action object with the given tag.
     */
    PhysicsAction* getActionByTag(int tag);
    
    /**
     * Returns the numbers of actions that are running plus the ones that are schedule to run (actions in actionsToAdd and actions arrays).
     *
     * Composable actions are counted as 1 action. Example:
     *    If you are running 1 Sequence of 7 actions, it will return 1.
     *    If you are running 7 Sequences of 2 actions, it will return 7.
     * @todo Rename to getNumberOfRunningActions()
     *
     * @return The number of actions that are running plus the ones that are schedule to run.
     */
    ssize_t getNumberOfRunningActions() const;
    
    
    /** get the world body added to. */
    inline PhysicsWorld* getWorld() const { return _world; }
    
    
    virtual Vec3 getActionPosition(void) { return Vec3::ZERO; }
    
    virtual void setActionPosition(const Vec3& position) {}
    
    virtual Vec3 getActionRotation(void) { return Vec3::ZERO; }
    
    virtual void setActionRotation(const Vec3& rotation) {}
    
    virtual void stopAction() {}
    
    
CC_CONSTRUCTOR_ACCESS:
    // PhysicsObject should be created using create();
    PhysicsObject();
    
    virtual ~PhysicsObject();
    
    
protected:
    
    PhysicsActionManager *_actionManager;  ///< a pointer to ActionManager singleton, which is used to handle all the actions
    
    PhysicsWorld* _world;
    
};


NS_CC_END

#endif /* defined(__cocos2d_libs__CCPhysicsObject__) */
