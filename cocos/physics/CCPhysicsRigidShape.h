//
//  CCPhysicsRigidShape.h
//  cocos2d_libs
//
//  Created by Federico Spadoni on 19/02/16.
//
//

#ifndef __cocos2d_libs__CCPhysicsRigidShape__
#define __cocos2d_libs__CCPhysicsRigidShape__

#include "base/ccConfig.h"
#if CC_USE_PHYSICS

#include "base/CCRef.h"
#include "math/CCGeometry.h"


struct cpShape;

NS_CC_BEGIN

class PhysicsBody;


/**
 *  @addtogroup physics
 *  @{
 *  @addtogroup physics_2d
 *  @{
 
 */

/**
 * @brief A shape for body. You do not create PhysicsWorld objects directly, instead, you can view PhysicsBody to see how to create it.
 */
class CC_DLL PhysicsRigidShape
{

public:
    /**
     * Creates a PhysicsShapeCircle with specified value.
     *
     * @param   radius A float number, it is the circle's radius.
     * @param   material A PhysicsMaterial object, the default value is PHYSICSSHAPE_MATERIAL_DEFAULT.
     * @param   offset A Vec2 object, it is the offset from the body's center of gravity in body local coordinates.
     * @return  An autoreleased PhysicsShapeCircle object pointer.
     */
    static cpShape* createCircle(float radius, float elasticity, float friction);
    

    /**
     * Creates a PhysicsShapePolygon with specified value.
     *
     * @param   points A Vec2 object pointer, it is an array of Vec2.
     * @param   count An integer number, contains the count of the points array.
     * @param   material A PhysicsMaterial object, the default value is PHYSICSSHAPE_MATERIAL_DEFAULT.
     * @param   offset A Vec2 object, it is the offset from the body's center of gravity in body local coordinates.
     * @return  An autoreleased PhysicsShapePolygon object pointer.
     */
    static cpShape* createPoly(const Vec2* points, int count, float elasticity, float friction );
    
    
    /**
     * Creates a PhysicsShapeBox with specified value.
     *
     * @param   size Size contains this box's width and height.
     * @param   material A PhysicsMaterial object, the default value is PHYSICSSHAPE_MATERIAL_DEFAULT.
     * @param   offset A Vec2 object, it is the offset from the body's center of gravity in body local coordinates.
     * @return  An autoreleased PhysicsShapeBox object pointer.
     */
    static cpShape* createBox(const Size& size, float elasticity, float friction);
    
      /**
     * Creates a PhysicsShapeEdgeSegment with specified value.
     *
     * @param   a It's the edge's begin position.
     * @param   b It's the edge's end position.
     * @param   material A PhysicsMaterial object, the default value is PHYSICSSHAPE_MATERIAL_DEFAULT.
     * @param   border It's a edge's border width.
     * @return  An autoreleased PhysicsShapeEdgeSegment object pointer.
     */
    static cpShape* createEdgeSegmnt(const Vec2& a, const Vec2& b, float border, float elasticity, float friction );

    /**
     * Creates a PhysicsShapeEdgePolygon with specified value.
     *
     * @param   points A Vec2 object pointer, it contains an array of points.
     * @param   count An integer number, contains the count of the points array.
     * @param   material A PhysicsMaterial object, the default value is PHYSICSSHAPE_MATERIAL_DEFAULT.
     * @param   border It's a edge's border width.
     * @return  An autoreleased PhysicsShapeEdgePolygon object pointer.
     */
    static std::vector<cpShape*> createEdgePoly(const Vec2* points, int count,  float border, float elasticity, float friction);
    
       /**
     * Creates a PhysicsShapeEdgeBox with specified value.
     *
     * @param   size Size contains this box's width and height.
     * @param   material A PhysicsMaterial object, the default value is PHYSICSSHAPE_MATERIAL_DEFAULT.
     * @param   border It's a edge's border width.
     * @param   offset A Vec2 object, it is the offset from the body's center of gravity in body local coordinates.
     * @return  An autoreleased PhysicsShapeEdgeBox object pointer.
     */
    static std::vector<cpShape*> createEdgeBox(const Size& size, float border, float elasticity, float friction);
    
    /**
     * Creates a PhysicsShapeEdgeChain with specified value.
     *
     * @param   points A Vec2 object pointer, it contains an array of points.
     * @param   count An integer number, contains the count of the points array.
     * @param   material A PhysicsMaterial object, the default value is PHYSICSSHAPE_MATERIAL_DEFAULT.
     * @param   border It's a edge's border width.
     * @return  An autoreleased PhysicsShapeEdgeChain object pointer.
     */
    static std::vector<cpShape*> createEdgeChain(const Vec2* points, int count, float border, float elasticity, float friction);
    
  
};

/** @} */
/** @} */

NS_CC_END

#endif // CC_USE_PHYSICS

#endif /* defined(__cocos2d_libs__CCPhysicsRigidShape__) */
