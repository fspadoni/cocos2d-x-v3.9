//
//  CCPhysicsType.h
//  cocos2d_libs
//
//  Created by Federico Spadoni on 24/01/16.
//
//

#ifndef cocos2d_libs_CCPhysicsType_h
#define cocos2d_libs_CCPhysicsType_h

#include "base/ccTypes.h"
#include "math/Vec2.h"

NS_CC_BEGIN

//! a Vec2 with a vertex point, a tex coord point and a color 4B
struct CC_DLL V2F_C4F
{
    //! vertices (2F)
    Vec2       vertices;
    //! colors (4B)
    Color4F        colors;
};

//! a Vec2 with a vertex point, a tex coord point and a color 4F
struct CC_DLL V2F_T2F
{
    //! vertices (2F)
    Vec2       vertices;
    //! tex coords (2F)
    Tex2F          texCoords;
};


//! a Vec2 with a vertex point, Vec2 normal point and a color 4B
struct CC_DLL V2F_N2F_T2F_C4F
{
    //! vertices (2F)
    Vec2       vertices;
    // normals (2F)
    Vec2       normals;
    //  Tex2F
    Tex2F texCoords;
    //! colors (4B)
    Color4F        colors;
};

NS_CC_END

#endif
