//
//  CCPhysicsObject.cpp
//  cocos2d_libs
//
//  Created by Federico Spadoni on 02/03/16.
//
//

#include "CCPhysicsObject.h"

#include "physics-actions/CCPhysicsActionManager.h"
#include "physics-actions/CCPhysicsAction.h"
#include "base/CCDirector.h"
#include "physics/CCPhysicsWorld.h"

NS_CC_BEGIN


PhysicsObject::PhysicsObject()
: _world(nullptr)
{
    // set default actionManager
    _actionManager = Director::getInstance()->getPhysicsActionManager();
    _actionManager->retain();
}



PhysicsObject::~PhysicsObject()
{
    
    stopAllActions();
    CC_SAFE_RELEASE_NULL(_actionManager);
}

void PhysicsObject::setActionManager(PhysicsActionManager* actionManager)
{
    if( actionManager != _actionManager )
    {
        this->stopAllActions();
        CC_SAFE_RETAIN(actionManager);
        CC_SAFE_RELEASE(_actionManager);
        _actionManager = actionManager;
    }
}

// MARK: actions

PhysicsAction * PhysicsObject::runAction(PhysicsAction* action)
{
    CCASSERT( action != nullptr, "Argument must be non-nil");
    bool pause = getWorld() ? false : true;
    _actionManager->addAction(action, this, pause);
    return action;
}

void PhysicsObject::stopAllActions()
{
    _actionManager->removeAllActionsFromTarget(this);
}

void PhysicsObject::stopAction(PhysicsAction* action)
{
    _actionManager->removeAction(action);
}

void PhysicsObject::stopActionByTag(int tag)
{
    CCASSERT( tag != PhysicsAction::INVALID_TAG, "Invalid tag");
    _actionManager->removeActionByTag(tag, this);
}

void PhysicsObject::stopAllActionsByTag(int tag)
{
    CCASSERT( tag != PhysicsAction::INVALID_TAG, "Invalid tag");
    _actionManager->removeAllActionsByTag(tag, this);
}

void PhysicsObject::stopActionsByFlags(unsigned int flags)
{
    if (flags > 0)
    {
        _actionManager->removeActionsByFlags(flags, this);
    }
}

PhysicsAction * PhysicsObject::getActionByTag(int tag)
{
    CCASSERT( tag != PhysicsAction::INVALID_TAG, "Invalid tag");
    return _actionManager->getActionByTag(tag, this);
}

ssize_t PhysicsObject::getNumberOfRunningActions() const
{
    return _actionManager->getNumberOfRunningActionsInTarget(this);
}

NS_CC_END
