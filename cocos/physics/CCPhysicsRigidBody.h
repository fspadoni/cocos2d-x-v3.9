//
//  CCPhysicsRigidBody.h
//  cocos2d_libs
//
//  Created by Federico Spadoni on 19/02/16.
//
//
#ifndef __cocos2d_libs__CCPhysicsRigidBody__
#define __cocos2d_libs__CCPhysicsRigidBody__

#include "base/ccConfig.h"

#include "physics/CCPhysicsObject.h"

#if CC_USE_PHYSICS
//#include "2d/CCComponent.h"
//#include "base/CCRef.h"
//#include "base/ccTypes.h"
#include "base/ccMacros.h"
#include "math/Vec2.h"


#include <set>


//chipmunk forward decl
struct cpShape;
struct cpBody;
struct cpArbiter;


NS_CC_BEGIN

class Node;
class PhysicsWorld;
class PhysicsJoint;


/**
 * @addtogroup physics
 * @{
 * @addtogroup physics_2d
 * @{
 */


class CC_DLL PhysicsRigidBody : public PhysicsObject
{
public:
//    const static std::string COMPONENT_NAME;
    
    /**
     * Create a body with default mass and moment.
     *
     * This default mass value is 1.0.
     * This default moment value is 200.
     * @return  An autoreleased PhysicsRigidBody object pointer.
     */
    static PhysicsRigidBody* create(bool addToWorldStaticBody = true);
    
    
    /**
     * Create a body with mass and moment.
     *
     * @param mass This body's mass.
     * @param moment This body's moment.
     * @return  An autoreleased PhysicsRigidBody object pointer.
     */
    static PhysicsRigidBody* create(float mass, float moment);
    
    
    
    void addShape(cpShape* shape);
    
    void removeShape(cpShape* shape);
    
    
    void setPosition( const Vec2& position);
    
    void setRotation(float rotation);
    
    void setScale(float scaleX, float scaleY);
    
    
    
    /**
     * Applies a continuous force to body.
     *
     * @param force The force is applies to this body.
     * @param offset A Vec2 object, it is the offset from the body's center of gravity in world coordinates.
     */
    void applyForce(const Vec2& force, const Vec2& offset = Vec2::ZERO);
    
    /**
     * reset all the force applied to body.
     */
//    virtual void resetForces();
    
    /**
     * Applies a immediate force to body.
     *
     * @param impulse The impulse is applies to this body.
     * @param offset A Vec2 object, it is the offset from the body's center of gravity in world coordinates.
     */
    void applyImpulse(const Vec2& impulse, const Vec2& offset = Vec2::ZERO);
    
    /**
     * Applies a torque force to body.
     *
     * @param torque The torque is applies to this body.
     */
    void applyTorque(float torque);
    
    /**
     * Set the velocity of a body.
     *
     * @param velocity The velocity is set to this body.
     */
    void setVelocity(const Vec2& velocity);
    
    /** Get the velocity of a body. */
    Vec2 getVelocity();
    

    Vec2 getVelocityAtLocalPoint(const Vec2& point);
    
    Vec2 getVelocityAtWorldPoint(const Vec2& point);
    
    void setAngularVelocity(float velocity);
    
    float getAngularVelocity();
    
    
    /** remove the body from the world it added to */
    void removeFromWorld();

    
    
    /** get the node the body set to. */
//    Node* getNode() const { return _owner; }
    
   
    void setCollisionFilter(unsigned int category, unsigned int mask = ~(unsigned int)0u/*CP_ALL_CATEGORIES*/);
    
    
//    void getCollisionFilter(unsigned int category, unsigned int mask = CP_ALL_CATEGORIES);
    
    
    
    /** get the body position. */
    Vec2 getPosition() const;
    
    /** get the body rotation. */
    float getRotation();

    
    /**
     * @brief Test the body is dynamic or not.
     *
     * A dynamic body will effect with gravity.
     */
    bool isDynamic() const;
    /**
     * @brief Set dynamic to body.
     *
     * A dynamic body will effect with gravity.
     */
    void setDynamic(bool dynamic);
    
    /**
     * @brief Set the body mass.
     *
     * @attention If you need add/subtract mass to body, don't use setMass(getMass() +/- mass), because the mass of body may be equal to PHYSICS_INFINITY, it will cause some unexpected result, please use addMass() instead.
     */
    void setMass(float mass);
    
    /** Get the body mass. */
    float getMass() const;

    
    /**
     * @brief Set the body moment of inertia.
     *
     * @note If you need add/subtract moment to body, don't use setMoment(getMoment() +/- moment), because the moment of body may be equal to PHYSICS_INFINITY, it will cause some unexpected result, please use addMoment() instead.
     */
    void setMoment(float moment);
    
    /** Get the body moment of inertia. */
    float getMoment() const;
    

    
    
    /** Whether the body is at rest. */
    bool isResting() const;
    
    /** set body to rest */
    void setResting(bool rest) const;
  
    
    /** Get the body's tag. */
    inline int getTag() const { return _tag; }
    
    /** set the body's tag. */
    inline void setTag(int tag) { _tag = tag; }
    
    /** Convert the world point to local. */
    Vec2 world2Local(const Vec2& point);
    
    /** Convert the local point to world. */
    Vec2 local2World(const Vec2& point);
    
    /** Get the rigid body of chipmunk. */
    cpBody* getCPBody() const { return _cpBody; }
    
    const std::set<cpShape*>& getCPShapes() const { return _cpShapes; }
    
    
//    virtual void onEnter() override;
//    virtual void onExit() override;
//    virtual void onAdd() override;
//    virtual void onRemove() override;
    
    
    void notifyBeginCollision(bool notify) {  _notifyBeginCollision = notify; }
    
    void notifySeparateCollision(bool notify) { _notifySeparateCollision = notify; }
    
    bool notifyBeginCollision() const {  return  _notifyBeginCollision; }
    
    bool notifySeparateCollision() const { return _notifySeparateCollision; }
    
    
    virtual void collisionBegin( PhysicsRigidBody* collidingBody, cpArbiter* arb ) {}
    
    virtual void collisionSeparate(PhysicsRigidBody* collidingBody, cpArbiter* arb) {}
    
    
    //    Fede
    void destroy();
    
protected:
    
    PhysicsRigidBody();
    virtual ~PhysicsRigidBody();
    
    
    bool init(float mass, float moment);
    
    bool init( bool addToWorldStaticBody );
    
    
protected:

    
    cpBody* _cpBody;
    std::set<cpShape*> _cpShapes;
    
    int _tag;
    
    //    Fede:
    bool _destoryMark;
    

    bool _notifyBeginCollision;
    
    bool  _notifySeparateCollision;
    
    
    friend class PhysicsWorld;
    friend class PhysicsShape;
    friend class PhysicsJoint;
};

/** @} */
/** @} */


NS_CC_END

#endif // CC_USE_PHYSICS
#endif /* defined(__cocos2d_libs__CCPhysicsRigidBody__) */
