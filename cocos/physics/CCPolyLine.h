//
//  CCPolyLine.h
//  cocos2d_libs
//
//  Created by Federico Spadoni on 24/01/16.
//
//

#ifndef __cocos2d_libs__CCPolyLine__
#define __cocos2d_libs__CCPolyLine__

#include "base/CCRef.h"
#include "base/ccTypes.h"

#include "physics-utils/CCMemoryBuffer.h"
#include "math/Vec2.h"

NS_CC_BEGIN



class CC_DLL PolyLine : public cocos2d::Ref
{
public:
    
    static PolyLine* create();
    
    static PolyLine* createFromFile(const std::string& filename);
    
    enum{
        _input_line_size = 1024
    };
    
    inline unsigned int getNumNodes() const {return _numNodes;}
    inline unsigned int getNumSegments() const {return _numSegments;}
    inline unsigned int getGraphSize() const {return _graphSize;}
    inline unsigned int getNumConstrainedNodes() const { return _numConstrainedNodes; }
    
    void setScale(const float scale);
    
protected:
    
    PolyLine();
    virtual ~PolyLine();
    
    bool init();
    bool initFromFile(const std::string& filename);
    
    
    bool loadNodesFromFile(const std::string& filename);
    bool loadElementsFromFile(const std::string& filename);
    bool loadGraphFromFile(const std::string& _filename);
    bool loadConstrainedNodesFromFile(const std::string& filename);
    
    
    unsigned int _numNodes;
    unsigned int _numSegments;
    unsigned int _graphSize;
    unsigned int _numConstrainedNodes;
    
    BufferSharedPtr<Vec3> _nodes;
    BufferSharedPtr<unsigned short> _segments;
    
    // graph
    BufferSharedPtr<unsigned short> _rows;
    BufferSharedPtr<unsigned short> _columns;
    BufferSharedPtr<unsigned short> _diagonal;
    BufferSharedPtr<unsigned short> _constrainedNodes;
    
    BufferSharedPtr<Tex2F> _texCoords;
    
    float _scale;
    
    int alignment;
    
private:
    
    
    friend class PhysicsBeamBody;
};

NS_CC_END


#endif /* defined(__cocos2d_libs__CCPolyLine__) */
