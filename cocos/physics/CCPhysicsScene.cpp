//
//  CCPhysicsScene.cpp
//  cocos2d_libs
//
//  Created by Federico Spadoni on 03/12/14.
//
//
#include "physics/CCPhysicsScene.h"

#include "base/CCDirector.h"
//#include "2d/CCCamera.h"
#include "base/CCEventDispatcher.h"
//#include "base/CCEventListenerCustom.h"
//#include "renderer/CCRenderer.h"
#include "deprecated/CCString.h"

#if CC_USE_PHYSICS
#include "physics/CCPhysicsWorld.h"
#include "physics-thread/CCTaskScheduler.h"
#endif

#if CC_USE_3D_PHYSICS && CC_ENABLE_BULLET_INTEGRATION
#include "physics3d/CCPhysics3DWorld.h"
#include "physics3d/CCPhysics3DComponent.h"
#endif

#if CC_USE_NAVMESH
#include "navmesh/CCNavMesh.h"
#endif


#if PLATFORM_SUPPORTS_TBB
#include <tbb/tbb.h>
#include <tbb/task_scheduler_init.h>
#endif

NS_CC_BEGIN

#if CC_USE_PHYSICS


#if !PLATFORM_SUPPORTS_TBB

class SimulateTask : public Task
{
public:
    SimulateTask(PhysicsWorld* physicsWorld, const float dt, Task::Status* status)
    : Task(status)
    , _physicsWorld(physicsWorld)
    , _dt(dt)
    {
    }
    
    virtual ~SimulateTask()  {}
    
    virtual bool run(WorkerThread* thread)
    {
        _physicsWorld->step(_dt);
//         _physicsWorld->update(_dt, true);
        delete this;
        return true;
    }
    
private:
    PhysicsWorld* _physicsWorld;
    const float _dt;
};

#else

class SimulateTask : public tbb::task
{
public:
    SimulateTask(PhysicsWorld* physicsWorld, const float dt)
    : tbb::task()
    , _physicsWorld(physicsWorld)
    , _dt(dt)
    {
    }
    
    virtual ~SimulateTask()  {}
    
    virtual tbb::task* execute()
    {
        _physicsWorld->step(_dt);
        
        return nullptr;
    }
    
private:
    PhysicsWorld* _physicsWorld;
    const float _dt;
};
#endif


const char* PhysicsScene::_EVENT_SIM_LOOP_COMPLETION = "__simulation_loop_completion";

const int PhysicsScene::_sim_update_rates[4] = { _sim_update_rate_min, _sim_update_rate_low, _sim_update_rate_mid, _sim_update_rate_high };

int PhysicsScene::_current_update_rate = 2; // _sim_update_rate_mid

PhysicsScene::PhysicsScene(float hashCellSize, float hashCellCount)
: Scene()
, _hashCellSize(hashCellSize)
, _hashCellCount(hashCellCount)
, _frames(0)
, _accumDt(0.0)
, _simulationPaused(false)
//, _current_update_rate(2)
{
    
#if !PLATFORM_SUPPORTS_TBB
    TaskScheduler::getInstance().start(2);
#else
    _tbbScheduler = new tbb::task_scheduler_init( 2 );
    _rootTask = new( tbb::task::allocate_root() ) tbb::empty_task();
    _rootTask->set_ref_count( 1 );
#endif
    
}

PhysicsScene::~PhysicsScene()
{
#if PLATFORM_SUPPORTS_TBB
    _rootTask->destroy( *_rootTask );
    delete _tbbScheduler;
#endif
    Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(PhysicsScene::_EVENT_SIM_LOOP_COMPLETION);
}



PhysicsScene* PhysicsScene::create(float hashCellSize, float hashCellCount)
{
    PhysicsScene *ret = new (std::nothrow) PhysicsScene(hashCellSize, hashCellCount);
    if (ret && ret->initWithPhysics() )
    {
        ret->autorelease();
        return ret;
    }
    else
    {
        CC_SAFE_DELETE(ret);
        return nullptr;
    }
}

bool PhysicsScene::initWithPhysics()
{
    bool ret = Scene::initWithPhysics();
    if ( ret )
        _physicsWorld->setAutoStep(false);
    
    return ret;
}



std::string PhysicsScene::getDescription() const
{
    return StringUtils::format("<PhysicsScene | tag = %d>", _tag);
}

PhysicsScene* PhysicsScene::getScene() const
{
    // FIX ME: should use const_case<> to fix compiling error
    return const_cast<PhysicsScene*>(this);
}

void PhysicsScene::addSoftBodyToPhysicsWorld(PhysicsSoftBody* softbody)
{
    if (_physicsWorld)
    {
        _physicsWorld->addSoftBody(softbody);
    }
}

void PhysicsScene::addBeamBodyToPhysicsWorld(PhysicsBeamBody* beambody)
{
    if (_physicsWorld)
    {
        _physicsWorld->addBeamBody(beambody);
    }
}

void PhysicsScene::addRigidBodyToPhysicsWorld(PhysicsRigidBody* body)
{
    if (_physicsWorld)
    {
        _physicsWorld->addRigidBody(body);
    }
}

const int PhysicsScene::computeUpdateRate(int frameRate)
{
    if ( frameRate > 58 )
    {
//        try to increase
        ++_current_update_rate;
        if ( _current_update_rate == _sim_update_rate_levels )
            --_current_update_rate;
    }
    else
    {
//        decrease
        _current_update_rate = std::max(0, _current_update_rate-1);
        
    }
    
    return _current_update_rate;
}


void PhysicsScene::adjustUpdateRate(float delta)
{
    
    ++_frames;
    _accumDt += delta;
    
    if (_accumDt > CC_DIRECTOR_STATS_INTERVAL)
    {
        int _frameRate = _frames / _accumDt;
        _frames = 0;
        _accumDt = 0.0;
        _frameRate = _sim_update_rates[computeUpdateRate(_frameRate)];
        _physicsWorld->setUpdateRate( _frameRate );
    }
}

void PhysicsScene::cleanup()
{
    
    // actions
    _physicsWorld->cleanup();
//    _physicsWorld->unscheduleAllCallbacks();
    
    
    Node::cleanup();
}



void PhysicsScene::stepPhysicsAndNavigation(float deltaTime)
{
//    Node::update(delta);
    
//    if (nullptr != _physicsWorld && _physicsWorld->isAutoStep())
//    {
//        _physicsWorld->update(delta);
//    }
    
#if !PLATFORM_SUPPORTS_TBB
//    wait for simulation step completion
    WorkerThread::getCurrent()->workUntilDone( &_simLoopStatus );
#else
    _rootTask->wait_for_all();
#endif
    
    //    --- single thread begin-----
    //    thread safe updates

    updateThreadSafe(deltaTime);
    
    
    
    if ( !_simulationPaused )
    {
        
        //     update vertex buffers before running another simulation step
        
        _eventDispatcher->dispatchCustomEvent( _EVENT_SIM_LOOP_COMPLETION, this);
        
        
        //    check large time step
        //    adjustUpdateRate(delta);
        
        
#if CC_USE_3D_PHYSICS && CC_ENABLE_BULLET_INTEGRATION
        if (_physics3DWorld)
        {
            _physics3DWorld->stepSimulate(deltaTime);
        }
#endif
#if CC_USE_NAVMESH
        if (_navMesh)
        {
            _navMesh->update(deltaTime);
        }
#endif
        
        //    --- single thread end -----
        
#if CC_USE_PHYSICS
        simulate(deltaTime);
#endif
        
    }

    
}

void PhysicsScene::simulate(float delta)
{
    if (nullptr != _physicsWorld )
    {
        
#if !PLATFORM_SUPPORTS_TBB
        WorkerThread* thread = WorkerThread::getCurrent();
        Task* task = new SimulateTask( _physicsWorld, delta,  &_simLoopStatus );
        thread->addTask( task );
#else
        _rootTask->set_ref_count( 2 );
        tbb::task& simTaskTbb = * new( _rootTask->allocate_child() ) SimulateTask(_physicsWorld, delta );
        _rootTask->spawn( simTaskTbb );
#endif
    }
    
}

void PhysicsScene::resetPhysicsWorld()
{
    if (_physicsWorld)
    {
        _physicsWorld->reset();
    }
}



#endif // CC_USE_PHYSICS

NS_CC_END