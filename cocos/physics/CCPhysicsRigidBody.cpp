//
//  CCPhysicsRigidBody.cpp
//  cocos2d_libs
//
//  Created by Federico Spadoni on 19/02/16.
//
//

#include "physics/CCPhysicsRigidBody.h"



#include "2d/CCScene.h"
#include "physics/CCPhysicsShape.h"
#include "physics/CCPhysicsJoint.h"
#include "physics/CCPhysicsWorld.h"
#include "physics-utils/CCPhysicsHelper.h"

#include "chipmunk.h"


NS_CC_BEGIN

extern const float PHYSICS_INFINITY;

//const std::string PhysicsRigidBody::COMPONENT_NAME = "PhysicsRigidBody";




PhysicsRigidBody::PhysicsRigidBody()
: PhysicsObject()
, _cpBody(nullptr)
, _tag(0)
, _notifyBeginCollision(false)
, _notifySeparateCollision(false)
{
}


PhysicsRigidBody::~PhysicsRigidBody()
{
    

}

void PhysicsRigidBody::destroy()
{
    
    if (_cpBody)
        cpBodyFree(_cpBody);
    
    for (auto shape : _cpShapes)
    {
        cpShapeFree(shape);
    }
}

PhysicsRigidBody* PhysicsRigidBody::create(bool addToWorldStaticBody)
{
    PhysicsRigidBody* body = new (std::nothrow) PhysicsRigidBody();
    if (body && body->init(addToWorldStaticBody))
    {
        body->autorelease();
        return body;
    }
    
    CC_SAFE_DELETE(body);
    return nullptr;
}


PhysicsRigidBody* PhysicsRigidBody::create(float mass, float moment)
{
    PhysicsRigidBody* body = new (std::nothrow) PhysicsRigidBody();
    if (body)
    {

        if (body->init(mass, moment))
        {
            body->autorelease();
            return body;
        }
    }
    
    CC_SAFE_DELETE(body);
    return nullptr;
    
}


bool PhysicsRigidBody::init(bool addToWorldStaticBody)
{
    do
    {
        if ( addToWorldStaticBody )
        {
            _cpBody = nullptr;
        }
        else
        {
            _cpBody = cpBodyNewStatic();
            CC_BREAK_IF(_cpBody == nullptr);
        }
        
        return true;
    } while (false);
    
    return false;
}

bool PhysicsRigidBody::init(float mass, float moment)
{
    do
    {
        _cpBody = cpBodyNew(mass, moment);
        
        CC_BREAK_IF(_cpBody == nullptr);
        
        return true;
    } while (false);
    
    return false;
}

void PhysicsRigidBody::addShape(cpShape* shape)
{
    cpShapeSetBody(shape, _cpBody);
    cpShapeSetUserData(shape, this);
    _cpShapes.insert(shape);
}

void PhysicsRigidBody::removeShape(cpShape* shape)
{
    cpShapeSetUserData(shape, nullptr);
    cpShapeSetBody(shape, _cpBody);
    _cpShapes.erase(shape);
}


void PhysicsRigidBody::setCollisionFilter(unsigned int category, unsigned int mask )
{
    cpShapeFilter filter = cpShapeFilterNew(CP_NO_GROUP, category, mask);
    
    for ( auto shape : _cpShapes )
    {
        cpShapeSetFilter(shape, filter);
    }
}


void PhysicsRigidBody::setPosition( const Vec2& position)
{ cpBodySetPosition(_cpBody, PhysicsHelper::point2cpv(position) ); }

void PhysicsRigidBody::setRotation(float rotation)
{ cpBodySetAngle(_cpBody, rotation); }

void PhysicsRigidBody::setScale(float scaleX, float scaleY)
{
}

void PhysicsRigidBody::applyForce(const Vec2& force, const Vec2& offset )
{ cpBodyApplyForceAtWorldPoint( _cpBody, PhysicsHelper::point2cpv(force), PhysicsHelper::point2cpv(offset)); }


void PhysicsRigidBody::applyImpulse(const Vec2& impulse, const Vec2& offset)
{ cpBodyApplyImpulseAtWorldPoint( _cpBody, PhysicsHelper::point2cpv(impulse), PhysicsHelper::point2cpv(offset)); }


void PhysicsRigidBody::applyTorque(float torque)
{ cpBodySetTorque(_cpBody, torque); }


void PhysicsRigidBody::setVelocity(const Vec2& velocity)
{ cpBodySetVelocity(_cpBody, PhysicsHelper::point2cpv(velocity)); }


Vec2 PhysicsRigidBody::PhysicsRigidBody::getVelocity()
{ return PhysicsHelper::cpv2point(cpBodyGetVelocity(_cpBody)); }


Vec2 PhysicsRigidBody::getVelocityAtLocalPoint(const Vec2& point)
{ return PhysicsHelper::cpv2point(cpBodyGetVelocityAtLocalPoint(_cpBody, PhysicsHelper::point2cpv(point))); }

Vec2 PhysicsRigidBody::getVelocityAtWorldPoint(const Vec2& point)
{ return PhysicsHelper::cpv2point(cpBodyGetVelocityAtWorldPoint(_cpBody, PhysicsHelper::point2cpv(point))); }

void PhysicsRigidBody::setAngularVelocity(float velocity)
{ cpBodySetAngularVelocity(_cpBody, velocity); }

float PhysicsRigidBody::getAngularVelocity()
{ return PhysicsHelper::cpfloat2float(cpBodyGetAngularVelocity(_cpBody)); }


Vec2 PhysicsRigidBody::getPosition() const
{ return PhysicsHelper::cpv2point( cpBodyGetPosition(_cpBody) ); }


float PhysicsRigidBody::getRotation()
{ return - cpBodyGetAngle(_cpBody) * 180.0 / M_PI; }



bool PhysicsRigidBody::isDynamic() const
{ return cpBodyGetType(_cpBody) == CP_BODY_TYPE_DYNAMIC; }


void PhysicsRigidBody::setDynamic(bool dynamic)
{
}

void PhysicsRigidBody::setMass(float mass)
{ cpBodySetMass(_cpBody, mass); }

float PhysicsRigidBody::getMass() const
{ return cpBodyGetMass(_cpBody); }


void PhysicsRigidBody::setMoment(float moment)
{ cpBodySetMoment(_cpBody, moment); }


float PhysicsRigidBody::getMoment() const
{ return cpBodyGetMoment(_cpBody); }


bool PhysicsRigidBody::isResting() const
{ return cpBodyIsSleeping(_cpBody); }


void PhysicsRigidBody::setResting(bool rest) const
{
    if (rest && !isResting())
    {
        cpBodySleep(_cpBody);
    }
    else if(!rest && isResting())
    {
        cpBodyActivate(_cpBody);
    }
}


Vec2 PhysicsRigidBody::world2Local(const Vec2& point)
{
    return PhysicsHelper::cpv2point(cpBodyWorldToLocal(_cpBody, PhysicsHelper::point2cpv(point)));
}

Vec2 PhysicsRigidBody::local2World(const Vec2& point)
{
    return PhysicsHelper::cpv2point(cpBodyLocalToWorld(_cpBody, PhysicsHelper::point2cpv(point)));
}



//void PhysicsRigidBody::addToPhysicsWorld()
//{
//    if (_owner)
//    {
//        auto scene = _owner->getScene();
////        if (scene)
////            scene->getPhysicsWorld()->addBody(this);
//    }
//}




//void PhysicsRigidBody::removeFromPhysicsWorld()
//{
//    if (_world)
//    {
////        _world->removeBody(this);
//        _world = nullptr;
//    }
//}




NS_CC_END