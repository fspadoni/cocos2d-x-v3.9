//
//  CCPhysicsStatistics.h
//  cocos2d_libs
//
//  Created by Federico Spadoni on 20/01/15.
//
//

#ifndef cocos2d_libs_CCPhysicsStatistics_h
#define cocos2d_libs_CCPhysicsStatistics_h


#include "base/ccConfig.h"

#if CC_USE_PHYSICS

#include "base/ccMacros.h"
//#include "platform/CCPlatformMacros.h"

struct cpSpaceStats;

NS_CC_BEGIN


class CC_DLL PhysicsStatistics
{
    int _counter;
    int _substeps;
    
public:
    
    unsigned long _stamps;
    
    int _numNodes;
    int _numElements;
    int _numArbiters;
    int _numContacts;
    int _numCollisionTests;
    
    // double[2] -> [0][ms] , [1][%]
    double _assemble[2];
    double _solve[2];
    double _broadphase[2];
    double _narrowphase[2];
    double _graphs[2];
    double _contacts[2];
    double _time[2];
    
    PhysicsStatistics();
    
    void setSubSteps(int substeps) {_substeps = substeps;}
    
    void updateTime(cpSpaceStats* stats);
    
    void updateRelativeTime();
    
    void reset();
};


NS_CC_END

#endif // CC_USE_PHYSICS

#endif // cocos2d_libs_CCPhysicsStatistics_h
