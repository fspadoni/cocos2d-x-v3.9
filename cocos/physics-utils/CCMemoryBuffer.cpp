//
//  CCMemoryBuffer.cpp
//  cocos2d_libs
//
//  Created by Federico Spadoni on 09/11/14.
//
//

#include "CCMemoryBuffer.h"

NS_CC_BEGIN

BufferManager* BufferManager::s_singleInstance = nullptr;

BufferManager* BufferManager::getInstance()
{
    if (s_singleInstance == nullptr)
    {
        s_singleInstance = new (std::nothrow) BufferManager();
    }
    return s_singleInstance;
}



NS_CC_END