//
//  CCPageMemoryPool.cpp
//  cocos2d_libs
//
//  Created by Federico Spadoni on 11/11/14.
//
//

#include "CCPageMemoryPool.h"
#include "base/ccMacros.h"


NS_CC_BEGIN


class MemoryWithSize
{
public:
    int m_size;
    enum
    {
        MAGIC_MEMORY_WITH_SIZE = 0x2345656
    } m_magic;
    int m_dummy[2];
};



PageMemoryPool* PageMemoryPool::s_singleInstance = nullptr;

// init static array
int PageMemoryPool::m_row_to_size[MEMORY_MAX_SIZE_BLOCK] = {0};


PageMemoryPool* PageMemoryPool::getInstance()
{
    if (s_singleInstance == nullptr)
    {
        s_singleInstance = new (std::nothrow) PageMemoryPool();
    }
    return s_singleInstance;
}


void PageMemoryPool::destroyInstance()
{
    if (s_singleInstance != nullptr)
    {
        delete s_singleInstance;
    }
    s_singleInstance = nullptr;
}

PageMemoryPool::PageMemoryPool()
: m_allocated_memory_blocks(0)
{
}


PageMemoryPool::PageMemoryPool(BufferSharedPtr<char> buffer)
{
    init_memory( buffer );
}


PageMemoryPool::~PageMemoryPool()
{
    while ( m_allocated_memory_blocks )
    {
        MemoryBlock *b = m_allocated_memory_blocks;
        m_allocated_memory_blocks = m_allocated_memory_blocks->m_next;
        PageMemoryPool::system_free( (void *)b );
    }
}


void PageMemoryPool::init_memory( BufferSharedPtr<char> buffer)
{
    _buffers.insert( buffer );
    m_memory_start = buffer->lockForWrite();
    m_used_end = buffer->lockForWrite();
    m_memory_end = m_used_end + buffer->getSizeInBytes();
    m_allocated_memory_blocks = 0;
    
    for (int i = MEMORY_MAX_SIZE_BLOCK-1; i>=0 ; i-- )
    {
        m_free_list[i] = NULL;
        m_blocks_in_use[i] = 0;
    }
    
    for (int row = 0; row < MEMORY_MAX_SIZE_BLOCK; row++ )
    {
//        int row = size_to_row(j);
//        m_size_to_row[ j ] = row;
        PageMemoryPool::m_row_to_size[row] = (row+1) * MEMORY_PAGE_SIZE;
    }
    { // statistics
        MemoryStatistics &s = m_statistics;
        s.m_max_size_in_use = 0;
        s.m_size_in_use = 0;
        s.m_n_allocates = 0;
        s.m_blocks_in_use = 0;
    }
}


int PageMemoryPool::getMaxPoolCapacity(int count, int typeSize)
{
    int memSize = count*typeSize;
    
    if ( memSize > _POOL_SIZE_16)
    {
        return count;
    }
    
    int row = PageMemoryPool::size_to_row(memSize);
    int poolSize = PageMemoryPool::m_row_to_size[ row ];
    return poolSize / typeSize;
    
}


void *PageMemoryPool::allocate_real( int size )
{
    if ( size > _POOL_SIZE_16)
    {
        // aggiungi al Log
        CCLOG("memory block to big for the pools: block size %i  max pool block size%i\n ", size, _POOL_SIZE_16 );
        return PageMemoryPool::system_malloc( size );
    }
    
    
    int row = size_to_row(size);//m_size_to_row[ size ];
    size = PageMemoryPool::m_row_to_size[ row ];
    
    int allocated_size = size;
    void *result;
    
    // allocate first block
    if ( size + m_used_end > m_memory_end)
    {
        // aggiungi al Log
        CCLOG("running out of space: block size %i\n", size );
        
        // allocate other NUMBER_BLOCK_TO_ALLOCATE blocks
        ssize_t memory_size = NEW_MEMORY_BLOCK; // MEMORY_PAGE_SIZE * (row+1) * NUMBER_BLOCK_TO_ALLOCATE;
        
        cocos2d::BufferSharedPtr<char> memoryBuffer = Buffer<char>::create(memory_size);
        _buffers.insert(memoryBuffer);
        
        MemoryBlock *b = memoryBuffer->lockForWrite<MemoryBlock>();
//        MemoryBlock *b = (MemoryBlock *)PageMemoryPool::system_malloc( memory_size );
        
        b->m_next = m_allocated_memory_blocks;
        m_allocated_memory_blocks = b;
        m_memory_start = (char *)(b+1);
        m_used_end = m_memory_start;
        m_memory_end = m_used_end + memory_size;
    }
    
    result = (void *)m_used_end;
    MemoryElem *el = (MemoryElem *) m_used_end;
    el->m_magic = MEMORY_ALLOC_MAGIC_NUMBER;
    m_used_end += size;
    
    // allocate rest to get make sure the alignment is ok
    int biu = m_blocks_in_use[row];
    while ( allocated_size < 0 /*256*/ )
    {
        if ( size + m_used_end < m_memory_end){
            MemoryElem *el = (MemoryElem *) m_used_end;
            el->m_magic = MEMORY_ALLOC_MAGIC_NUMBER;
            this->deallocate( m_used_end, size );
            m_used_end += size;
        }else{
            break;
        }
        allocated_size += size;
    }
    m_blocks_in_use[row] = biu;
    memset(result, 0, allocated_size);
    return result;
}

void* PageMemoryPool::allocate(int size)
{
#ifdef SIMSTEP_MEMORY_ENABLE_STATISTICS
    MemoryStatistics &s = m_statistics[cl];
    s.m_size_in_use += size;
    s.m_blocks_in_use += 1;
    s.m_n_allocates += 1;
    if ( s.m_size_in_use > 	s.m_max_size_in_use)
    {
        s.m_max_size_in_use = s.m_size_in_use;
    }
#endif
    
#ifdef SIMSTEP_MEMORY_ENABLE_DEBUG_CHECK
    MemoryWithSize *x = (MemoryWithSize *)this->PageMemoryPool::system_malloc( size + sizeof( MemoryWithSize ), MEMORY_CACHE_ALIGNMENT );
    x->m_size = size;
    x->m_magic = MemoryWithSize::MAGIC_MEMORY_WITH_SIZE;
    return (void *)(x+1);
#else
    
    if ( size <= MEMORY_MAX_PAGE_SIZE_BLOCK){
        int row = size_to_row(size);
        int allocated_size = PageMemoryPool::m_row_to_size[ row ];
        m_blocks_in_use[row]++;
        MemoryElem *n = m_free_list[row];
        if ( n ){
            m_free_list[row] = n->m_next;
            n->m_magic = MEMORY_ALLOC_MAGIC_NUMBER;
            memset(n, 0, allocated_size);
            return (void *)n;
        }
    }
    return allocate_real( size );
#endif
}


void* PageMemoryPool::allocateShare(int size)
{
    
#ifdef SIMSTEP_MEMORY_ENABLE_STATISTICS
    MemoryStatistics &s = m_statistics[cl];
    s.m_size_in_use += size;
    s.m_blocks_in_use += 1;
    s.m_n_allocates += 1;
    if ( s.m_size_in_use > 	s.m_max_size_in_use)
    {
        s.m_max_size_in_use = s.m_size_in_use;
    }
#endif
    
#ifdef SIMSTEP_MEMORY_ENABLE_DEBUG_CHECK
    MemoryWithSize *x = (MemoryWithSize *)this->PageMemoryPool::system_malloc( size + sizeof( MemoryWithSize ), MEMORY_CACHE_ALIGNMENT );
    x->m_size = size;
    x->m_magic = MemoryWithSize::MAGIC_MEMORY_WITH_SIZE;
    return (void *)(x+1);
#else
    
    int sizeAligned = ( MEMORY_CACHE_LINE > size) ? MEMORY_CACHE_LINE : size;
    
    if ( size <= MEMORY_MAX_PAGE_SIZE_BLOCK)
    {
        int row = size_to_row(sizeAligned);
        m_blocks_in_use[row]++;
        MemoryElem *n = m_free_list[row];
        if ( n ){
            m_free_list[row] = n->m_next;
            n->m_magic = MEMORY_ALLOC_MAGIC_NUMBER;
            return (void *)n;
        }
    }
    return allocate_real( sizeAligned );
#endif
}

void  PageMemoryPool::deallocateShare(void* p, int size)
{
    
#ifdef SIMSTEP_MEMORY_ENABLE_STATISTICS
    MemoryStatistics &s = m_statistics[cl];
    s.m_size_in_use -= size;
    s.m_blocks_in_use -= 1;
#endif
    
#ifdef SIMSTEP_MEMORY_ENABLE_DEBUG_CHECK
    {
        MemoryWithSize *x = (MemoryWithSize *)p;
        x--;
        assert( x->m_magic == MemoryWithSize::MAGIC_MEMORY_WITH_SIZE );
        assert(  x->m_size == size );
        this->system_free( x );
    }
#else
    int sizeAligned = ( MEMORY_CACHE_LINE > size) ? MEMORY_CACHE_LINE : size;
    
    if ( size <= MEMORY_MAX_PAGE_SIZE_BLOCK )
    {
        MemoryElem *me = (MemoryElem *)p;
        int row = size_to_row(sizeAligned);
        m_blocks_in_use[row]--;
        me->m_next = m_free_list[row];
        assert( me->m_magic != MEMORY_FREE_MAGIC_NUMBER);
        me->m_magic = MEMORY_FREE_MAGIC_NUMBER;
        m_free_list[row] = me;
    }
    else
    {
        PageMemoryPool::system_free((char *)p);
    }
#endif
}


void PageMemoryPool::deallocate(void* p, int size)
{
#ifdef SIMSTEP_MEMORY_ENABLE_STATISTICS
    MemoryStatistics &s = m_statistics[cl];
    s.m_size_in_use -= size;
    s.m_blocks_in_use -= 1;
#endif
    
#ifdef SIMSTEP_MEMORY_ENABLE_DEBUG_CHECK
    {
        MemoryWithSize *x = (MemoryWithSize *)p;
        x--;
        assert( x->m_magic == MemoryWithSize::MAGIC_MEMORY_WITH_SIZE );
        assert(  x->m_size == size );
        this->system_free( x );
    }
#else
    if ( size <= MEMORY_MAX_PAGE_SIZE_BLOCK )
    {
        MemoryElem *me = (MemoryElem *)p;
        int row = size_to_row(size);
        m_blocks_in_use[row]--;
        me->m_next = m_free_list[row];
        assert( me->m_magic != MEMORY_FREE_MAGIC_NUMBER);
        me->m_magic = MEMORY_FREE_MAGIC_NUMBER;
        m_free_list[row] = me;
    }
    else
    {
        PageMemoryPool::system_free((char *)p);
    }
#endif
}

int PageMemoryPool::size_to_row( int size )
{
    int row = size / MEMORY_PAGE_SIZE;
    if ( row > MEMORY_MAX_SIZE_BLOCK )
    {
        CCLOG("memory block to big for the pools: block size %i  max pool block size%i\n ", size, _POOL_SIZE_16 );
        /*break;*/
        row = -1;
    }
    return row;    
}



void* PageMemoryPool::allocate_debug(int n,
                                 const char* file,
                                 int line)
{
    return new char[n];
}

void PageMemoryPool::deallocate_debug(
                                  void* p,
                                  int n,
                                  const char* file,
                                  int line)
{
    delete[] static_cast<char*>(p);
}



void* PageMemoryPool::allocate_and_store_size(int byte_size)
{
    MemoryWithSize *x = (MemoryWithSize *)this->allocate( byte_size + sizeof( MemoryWithSize ) );
    x->m_size = byte_size; 
    x->m_magic = MemoryWithSize::MAGIC_MEMORY_WITH_SIZE;
    return (void *)(x+1);
}

void  PageMemoryPool::deallocate_stored_size(void* p)
{
    if(p)
    {
        MemoryWithSize *x = (MemoryWithSize *)p;
        x--;
        assert( x->m_magic == MemoryWithSize::MAGIC_MEMORY_WITH_SIZE );
        this->deallocate( x, x->m_size + sizeof( MemoryWithSize ) );
    }
}



void *PageMemoryPool::system_malloc( size_t size )
{
    size += MEMORY_PAGE_SIZE - (size % MEMORY_PAGE_SIZE);
    return calloc ( 1, size );

}

void PageMemoryPool::system_free( void *data )
{
    free( data );
}


NS_CC_END