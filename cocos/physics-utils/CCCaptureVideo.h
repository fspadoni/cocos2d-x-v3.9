//
//  CCCaptureVideo.h
//  cocos2d_libs
//
//  Created by Federico Spadoni on 02/06/15.
//
//

#ifndef __cocos2d_libs__CCCaptureVideo__
#define __cocos2d_libs__CCCaptureVideo__

#include "base/ccMacros.h"
#include "renderer/CCRenderCommand.h"

#include "physics-utils/CCMemoryBuffer.h"

/** @file ccUtils.h
 Misc free functions
 */

NS_CC_BEGIN

    
namespace utils
{
    /** Capture a video
     * To ensure the snapshot is applied after everything is updated and rendered in the current frame,
     * we need to wrap the operation with a custom command which is then inserted into the tail of the render queue.
     * @param afterCaptured, specify the callback function which will be invoked after the snapshot is done.
     * @param filename, specify a filename where the snapshot is stored. This parameter can be either an absolute path or a simple
     * base filename ("hello.png" etc.), don't use a relative path containing directory names.("mydir/hello.png" etc.)
     * @since v3.2
     */
    class CC_DLL FFMpegVideo : public RenderCommand
    {
    public:
        
        FFMpegVideo();
        ~FFMpegVideo();
        
    public:
        
        void init();
        
        void capture( const std::string& filename, const std::string& ffmpegPath, const int fpsIn, const int fpsOut );

        void stopCapture();
        
        
        void execute();
        
        inline bool isTranslucent() { return true; }
        std::function<void()> func;
        
    private:
        
        void onCaptureFFMpegVideo();
        
    protected:
        
        std::string _filename;
        
        cocos2d::BufferSharedPtr<char> _captureBuffer;

        FILE* _ffmpeg;
        int _width;
        int _height;
        
    };
    
    void CC_DLL captureFFMpegVideo(const std::function<void(bool, const std::string&)>& afterCaptured, const std::string& filename, const std::string& ffmpegPath, const int fpsIn = 60, const int fpsOut = 60);
    
    
} // namespace utils

NS_CC_END


#endif /* defined(__cocos2d_libs__CCCaptureVideo__) */
