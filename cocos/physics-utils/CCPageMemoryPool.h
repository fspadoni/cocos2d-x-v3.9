//
//  CCPageMemoryPool.h
//  cocos2d_libs
//
//  Created by Federico Spadoni on 11/11/14.
//
//

#ifndef cocos2d_libs_CCPageMemoryPool_h
#define cocos2d_libs_CCPageMemoryPool_h

#include "base/CCRef.h"
#include "physics-utils/CCMemoryBuffer.h"

NS_CC_BEGIN


template<typename T>
class PoolBuffer;
template<typename T>
class PoolBufferSharedPtr;


class CC_DLL PageMemoryPool
{
    
public:
    
    enum
    {
        MEMORY_PAGE_SIZE = 4096,
        
        MEMORY_CACHE_LINE = 128,
        
        MEMORY_MAX_PAGE_SIZE_BLOCK = 65536,
//        MEMORY_MAX_SIZE_SMALL_BLOCK = 512,
        MEMORY_MAX_SIZE_BLOCK = 16,
        
    
        // The number of small chunk sizes
//        MEMORY_MAX_SMALL_ROW  = 4096,
        
        _POOL_SIZE_1 = 1 * MEMORY_PAGE_SIZE,
        _POOL_SIZE_2 = 2 * MEMORY_PAGE_SIZE,
        _POOL_SIZE_3 = 3 * MEMORY_PAGE_SIZE,
        _POOL_SIZE_4 = 4 * MEMORY_PAGE_SIZE,
        _POOL_SIZE_5 = 5 * MEMORY_PAGE_SIZE,
        _POOL_SIZE_6 = 6 * MEMORY_PAGE_SIZE,
        _POOL_SIZE_7 = 7 * MEMORY_PAGE_SIZE,
        _POOL_SIZE_8 = 8 * MEMORY_PAGE_SIZE,
        _POOL_SIZE_9 = 9 * MEMORY_PAGE_SIZE,
        _POOL_SIZE_10 = 10 * MEMORY_PAGE_SIZE,
        _POOL_SIZE_11 = 11 * MEMORY_PAGE_SIZE,
        _POOL_SIZE_12 = 12 * MEMORY_PAGE_SIZE,
        _POOL_SIZE_13 = 13 * MEMORY_PAGE_SIZE,
        _POOL_SIZE_14 = 14 * MEMORY_PAGE_SIZE,
        _POOL_SIZE_15 = 15 * MEMORY_PAGE_SIZE,
        _POOL_SIZE_16 = 16 * MEMORY_PAGE_SIZE,
        

        
        //// The number of small and large chunk sizes
        //MEMORY_MAX_ALL_ROW = (MEMORY_MAX_SMALL_ROW+4),
        
        //// The largest small block we allocate from this pool
        //MEMORY_MAX_SIZE_SMALL_BLOCK  = 512,
        
        //// The largest large block we allocate from this pool
        //MEMORY_MAX_SIZE_LARGE_BLOCK = 8192,
        
        //// The low bits we ignore when indexing into the large arrays
        //MEMORY_LARGE_BLOCK_RSHIFT_BITS = 10,
        
        // How much we allocate when the  pool becomes full
        NUMBER_BLOCK_TO_ALLOCATE = 16,
        MEMORY_SYSTEM_PAGE_SIZE = 8192,
        
        NEW_MEMORY_BLOCK = 1048576, /* 65536 * 16 */
        
        // Debugging
        MEMORY_ALLOC_MAGIC_NUMBER = 0x3425232,
        MEMORY_FREE_MAGIC_NUMBER = 0x3425234
        
    };
	
    
    static PageMemoryPool* getInstance();
    
    static void destroyInstance();
    
    
    void init_memory( BufferSharedPtr<char> buffer );
    //: initialized the memory pool

    static int getMaxPoolCapacity(int count, int size);
    
    
private:
    
    static PageMemoryPool* s_singleInstance;
    
    
CC_CONSTRUCTOR_ACCESS:
    
    PageMemoryPool();
    //: a really empty constructor
    
    PageMemoryPool(BufferSharedPtr<char> buffer);
    
    
    virtual ~PageMemoryPool();
    
    
    
public:
    
    void* allocate(int byte_size);
    //: allocate a piece of memory
    //: Note: the size of that piece is not stored, so
    //: the user has to do remember the size !!!
    //: the memory class is currently used only for statistics
    
    void  deallocate(void*, int byte_size);
    //: deallocate a piece of memory
    
    
    void* allocateShare(int byte_size);
    //: allocate a piece of memory
    //: Note: the size of that piece is not stored, so
    //: the user has to do remember the size !!!
    //: the memory class is currently used only for statistics
    
    void  deallocateShare(void*, int byte_size);
    //: deallocate a piece of memory
    
    
    void* allocate_and_store_size(int byte_size);
    //: allocate a piece of memory
    //: Note: the size of that piece is stored, so
    //: 16 bytes of memory are wasted
    void  deallocate_stored_size(void*);
    //: deallocate a piece of memory which has been allocated of allocate_and_store_size
    
    void* allocate_debug(int n, const char* file, int line);
    void  deallocate_debug(void*, int n,const char* file,int line);
    
    
    //void print_statistics(class hk_Console *); // see Memory_Util
    
	
public: // THE interfaces to the system allocate, change this if you want to add in your own big block memory allocation
	
    static void *system_malloc( size_t size );
    static void system_free(	 void *data );
    //static inline void* memcpy(void* dest,const void* src,int size);
    //static inline void* memset(void* dest, uchar val, int32 size);
    
private:
    void* allocate_real(int size);
    static inline int size_to_row(int size);
    
protected:
    //friend class Memory_Util;
    class MemoryElem {
    public:
        MemoryElem *m_next;
        int m_magic;
    };
    
    class MemoryStatistics {
    public:
        int m_size_in_use;
        int m_n_allocates;
        int m_blocks_in_use;
        int m_max_size_in_use;
    };
    
    class MemoryBlock
    {
    public:
        MemoryBlock *m_next;
        int		m_pad[(MEMORY_CACHE_LINE - sizeof(MemoryBlock *))/ sizeof(int)];
    };
    
protected:
    
    typedef std::set< BufferSharedPtr<char> > BufferList;
    BufferList _buffers;
    
    MemoryElem *m_free_list[MEMORY_MAX_SIZE_BLOCK];
    MemoryBlock *m_allocated_memory_blocks;
    
    int  m_blocks_in_use[MEMORY_MAX_SIZE_BLOCK];
//    BufferSharedPtr<char> m_buffer;
    char *m_memory_start;
    char *m_memory_end;
    char *m_used_end;
    
    
    static int m_row_to_size[MEMORY_MAX_SIZE_BLOCK];
    MemoryStatistics m_statistics;
//    char m_size_to_row[ MEMORY_MAX_SIZE_BLOCK];
    
};


template<typename T>
class CC_DLL PoolBuffer : public Buffer<T>
{
    
public:
    
    static PoolBufferSharedPtr<T> create(ssize_t numNodes = 1)
    {
        CCASSERT(numNodes > 0,"");
        PoolBuffer<T>* buf = new (std::nothrow) PoolBuffer<T>( sizeof(T), numNodes);
        if (buf)
        {
            buf->autorelease();
        }
        else
        {
            CC_SAFE_RELEASE_NULL(buf);
        }
        
        return PoolBufferSharedPtr<T>(buf);
    }
    
    static PoolBufferSharedPtr<T> clone(const PoolBuffer<T>* other)
    {
        CCASSERT(other != nullptr, "");
        PoolBuffer<T>* buf = new (std::nothrow) PoolBuffer<T>(*other);
        if (buf)
        {
            buf->autorelease();
        }
        else
        {
            CC_SAFE_RELEASE_NULL(buf);
        }
        
        return PoolBufferSharedPtr<T>(buf);
    }
    //    friend class BufferManager;
    
protected:
    
    PoolBuffer(unsigned int dataSize, unsigned int numData )
    : Buffer<T>()
    {
        this->_dataSize = dataSize;
        this->_nbData = numData;
        this->_sizeInBytes = dataSize*numData;
        // Allocate aligned memory
        this->_data = static_cast<T*>( PageMemoryPool::getInstance()->allocate( this->_sizeInBytes ) );
    }
    
    PoolBuffer(const PoolBuffer<T>& other )
    : Buffer<T>()
    {
        this->_dataSize = other._dataSize;
        this->_nbData = other._nbData;
        this->_sizeInBytes = other._sizeInBytes;
        // Allocate aligned memory
        this->_data = static_cast<T*>( PageMemoryPool::getInstance()->allocate( other._sizeInBytes ) );
        this->copyData(other);
    }
    

public:
    
    PoolBuffer()
    : Buffer<T>()
    {
    }
    
    //    explicit Buffer(Buffer<T>* buf)
    //    {
    //        this = buf;
    //        this->retain();
    //    }
    
    ~PoolBuffer()
    {
        if ( this->_data )
        {
            PageMemoryPool::getInstance()->deallocate(this->_data, this->_sizeInBytes);
            this->_data = nullptr;
        }
    }
    
};


template<typename T>
class PoolBufferSharedPtr : public RefPtr< PoolBuffer<T> >
{
public:
    
    PoolBufferSharedPtr() : RefPtr< PoolBuffer<T> >()
    {
    }
    
    explicit PoolBufferSharedPtr(PoolBuffer<T>* buf) : RefPtr< PoolBuffer<T> >(buf)
    {
    }
    
};



NS_CC_END


#endif
