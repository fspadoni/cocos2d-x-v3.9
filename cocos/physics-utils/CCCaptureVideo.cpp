//
//  CCCaptureVideo.cpp
//  cocos2d_libs
//
//  Created by Federico Spadoni on 02/06/15.
//
//

#include "CCCaptureVideo.h"

#include "base/CCDirector.h"
#include "renderer/CCRenderer.h"

#include <sstream>
#include <stdio.h>

NS_CC_BEGIN;

namespace utils
{
    
    FFMpegVideo::FFMpegVideo()
    : RenderCommand()
    , func(nullptr)
    {
        _type = RenderCommand::Type::CUSTOM_COMMAND;
        _globalOrder = std::numeric_limits<float>::max();
        
        init();
    }
    
    void FFMpegVideo::init()
    {
 
        int _width = Director::getInstance()->getOpenGLView()->getFrameSize().width;
        int _height = Director::getInstance()->getOpenGLView()->getFrameSize().height;
        const int memory_size = _width * _height * 4;
        
        _captureBuffer = Buffer<char>::create(memory_size);

    }
    
    FFMpegVideo::~FFMpegVideo()
    {
      
    }
    
    void FFMpegVideo::execute()
    {
        if(func)
        {
            func();
        }
    }

    
    void FFMpegVideo::capture( const std::string& filename, const std::string& ffmpegPath, const int fpsIn, const int fpsOut)
    {
        std::ostringstream cmd;
        cmd << ffmpegPath <<  " -r " << fpsIn << " -f rawvideo -pix_fmt rgba -s " << _width << "x" << _height << "-i -  -threads 0 -preset fast -y -pix_fmt yuv420p -crf 21 -vf vflip -r " << fpsOut << " " << filename;
        
        _ffmpeg = popen( cmd.str().c_str() , "w");
        
        _filename = filename;
        
        if ( _ffmpeg )
        {
//            static CustomCommand captureFFMpegVideoCommand;
            this->func = std::bind( &FFMpegVideo::onCaptureFFMpegVideo, this );
            Director::getInstance()->getRenderer()->addCommand(this);
            
        }
        else
        {
            
        }

    }
    
    void FFMpegVideo::stopCapture()
    {
        if ( _ffmpeg )
        {
            if ( pclose(_ffmpeg) )
            {
                
            }
            else{
//                error
            }
            
        }
    }

    
    void FFMpegVideo::onCaptureFFMpegVideo()
    {
        glPixelStorei(GL_PACK_ALIGNMENT, 1);
        glReadPixels(0, 0, _width, _height, GL_RGBA, GL_UNSIGNED_BYTE, _captureBuffer);
        fwrite(_captureBuffer, _captureBuffer->getSizeInBytes() , 1, _ffmpeg);
        
        if ( !ferror(_ffmpeg) )
        {
            
        }

    }
    
    
    
} // namespace utils

NS_CC_END
