//
//  CCPhysicsStatistics.cpp
//  cocos2d_libs
//
//  Created by Federico Spadoni on 21/01/15.
//
//

#include "CCPhysicsStatistics.h"

#if CC_USE_PHYSICS

#include "chipmunk.h"


NS_CC_BEGIN

PhysicsStatistics::PhysicsStatistics()
: _counter(0)
, _substeps(1)
, _stamps(0)
, _numNodes(0)
, _numElements(0)
, _numArbiters(0)
, _numContacts(0)
, _numCollisionTests(0)
, _assemble {0.0,0.0}
, _solve {0.0,0.0}
, _broadphase {0.0,0.0}
, _narrowphase {0.0,0.0}
, _graphs {0.0,0.0}
, _contacts {0.0,0.0}
, _time {0.0,0.0}
{
}


void PhysicsStatistics::updateTime(cpSpaceStats* stats)
{
    ++_counter;
    
    _stamps = stats->stamps;
    _numNodes = stats->numNodes;
    _numElements = stats->numElements;
    _numArbiters = stats->numArbiters;
    _numContacts = stats->numContacts;
    _numCollisionTests = stats->numCollisionTests;
//    _nodes = stats->nodes;
//    _triangles = stats->triangles;
    _assemble[0] += stats->assemble;
    _solve[0] += stats->solve;
    _broadphase[0] += stats->broadphase;
    _narrowphase[0] += stats->narrowphase;
    _graphs[0] += stats->graphs;
    _contacts[0] += stats->contacts;
    _time[0] += stats->time;
}

void PhysicsStatistics::updateRelativeTime()
{
    const double invTime = 100.0 / _time[0];
    _assemble[1] = _assemble[0] * invTime;
    _solve[1] = _solve[0] * invTime;
    _broadphase[1] = _broadphase[0] * invTime;
    _narrowphase[1] = _narrowphase[0] * invTime;
    _graphs[1] = _graphs[0] * invTime;
    _contacts[1] = _contacts[0] * invTime;
    
    const double scale = static_cast<double>(_substeps) / static_cast<double>(_counter);
    _assemble[0] *= scale;
    _solve[0] *= scale;
    _broadphase[0] *= scale;
    _narrowphase[0] *= scale;
    _graphs[0] *= scale;
    _contacts[0] *= scale;
    _time[0] *= scale;
}


//void PhysicsStatistics::avarageTime(const float iters)
//{
//    const double invTime = 100.0 / _time[0];
//    _assemble[1] = _assemble[0] * invTime;
//    _solve[1] = _solve[0] * invTime;
//    _broadphase[1] = _broadphase[0] * invTime;
//    _narrowphase[1] = _narrowphase[0] * invTime;
//    _graphs[1] = _graphs[0] * invTime;
//    _contacts[1] = _contacts[0] * invTime;
//}

void PhysicsStatistics::reset()
{
    _counter = 0;
    _stamps = 0;
    _assemble[0] = _assemble[1] = 0.0;
    _solve[0] =  _solve[1] = 0.0;
    _broadphase[0] = _broadphase[1] = 0.0;
    _narrowphase[0] = _narrowphase[1] = 0.0;
    _graphs[0] = _graphs[1] = 0.0;
    _contacts[0] = _contacts[1] = 0.0;
    _time[0] = _time[1] = 0.0;
}

NS_CC_END


#endif // CC_USE_PHYSICS