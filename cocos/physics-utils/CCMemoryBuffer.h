//
//  CCMemoryBuffer.h
//  cocos2d_libs
//
//  Created by Federico Spadoni on 09/11/14.
//
//

#ifndef cocos2d_libs_CCMemoryBuffer_h
#define cocos2d_libs_CCMemoryBuffer_h

#include "base/CCRef.h"
#include "base/CCRefPtr.h"
#include "base/ccMacros.h"
#include <set> 

NS_CC_BEGIN



template<typename T>
class Buffer;
template<typename T>
class BufferSharedPtr;
class SimdChunk;


class CC_DLL BufferManager
{
    
public:
    
    enum
    {
        SIMD_ALIGN = 16,
        CACHE_ALIGN  = 128
    };
    
    
    template<typename T>
    Buffer<T>* createBuffer( size_t numNodes = 1)
    {
        CCASSERT (numNodes > 0, "Buffer<t>::createBuffer size = 0");
        Buffer<T>* buf = new Buffer<T>( sizeof(T), numNodes/*, this */);
        _buffers.insert(buf);
        return buf;
    }
    
    
    template<typename T>
    void notifyBufferDestroyed(Buffer<T>* buf)
    {
        BufferList::iterator i = _buffers.find(buf);
        if (i != _buffers.end())
        {
            _buffers.erase(i);
        }
    }
    
    
    static BufferManager* getInstance(void);
   
    
private:
    
    BufferManager();
    virtual ~BufferManager();
    
    typedef std::set<SimdChunk*> BufferList;
    BufferList _buffers;
    
    static BufferManager* s_singleInstance;
    
};



class SimdChunk
{
    
public:
    /// Returns the size of this buffer in bytes
    unsigned int getSizeInBytes(void) const { return _sizeInBytes; }
    
    /// Gets the size in bytes of a single vertex in this buffer
    unsigned int getDataSize(void) const { return _dataSize; }
    
    /// Get the number of vertices in this buffer
    unsigned int getSize(void) const { return _nbData; }
    
    
protected:
    
    SimdChunk()
    : _sizeInBytes(0)
    , _nbData(0)
    ,_dataSize(0)
    {
    }
    
    SimdChunk(unsigned int dataSize, unsigned int numData )
    : _dataSize( dataSize )
    , _nbData(numData)
    , _sizeInBytes(dataSize * numData)
    {
        // Calculate the size of the vertices
//        _sizeInBytes = _dataSize * _nbData;
    }
    
    ~SimdChunk() {}
    
    
    unsigned int _sizeInBytes;
    unsigned int _nbData;
    unsigned int _dataSize;
    
};

template<typename T>
class CC_DLL Buffer : public SimdChunk, public Ref
{

public:
    
    static BufferSharedPtr<T> create(ssize_t numNodes = 1)
    {
        CCASSERT(numNodes > 0,"");
        Buffer<T>* buf = new (std::nothrow) Buffer<T>( sizeof(T), numNodes);
        if (buf)
        {
            buf->autorelease();
        }
        else
        {
            CC_SAFE_RELEASE_NULL(buf);
        }
        
        return BufferSharedPtr<T>(buf);
    }
    
//    friend class BufferManager;
 
protected:
    
    Buffer(unsigned int dataSize, unsigned int numData/*, BufferManager* mgr*/  )
    : SimdChunk( dataSize, numData/*, mgr*/ )
    {
        // Allocate aligned memory
        _data = static_cast<T*>( malloc(_sizeInBytes /*, BufferManager::CACHE_ALIGN*/) );
    }
    

    
public:
    
    Buffer()
    : SimdChunk()
    , _data(nullptr)
    {
    }
    
//    explicit Buffer(Buffer<T>* buf)
//    {
//        this = buf;
//        this->retain();
//    }
    
    ~Buffer()
    {
        if ( _data )
        {
            free( _data);
            _data = nullptr;
        }
    }
    
    inline const T* lockForRead(void) const
    {return _data;}
    
    inline const T* lockForRead(const unsigned int& idx) const
    {return _data + idx;}
    
    inline T* lockForWrite(void) const
    {
        CCASSERT( sizeof(T) <= _dataSize, "Buffer<T>::lockForWrite: sizeof(T) <= _dataSize" );
        return _data;
    }
    
    inline T* lockForWrite(const unsigned int& idx ) const
    {
        CCASSERT( sizeof(T) <= _dataSize, "Buffer<T>::lockForWrite: sizeof(T) <= _dataSize" );
        return _data + idx;
    }
    
    template<typename Y>
    inline const Y* lockForRead(void) const
    {return reinterpret_cast<const Y*>(_data);}
    
    template<typename Y>
    inline const Y* lockForRead(const unsigned int& idx) const
    {return reinterpret_cast<const Y*>(_data) + idx;}
    
    
    template<typename Y>
    inline Y* lockForWrite(void) const
    {
        CCASSERT( sizeof(T) <= _dataSize, "Buffer<T>::lockForWrite: sizeof(T) <= _dataSize" );
        return reinterpret_cast<Y*>(_data);
    }
    
    template<typename Y>
    inline Y* lockForWrite(const unsigned int& idx ) const
    {
        CCASSERT( sizeof(T) <= _dataSize, "Buffer<T>::lockForWrite: sizeof(T) <= _dataSize" );
        return reinterpret_cast<Y*>(_data) + idx;
    }
    
    
    inline void unlock(void) const {};
    
    inline void unlock(void) {};
    
    
    inline void reset(void)
    {
        T* pDst = this->lockForWrite();
        memset( (void*)pDst, 0, _sizeInBytes );
        this->unlock();
    }
    
    inline void reset(const T& val )
    {
        T* pDst = this->lockForWrite();
        int N = _nbData;
        while ( N-- > 0 )
        {
            memcpy( (void*)pDst++, (void*)&val, _dataSize );
        }
        this->unlock();
    }
    
    inline void readData( void* pDest, unsigned int offset, unsigned int length )
    {
        const T* pSrc = this->lockForRead();
        memcpy(pDest, (void*)(pSrc + offset), length*_dataSize );
        this->unlock();
    }
    
    inline void writeData(  const void* pSource, unsigned int offset, unsigned int length )
    {
        T* pDst = this->lockForWrite();
        memcpy((void*)(pDst + offset), pSource, length*_dataSize );
        this->unlock();
    }
    
    
    //* Copy data from another buffer into this one
    inline void copyData( const Buffer<T>& srcBuffer, unsigned int srcOffset,
                         unsigned int dstOffset, unsigned int length )
    {
        const T* srcData =
        srcBuffer.lockForRead<T>(srcOffset);
        this->writeData( (void*)srcData, dstOffset, length );
        srcBuffer.unlock();
    }
    
    
    //* Copy all data from another buffer into this one. 
    inline void copyData( const Buffer<T>& srcBuffer )
    {
        copyData(srcBuffer, 0, 0, _nbData );
    }
    
protected:
    
    T* _data;
    
    
};



template<typename T>
class BufferSharedPtr : public RefPtr< Buffer<T> >
{
public:
    
    BufferSharedPtr() : RefPtr< Buffer<T> >()
    {
    }
    
    explicit BufferSharedPtr(Buffer<T>* buf) : RefPtr< Buffer<T> >(buf)
    {
    }
    
};



NS_CC_END


#endif
