//
//  CCBeamBodyDrawNodeT.cpp
//  cocos2d_libs
//
//  Created by Federico Spadoni on 06/03/16.
//
//

#include "physics-render/CCBeamBodyDrawNodeT.h"
#include "physics-shaders/CCPhysicsShaders.h"

NS_CC_BEGIN



template<> bool BeamBodyDrawNodeT<V2F_T2F>::initWithTextureImpl(Texture2D *texture, const Rect& rect)
{
    
    setGLProgramState(GLProgramState::getOrCreateWithGLProgramName(GLProgram::SHADER_NAME_POSITION_TEXTURE));
    
    

    
    _blendFunc = BlendFunc::ALPHA_PREMULTIPLIED;
    
    
    //        init buffers
    unsigned int vertexCount = PageMemoryPool::MEMORY_PAGE_SIZE / sizeof(V2F_T2F);
    unsigned int indexCount = PageMemoryPool::MEMORY_PAGE_SIZE / sizeof(unsigned short);
    
    ensureBufferCapacity(vertexCount, indexCount);
    //        _buffer = PoolBuffer<V2F_C4B_T2F>::create(_bufferCapacity);
    
    const V2F_T2F* databuf = _buffer->lockForWrite();
    
    if (Configuration::getInstance()->supportsShareableVAO())
    {
        glGenVertexArrays(1, &_vao);
        GL::bindVAO(_vao);
    }
    
    glGenBuffers(1, &_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(V2F_T2F)* _bufferCapacity, databuf, GL_STREAM_DRAW);
    
    // vertex
    glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_POSITION);
    glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_POSITION, 2, GL_FLOAT, GL_FALSE, sizeof(V2F_T2F), (GLvoid *)offsetof(V2F_T2F, vertices));
    //        // color
    //        glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_COLOR);
    //        glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_COLOR, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(V2F_C4B_T2F), (GLvoid *)offsetof(V2F_C4B_T2F, colors));
    // texcood
    glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_TEX_COORD);
    glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_TEX_COORD, 2, GL_FLOAT, GL_FALSE, sizeof(V2F_T2F), (GLvoid *)offsetof(V2F_T2F, texCoords));
    
    // update texture (calls updateBlendFunc)
    setTexture(texture);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    if (Configuration::getInstance()->supportsShareableVAO())
    {
        GL::bindVAO(0);
    }
    
    CHECK_GL_ERROR_DEBUG();
    
    _buffer->unlock();
    
    _dirty = true;
    
    // texture parameters
    Texture2D::TexParams    texParams;
    texParams.minFilter = GL_LINEAR;
    texParams.magFilter = GL_LINEAR;
    texParams.wrapS = GL_REPEAT;
    texParams.wrapT = GL_REPEAT;
    texture->setTexParameters(texParams);
    
    
    //    setDirty(true);
    return true;
    
}



template<> void BeamBodyDrawNodeT<V2F_T2F>::updateVertexBufferImpl()
{
    
    //    compute buffer size
    _bufferCount = 0;
    _indexCount = 0;
    
    BeamBodyList::const_iterator last = _beambodies.end();
    for ( BeamBodyList::const_iterator iter = _beambodies.begin(); iter != last; ++iter )
    {
        unsigned int numActiveSegms = (*iter)->getNumActiveSegments();
        _bufferCount += numActiveSegms;
        _indexCount += numActiveSegms;
    }
    
    _bufferCount *= 4;
    _indexCount *= 6;
    ensureBufferCapacity(_bufferCount, _indexCount);
    
    if(_bufferCount)
    {
        V2F_T2F* triangles = _buffer->lockForWrite();
        
        for ( BeamBodyList::const_iterator iter = _beambodies.begin(); iter != last; ++iter )
        {
            (*iter)->fillDisplayBuffer( triangles );
            
            triangles += 4 * (*iter)->getNumActiveSegments();
        }
        
        _dirty = true;
    }

}

template<> void BeamBodyDrawNodeT<V2F_T2F>::onDraw(const Mat4 &transform, uint32_t flags)
{
    
    auto glProgram = getGLProgram();
    glProgram->use();
    glProgram->setUniformsForBuiltins(_modelViewTransform);
    //    _glProgramState->apply(transform);
    
    // Set material
    GL::bindTexture2D( _texture->getName() );
    GL::blendFunc(_blendFunc.src, _blendFunc.dst);
    
    
    
    if (_dirty)
    {
        const V2F_T2F* databuf = _buffer->lockForRead();
        glBindBuffer(GL_ARRAY_BUFFER, _vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(V2F_T2F)*_bufferCapacity, databuf, GL_STREAM_DRAW);
        
        _dirty = false;
    }
    if (Configuration::getInstance()->supportsShareableVAO())
    {
        GL::bindVAO(_vao);
    }
    else
    {
        GL::enableVertexAttribs(GL::VERTEX_ATTRIB_FLAG_POSITION | GL::VERTEX_ATTRIB_FLAG_TEX_COORD);
        
        glBindBuffer(GL_ARRAY_BUFFER, _vbo);
        // vertex
        glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_POSITION, 2, GL_FLOAT, GL_FALSE, sizeof(V2F_T2F), (GLvoid *)offsetof(V2F_T2F, vertices));
        //        // color
        //        glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_COLOR, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(V2F_C4B_T2F), (GLvoid *)offsetof(V2F_C4B_T2F, colors));
        // texcood
        glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_TEX_COORD, 2, GL_FLOAT, GL_FALSE, sizeof(V2F_T2F), (GLvoid *)offsetof(V2F_T2F, texCoords));
    }
    
    
    //    glPolygonMode(GL_FRONT, GL_LINE);
    
    //    glDrawArrays(GL_LINE_LOOP, 0, _bufferCount);
    glDrawArrays(GL_TRIANGLES, 0, _bufferCount);
    
    //    glPolygonMode(GL_FRONT, GL_FILL);
    
    
    CC_INCREMENT_GL_DRAWS(1);
    CHECK_GL_ERROR_DEBUG();
    
    if (Configuration::getInstance()->supportsShareableVAO())
    {
        //Unbind VAO
        GL::bindVAO(0);
    }
    else
    {
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
    
}






template<> bool BeamBodyDrawNodeT<V2F_N2F_T2F_C4F>::initWithTextureImpl(Texture2D *texture, const Rect& rect)
{

//    SHADER_NAME_POSITION_TEXTURE_COLOR
    _glShaderProgram = GLProgram::createWithByteArrays(ccShader_2D_LINE_vert, ccShader_2D_LINE_frag  ); // ccShader_2D_LINE_frag
    auto glProgramState = GLProgramState::getOrCreateWithGLProgram(_glShaderProgram);
    
    setGLProgramState(glProgramState);

//    glProgramState->getGLProgram()->bindPredefinedVertexAttribs();
    
//    ssize_t count = glProgramState->getVertexAttribCount();
    
    _blendFunc = BlendFunc::ALPHA_PREMULTIPLIED;
    
    
    //        init buffers
    unsigned int vertexCount = PageMemoryPool::MEMORY_PAGE_SIZE / sizeof(V2F_N2F_T2F_C4F);
    unsigned int indexCount = PageMemoryPool::MEMORY_PAGE_SIZE / sizeof(unsigned short);
    
    ensureBufferCapacity(vertexCount, indexCount);

    
    const V2F_N2F_T2F_C4F* databuf = _buffer->lockForWrite();
    
    if (Configuration::getInstance()->supportsShareableVAO())
    {
        glGenVertexArrays(1, &_vao);
        GL::bindVAO(_vao);
    }
    
    glGenBuffers(1, &_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(V2F_N2F_T2F_C4F)* _bufferCapacity, databuf, GL_STREAM_DRAW);
    
    // vertex
    glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_POSITION);
    glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_POSITION, 2, GL_FLOAT, GL_FALSE, sizeof(V2F_N2F_T2F_C4F), (GLvoid *)offsetof(V2F_N2F_T2F_C4F, vertices));
    // normal
    glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_NORMAL);
    glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_NORMAL, 2, GL_FLOAT, GL_FALSE, sizeof(V2F_N2F_T2F_C4F), (GLvoid *)offsetof(V2F_N2F_T2F_C4F, normals));
    // texture coords
    glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_TEX_COORD);
    glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_TEX_COORD, 2, GL_FLOAT, GL_FALSE, sizeof(V2F_N2F_T2F_C4F), (GLvoid *)offsetof(V2F_N2F_T2F_C4F, texCoords));
    // color
    glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_COLOR);
    glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_COLOR, 4, GL_FLOAT, GL_FALSE, sizeof(V2F_N2F_T2F_C4F), (GLvoid *)offsetof(V2F_N2F_T2F_C4F, colors));

    
    // update texture (calls updateBlendFunc)
    setTexture(texture);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    if (Configuration::getInstance()->supportsShareableVAO())
    {
        GL::bindVAO(0);
    }
    
    CHECK_GL_ERROR_DEBUG();
    
    _buffer->unlock();
    
    _dirty = true;
    
    // texture parameters
    Texture2D::TexParams    texParams;
    texParams.minFilter = GL_LINEAR;
    texParams.magFilter = GL_LINEAR;
    texParams.wrapS = GL_REPEAT;
    texParams.wrapT = GL_REPEAT;
    texture->setTexParameters(texParams);
    
    
    //    setDirty(true);
    return true;
    
}


template<> void BeamBodyDrawNodeT<V2F_N2F_T2F_C4F>::updateVertexBufferImpl()
{
    
    //    compute buffer size
    _bufferCount = 0;
    _indexCount = 0;
    
    BeamBodyList::const_iterator last = _beambodies.end();
    for ( BeamBodyList::const_iterator iter = _beambodies.begin(); iter != last; ++iter )
    {
        unsigned int numActiveSegms = (*iter)->getNumActiveSegments();
        _bufferCount += numActiveSegms;
        _indexCount += numActiveSegms;
    }
    
    _bufferCount *= 6;
    _indexCount *= 6;
    ensureBufferCapacity(_bufferCount, _indexCount);
    
    if(_bufferCount)
    {
        V2F_N2F_T2F_C4F* triangles = _buffer->lockForWrite();
        
        for ( BeamBodyList::const_iterator iter = _beambodies.begin(); iter != last; ++iter )
        {
            (*iter)->fillDisplayBuffer( triangles );
            
            triangles += 6 * (*iter)->getNumActiveSegments();
        }
        
        _dirty = true;
    }
    
}

template<> void BeamBodyDrawNodeT<V2F_N2F_T2F_C4F>::onDraw(const Mat4 &transform, uint32_t flags)
{
    
//    auto glProgram = getGLProgram();
//    glProgram->use();
//    glProgram->setUniformsForBuiltins(transform); //_modelViewTransform);
    
    
    
//    auto glProgramState = getGLProgramState();
//    glProgramState->setVertexAttribPointer("a_position", 2, GL_FLOAT, GL_FALSE, 0, vertices);
//    glProgramState->apply(transform);
    
    // Set material
    GL::bindTexture2D( _texture->getName() );
    
    _blendFunc = BlendFunc::ADDITIVE;
    GL::blendFunc(_blendFunc.src, _blendFunc.dst);
    
    
    
//    if (_dirty)
    {
        const V2F_N2F_T2F_C4F* databuf = _buffer->lockForRead();
        glBindBuffer(GL_ARRAY_BUFFER, _vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(V2F_N2F_T2F_C4F)*_bufferCapacity, databuf, GL_STREAM_DRAW);
        
        _dirty = false;
    }
//    if (Configuration::getInstance()->supportsShareableVAO())
//    {
//        GL::bindVAO(_vao);
//    }
//    else
//    {
//        GL::enableVertexAttribs(GL::VERTEX_ATTRIB_FLAG_POSITION | GL::VERTEX_ATTRIB_FLAG_NORMAL | GL::VERTEX_ATTRIB_FLAG_TEX_COORD | GL::VERTEX_ATTRIB_FLAG_COLOR );
//        
//        glBindBuffer(GL_ARRAY_BUFFER, _vbo);
//        // vertex
//        glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_POSITION);
//        glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_POSITION, 2, GL_FLOAT, GL_FALSE, sizeof(V2F_N2F_T2F_C4F), (GLvoid *)offsetof(V2F_N2F_T2F_C4F, vertices));
//        // normal
//        glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_NORMAL);
//        glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_NORMAL, 2, GL_FLOAT, GL_FALSE, sizeof(V2F_N2F_T2F_C4F), (GLvoid *)offsetof(V2F_N2F_T2F_C4F, normals));
//        // texture coords
//        glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_TEX_COORD);
//        glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_TEX_COORD, 2, GL_FLOAT, GL_FALSE, sizeof(V2F_N2F_T2F_C4F), (GLvoid *)offsetof(V2F_N2F_T2F_C4F, texCoords));
//        // color
//        glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_COLOR);
//        glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_COLOR, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(V2F_N2F_T2F_C4F), (GLvoid *)offsetof(V2F_N2F_T2F_C4F, colors));
//    }
    
    auto glProgramState = getGLProgramState();
    
//    ssize_t count = glProgramState->getVertexAttribCount();
    
    
    glProgramState->setVertexAttribPointer("a_position",  2, GL_FLOAT, GL_FALSE, sizeof(V2F_N2F_T2F_C4F),  (GLvoid *)offsetof(V2F_N2F_T2F_C4F, vertices) );
    glProgramState->setVertexAttribPointer("a_normal", 2, GL_FLOAT, GL_FALSE, sizeof(V2F_N2F_T2F_C4F), (GLvoid *)offsetof(V2F_N2F_T2F_C4F, normals) );
    glProgramState->setVertexAttribPointer("a_texCoord", 2, GL_FLOAT, GL_FALSE, sizeof(V2F_N2F_T2F_C4F), (GLvoid *)offsetof(V2F_N2F_T2F_C4F, texCoords) );
    glProgramState->setVertexAttribPointer("a_color", 4, GL_FLOAT, GL_FALSE, sizeof(V2F_N2F_T2F_C4F), (GLvoid *)offsetof(V2F_N2F_T2F_C4F, colors) );
    
    glProgramState->apply(transform);
    
//    _glProgramState->applyGLProgram(transform);
//    _glProgramState->applyUniforms();
    
    
    //    glPolygonMode(GL_FRONT, GL_LINE);
    
//    glLineWidth(4);
    glProgramState->setUniformFloatv("thickness", 1, &_beamThickness);
    
//    glDrawArrays(GL_LINES, 0, _bufferCount);
    glDrawArrays(GL_TRIANGLES, 0, _bufferCount);
//    glDrawArrays(GL_POINTS, 0, _bufferCount);
    
    //    glPolygonMode(GL_FRONT, GL_FILL);
    
    
    CC_INCREMENT_GL_DRAWS(1);
    CHECK_GL_ERROR_DEBUG();
    
//    if (Configuration::getInstance()->supportsShareableVAO())
//    {
//        //Unbind VAO
//        GL::bindVAO(0);
//    }
//    else
//    {
        glBindBuffer(GL_ARRAY_BUFFER, 0);
//    }
    
}

NS_CC_END