//
//  CCSoftBodyDrawNode.cpp
//  cocos2d_libs
//
//  Created by Federico Spadoni on 29/11/14.
//
//

#include "CCSoftBodyDrawNode.h"

#include "renderer/CCTextureCache.h"
#include "renderer/CCTexture2D.h"
#include "base/CCDirector.h"
#include "base/CCConfiguration.h"
#include "renderer/CCRenderer.h"
#include "renderer/ccGLStateCache.h"
#include "renderer/CCGLProgramState.h"
//#include "xxhash.h"

#include "base/CCEventListenerCustom.h"
#include "base/CCEventDispatcher.h"

#include "physics/CCPhysicsWorld.h"
#include "physics/CCPhysicsScene.h"
#include "physics/CCPhysicsType.h"

NS_CC_BEGIN

#define CC_INCREMENT_GL_VERTICES(__n__) Director::getInstance()->getRenderer()->addDrawnVertices(__n__)


SoftBodyDrawNode* SoftBodyDrawNode::create()
{
    SoftBodyDrawNode *drawnode = new (std::nothrow) SoftBodyDrawNode();
    if (drawnode && drawnode->init())
    {
        drawnode->autorelease();
        return drawnode;
    }
    CC_SAFE_DELETE(drawnode);
    return nullptr;
}


SoftBodyDrawNode* SoftBodyDrawNode::createWithTexture(Texture2D *texture)
{
    SoftBodyDrawNode *drawnode = new (std::nothrow) SoftBodyDrawNode();
    if (drawnode && drawnode->initWithTexture(texture))
    {
        drawnode->autorelease();
        return drawnode;
    }
    CC_SAFE_DELETE(drawnode);
    return nullptr;
}



SoftBodyDrawNode* SoftBodyDrawNode::create(const std::string& filename)
{
    SoftBodyDrawNode *drawnode = new (std::nothrow) SoftBodyDrawNode();
    if (drawnode && drawnode->initWithFile(filename))
    {
        drawnode->autorelease();
        return drawnode;
    }
    CC_SAFE_DELETE(drawnode);
    return nullptr;
}

bool SoftBodyDrawNode::init(void)
{
    return initWithTexture(nullptr, Rect::ZERO );
}


bool SoftBodyDrawNode::initWithTexture(Texture2D *texture)
{
    CCASSERT(texture != nullptr, "Invalid texture for DeformableSprite");
    
    Rect rect = Rect::ZERO;
    rect.size = texture->getContentSize();
    
    return initWithTexture(texture, rect);
}


bool SoftBodyDrawNode::initWithTexture(Texture2D *texture, const Rect& rect)
{
    bool result;
    if (Node::init())
    {

        setGLProgramState(GLProgramState::getOrCreateWithGLProgramName(GLProgram::SHADER_NAME_POSITION_TEXTURE));
        
        
        _blendFunc = BlendFunc::ALPHA_PREMULTIPLIED;
    
        
//        init buffers
        unsigned int vertexCount = PageMemoryPool::MEMORY_PAGE_SIZE / sizeof(V2F_T2F);
        unsigned int indexCount = PageMemoryPool::MEMORY_PAGE_SIZE / sizeof(unsigned short);

        ensureBufferCapacity(vertexCount, indexCount);
//        _buffer = PoolBuffer<V2F_C4F_T2F>::create(_bufferCapacity);
        
        const V2F_T2F* databuf = _buffer->lockForWrite();
        
        if (Configuration::getInstance()->supportsShareableVAO())
        {
            glGenVertexArrays(1, &_vao);
            GL::bindVAO(_vao);
        }

        glGenBuffers(1, &_vbo);
        glBindBuffer(GL_ARRAY_BUFFER, _vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(V2F_T2F)* _bufferCapacity, databuf, GL_STREAM_DRAW);

        // vertex
        glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_POSITION);
        glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_POSITION, 2, GL_FLOAT, GL_FALSE, sizeof(V2F_T2F), (GLvoid *)offsetof(V2F_T2F, vertices));
//        // color
//        glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_COLOR);
//        glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_COLOR, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(V2F_C4F_T2F), (GLvoid *)offsetof(V2F_C4F_T2F, colors));
        // texcood
        glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_TEX_COORD);
        glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_TEX_COORD, 2, GL_FLOAT, GL_FALSE, sizeof(V2F_T2F), (GLvoid *)offsetof(V2F_T2F, texCoords));

        // update texture (calls updateBlendFunc)
        setTexture(texture);        

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        if (Configuration::getInstance()->supportsShareableVAO())
        {
            GL::bindVAO(0);
        }
        
        CHECK_GL_ERROR_DEBUG();

        _buffer->unlock();

        _dirty = true;
        
        // texture parameters
        Texture2D::TexParams    texParams;
        texParams.minFilter = GL_LINEAR;
        texParams.magFilter = GL_LINEAR;
        texParams.wrapS = GL_REPEAT;
        texParams.wrapT = GL_REPEAT;
        texture->setTexParameters(texParams);

        
        
//        init wire frame buffers
        vertexCount = PageMemoryPool::MEMORY_PAGE_SIZE / sizeof(V2F_C4F);
        indexCount = PageMemoryPool::MEMORY_PAGE_SIZE / sizeof(unsigned short);
        
        ensureWireFrameBufferCapacity(vertexCount, indexCount);
        
        const V2F_C4F* dataWireFramebuf = _wireFrameBuffer->lockForWrite();
        
        if (Configuration::getInstance()->supportsShareableVAO())
        {
            glGenVertexArrays(1, &_wf_vao);
            GL::bindVAO(_wf_vao);
        }
        
        glGenBuffers(1, &_wf_vbo);
        glBindBuffer(GL_ARRAY_BUFFER, _wf_vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(V2F_C4F)* _bufferCapacity, dataWireFramebuf, GL_STREAM_DRAW);
        
        // vertex
        glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_POSITION);
        glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_POSITION, 2, GL_FLOAT, GL_FALSE, sizeof(V2F_C4F), (GLvoid *)offsetof(V2F_C4F, vertices));
        // color
        glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_COLOR);
        glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_COLOR, 4, GL_FLOAT, GL_TRUE, sizeof(V2F_C4F), (GLvoid *)offsetof(V2F_C4F, colors));
        // texcood
//        glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_TEX_COORD);
//        glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_TEX_COORD, 2, GL_FLOAT, GL_FALSE, sizeof(V2F_C4F), (GLvoid *)offsetof(V2F_T2F, texCoords));
        
        // update texture (calls updateBlendFunc)
//        setTexture(texture);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        if (Configuration::getInstance()->supportsShareableVAO())
        {
            GL::bindVAO(0);
        }
        
        CHECK_GL_ERROR_DEBUG();
        
        _wireFrameBuffer->unlock();

        
        
        
//#if CC_ENABLE_CACHE_TEXTURE_DATA
//        // Need to listen the event only when not use batchnode, because it will use VBO
//        auto listener = EventListenerCustom::create(EVENT_RENDERER_RECREATED, [this](EventCustom* event){
//            /** listen the event that renderer was recreated on Android/WP8 */
//            this->init();
//        });
//        
//        _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
//#endif
        
        
//        register to simulation loop completion event listener
        _simLoopCompletionListener = EventListenerCustom::create( PhysicsScene::_EVENT_SIM_LOOP_COMPLETION,                                                                   CC_CALLBACK_1(SoftBodyDrawNode::updateVertexBuffer, this ) );
        
        _eventDispatcher->addEventListenerWithSceneGraphPriority(_simLoopCompletionListener, this);

        
        result = true;
    }
    else
    {
        result = false;
    }

    
//    setDirty(true);
    return result;
}

bool SoftBodyDrawNode::initWithFile(const std::string& filename)
{
    CCASSERT(filename.size()>0, "Invalid filename for DeformableSprite");
    
    Texture2D *texture = Director::getInstance()->getTextureCache()->addImage(filename);
    if (texture)
    {
        Rect rect = Rect::ZERO;
        rect.size = texture->getContentSize();
        return initWithTexture(texture, rect);
    }
    
    // don't release here.
    // when load texture failed, it's better to get a "transparent" DeformableSprite then a crashed program
    // this->release();
    return false;
}



void SoftBodyDrawNode::ensureBufferCapacity(const unsigned int vertexCount, const unsigned int indexCount)
{
    CCASSERT(vertexCount>=0, "SoftBodyDrawNode: capacity must be >= 0");
    
    if( vertexCount > _bufferCapacity)
    {
        _bufferCapacity = PageMemoryPool::getMaxPoolCapacity( vertexCount, sizeof(V2F_T2F));
        _buffer = PoolBuffer<V2F_T2F>::create(_bufferCapacity);
	}
    if ( indexCount > _indicesCapacity )
    {
        _indicesCapacity = PageMemoryPool::getMaxPoolCapacity(indexCount, sizeof(unsigned short));
        _indices = PoolBuffer<unsigned short>::create(_indicesCapacity );

    }
}


void SoftBodyDrawNode::ensureWireFrameBufferCapacity(const unsigned int vertexCount, const unsigned int indexCount)
{
    CCASSERT(vertexCount>=0, "SoftBodyDrawNode: capacity must be >= 0");
    
    if( vertexCount > _wireFrameBufferCapacity)
    {
        _wireFrameBufferCapacity = PageMemoryPool::getMaxPoolCapacity( vertexCount, sizeof(V2F_C4F));
        _wireFrameBuffer = PoolBuffer<V2F_C4F>::create(_wireFrameBufferCapacity);
	}
}

SoftBodyDrawNode::SoftBodyDrawNode(void)
: _texture(nullptr)
, _simLoopCompletionListener(nullptr)
, _vao(0)
, _vbo(0)
, _bufferCount(0)
, _bufferCapacity(0)
, _wireFrameBufferCount(0)
, _wireFrameBufferCapacity(0)
, _dirty(false)
, _blendFunc(BlendFunc::DISABLE)
, _displayColor(1.0f, 1.0f, 1.0f, 1.0f)
, _materialID(0)
, _wireFrame(false)
{
    
}

SoftBodyDrawNode::~SoftBodyDrawNode(void)
{
    glDeleteBuffers(1, &_vbo);
    _vbo = 0;
    
    if (Configuration::getInstance()->supportsShareableVAO())
    {
        GL::bindVAO(0);
        glDeleteVertexArrays(1, &_vao);
        _vao = 0;
    }
    
    CC_SAFE_RELEASE(_texture);
    
    Director::getInstance()->getEventDispatcher()->removeEventListener(_simLoopCompletionListener);
    
//    SoftBodyList::const_iterator last = _softbodies.end();
//    for ( SoftBodyList::const_iterator iter = _softbodies.begin(); iter != last; ++iter )
//    {
//        (*iter)->release();
//    }
    _softbodies.clear();
    
}

/*
 * Texture methods
 */

/*
 * This array is the data of a white image with 2 by 2 dimension.
 * It's used for creating a default texture when DeformableSprite's texture is set to nullptr.
 * Supposing codes as follows:
 *
 *   auto sp = new (std::nothrow) DeformableSprite();
 *   sp->init();  // Texture was set to nullptr, in order to make opacity and color to work correctly, we need to create a 2x2 white texture.
 *
 * The test is in "TestCpp/DeformableSpriteTest/DeformableSprite without texture".
 */
static unsigned char cc_2x2_white_image[] = {
    // RGBA8888
    0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF
};

#define CC_2x2_WHITE_IMAGE_KEY  "/cc_2x2_white_image"

Texture2D* SoftBodyDrawNode::getTexture() const
{
    return _texture;
}

void SoftBodyDrawNode::setTexture(Texture2D *texture)
{
    // accept texture==nil as argument
    CCASSERT( !texture || dynamic_cast<Texture2D*>(texture), "setTexture expects a Texture2D. Invalid argument");
    
    if (texture == nullptr)
    {
        // Gets the texture by key firstly.
        texture = Director::getInstance()->getTextureCache()->getTextureForKey(CC_2x2_WHITE_IMAGE_KEY);
        
        // If texture wasn't in cache, create it from RAW data.
        if (texture == nullptr)
        {
            Image* image = new (std::nothrow) Image();
            bool isOK = image->initWithRawData(cc_2x2_white_image, sizeof(cc_2x2_white_image), 2, 2, 8);
            CC_UNUSED_PARAM(isOK);
            CCASSERT(isOK, "The 2x2 empty texture was created unsuccessfully.");
            
            texture = Director::getInstance()->getTextureCache()->addImage(image, CC_2x2_WHITE_IMAGE_KEY);
            CC_SAFE_RELEASE(image);
        }
    }
    
    if ( _texture != texture)
    {
        CC_SAFE_RETAIN(texture);
        CC_SAFE_RELEASE(_texture);
        _texture = texture;
        updateBlendFunc();
    }
}


//void SoftBodyDrawNode::genMaterialID()
//{
//    if(_glProgramState->getUniformCount() > 0)
//    {
//        _materialID = Renderer::MATERIAL_ID_DO_NOT_BATCH;
//    }
//    else
//    {
//        int glProgram = (int)_glProgramState->getGLProgram()->getProgram();
//        int intArray[4] = { glProgram, (int)_textureID, (int)_blendFunc.src, (int)_blendFunc.dst};
//        
//        _materialID = XXH32((const void*)intArray, sizeof(intArray), 0);
//    }
//}


void SoftBodyDrawNode::updateBlendFunc(void)
{
    
    // it is possible to have an untextured DeformableSprite
    if (! _texture || ! _texture->hasPremultipliedAlpha())
    {
        _blendFunc = BlendFunc::ALPHA_NON_PREMULTIPLIED;
        setOpacityModifyRGB(false);
    }
    else
    {
        _blendFunc = BlendFunc::ALPHA_PREMULTIPLIED;
        setOpacityModifyRGB(true);
    }
}

void SoftBodyDrawNode::addSoftBody(PhysicsSoftBody* softbody)
{
    
    std::pair<SoftBodyListIterator,bool> iter = _softbodies.insert(softbody);
    if ( iter.second == true )
    {
        // softbody added
//        softbody->retain();
    }
    
    softbody->setDrawNode(this);
    
    // add to the physics world
//    getPhysicsWorld
    Scene *scene = Director::sharedDirector()->getRunningScene();
//    GameScene* gameScene = dynamic_cast<GameScene*>(scene);
//    if(gameScene != NULL)
//    {
        // scene is type of GameScene
//    }
}

void SoftBodyDrawNode::removeSoftBody(PhysicsSoftBody* softbody)
{
    SoftBodyList::iterator iter = _softbodies.find(softbody);
    if (iter != _softbodies.end())
    {
        (*iter)->setDrawNode(nullptr);
        _softbodies.erase(iter);
    }
}


void SoftBodyDrawNode::updateVertexBuffer(EventCustom* event)
{
    
    
    SoftBodyList::const_iterator last = _softbodies.end();
    
    if ( !_wireFrame )
    {
        //    compute buffer size
        _bufferCount = 0;
        _indexCount = 0;
        for ( SoftBodyList::const_iterator iter = _softbodies.begin(); iter != last; ++iter )
        {
            unsigned int numActiveTriangs = (*iter)->getNumActiveTriangles();
            _bufferCount += numActiveTriangs;
            _indexCount += numActiveTriangs;
        }
        
        _bufferCount *= 3;
        _indexCount *= 3;
        ensureBufferCapacity(_bufferCount, _indexCount);
        
        if(_bufferCount)
        {
            V2F_T2F* triangles = _buffer->lockForWrite();
            
            for ( SoftBodyList::const_iterator iter = _softbodies.begin(); iter != last; ++iter )
            {
                (*iter)->fillDisplayBuffer( triangles );
                
                triangles += 3 * (*iter)->getNumActiveTriangles();
            }
            
            _dirty = true;
        }
    }
    else
    {
        //    compute buffer size
        _wireFrameBufferCount = 0;
        _indexCount = 0;
        for ( SoftBodyList::const_iterator iter = _softbodies.begin(); iter != last; ++iter )
        {
            unsigned int numActiveTriangs = (*iter)->getNumActiveTriangles();
            _wireFrameBufferCount += numActiveTriangs;
            _indexCount += numActiveTriangs;
        }
        
        _wireFrameBufferCount *= 6;
        _indexCount *= 6;
        
        ensureWireFrameBufferCapacity(_wireFrameBufferCount, _indexCount);
        
        if(_wireFrameBufferCount)
        {
            V2F_C4F* lines = _wireFrameBuffer->lockForWrite();
            
            for ( SoftBodyList::const_iterator iter = _softbodies.begin(); iter != last; ++iter )
            {
                (*iter)->fillDisplayWireFrameBuffer( lines );
                
                lines += 6 * (*iter)->getNumActiveTriangles();
            }
            
            _dirty = true;
        }

    }
    
    CC_INCREMENT_GL_VERTICES(_bufferCount);
}

void SoftBodyDrawNode::updateTransform(void)
{
    Node::updateTransform();
}

void SoftBodyDrawNode::draw(Renderer *renderer, const Mat4 &transform, uint32_t flags)
{

//    updateVertexBuffer();
    
    if(_bufferCount || _wireFrameBufferCount )
    {
        _customCommand.init(_globalZOrder);
        
        if ( !_wireFrame )
        {
            _customCommand.func = CC_CALLBACK_0(SoftBodyDrawNode::onDraw, this, transform, flags);
        }
        else
        {
            _customCommand.func = CC_CALLBACK_0(SoftBodyDrawNode::onDrawWireFrame, this, transform, flags);
        }
            
        renderer->addCommand(&_customCommand);
    }
}

void SoftBodyDrawNode::onDraw(const Mat4 &transform, uint32_t flags)
{
    
    auto glProgram = getGLProgram();
    glProgram->use();
    glProgram->setUniformsForBuiltins(_modelViewTransform);
//    _glProgramState->apply(transform);
    
    // Set material
    GL::bindTexture2D( _texture->getName() );    
    GL::blendFunc(_blendFunc.src, _blendFunc.dst);
    
    
    
//    if (_dirty)
    {
        const V2F_T2F* databuf = _buffer->lockForRead();
        glBindBuffer(GL_ARRAY_BUFFER, _vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(V2F_T2F)*_bufferCapacity, databuf, GL_STREAM_DRAW);
        
        _dirty = false;
    }
    if (Configuration::getInstance()->supportsShareableVAO())
    {
        GL::bindVAO(_vao);
    }
    else
    {
        GL::enableVertexAttribs(GL::VERTEX_ATTRIB_FLAG_POSITION | GL::VERTEX_ATTRIB_FLAG_TEX_COORD);
        
//        glBindBuffer(GL_ARRAY_BUFFER, _vbo);
        // vertex
        glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_POSITION, 2, GL_FLOAT, GL_FALSE, sizeof(V2F_T2F), (GLvoid *)offsetof(V2F_T2F, vertices));
//        // color
//        glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_COLOR, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(V2F_C4F_T2F), (GLvoid *)offsetof(V2F_C4F_T2F, colors));
        // texcood
        glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_TEX_COORD, 2, GL_FLOAT, GL_FALSE, sizeof(V2F_T2F), (GLvoid *)offsetof(V2F_T2F, texCoords));
    }
    
    
//    glPolygonMode(GL_FRONT, GL_LINE);

//    glDrawArrays(GL_LINE_LOOP, 0, _bufferCount);
    glDrawArrays(GL_TRIANGLES, 0, _bufferCount);
    
//    glPolygonMode(GL_FRONT, GL_FILL);
    
    
    CC_INCREMENT_GL_DRAWS(1);
    CHECK_GL_ERROR_DEBUG();
    
    if (Configuration::getInstance()->supportsShareableVAO())
    {
        //Unbind VAO
        GL::bindVAO(0);
    }
//    else
    {
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
    
}

void SoftBodyDrawNode::onDrawWireFrame(const Mat4 &transform, uint32_t flags)
{
    auto glProgram = getGLProgram();
    glProgram->use();
    glProgram->setUniformsForBuiltins(_modelViewTransform);
    //    _glProgramState->apply(transform);
    
    // Set material
//    GL::bindTexture2D( _texture->getName() );
    GL::blendFunc(_blendFunc.src, _blendFunc.dst);
    
    
    if (_dirty)
    {
        const V2F_C4F* databuf = _wireFrameBuffer->lockForRead();
        glBindBuffer(GL_ARRAY_BUFFER, _wf_vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(V2F_C4F)*_wireFrameBufferCapacity, databuf, GL_STREAM_DRAW);
        
        _dirty = false;
    }
    if (Configuration::getInstance()->supportsShareableVAO())
    {
        GL::bindVAO(_wf_vao);
    }
    else
    {
        GL::enableVertexAttribs(GL::VERTEX_ATTRIB_FLAG_POSITION | GL::VERTEX_ATTRIB_FLAG_COLOR );
        
        glBindBuffer(GL_ARRAY_BUFFER, _wf_vbo);
        // vertex
        glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_POSITION, 2, GL_FLOAT, GL_FALSE, sizeof(V2F_C4F), (GLvoid *)offsetof(V2F_C4F, vertices));
        // color
        glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_COLOR, 4, GL_FLOAT, GL_TRUE, sizeof(V2F_C4F), (GLvoid *)offsetof(V2F_C4F, colors));

    }
    
    glLineWidth(2);
    
    glDrawArrays(GL_LINES, 0, _wireFrameBufferCount);
    
    
    
    CC_INCREMENT_GL_DRAWS(1);
    CHECK_GL_ERROR_DEBUG();
    
    if (Configuration::getInstance()->supportsShareableVAO())
    {
        //Unbind VAO
        GL::bindVAO(0);
    }
    else
    {
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
}

void SoftBodyDrawNode::setWireFrame(bool wireFrame)
{
    _wireFrame = wireFrame;
    
    if (!_wireFrame)
    {
        setGLProgramState(GLProgramState::getOrCreateWithGLProgramName(GLProgram::SHADER_NAME_POSITION_TEXTURE));
    }
    else
    {
        setGLProgramState(GLProgramState::getOrCreateWithGLProgramName(GLProgram::SHADER_NAME_POSITION_COLOR));
    }
}


const BlendFunc& SoftBodyDrawNode::getBlendFunc() const
{
    return _blendFunc;
}

void SoftBodyDrawNode::setBlendFunc(const BlendFunc &blendFunc)
{
    _blendFunc = blendFunc;
}

NS_CC_END
