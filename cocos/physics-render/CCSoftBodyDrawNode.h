//
//  CCPhysicsSoftBodySprite.h
//  cocos2d_libs
//
//  Created by Federico Spadoni on 29/11/14.
//
//

#ifndef __cocos2d_libs__CCSoftBodyDrawNode__
#define __cocos2d_libs__CCSoftBodyDrawNode__


#include "2d/CCNode.h"
#include "base/ccTypes.h"
//#include "extensions/ExtensionMacros.h"
//#include "extensions/ExtensionExport.h"
#include "renderer/CCCustomCommand.h"
#include "physics/CCPhysicsSoftBody.h"


#include "renderer/CCTrianglesCommand.h"

#include <set>

//#if (CC_ENABLE_CHIPMUNK_INTEGRATION)



NS_CC_BEGIN



class CC_DLL SoftBodyDrawNode : public Node, public TextureProtocol
{
public:
    
    static SoftBodyDrawNode* create();
    /** Creates an sprite with a texture.
     The rect used will be the size of the texture.
     The offset will be (0,0).
     */
    static SoftBodyDrawNode* createWithTexture(Texture2D *pTexture);
    
    /** Creates an sprite with an image filename.
     The rect used will be the size of the image.
     The offset will be (0,0).
     */
    static SoftBodyDrawNode* create(const std::string& pszFileName);
    

    /**
     * Returns the currently used texture
     *
     * @return  The texture that is currenlty being used.
     * @js NA
     * @lua NA
     */
    virtual Texture2D* getTexture() const override;
    
    /**
     * Sets a new texuture. It will be retained.
     *
     * @param   texture A valid Texture2D object, which will be applied to this sprite object.
     * @js NA
     * @lua NA
     */
    virtual void setTexture(Texture2D *texture) override;

    virtual void updateBlendFunc(void);
    
    
    void addSoftBody(PhysicsSoftBody* softbody);
    
    void removeSoftBody(PhysicsSoftBody* softbody);
    
    
    const std::string& getTextureFileName() const { return _textureFileName; }
    
    /**
     * @js NA
     * @lua NA
     */
    const BlendFunc& getBlendFunc() const override;
    /**
     * @code
     * When this function bound into js or lua,the parameter will be changed
     * In js: var setBlendFunc(var src, var dst)
     * @endcode
     * @lua NA
     */
    void setBlendFunc(const BlendFunc &blendFunc) override;
    
    virtual void updateTransform(void) override;
    
    void updateVertexBuffer(EventCustom* event);
    
    
    // Overrides
    virtual void draw(Renderer *renderer, const Mat4 &transform, uint32_t flags) override;
    
    void onDraw(const Mat4 &transform, uint32_t flags);

    void onDrawWireFrame(const Mat4 &transform, uint32_t flags);
    
    void setWireFrame(bool wireFrame);
    
    
CC_CONSTRUCTOR_ACCESS:
    
    SoftBodyDrawNode();
    virtual ~SoftBodyDrawNode();

    
protected:
    
    /* Initializes an empty sprite with nothing init. */
    virtual bool init(void) override;
    
    /**
     * Initializes a sprite with a texture.
     *
     * After initialization, the rect used will be the size of the texture, and the offset will be (0,0).
     *
     * @param   texture    A pointer to an existing Texture2D object.
     *                      You can use a Texture2D object for many sprites.
     * @return  true if the sprite is initialized properly, false otherwise.
     */
    virtual bool initWithTexture(Texture2D *texture);
    
    /**
     * Initializes a sprite with a texture and a rect.
     *
     * After initialization, the offset will be (0,0).
     *
     * @param   texture    A pointer to an exisiting Texture2D object.
     *                      You can use a Texture2D object for many sprites.
     * @param   rect        Only the contents inside rect of this texture will be applied for this sprite.
     * @return  true if the sprite is initialized properly, false otherwise.
     */
    virtual bool initWithTexture(Texture2D *texture, const Rect& rect);

    
    bool initWithFile(const std::string& filename);
    
    
    void ensureBufferCapacity(const unsigned int vertexCount, const unsigned int indexCount);
    
    void ensureWireFrameBufferCapacity(const unsigned int vertexCount, const unsigned int indexCount);
    
    
//    void genMaterialID();
    
    std::string _textureFileName;
    
    EventListenerCustom* _simLoopCompletionListener;
    
    
    typedef std::set<PhysicsSoftBody*>::iterator SoftBodyListIterator;
    typedef std::set<PhysicsSoftBody*> SoftBodyList;
    SoftBodyList _softbodies;
    
    PoolBufferSharedPtr<V2F_T2F> _buffer;
    PoolBufferSharedPtr<V2F_C4F> _wireFrameBuffer;
    
    CustomCommand _customCommand;
    
    GLuint      _vao;
    GLuint      _vbo;

    // wireframe buffers
    GLuint      _wf_vao;
    GLuint      _wf_vbo;
    
    int         _bufferCapacity;
    GLsizei     _bufferCount;
    
    int         _wireFrameBufferCapacity;
    GLsizei     _wireFrameBufferCount;
    
    Texture2D*  _texture;              /// Texture2D object that is used to render
    BlendFunc   _blendFunc;
    
    uint32_t _materialID;
    GLuint _textureID;

    Vec4 _displayColor; // in order to support tint and fade in fade out

    
    bool        _dirty;
    
    
    TrianglesCommand _cmd;
    
    PoolBufferSharedPtr<unsigned short> _indices;
    GLsizei _indexCount;
    int _indicesCapacity;
    
    bool _wireFrame;
    
private:
    CC_DISALLOW_COPY_AND_ASSIGN(SoftBodyDrawNode);
    
    
};

NS_CC_END

//#endif // CC_ENABLE_CHIPMUNK_INTEGRATION || CC_ENABLE_BOX2D_INTEGRATION



#endif /* defined(__cocos2d_libs__CCSoftBodyDrawNode__) */
