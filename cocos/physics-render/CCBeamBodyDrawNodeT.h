//
//  CCBeamBodyDrawNodeT.h
//  cocos2d_libs
//
//  Created by Federico Spadoni on 06/03/16.
//
//

#ifndef __cocos2d_libs__CCBeamBodyDrawNodeT__
#define __cocos2d_libs__CCBeamBodyDrawNodeT__

#include "2d/CCNode.h"
#include "base/ccTypes.h"
#include "base/CCDirector.h"
#include "base/CCConfiguration.h"
#include "base/CCEventListenerCustom.h"
#include "base/CCEventDispatcher.h"

#include "renderer/CCTextureCache.h"
#include "renderer/CCRenderer.h"
#include "renderer/ccGLStateCache.h"
#include "renderer/CCGLProgramState.h"
#include "renderer/CCCustomCommand.h"
#include "renderer/CCTrianglesCommand.h"

//#include "xxhash/xxhash.h"

#include "physics/CCPhysicsBeamBody.h"
#include "physics/CCPhysicsType.h"
#include "physics/CCPhysicsScene.h"


#include <set>
#include <type_traits>

//#if (CC_ENABLE_CHIPMUNK_INTEGRATION || CC_ENABLE_BOX2D_INTEGRATION)

#define CC_INCREMENT_GL_VERTICES(__n__) Director::getInstance()->getRenderer()->addDrawnVertices(__n__)


NS_CC_BEGIN

static unsigned char cc_2x2_white_image[] =
{
    // RGBA8888
    0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF
};

#define CC_2x2_WHITE_IMAGE_KEY  "/cc_2x2_white_image"



template <class T>
class  CC_DLL BeamBodyDrawNodeT : public Node, public TextureProtocol
{
    
public:
    
// inherit and  specialize these functions
    bool initWithTextureImpl(Texture2D *texture, const Rect& rect);
//    {
//        CCASSERT( false, "FATAL: BeamBodyDrawNodeT has no specialization for function bool initWithTextureImpl(Texture2D *texture, const Rect& rect)!");
//    }
    
    
    void onDrawImpl(const Mat4 &transform, uint32_t flags)
    {
        CCASSERT( false, "FATAL: BeamBodyDrawNodeT has no specialization for function void onDrawImpl(const Mat4 &transform, uint32_t flags)!");

    }
    
    void updateVertexBufferImpl()
    {
        CCASSERT( false, "FATAL: BeamBodyDrawNodeT has no specialization for function void updateVertexBufferImpl()!");
    }

    
    
    
    static BeamBodyDrawNodeT* create()
    {
        BeamBodyDrawNodeT *drawnode = new (std::nothrow) BeamBodyDrawNodeT();
        if (drawnode && drawnode->init())
        {
            drawnode->autorelease();
            return drawnode;
        }
        CC_SAFE_DELETE(drawnode);
        return nullptr;
    }
    
    /** Creates an sprite with a texture.
     The rect used will be the size of the texture.
     The offset will be (0,0).
     */
    static BeamBodyDrawNodeT* createWithTexture(Texture2D *texture)
    {
        BeamBodyDrawNodeT *drawnode = new (std::nothrow) BeamBodyDrawNodeT();
        if (drawnode && drawnode->initWithTexture(texture))
        {
            drawnode->autorelease();
            return drawnode;
        }
        CC_SAFE_DELETE(drawnode);
        return nullptr;
    }
    
    
    /** Creates an sprite with an image filename.
     The rect used will be the size of the image.
     The offset will be (0,0).
     */
    static BeamBodyDrawNodeT* create(const std::string& filename)
    {
        BeamBodyDrawNodeT *drawnode = new (std::nothrow) BeamBodyDrawNodeT();
        if (drawnode && drawnode->initWithFile(filename))
        {
            drawnode->autorelease();
            return drawnode;
        }
        CC_SAFE_DELETE(drawnode);
        return nullptr;
    }
    
    
    /**
     * Returns the currently used texture
     *
     * @return  The texture that is currenlty being used.
     * @js NA
     * @lua NA
     */
    virtual Texture2D* getTexture() const override
    {
        return _texture;
    }


    /**
     * Sets a new texuture. It will be retained.
     *
     * @param   texture A valid Texture2D object, which will be applied to this sprite object.
     * @js NA
     * @lua NA
     */

    virtual void setTexture(Texture2D *texture) override
    {
        CCASSERT( !texture || dynamic_cast<Texture2D*>(texture), "setTexture expects a Texture2D. Invalid argument");

        if (texture == nullptr)
        {
            // Gets the texture by key firstly.
            texture = Director::getInstance()->getTextureCache()->getTextureForKey(CC_2x2_WHITE_IMAGE_KEY);
    
            // If texture wasn't in cache, create it from RAW data.
            if (texture == nullptr)
            {
                Image* image = new (std::nothrow) Image();
                bool isOK = image->initWithRawData(cc_2x2_white_image, sizeof(cc_2x2_white_image), 2, 2, 8);
                CC_UNUSED_PARAM(isOK);
                CCASSERT(isOK, "The 2x2 empty texture was created unsuccessfully.");
        
                texture = Director::getInstance()->getTextureCache()->addImage(image, CC_2x2_WHITE_IMAGE_KEY);
                CC_SAFE_RELEASE(image);
            }
        }

        if ( _texture != texture)
        {
            CC_SAFE_RETAIN(texture);
            CC_SAFE_RELEASE(_texture);
            _texture = texture;
            updateBlendFunc();
        }
    }



    virtual void updateBlendFunc(void)
    {
        // it is possible to have an untextured DeformableSprite
        if (! _texture || ! _texture->hasPremultipliedAlpha())
        {
            _blendFunc = BlendFunc::ALPHA_NON_PREMULTIPLIED;
            setOpacityModifyRGB(false);
        }
        else
        {
            _blendFunc = BlendFunc::ALPHA_PREMULTIPLIED;
            setOpacityModifyRGB(true);
        }
    }



    void addBeamBody(PhysicsBeamBody* beambody)
    {
        std::pair<BeamBodyListIterator,bool> iter = _beambodies.insert(beambody);
        if ( iter.second == true )
        {
        }
        
//        beambody->setDrawNode(this);
    }

    void removeBeamBody(PhysicsBeamBody* beambody)
    {
        BeamBodyList::iterator iter = _beambodies.find(beambody);
        if (iter != _beambodies.end())
        {
//            (*iter)->setDrawNode(nullptr);
            _beambodies.erase(iter);
        }
    }

    void setThickness(const float thickness) { _beamThickness = thickness; }

    const std::string& getTextureFileName() const { return _textureFileName; }
    
    /**
     * @js NA
     * @lua NA
     */
    const BlendFunc& getBlendFunc() const override { return _blendFunc; }
    /**
     * @code
     * When this function bound into js or lua,the parameter will be changed
     * In js: var setBlendFunc(var src, var dst)
     * @endcode
     * @lua NA
     */
    void setBlendFunc(const BlendFunc &blendFunc) override { _blendFunc = blendFunc; }
    
    virtual void updateTransform(void) override { Node::updateTransform(); }


    void updateVertexBuffer(EventCustom* event)
    {

        BeamBodyList::const_iterator last = _beambodies.end();
        
        if ( !_wireFrame )
        {
            updateVertexBufferImpl();
        }
        else
        {
            //    compute buffer size
            _wireFrameBufferCount = 0;
            _indexCount = 0;
            for ( BeamBodyList::const_iterator iter = _beambodies.begin(); iter != last; ++iter )
            {
                unsigned int numActiveTriangs = (*iter)->getNumActiveSegments();
                _wireFrameBufferCount += numActiveTriangs;
                _indexCount += numActiveTriangs;
            }
            
            _wireFrameBufferCount *= 2;
            _indexCount *= 2;
            
            ensureWireFrameBufferCapacity(_wireFrameBufferCount, _indexCount);
            
            if(_wireFrameBufferCount)
            {
                V2F_C4F* lines = _wireFrameBuffer->lockForWrite();
                
                for ( BeamBodyList::const_iterator iter = _beambodies.begin(); iter != last; ++iter )
                {
                    (*iter)->fillDisplayWireFrameBuffer( lines );
                    
                    lines += 2 * (*iter)->getNumActiveSegments();
                }
                
                _dirty = true;
            }
            
        }
        
        CC_INCREMENT_GL_VERTICES(_bufferCount);
    }



    // Overrides
    virtual void draw(Renderer *renderer, const Mat4 &transform, uint32_t flags) override
    {

        if(_bufferCount || _wireFrameBufferCount )
        {
            _customCommand.init(_globalZOrder);
    
            if ( !_wireFrame )
            {
                _customCommand.func = CC_CALLBACK_0(BeamBodyDrawNodeT<T>::onDraw, this, transform, flags);
            }
            else
            {
                _customCommand.func = CC_CALLBACK_0(BeamBodyDrawNodeT<T>::onDrawWireFrame, this, transform, flags);
            }
    
            renderer->addCommand(&_customCommand);
        }

    }



    void onDraw(const Mat4 &transform, uint32_t flags)
    {
        CCASSERT( false, "FATAL: BeamBodyDrawNodeT has no specialization for function void draw(Renderer *renderer, const Mat4 &transform, uint32_t flags)!");
        
        onDrawImpl(transform, flags);
    }


    
    void onDrawWireFrame(const Mat4 &transform, uint32_t flags)
    {
        auto glProgram = getGLProgram();
        glProgram->use();
        glProgram->setUniformsForBuiltins(_modelViewTransform);
        //    _glProgramState->apply(transform);
        
        // Set material
        //    GL::bindTexture2D( _texture->getName() );
        GL::blendFunc(_blendFunc.src, _blendFunc.dst);
        
        
//        if (_dirty)
        {
            const V2F_C4F* databuf = _wireFrameBuffer->lockForRead();
            glBindBuffer(GL_ARRAY_BUFFER, _wf_vbo);
            glBufferData(GL_ARRAY_BUFFER, sizeof(V2F_C4F)*_wireFrameBufferCapacity, databuf, GL_DYNAMIC_DRAW);
            
//            _dirty = false;
        }
        if (Configuration::getInstance()->supportsShareableVAO())
        {
            GL::bindVAO(_wf_vao);
        }
        else
        {
            GL::enableVertexAttribs(GL::VERTEX_ATTRIB_FLAG_POSITION | GL::VERTEX_ATTRIB_FLAG_COLOR );
            
//            glBindBuffer(GL_ARRAY_BUFFER, _wf_vbo);
            // vertex
            glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_POSITION, 2, GL_FLOAT, GL_FALSE, sizeof(V2F_C4F), (GLvoid *)offsetof(V2F_C4F, vertices));
            // color
            glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_COLOR, 4, GL_FLOAT, GL_TRUE, sizeof(V2F_C4F), (GLvoid *)offsetof(V2F_C4F, colors));
            
        }
        
        glLineWidth( _beamThickness );
  
//        glEnable(GL_LINE_SMOOTH);
//        glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
        
        glDrawArrays(GL_LINES, 0, _wireFrameBufferCount);
        
        
        
        CC_INCREMENT_GL_DRAWS(1);
        CHECK_GL_ERROR_DEBUG();
        
        if (Configuration::getInstance()->supportsShareableVAO())
        {
            //Unbind VAO
            GL::bindVAO(0);
        }
//        else
        {
            glBindBuffer(GL_ARRAY_BUFFER, 0);
        }

    }


    void setWireFrame(bool wireFrame)
    {
        _wireFrame = wireFrame;
    
        if (!_wireFrame)
        {
            setGLProgramState(GLProgramState::getOrCreateWithGLProgram(_glShaderProgram));
        }
        else
        {
            setGLProgramState(GLProgramState::getOrCreateWithGLProgramName(GLProgram::SHADER_NAME_POSITION_COLOR));
        }
    }


CC_CONSTRUCTOR_ACCESS:
    
    BeamBodyDrawNodeT()
    : _texture(nullptr)
    , _simLoopCompletionListener(nullptr)
    , _vao(0)
    , _vbo(0)
    , _bufferCount(0)
    , _bufferCapacity(0)
    , _wireFrameBufferCount(0)
    , _wireFrameBufferCapacity(0)
    , _dirty(false)
    , _glShaderProgram(nullptr)
    , _blendFunc(BlendFunc::DISABLE)
    , _displayColor(1.0f, 1.0f, 1.0f, 1.0f)
    , _beamThickness(4.0)
    , _materialID(0)
    , _wireFrame(false)
    {
    
    }

    virtual ~BeamBodyDrawNodeT()
    {
        glDeleteBuffers(1, &_vbo);
        _vbo = 0;
        
        if (Configuration::getInstance()->supportsShareableVAO())
        {
            GL::bindVAO(0);
            glDeleteVertexArrays(1, &_vao);
            _vao = 0;
        }
        
        CC_SAFE_RELEASE(_texture);
        
        Director::getInstance()->getEventDispatcher()->removeEventListener(_simLoopCompletionListener);
        
        _beambodies.clear();
    }

    
protected:
    
    /* Initializes an empty sprite with nothing init. */
    virtual bool init(void) override
    {
        return initWithTexture(nullptr);
    }
    
    /**
     * Initializes a sprite with a texture.
     *
     * After initialization, the rect used will be the size of the texture, and the offset will be (0,0).
     *
     * @param   texture    A pointer to an existing Texture2D object.
     *                      You can use a Texture2D object for many sprites.
     * @return  true if the sprite is initialized properly, false otherwise.
     */
    virtual bool initWithTexture(Texture2D *texture)
    {
        CCASSERT(texture != nullptr, "Invalid texture for DeformableSprite");
        
        Rect rect = Rect::ZERO;
        rect.size = texture->getContentSize();
        
        return initWithTexture(texture, rect);
    }


/**
 * Initializes a sprite with a texture and a rect.
 *
 * After initialization, the offset will be (0,0).
 *
 * @param   texture    A pointer to an exisiting Texture2D object.
 *                      You can use a Texture2D object for many sprites.
 * @param   rect        Only the contents inside rect of this texture will be applied for this sprite.
 * @return  true if the sprite is initialized properly, false otherwise.
 */
    bool initWithTexture(Texture2D *texture, const Rect& rect)
    {        
        bool result;
        
        if (Node::init())
        {
            
            _blendFunc = BlendFunc::ALPHA_PREMULTIPLIED;
            
            // texture parameters
            Texture2D::TexParams    texParams;
            texParams.minFilter = GL_LINEAR;
            texParams.magFilter = GL_LINEAR;
            texParams.wrapS = GL_REPEAT;
            texParams.wrapT = GL_REPEAT;
            texture->setTexParameters(texParams);
            
            
            //        init wire frame buffers
            unsigned int vertexCount = PageMemoryPool::MEMORY_PAGE_SIZE / sizeof(V2F_C4F);
            unsigned int indexCount = PageMemoryPool::MEMORY_PAGE_SIZE / sizeof(unsigned short);
            
            ensureWireFrameBufferCapacity(vertexCount, indexCount);
            
            const V2F_C4F* dataWireFramebuf = _wireFrameBuffer->lockForWrite();
            
            if (Configuration::getInstance()->supportsShareableVAO())
            {
                glGenVertexArrays(1, &_wf_vao);
                GL::bindVAO(_wf_vao);
            }
            
            glGenBuffers(1, &_wf_vbo);
            glBindBuffer(GL_ARRAY_BUFFER, _wf_vbo);
            glBufferData(GL_ARRAY_BUFFER, sizeof(V2F_C4F)* _bufferCapacity, dataWireFramebuf, GL_DYNAMIC_DRAW);
            
            // vertex
            glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_POSITION);
            glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_POSITION, 2, GL_FLOAT, GL_FALSE, sizeof(V2F_C4F), (GLvoid *)offsetof(V2F_C4F, vertices));
            // color
            glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_COLOR);
            glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_COLOR, 4, GL_FLOAT, GL_TRUE, sizeof(V2F_C4F), (GLvoid *)offsetof(V2F_C4F, colors));
            // texcood
            //        glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_TEX_COORD);
            //        glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_TEX_COORD, 2, GL_FLOAT, GL_FALSE, sizeof(V2F_C4F), (GLvoid *)offsetof(V2F_T2F, texCoords));
            
            // update texture (calls updateBlendFunc)
            //        setTexture(texture);
            
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            if (Configuration::getInstance()->supportsShareableVAO())
            {
                GL::bindVAO(0);
            }
            
            CHECK_GL_ERROR_DEBUG();
            
            _wireFrameBuffer->unlock();
            
            
            
            
            //#if CC_ENABLE_CACHE_TEXTURE_DATA
            //        // Need to listen the event only when not use batchnode, because it will use VBO
            //        auto listener = EventListenerCustom::create(EVENT_RENDERER_RECREATED, [this](EventCustom* event){
            //            /** listen the event that renderer was recreated on Android/WP8 */
            //            this->init();
            //        });
            //
            //        _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
            //#endif
            
            
            //        register to simulation loop completion event listener
            _simLoopCompletionListener = EventListenerCustom::create( PhysicsScene::_EVENT_SIM_LOOP_COMPLETION,                                                                   CC_CALLBACK_1(BeamBodyDrawNodeT<T>::updateVertexBuffer, this ) );
            
            _eventDispatcher->addEventListenerWithSceneGraphPriority(_simLoopCompletionListener, this);
            
            
            result = initWithTextureImpl(texture, rect);
            
            if ( getGLProgramState() == nullptr)
            {
                setGLProgramState(GLProgramState::getOrCreateWithGLProgramName(GLProgram::SHADER_NAME_POSITION_TEXTURE));
                setGLProgramState(GLProgramState::getOrCreateWithGLProgramName(GLProgram::SHADER_NAME_POSITION_TEXTURE_COLOR));
            }
        }
        else
        {
            result = false;
        }
        
        return result;
    }




    bool initWithFile(const std::string& filename)
    {
        CCASSERT(filename.size()>0, "Invalid filename for DeformableSprite");
        
        Texture2D *texture = Director::getInstance()->getTextureCache()->addImage(filename);
        if (texture)
        {
//            Rect rect = Rect::ZERO;
//            rect.size = texture->getContentSize();
            return initWithTexture(texture);
        }
        
        // don't release here.
        // when load texture failed, it's better to get a "transparent" DeformableSprite then a crashed program
        // this->release();
        return false;
    }



    void ensureBufferCapacity(const unsigned int vertexCount, const unsigned int indexCount)
    {
        CCASSERT(vertexCount>=0, "BeamBodyDrawNode: capacity must be >= 0");
        
        if( vertexCount > _bufferCapacity)
        {
            _bufferCapacity = PageMemoryPool::getMaxPoolCapacity( vertexCount, sizeof(T));
            _buffer = PoolBuffer<T>::create(_bufferCapacity);
        }
        if ( indexCount > _indicesCapacity )
        {
            _indicesCapacity = PageMemoryPool::getMaxPoolCapacity(indexCount, sizeof(unsigned short));
            _indices = PoolBuffer<unsigned short>::create(_indicesCapacity );
            
        }
    }

    void ensureWireFrameBufferCapacity(const unsigned int vertexCount, const unsigned int indexCount)
    {
        CCASSERT(vertexCount>=0, "BeamBodyDrawNode: capacity must be >= 0");
        
        if( vertexCount > _wireFrameBufferCapacity)
        {
            _wireFrameBufferCapacity = PageMemoryPool::getMaxPoolCapacity( vertexCount, sizeof(V2F_C4F));
            _wireFrameBuffer = PoolBuffer<V2F_C4F>::create(_wireFrameBufferCapacity);
        }
    }

    
//    void genMaterialID()
//    {
//        if(_glProgramState->getUniformCount() > 0)
//        {
//            _materialID = Renderer::MATERIAL_ID_DO_NOT_BATCH;
//        }
//        else
//        {
//            int glProgram = (int)_glProgramState->getGLProgram()->getProgram();
//            int intArray[4] = { glProgram, (int)_textureID, (int)_blendFunc.src, (int)_blendFunc.dst};
//            
//            _materialID = XXH32((const void*)intArray, sizeof(intArray), 0);
//        }
//    }

    std::string _textureFileName;
    
    EventListenerCustom* _simLoopCompletionListener;
    
    
    typedef std::set<PhysicsBeamBody*>::iterator BeamBodyListIterator;
    typedef std::set<PhysicsBeamBody*> BeamBodyList;
    BeamBodyList _beambodies;
    
    PoolBufferSharedPtr<T> _buffer;
    PoolBufferSharedPtr<V2F_C4F> _wireFrameBuffer;
    
    CustomCommand _customCommand;
    
    GLuint      _vao;
    GLuint      _vbo;
    
    // wireframe buffers
    GLuint      _wf_vao;
    GLuint      _wf_vbo;
    
    int         _bufferCapacity;
    GLsizei     _bufferCount;
    
    int         _wireFrameBufferCapacity;
    GLsizei     _wireFrameBufferCount;
    
    Texture2D*  _texture;              /// Texture2D object that is used to render
    BlendFunc   _blendFunc;
    
    uint32_t _materialID;
    GLuint _textureID;
    
    Vec4 _displayColor; // in order to support tint and fade in fade out

    float _beamThickness;

    bool        _dirty;
    
    GLProgram* _glShaderProgram;

    TrianglesCommand _cmd;
    
    PoolBufferSharedPtr<unsigned short> _indices;
    GLsizei _indexCount;
    unsigned int _indicesCapacity;
    
    bool _wireFrame;
    
private:
    CC_DISALLOW_COPY_AND_ASSIGN(BeamBodyDrawNodeT);
};


NS_CC_END

//#endif // CC_ENABLE_CHIPMUNK_INTEGRATION || CC_ENABLE_BOX2D_INTEGRATION


#endif /* defined(__cocos2d_libs__CCBeamBodyDrawNodeT__) */
