//
//  CCScrollingSprite.cpp
//  cocos2d_libs
//
//  Created by Federico Spadoni on 20/07/16.
//
//

#include "CCScrollingSprite.h"

#include "base/ccMacros.h"
#include "base/CCDirector.h"

NS_CC_BEGIN


ScrollingSprite* ScrollingSprite::createWithTexture(Texture2D *texture)
{
    ScrollingSprite *sprite = new (std::nothrow) ScrollingSprite();
    if (sprite && sprite->Sprite::initWithTexture(texture))
    {
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return nullptr;
}

ScrollingSprite* ScrollingSprite::createWithTexture(Texture2D *texture, const Rect& rect, bool rotated)
{
    ScrollingSprite *sprite = new (std::nothrow) ScrollingSprite();
    if (sprite && sprite->initWithTexture(texture, rect, rotated))
    {
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return nullptr;
}

ScrollingSprite* ScrollingSprite::create(const std::string& filename)
{
    ScrollingSprite *sprite = new (std::nothrow) ScrollingSprite();
    if (sprite && sprite->initWithFile(filename))
    {
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return nullptr;
}

ScrollingSprite* ScrollingSprite::create(const PolygonInfo& info)
{
    ScrollingSprite *sprite = new (std::nothrow) ScrollingSprite();
    if(sprite && sprite->initWithPolygon(info))
    {
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return nullptr;
}

ScrollingSprite* ScrollingSprite::create(const std::string& filename, const Rect& rect)
{
    ScrollingSprite *sprite = new (std::nothrow) ScrollingSprite();
    if (sprite && sprite->initWithFile(filename, rect))
    {
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return nullptr;
}


ScrollingSprite* ScrollingSprite::create()
{
    ScrollingSprite *sprite = new (std::nothrow) ScrollingSprite();
    if (sprite && sprite->init())
    {
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return nullptr;
}


// designated initializer
bool ScrollingSprite::initWithTexture(Texture2D *texture, const Rect& rect, bool rotated)
{
    
    // texture parameters
    Texture2D::TexParams    texParams;
    texParams.minFilter = GL_LINEAR;
    texParams.magFilter = GL_LINEAR;
    texParams.wrapS = GL_MIRRORED_REPEAT;
    texParams.wrapT = GL_MIRRORED_REPEAT;
    
    texture->setTexParameters(texParams);
    
    bool ret = Sprite::initWithTexture(texture, rect, rotated);
    
    if ( ret )
    {
        
        Size visibleSize = Director::getInstance()->getWinSize();
        Vec2 visibleSizVect(visibleSize.width, visibleSize.height );
        //Director::getInstance()->getScale();
        
        Vec2 atlasSize( (float)_texture->getPixelsWide(), (float)_texture->getPixelsHigh() );
//        Vec2 atlasSize( (float)_texture->getContentSize().width, (float)_texture->getContentSize().height );
        
        _topleft.x = -visibleSizVect.x/atlasSize.x;
        _topleft.y = -visibleSizVect.y/atlasSize.y;
        _topright.x = visibleSizVect.x/atlasSize.x;
        _topright.y = -visibleSizVect.y/atlasSize.y;
        _bottomleft.x = -visibleSizVect.x/atlasSize.x;
        _bottomleft.y = visibleSizVect.y/atlasSize.y;
        _bottomright.x = visibleSizVect.x/atlasSize.x;
        _bottomright.y = visibleSizVect.y/atlasSize.y;
        
        
        setScale( visibleSize.width/_texture->getContentSize().width,
                 visibleSize.height/_texture->getContentSize().height );
    }
    
    return ret;
}


ScrollingSprite::ScrollingSprite()
: Sprite()
, _scrollingPosition(Vec2::ZERO)//Vec2(Director::getInstance()->getVisibleSize().width,Director::getInstance()->getVisibleSize().height))
, _scrollingPositionOffset(Vec2(0.5,0.5))
, _scrollingLinearVelocity(Vec2(0.0,-20.0))
, _scrollingOrientation(0.0)
, _scrollingAngularVelocity(10.0)
{

}

ScrollingSprite::~ScrollingSprite()
{
    
}


void ScrollingSprite::visit(Renderer *renderer, const Mat4 &parentTransform, uint32_t parentFlags)
{
    //    Vec2 pos = position_;
    //    Vec2    pos = [self convertToWorldSpace:Vec2::ZERO];
//    Vec2 pos = this->absolutePosition();
//    if( ! pos.equals(_lastPosition) )
//    {
//        for( int i=0; i < _parallaxArray->num; i++ )
//        {
//            PointObject *point = (PointObject*)_parallaxArray->arr[i];
//            float x = -pos.x + pos.x * point->getRatio().x + point->getOffset().x;
//            float y = -pos.y + pos.y * point->getRatio().y + point->getOffset().y;
//            point->getChild()->setPosition(x,y);
//        }
//        _lastPosition = pos;
//    }
    
    if (! _texture)
    {
        return;
    }
    
    _scrollingPosition += _scrollingLinearVelocity * Director::getInstance()->getAnimationInterval();
    _scrollingOrientation += _scrollingAngularVelocity * Director::getInstance()->getAnimationInterval();
    
    float a = CC_DEGREES_TO_RADIANS( _scrollingOrientation );
    Vec2 row1( cosf( a ), sinf( a ));
    Vec2 row2( -sinf( a ), cosf( a ));
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 visibleSizVect(visibleSize.width, visibleSize.height);
//    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
//    Rect rect = CC_RECT_POINTS_TO_PIXELS(_rect);
    
    Vec2 atlasHalfSize( (float)_texture->getPixelsWide(), (float)_texture->getPixelsHigh() );
    
    
    float left, right, top, bottom;
    left  = _scrollingPositionOffset.x + (_scrollingPosition.x - visibleSize.width)/atlasHalfSize.x;
    right = _scrollingPositionOffset.x + (_scrollingPosition.x + visibleSize.width)/atlasHalfSize.x;
    top   = _scrollingPositionOffset.y + (_scrollingPosition.y - visibleSize.height)/atlasHalfSize.y;
    bottom = _scrollingPositionOffset.y + (_scrollingPosition.y + visibleSize.height)/atlasHalfSize.y;

    Vec2 topleft, topright, bottomleft, bottomright;

    Vec2 offset( _scrollingPosition.x/atlasHalfSize.x, _scrollingPosition.y/atlasHalfSize.y);
    offset += _scrollingPositionOffset;
    topleft.x  = offset.x + Vec2::dot(row1,_topleft);
    topleft.y  = offset.y + Vec2::dot(row2,_topleft);
    topright.x  = offset.x + Vec2::dot(row1,_topright);
    topright.y  = offset.y + Vec2::dot(row2,_topright);
    bottomleft.x = offset.x + Vec2::dot(row1,_bottomleft);
    bottomleft.y = offset.y + Vec2::dot(row2,_bottomleft);
    bottomright.x = offset.x + Vec2::dot(row1,_bottomright);
    bottomright.y = offset.y + Vec2::dot(row2,_bottomright);
    
//    topleft.x  = _scrollingPositionOffset.x + (_scrollingPosition.x - Vec2::dot(row1,visibleSizVect))/atlasHalfSize.x;
//    topleft.y  = _scrollingPositionOffset.y + (_scrollingPosition.y - Vec2::dot(row2,visibleSizVect))/atlasHalfSize.y;
//    topright.x  = _scrollingPositionOffset.x + (_scrollingPosition.x + Vec2::dot(row1,visibleSizVect))/atlasHalfSize.x;
//    topright.y  = _scrollingPositionOffset.y + (_scrollingPosition.y - Vec2::dot(row2,visibleSizVect))/atlasHalfSize.y;
//    bottomleft.x = _scrollingPositionOffset.x + (_scrollingPosition.x - Vec2::dot(row1,visibleSizVect))/atlasHalfSize.x;
//    bottomleft.y = _scrollingPositionOffset.y + (_scrollingPosition.y + Vec2::dot(row2,visibleSizVect))/atlasHalfSize.y;
//    bottomright.x = _scrollingPositionOffset.x + (_scrollingPosition.x + Vec2::dot(row1,visibleSizVect))/atlasHalfSize.x;
//    bottomright.y = _scrollingPositionOffset.y + (_scrollingPosition.y + Vec2::dot(row2,visibleSizVect))/atlasHalfSize.y;

    
    if(_flippedX)
    {
        std::swap(left, right);
    }
    
    if(_flippedY)
    {
        std::swap(top, bottom);
    }
    
    _quad.bl.texCoords.u = left;
    _quad.bl.texCoords.v = bottom;
    _quad.br.texCoords.u = right;
    _quad.br.texCoords.v = bottom;
    _quad.tl.texCoords.u = left;
    _quad.tl.texCoords.v = top;
    _quad.tr.texCoords.u = right;
    _quad.tr.texCoords.v = top;
    
    _quad.bl.texCoords.u = bottomleft.x;
    _quad.bl.texCoords.v = bottomleft.y;
    _quad.br.texCoords.u = bottomright.x;
    _quad.br.texCoords.v = bottomright.y;
    _quad.tl.texCoords.u = topleft.x;
    _quad.tl.texCoords.v = topleft.y;
    _quad.tr.texCoords.u = topright.x;
    _quad.tr.texCoords.v = topright.y;
    
    Node::visit(renderer, parentTransform, parentFlags);
}


NS_CC_END
