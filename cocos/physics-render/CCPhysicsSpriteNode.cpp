//
//  CCPhysicsSpriteNode.cpp
//  cocos2d_libs
//
//  Created by Federico Spadoni on 20/02/16.
//
//

#include "CCPhysicsSpriteNode.h"

#include "base/CCEventListenerCustom.h"
#include "base/CCEventDispatcher.h"
#include "base/CCDirector.h"

#include "physics/CCPhysicsRigidBody.h"
#include "physics/CCPhysicsScene.h"

NS_CC_BEGIN


PhysicsSpriteNode * PhysicsSpriteNode::create()
{
    PhysicsSpriteNode * ret = new (std::nothrow) PhysicsSpriteNode();
    if (ret && ret->init())
    {
        ret->autorelease();
    }
    else
    {
        CC_SAFE_DELETE(ret);
    }
    return ret;
}


PhysicsSpriteNode::PhysicsSpriteNode()
: Node()
, _simLoopCompletionListener(nullptr)
{
    
}


PhysicsSpriteNode::~PhysicsSpriteNode()
{
    
    Director::getInstance()->getEventDispatcher()->removeEventListener(_simLoopCompletionListener);
    
}



bool PhysicsSpriteNode::init()
{
 
//        register to simulation loop completion event listener
    _simLoopCompletionListener = EventListenerCustom::create( PhysicsScene::_EVENT_SIM_LOOP_COMPLETION,                                                                   CC_CALLBACK_1(PhysicsSpriteNode::updateChildPositions, this ) );
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(_simLoopCompletionListener, this);
    
    return true;
}


void PhysicsSpriteNode::addChildAndRigidBody(Node * child, PhysicsRigidBody* body)
{
    if (body != nullptr)
    {
        addToNodeBodies(child,body);
        Node::addChild(child);
    }
}


void PhysicsSpriteNode::addChildAndRigidBody(Node * child, PhysicsRigidBody* body, int localZOrder)
{
    if (body != nullptr)
    {
        addToNodeBodies(child,body);
        Node::addChild(child, localZOrder);
    }
}


void PhysicsSpriteNode::addChildAndRigidBody(Node* child, PhysicsRigidBody* body, int localZOrder, int tag)
{
    if (body != nullptr)
    {
        addToNodeBodies(child,body);
        Node::addChild(child, localZOrder, tag);
    }
}


void PhysicsSpriteNode::addChildAndRigidBody(Node* child, PhysicsRigidBody* body, int localZOrder, const std::string &name)
{
    if (body != nullptr)
    {
        addToNodeBodies(child,body);
        Node::addChild(child, localZOrder, name);
    }
}

void PhysicsSpriteNode::addToNodeBodies(Node * child, PhysicsRigidBody* body)
{
    if (body->isDynamic() )
    {
        _nodeBodies[child] = body;
    }
    else
    {
        _nodeStaticBodies[child] = body;
    }
}


void PhysicsSpriteNode::onEnter()
{
    
    for (auto&& nodebody : _nodeBodies)
    {
        nodebody.first->setPosition( nodebody.second->getPosition() );
        nodebody.first->setRotation( nodebody.second->getRotation() );
    }
    
    for (auto&& nodebody : _nodeStaticBodies)
    {
        nodebody.first->setPosition(nodebody.second->getPosition());
        nodebody.first->setRotation( nodebody.second->getRotation() );
    }
    
    Node::onEnter();
}


void PhysicsSpriteNode::updateChildPositions(EventCustom* event)
{
    
    for (auto&& nodebody : _nodeBodies)
    {
        nodebody.first->setPosition(nodebody.second->getPosition());
        nodebody.first->setRotation( nodebody.second->getRotation() );
    }
    
    
}



NS_CC_END