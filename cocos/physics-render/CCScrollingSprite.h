//
//  CCScrollingSprite.h
//  cocos2d_libs
//
//  Created by Federico Spadoni on 20/07/16.
//
//

#ifndef __cocos2d_libs__CCScrollingSprite__
#define __cocos2d_libs__CCScrollingSprite__

#include "2d/CCSprite.h"


NS_CC_BEGIN


class CC_DLL ScrollingSprite : public Sprite
{
    
public:

    /**
     * Creates an empty sprite without texture. You can call setTexture method subsequently.
     *
     * @memberof Sprite
     * @return An autoreleased sprite object.
     */
    static ScrollingSprite* create();
    
    /**
     * Creates a sprite with an image filename.
     *
     * After creation, the rect of sprite will be the size of the image,
     * and the offset will be (0,0).
     *
     * @param   filename A path to image file, e.g., "scene1/monster.png".
     * @return  An autoreleased sprite object.
     */
    static ScrollingSprite* create(const std::string& filename);
    
    /**
     * Creates a polygon sprite with a polygon info.
     *
     * After creation, the rect of sprite will be the size of the image,
     * and the offset will be (0,0).
     *
     * @param   polygonInfo A path to image file, e.g., "scene1/monster.png".
     * @return  An autoreleased sprite object.
     */
    static ScrollingSprite* create(const PolygonInfo& info);
    
    /**
     * Creates a sprite with an image filename and a rect.
     *
     * @param   filename A path to image file, e.g., "scene1/monster.png".
     * @param   rect     A subrect of the image file.
     * @return  An autoreleased sprite object.
     */
    static ScrollingSprite* create(const std::string& filename, const Rect& rect);
    
    /**
     * Creates a sprite with a Texture2D object.
     *
     * After creation, the rect will be the size of the texture, and the offset will be (0,0).
     *
     * @param   texture    A pointer to a Texture2D object.
     * @return  An autoreleased sprite object.
     */
    static ScrollingSprite* createWithTexture(Texture2D *texture);
    
    /**
     * Creates a sprite with a texture and a rect.
     *
     * After creation, the offset will be (0,0).
     *
     * @param   texture     A pointer to an existing Texture2D object.
     *                      You can use a Texture2D object for many sprites.
     * @param   rect        Only the contents inside the rect of this texture will be applied for this sprite.
     * @param   rotated     Whether or not the rect is rotated.
     * @return  An autoreleased sprite object.
     */
    static ScrollingSprite* createWithTexture(Texture2D *texture, const Rect& rect, bool rotated=false);
    
    
    /**
     * Initializes a sprite with a texture and a rect in points, optionally rotated.
     *
     * After initialization, the offset will be (0,0).
     * @note    This is the designated initializer.
     *
     * @param   texture    A Texture2D object whose texture will be applied to this sprite.
     * @param   rect        A rectangle assigned the contents of texture.
     * @param   rotated     Whether or not the texture rectangle is rotated.
     * @return  True if the sprite is initialized properly, false otherwise.
     */
    virtual bool initWithTexture(Texture2D *texture, const Rect& rect, bool rotated) override;
    
    
    //
    // Overrides
    //
    virtual void visit(Renderer *renderer, const Mat4 &parentTransform, uint32_t parentFlags) override;
    
    
    
    void setScrollingPosition(const Vec2 pos) { _scrollingPosition = pos; }
    
    void setScrollingLinearVelocity(const Vec2 lvel) { _scrollingLinearVelocity = lvel; }
    
    void setScrollingAngularVelocity(const float avel) { _scrollingAngularVelocity = avel; }
    
    
CC_CONSTRUCTOR_ACCESS:
    
    ScrollingSprite();
    virtual ~ScrollingSprite();
    
private:
    
    
    Vec2 _scrollingPosition;
    Vec2 _scrollingPositionOffset;
    Vec2 _scrollingLinearVelocity;
    
    float _scrollingOrientation;
    float _scrollingAngularVelocity;
    
    Vec2 _topleft;
    Vec2 _topright;
    Vec2 _bottomleft;
    Vec2 _bottomright;
};


NS_CC_END

#endif /* defined(__cocos2d_libs__CCScrollingSprite__) */
