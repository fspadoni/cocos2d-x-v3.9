//
//  CCPhysicsSpriteNode.h
//  cocos2d_libs
//
//  Created by Federico Spadoni on 20/02/16.
//
//

#ifndef __cocos2d_libs__CCPhysicsSpriteNode__
#define __cocos2d_libs__CCPhysicsSpriteNode__

#include "base/ccConfig.h"
#if CC_USE_PHYSICS

#include "2d/CCNode.h"

#include <set>

NS_CC_BEGIN


class PhysicsRigidBody;
class EventListenerCustom;
class EventCustom;

class CC_DLL PhysicsSpriteNode : public Node
{
    
public:
    
    static PhysicsSpriteNode* create();


    virtual void onEnter();
    

    void addChildAndRigidBody(Node * child, PhysicsRigidBody* body);


    void addChildAndRigidBody(Node * child, PhysicsRigidBody* body, int localZOrder);


    void addChildAndRigidBody(Node* child, PhysicsRigidBody* body, int localZOrder, int tag);


    void addChildAndRigidBody(Node* child, PhysicsRigidBody* body, int localZOrder, const std::string &name);



    void updateChildPositions(EventCustom* event);


protected:
    
    void addToNodeBodies(Node * child, PhysicsRigidBody* body);
    
    
CC_CONSTRUCTOR_ACCESS:
//      Nodes should be created using create();
    PhysicsSpriteNode();
    virtual ~PhysicsSpriteNode();

    virtual bool init();

private:
    CC_DISALLOW_COPY_AND_ASSIGN(PhysicsSpriteNode);


    EventListenerCustom* _simLoopCompletionListener;

    typedef std::map<Node*,PhysicsRigidBody*>::iterator NodeBodiesMapIterator;
    typedef std::map<Node*,PhysicsRigidBody*> NodeBodiesMap;

    NodeBodiesMap _nodeBodies;
    
    NodeBodiesMap _nodeStaticBodies;
};



NS_CC_END

#endif // CC_USE_PHYSICS

#endif /* defined(__cocos2d_libs__CCPhysicsSpriteNode__) */
