//
//  RenderPass.h
//  Slingshot
//
//  Created by Federico Spadoni on 11/03/16.
//
//

#ifndef __cocos2d_libs__CCRenderPass__
#define __cocos2d_libs__CCRenderPass__

#include "cocos2d.h"

//#include "physics/CCPhysicsType.h"

NS_CC_BEGIN


class CC_DLL RenderPass : protected cocos2d::Sprite
{
    
public:
    
    static RenderPass* create();
    
	static RenderPass* create(cocos2d::Texture2D* inputTexture);//, const std::string& vertexShaderFile, const std::string& fragmentShaderFile);
    
    
    //static RenderPass* createWithByteArrays(cocos2d::Texture2D* inputTexture, const GLchar* vShaderByteArray, const GLchar* fShaderByteArray);
    
    
    
    //    bool init(cocos2d::Texture2D* inputTexture, const GLchar* vShaderByteArray, const GLchar* fShaderByteArray);
    
    
    //    void draw(cocos2d::Renderer* renderer, const cocos2d::Mat4 &transform, uint32_t flags) override;
    void visit(cocos2d::Renderer* renderer, const cocos2d::Mat4 &parentTransform, uint32_t parentFlags) override;
    
    //    void draw(RenderPass* layer);
    
    void setInputTexture(cocos2d::Texture2D* inputTexture, bool resizeOutputTexture = false );
    
    void setOutputTextureSize(int width, int height);
    
    void setGLShader(const std::string& vertexShaderFile, const std::string& fragmentShaderFile);
    
    void setGLShader(const GLchar* vShaderByteArray, const GLchar* fShaderByteArray);
    
    cocos2d::Texture2D* getOutputTexture() const { return _renderTexture->getSprite()->getTexture(); }
    
    cocos2d::GLProgramState* getGLProgramState() const { return cocos2d::Node::getGLProgramState(); }
    
    
    operator Node*() { return this; }
    
protected:
	RenderPass();
	virtual ~RenderPass();
    
    bool init();
    
    bool init(cocos2d::Texture2D* inputTexture);//, const std::string& vertexShaderFile, const std::string& fragmentShaderFile);
    
    
    
private:
    
    
    
    cocos2d::RenderTexture* _renderTexture;
    //    cocos2d::Texture2D* _inputSprite;
    
    
};



class SpriteRenderPass : protected cocos2d::Sprite
{
    
public:
    static SpriteRenderPass* create(int width, int height, cocos2d::Texture2D::PixelFormat eFormat = cocos2d::Texture2D::PixelFormat::RGBA8888);
    
    static SpriteRenderPass* create(cocos2d::Texture2D* texture);
    
    
    void setGLShader(const std::string& vertexShaderFile, const std::string& fragmentShaderFile);
    
    void setGLShader(const GLchar* vShaderByteArray, const GLchar* fShaderByteArray);
    
    
    virtual void visit(cocos2d::Renderer* renderer, const cocos2d::Mat4 &parentTransform, uint32_t parentFlags) override;
    
    virtual void draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags) override;
    
    void onDraw(const cocos2d::Mat4 &transform, uint32_t flags);
    
    
    void addTexture(cocos2d::Texture2D* texture, const unsigned int num);
    
    //    void setTexture(cocos2d::Texture2D* texture) override { cocos2d::Sprite::setTexture(texture);}
    //
    //
    //    operator Node*() { return this; }
    
    
protected:
	SpriteRenderPass();
	virtual ~SpriteRenderPass();
    
    
    bool init(int width, int height, cocos2d::Texture2D::PixelFormat eFormat);
    
    
    cocos2d::Texture2D* createTexture(int w, int h, cocos2d::Texture2D::PixelFormat format);
    
    void setupBuffers();
    
    void transformQuad(const cocos2d::Mat4 &modelView);
    
    
protected:
    
    
    cocos2d::Map<GLuint, cocos2d::Texture2D*> _inputTextures;
    
    cocos2d::CustomCommand _customCommand;
    
    
    cocos2d::V3F_T2F _quadVerts[4];  // triangs to be rendered
    GLushort            _quadIndices[6];      // indices
    GLuint              _vao;
    GLuint              _vbo[2]; //0: vertex  1: indices
    
};



NS_CC_END

#endif /* defined(__cocos2d_libs__CCRenderPass__) */
