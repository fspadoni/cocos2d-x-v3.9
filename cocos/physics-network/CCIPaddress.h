//
//  CCIPaddress.h
//  cocos2d_libs
//
//  Created by Federico Spadoni on 12/04/16.
//
//

#ifndef __cocos2d_libs__CCIPAaddress__
#define __cocos2d_libs__CCIPAaddress__

#include "base/ccMacros.h"
#include "physics-network/CCSocketPlatform.h"


NS_CC_BEGIN

/**
 * Represents an internet ip address, using the relatively standard SOCKADDR_IN structure. All data is in network byte order
 */

class CC_DLL IPAddress
{
    
public:
	/**
	 * Constructor. Sets address to default state
	 */
	IPAddress();
	/**
	 * Sets the ip address from a host byte order uint32
	 *
	 * @param InAddr the new address to use (must convert to network byte order)
	 */
	void setIp(unsigned int InAddr);
    
	/**
	 * Sets the ip address from a string ("A.B.C.D")
	 *
	 * @param InAddr the string containing the new ip address to use
	 */
	void setIp(const std::string& AddressString, bool& bIsValid);
    
    
	/**
	 * Sets the ip address using a network byte order ip address
	 *
	 * @param IpAddr the new ip address to use
	 */
	void setIp(const in_addr& IpAddr);
    
    
	/**
	 * Copies the network byte order ip address to a host byte order dword
	 *
	 * @param OutAddr the out param receiving the ip address
	 */
	void getIp(unsigned int& OutAddr) const;
    
	/**
	 * Copies the network byte order ip address
	 *
	 * @param OutAddr the out param receiving the ip address
	 */
	void getIp(in_addr& OutAddr) const;
    
	/**
	 * Sets the port number from a host byte order int
	 *
	 * @param InPort the new port to use (must convert to network byte order)
	 */
	void setPort(int InPort);
    
	/**
	 * Copies the port number from this address and places it into a host byte order int
	 *
	 * @param OutPort the host byte order int that receives the port
	 */
	void getPort(int& OutPort) const;
    
	/**
	 * Returns the port number from this address in host byte order
	 */
	int getPort(void) const;
    
	/** Sets the address to be any address */
	void setAnyAddress(void);
    
	/** Sets the address to broadcast */
	void setBroadcastAddress();
    
	/**
	 * Converts this internet ip address to string form
	 *
	 * @param bAppendPort whether to append the port information or not
	 */
    std::string ToString(bool bAppendPort) const;
    
	/**
	 * Compares two internet ip addresses for equality
	 *
	 * @param Other the address to compare against
	 */
	bool operator==(const IPAddress& Other) const;
    
	/**
	 * Is this a well formed internet address
	 *
	 * @return true if a valid IP, false otherwise
	 */
	bool isValid() const
	{
		return _address.sin_addr.s_addr != 0;
	}
    
 	operator sockaddr*(void)
 	{
 		return (sockaddr*)&_address;
 	}
    
 	operator const sockaddr*(void) const
 	{
 		return (const sockaddr*)&_address;
 	}



protected:

/** The internet ip address structure */
    sockaddr_in _address;

};


NS_CC_END


#endif /* defined(__cocos2d_libs__CCIPAaddress__) */
