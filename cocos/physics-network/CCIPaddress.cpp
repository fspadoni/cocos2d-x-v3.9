//
//  CCIPaddress.cpp
//  cocos2d_libs
//
//  Created by Federico Spadoni on 12/04/16.
//
//

#include "CCIPaddress.h"

#include <sstream>      // std::ostringstream


NS_CC_BEGIN


IPAddress::IPAddress()
{
    memset(&_address, 0, sizeof(sockaddr_in));
    _address.sin_family = AF_INET;
}
/**
 * Sets the ip address from a host byte order uint32
 *
 * @param InAddr the new address to use (must convert to network byte order)
 */
void IPAddress::setIp(unsigned int InAddr)
{
    _address.sin_addr.s_addr = htonl(InAddr);
}

/**
 * Sets the ip address from a string ("A.B.C.D")
 *
 * @param InAddr the string containing the new ip address to use
 */
void IPAddress::setIp(const std::string& AddressString, bool& bIsValid)
{
    int A, B, C, D;
    int Port = 0;
    
//Fede:
//    sockaddr_in myAddr;
//    myAddr.sin_family = AF_INET;
//    myAddr.sin_port = htons( 80 );
//    InetPton(AF_INET, "65.254.248.180", &myAddr.sin_addr);
    
    //        std::string AddressString = InAddr;
    
    std::vector<std::string> PortTokens;
    //		AddressString.ParseIntoArray(&PortTokens, TEXT(":"), true);
    
    //        const char portDelim = ':';
    {
        std::size_t found, start = 0;
        found = AddressString.find_first_of(':', start);
        do
        {
            std::string s = AddressString.substr(start,found);
            PortTokens.push_back(s);
            start += found;
            
        } while (found != std::string::npos);
        
    }
    
    
    // look for a port number
    if (PortTokens.size() > 1)
    {
        Port = atoi( PortTokens[1].c_str() );
    }
    
    // now split the part before the : into a.b.c.d
    std::vector<std::string> AddrTokens;
    
    {
        std::size_t found, start = 0;
        found = PortTokens[0].find_first_of('.', start);
        do
        {
            std::string s = PortTokens[0].substr(start,found);
            PortTokens.push_back(s);
            start += found;
            
        } while (found != std::string::npos);
        
    }
    
    
    if (AddrTokens.size() < 4)
    {
        bIsValid = false;
        return;
    }
    
    A = atoi(AddrTokens[0].c_str());
    B = atoi(AddrTokens[1].c_str());
    C = atoi(AddrTokens[2].c_str());
    D = atoi(AddrTokens[3].c_str());
    
    // Make sure the address was valid
    if ((A & 0xFF) == A && (B & 0xFF) == B && (C & 0xFF) == C && (D & 0xFF) == D)
    {
        setIp((A << 24) | (B << 16) | (C << 8) | (D << 0));
        
        if (Port != 0)
        {
            setPort(Port);
        }
        
        bIsValid = true;
    }
    else
    {
        //debugf(TEXT("Invalid IP address string (%s) passed to SetIp"),InAddr);
        bIsValid = false;
    }
}


/**
 * Sets the ip address using a network byte order ip address
 *
 * @param IpAddr the new ip address to use
 */
void IPAddress::setIp(const in_addr& IpAddr)
{
    _address.sin_addr = IpAddr;
}


/**
 * Copies the network byte order ip address to a host byte order dword
 *
 * @param OutAddr the out param receiving the ip address
 */
void IPAddress::getIp(unsigned int& OutAddr) const
{
    OutAddr = ntohl(_address.sin_addr.s_addr);
}

/**
 * Copies the network byte order ip address
 *
 * @param OutAddr the out param receiving the ip address
 */
void IPAddress::getIp(in_addr& OutAddr) const
{
    OutAddr = _address.sin_addr;
}

/**
 * Sets the port number from a host byte order int
 *
 * @param InPort the new port to use (must convert to network byte order)
 */
void IPAddress::setPort(int InPort)
{
    _address.sin_port = htons(InPort);
}

/**
 * Copies the port number from this address and places it into a host byte order int
 *
 * @param OutPort the host byte order int that receives the port
 */
void IPAddress::getPort(int& OutPort) const
{
    OutPort = ntohs(_address.sin_port);
}

/**
 * Returns the port number from this address in host byte order
 */
int IPAddress::getPort(void) const
{
    return ntohs(_address.sin_port);
}

/** Sets the address to be any address */
void IPAddress::setAnyAddress(void)
{
    setIp(INADDR_ANY);
    setPort(0);
}

/** Sets the address to broadcast */
void IPAddress::setBroadcastAddress()
{
    setIp(INADDR_BROADCAST);
    setPort(0);
}

/**
 * Converts this internet ip address to string form
 *
 * @param bAppendPort whether to append the port information or not
 */
std::string IPAddress::ToString(bool bAppendPort) const
{
    unsigned int LocalAddr = ntohl(_address.sin_addr.s_addr);
    
    // Get the individual bytes
    const int A = (LocalAddr >> 24) & 0xFF;
    const int B = (LocalAddr >> 16) & 0xFF;
    const int C = (LocalAddr >>  8) & 0xFF;
    const int D = (LocalAddr >>  0) & 0xFF;
    
    std::ostringstream buffer;
    
    if (bAppendPort)
    {
        buffer << A <<"."<< B <<"."<< C <<"."<< D <<"."<< getPort();
    }
    else
    {
        buffer << A <<"."<< B <<"."<< C <<"."<< D;
    }
    
    return std::string(buffer.str());
}

/**
 * Compares two internet ip addresses for equality
 *
 * @param Other the address to compare against
 */
bool IPAddress::operator==(const IPAddress& Other) const
{
    IPAddress& OtherBSD = (IPAddress&)Other;
    return _address.sin_addr.s_addr == OtherBSD._address.sin_addr.s_addr &&
    _address.sin_port == OtherBSD._address.sin_port &&
    _address.sin_family == OtherBSD._address.sin_family;
}



NS_CC_END
