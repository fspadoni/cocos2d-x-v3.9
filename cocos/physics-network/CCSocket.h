//
//  CCSocket.h
//  cocos2d_libs
//
//  Created by Federico Spadoni on 11/04/16.
//
//

#ifndef __cocos2d_libs__CCSocket__
#define __cocos2d_libs__CCSocket__

#include "base/ccMacros.h"
#include "CCSocketPlatform.h"
#include "CCSocketTypes.h"
#include "CCIPaddress.h"

#include <string>

NS_CC_BEGIN

/**
 * This is our abstract base class that hides the platform specific socket implementation
 */
class CC_DLL Socket
{    
    
protected:
	/** Indicates the type of socket this is */
	const SocketType _socketType;
    
	/** Debug description of socket usage. */
    std::string _socketDescription;
    
public:
	/** Default ctor */
	Socket()
        : _socketType(SOCKTYPE_Unknown)
        , _socketDescription((""))
    {
    }
    
	/**
	 * Specifies the type of socket being created
	 *
	 * @param InSocketType the type of socket being created
	 * @param InSocketDescription the debug description of the socket
	 */
	Socket(enum SocketType InSocketType, const std::string& InSocketDescription)
        : _socketType(InSocketType)
        , _socketDescription(InSocketDescription)
    {
    }
    
    Socket(SOCKET InSocket, SocketType InSocketType, const std::string& InSocketDescription)
        : _socket(InSocket)
        , _socketType(InSocketType)
        , _socketDescription(InSocketDescription)
	{ }

    
	/**
	 * Virtual destructor
	 */
	virtual ~Socket()
	{
        close();
	}
    
	/**
	 * Closes the socket
	 *
	 * @return true if it closes without errors, false otherwise
	 */
	bool close();
    
	/**
	 * Binds a socket to a network byte ordered address
	 *
	 * @param Addr the address to bind to
	 *
	 * @return true if successful, false otherwise
	 */
	bool bind(const IPAddress& Addr);
    
	/**
	 * Connects a socket to a network byte ordered address
	 *
	 * @param Addr the address to connect to
	 *
	 * @return true if successful, false otherwise
	 */
	bool connect(const IPAddress& Addr);
    
	/**
	 * Places the socket into a state to listen for incoming connections
	 *
	 * @param MaxBacklog the number of connections to queue before refusing them
	 *
	 * @return true if successful, false otherwise
	 */
	bool listen(int MaxBacklog);
    
	/**
	 * Queries the socket to determine if there is a pending connection
	 *
	 * @param bHasPendingConnection out parameter indicating whether a connection is pending or not
	 *
	 * @return true if successful, false otherwise
	 */
	bool hasPendingConnection(bool& bHasPendingConnection);
    
	/**
     * Queries the socket to determine if there is pending data on the queue
     *
     * @param PendingDataSize out parameter indicating how much data is on the pipe for a single recv call
     *
     * @return true if the socket has data, false otherwise
     */
	bool hasPendingData(unsigned int& PendingDataSize);
    
	/**
	 * Accepts a connection that is pending
	 *
	 * @param		SocketDescription debug description of socket
	 * @return		The new (heap-allocated) socket, or NULL if unsuccessful.
	 */
	Socket* accept(const std::string& socketDescription);
    
	/**
	 * Accepts a connection that is pending
	 *
	 * @param OutAddr the address of the connection
	 * @param		SocketDescription debug description of socket
	 *
	 * @return		The new (heap-allocated) socket, or NULL if unsuccessful.
	 */
	Socket* accept(IPAddress& OutAddr, const std::string& socketDescription);
    
	/**
	 * Sends a buffer to a network byte ordered address
	 *
	 * @param Data the buffer to send
	 * @param Count the size of the data to send
	 * @param BytesSent out param indicating how much was sent
	 * @param Destination the network byte ordered address to send to
	 */
	bool sendTo(const unsigned char* Data, size_t Count, ssize_t& BytesSent, const IPAddress& Destination);
    
	/**
	 * Sends a buffer on a connected socket
	 *
	 * @param Data the buffer to send
	 * @param Count the size of the data to send
	 * @param BytesSent out param indicating how much was sent
	 */
	bool send(const unsigned char* Data, size_t Count, ssize_t& BytesSent);
    
	/**
	 * Reads a chunk of data from the socket. Gathers the source address too
	 *
	 * @param Data the buffer to read into
	 * @param BufferSize the max size of the buffer
	 * @param BytesRead out param indicating how many bytes were read from the socket
	 * @param Source out param receiving the address of the sender of the data
	 * @param Flags the receive flags
	 */
	bool recvFrom(unsigned char* Data, size_t BufferSize, ssize_t& BytesRead, IPAddress& Source, SocketReceiveFlags::Type Flags = SocketReceiveFlags::None);
    
	/**
	 * Reads a chunk of data from a connected socket
	 *
	 * @param Data the buffer to read into
	 * @param BufferSize the max size of the buffer
	 * @param BytesRead out param indicating how many bytes were read from the socket
	 * @param Flags the receive flags
	 */
	bool recv(unsigned char* Data, size_t BufferSize, ssize_t& BytesRead, SocketReceiveFlags::Type Flags = SocketReceiveFlags::None);
    
	/**
	 * Blocks until the specified condition is met.
	 *
	 * @param Condition - The condition to wait for.
	 * @param WaitTime - The maximum time to wait.
	 *
	 * @return true if the condition was met, false if the time limit expired or an error occurred.
	 */
	bool wait (SocketWaitConditions::Type Condition, unsigned int WaitTime /*millisec*/);
    
	/**
	 * Determines the connection state of the socket
	 */
	SocketConnectionState getConnectionState();
    
	/**
	 * Reads the address the socket is bound to and returns it
	 *
	 * @param OutAddr address the socket is bound to
	 */
	void getAddress(IPAddress& OutAddr);
    
	/**
	 * Sets this socket into non-blocking mode
	 *
	 * @param bIsNonBlocking whether to enable blocking or not
	 *
	 * @return true if successful, false otherwise
	 */
	bool setNonBlocking(bool bIsNonBlocking = true);
    
	/**
	 * Sets a socket into broadcast mode (UDP only)
	 *
	 * @param bAllowBroadcast whether to enable broadcast or not
	 *
	 * @return true if successful, false otherwise
	 */
	bool setBroadcast(bool bAllowBroadcast = true);
    
	/**
	 * Joins this socket to the specified multicast group.
	 *
	 * The multicast group address must be in the range 224.0.0.0 to 239.255.255.255.
	 *
	 * @param GroupAddress - The IP address of the multicast group.
	 *
	 * @return true on success, false otherwise.
	 *
	 * @see LeaveMulticastGroup
	 * @see SetMulticastLoopback
	 * @see SetMulticastTtl
	 */
	bool joinMulticastGroup (const IPAddress& GroupAddress);
    
	/**
	 * Removes this UDP client from the specified multicast group.
	 *
	 * @param The multicast group address to leave.
	 *
	 * @return true on success, false otherwise.
	 *
	 * @see JoinMulticastGroup
	 * @see SetMulticastLoopback
	 * @see SetMulticastTtl
	 */
	bool leaveMulticastGroup (const IPAddress& GroupAddress);
    
	/**
	 * Enables or disables multicast loopback on the socket (UDP only).
	 *
	 * This setting determines whether multicast datagrams are looped
	 * back to the sending socket. By default, multicast loopback is
	 * enabled. It must be enabled if more than one listener is present
	 * on a host.
	 *
	 * @param bLoopback - Whether loopback should be enabled.
	 *
	 * @see LeaveMulticastGroup
	 * @see JoinMulticastGroup
	 * @see SetMulticastTtl
	 */
	bool setMulticastLoopback (bool bLoopback);
    
	/**
	 * Sets the time to live (TTL) for multicast datagrams.
	 *
	 * The default TTL for multicast datagrams is 1, which prevents them
	 * from being forwarded beyond the local subnet. Higher values will
	 * allow multicast datagrams to be sent into neighboring subnets, if
	 * multicast capable routers are present.
	 *
	 * @param TimeToLive - Number of hops the datagram can make.
	 *
	 * @see LeaveMulticastGroup
	 * @see JoinMulticastGroup
	 * @see SetMulticastLoopback
	 */
	bool setMulticastTtl (unsigned char TimeToLive);
    
	/**
	 * Sets whether a socket can be bound to an address in use
	 *
	 * @param bAllowReuse whether to allow reuse or not
	 *
	 * @return true if the call succeeded, false otherwise
	 */
	bool setReuseAddr(bool bAllowReuse = true);
    
	/**
	 * Sets whether and how long a socket will linger after closing
	 *
	 * @param bShouldLinger whether to have the socket remain open for a time period after closing or not
	 * @param Timeout the amount of time to linger before closing
	 *
	 * @return true if the call succeeded, false otherwise
	 */
	bool setLinger(bool bShouldLinger = true, int Timeout = 0);
    
	/**
	 * Enables error queue support for the socket
	 *
	 * @param bUseErrorQueue whether to enable error queuing or not
	 *
	 * @return true if the call succeeded, false otherwise
	 */
	virtual bool setRecvErr(bool bUseErrorQueue = true);
    
	/**
	 * Sets the size of the send buffer to use
	 *
	 * @param Size the size to change it to
	 * @param NewSize the out value returning the size that was set (in case OS can't set that)
	 *
	 * @return true if the call succeeded, false otherwise
	 */
	bool setSendBufferSize(int Size, int& NewSize);
    
	/**
	 * Sets the size of the receive buffer to use
	 *
	 * @param Size the size to change it to
	 * @param NewSize the out value returning the size that was set (in case OS can't set that)
	 *
	 * @return true if the call succeeded, false otherwise
	 */
	bool setReceiveBufferSize(int Size, int& NewSize);
    
	/**
	 * Reads the port this socket is bound to.
	 */
	int getPortNo();
    
	/**
	 * @return The type of protocol the socket is bound to
	 */
	inline enum SocketType getSocketType() const
	{
		return _socketType;
	}
    
	/**
	 * @return The debug description of the socket
	 */
	inline std::string getDescription() const
	{
		return _socketDescription;
	}
    
    /**
	 * Return the Socket for anyone who knows they have an FSocketBSD
	 */
	SOCKET GetNativeSocket()
	{
		return _socket;
	}
    
protected:
    
    /** This is generally select(), but makes it easier for platforms without select to replace it. */
	virtual SocketInternalState::Return hasState(SocketInternalState::Param State, unsigned int WaitTime );
    
    /** Holds the BSD socket object. */
	SOCKET _socket;
    
	/** Last activity time. */
//	FDateTime _lastActivityTime;
//    std::chrono::time_point<std::chrono::system_clock> _lastActivityTime;
    std::tm _lastActivityTime;
    
	/** Pointer to the subsystem that created it. */
//	ISocketSubsystem * _socketSubsystem;
    
};

NS_CC_END


#endif /* defined(__cocos2d_libs__CCSocket__) */
