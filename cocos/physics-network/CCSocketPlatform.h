//
//  CCSocketPlatform.h
//  cocos2d_libs
//
//  Created by Federico Spadoni on 11/04/16.
//
//

#ifndef cocos2d_libs_CCSocketPlatform_h
#define cocos2d_libs_CCSocketPlatform_h


#include "physics-network/CCSocketTypes.h"


#include <fcntl.h>

//#if ( (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT) )
#if defined(_MSC_VER) || defined(__MINGW32__)

//#include "AllowWindowsPlatformTypes.h"
#include <winsock2.h>
#include <ws2tcpip.h>
typedef int32 SOCKLEN;
//#include "HideWindowsPlatformTypes.h"

#else

#include <sys/socket.h>

//#if PLATFORM_HAS_BSD_SOCKET_FEATURE_IOCTL
#if ( (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) || (CC_TARGET_PLATFORM == CC_PLATFORM_LINUX) )
#include <sys/ioctl.h>
#endif

#include <netinet/in.h>
#include <arpa/inet.h>

//#if PLATFORM_HAS_BSD_SOCKET_FEATURE_GETHOSTNAME
#include <netdb.h>
//#endif
#include <unistd.h>

#define ioctlsocket ioctl
#define SOCKET_ERROR -1
#define INVALID_SOCKET -1

typedef socklen_t SOCKLEN;
typedef int SOCKET;
typedef sockaddr_in SOCKADDR_IN;
typedef struct timeval TIMEVAL;

NS_CC_BEGIN

inline int closesocket(int socket)
{
    shutdown(socket, SHUT_RDWR); // gracefully shutdown if connected
    return close(socket);
}

NS_CC_END

#endif


NS_CC_BEGIN

// Since the flag constants may have different values per-platform, translate into corresponding system constants.
// For example, MSG_WAITALL is 0x8 on Windows, but 0x100 on other platforms.
inline int TranslateFlags(SocketReceiveFlags::Type Flags)
{
	int TranslatedFlags = 0;
    
	if (Flags & SocketReceiveFlags::Peek)
	{
		TranslatedFlags |= MSG_PEEK;
	}
    
	if (Flags & SocketReceiveFlags::WaitAll)
	{
		TranslatedFlags |= MSG_WAITALL;
	}
    
	return TranslatedFlags;
}

NS_CC_END


#endif
