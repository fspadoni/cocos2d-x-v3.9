//
//  CCSocketManager.h
//  cocos2d_libs
//
//  Created by Federico Spadoni on 12/04/16.
//
//

#ifndef __cocos2d_libs__CCSocketManager__
#define __cocos2d_libs__CCSocketManager__

#include "base/ccMacros.h"
#include "CCSocketPlatform.h"
#include "CCIPaddress.h"
#include "CCSocketTypes.h"

#include <vector>
#include <map>

NS_CC_BEGIN


class CC_DLL SocketManager
{
public:
    

    static SocketManager* getInstance();
    
    
    
	// Begin ISocketSubsystem interface
    
	IPAddress* createInternetAddr( unsigned int Address = 0, unsigned int Port = 0 );
    
	virtual class Socket* createSocket( const SocketType& SocketType, const std::string& socketDescription, bool bForceUDP = false );
    
	virtual void destroySocket( class Socket* Socket );
    
	virtual SocketErrors getHostByName( const std::string& HostName, IPAddress& OutAddr );
    

//	virtual class FResolveInfo* GetHostByName(const ANSICHAR* HostName);
    
    
	bool getHostName( std::string& HostName );
    
    
	virtual SocketErrors getLastErrorCode( );
    
	virtual bool getLocalAdapterAddresses( std::vector<IPAddress*>& OutAdresses )
	{
		bool bCanBindAll;
        
		OutAdresses.push_back( &getLocalHostAddr(bCanBindAll) );
        
		return true;
	}
    
	virtual const std::string getSocketAPIName( ) const;
    
	virtual bool RequiresChatDataBeSeparate( )
	{
		return false;
	}
    
	virtual bool RequiresEncryptedPackets( )
	{
		return false;
	}
    
	virtual SocketErrors TranslateErrorCode( int Code );
    
    
    /**
	 * Returns a human readable string from an error code
	 *
	 * @param Code the error code to check
	 */
    std::string getSocketError(SocketErrors Code = SE_GET_LAST_ERROR_CODE);
    
    
    /**
	 *	Get local IP to bind to
	 */
	IPAddress& getLocalBindAddr();
    
    /**
	 * Bind to next available port.
	 *
	 * @param Socket The socket that that will bind to the port
	 * @param Addr The local address and port that is being bound to (usually the result of GetLocalBindAddr()). This addresses port number will be modified in place
	 * @param PortCount How many ports to try
	 * @param PortIncrement The amount to increase the port number by on each attempt
	 *
	 * @return The bound port number, or 0 on failure
	 */
	int BindNextPort(class Socket* socket, IPAddress& Addr, int PortCount, int PortIncrement);
    
	/**
	 * Uses the platform specific look up to determine the host address
	 *
	 * @param Out the output device to log messages to
	 * @param bCanBindAll true if all can be bound (no primarynet), false otherwise
	 *
	 * @return The local host address
	 */
	IPAddress& getLocalHostAddr( bool& bCanBindAll);
    
	/**
	 * Checks the host name cache for an existing entry (faster than resolving again)
	 *
	 * @param HostName the host name to search for
	 * @param Addr the out param that the IP will be copied to
	 *
	 * @return true if the host was found, false otherwise
	 */
	bool getHostByNameFromCache(const std::string& HostName, IPAddress*& Addr);
    
	/**
	 * Stores the ip address with the matching host name
	 *
	 * @param HostName the host name to search for
	 * @param Addr the IP that will be copied from
	 */
	void addHostNameToCache(const std::string& HostName, IPAddress* Addr);
    
	/**
	 * Removes the host name to ip mapping from the cache
	 *
	 * @param HostName the host name to search for
	 */
	void removeHostNameFromCache(const std::string& HostName);

    
    
protected:
    
    SocketManager() {}
    
    SocketManager(const SocketManager& sm) = delete;
    
    virtual ~SocketManager() {}
    
    
	/**
	 * Allows a subsystem subclass to create a SocketBSD sub class.
	 */
	Socket* InternalSocketFactory( SOCKET socket, SocketType SocketType, const std::string& socketDescription );
    
	// allow BSD sockets to use this when creating new sockets from accept() etc
	friend Socket;
    
private:
    
    static SocketManager* s_singleInstance;
    
	// Used to prevent multiple threads accessing the shared data.
//	FCriticalSection HostByNameSynch;
    
    /** Stores a resolved IP address for a given host name */
    std::map<std::string, IPAddress*> _hostNameCache;
};

NS_CC_END

#endif /* defined(__cocos2d_libs__CCSocketManager__) */
