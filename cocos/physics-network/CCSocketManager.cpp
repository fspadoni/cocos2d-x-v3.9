//
//  CCSocketManager.cpp
//  cocos2d_libs
//
//  Created by Federico Spadoni on 12/04/16.
//
//

#include "CCSocketManager.h"
#include "CCSocket.h"

NS_CC_BEGIN


SocketManager* SocketManager::s_singleInstance = nullptr;

SocketManager* SocketManager::getInstance()
{
    if (s_singleInstance == nullptr)
    {
        s_singleInstance = new (std::nothrow) SocketManager();
    }
    return s_singleInstance;
}


Socket* SocketManager::InternalSocketFactory(SOCKET socket, SocketType SocketType, const std::string& socketDescription)
{
	// return a new socket object
	return new Socket(socket, SocketType, socketDescription);
}

Socket* SocketManager::createSocket(const SocketType& socketType, const std::string& socketDescription, bool bForceUDP)
{
	SOCKET _socket = INVALID_SOCKET;
	Socket* NewSocket = NULL;
	switch (socketType)
	{
        case SOCKTYPE_Datagram:
            // Creates a data gram (UDP) socket
            _socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
            NewSocket = (_socket != INVALID_SOCKET) ? InternalSocketFactory(_socket, SOCKTYPE_Datagram, socketDescription) : NULL;
            break;
        case SOCKTYPE_Streaming:
            // Creates a stream (TCP) socket
            _socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
            NewSocket = (_socket != INVALID_SOCKET) ? InternalSocketFactory(_socket, SOCKTYPE_Streaming, socketDescription) : NULL;
            break;
        default:
            break;
	}
    
	if (!NewSocket)
	{
        CCLOG( "Failed to create socket %d [%s]", socketType, socketDescription.c_str() );
//		UE_LOG(LogSockets, Warning, TEXT("Failed to create socket %s [%s]"), *SocketType.ToString(), *SocketDescription);
	}
    
	return NewSocket;
}

/**
 * Cleans up a socket class
 *
 * @param Socket the socket object to destroy
 */
void SocketManager::destroySocket(Socket* socket)
{
	delete socket;
}


SocketErrors SocketManager::getHostByName(const std::string& HostName, IPAddress& OutAddr)
{

	SocketErrors ErrorCode = SE_NO_ERROR;
	// gethostbyname() touches a static object so lock for thread safety
//	ScopeLock ScopeLock(&HostByNameSynch);
	hostent* HostEnt = gethostbyname(HostName.c_str());
	if (HostEnt != NULL)
	{
		// Make sure it's a valid type
		if (HostEnt->h_addrtype == PF_INET)
		{
			// Copy the data before letting go of the lock. This is safe only
			// for the copy locally. If another thread is reading this while
			// we are copying they will get munged data. This relies on the
			// consumer of this class to call the resolved() accessor before
			// attempting to read this data
			((IPAddress&)OutAddr).setIp(*(in_addr*)(*HostEnt->h_addr_list));
		}
		else
		{
			ErrorCode = SE_HOST_NOT_FOUND;
		}
	}
	else
	{
		ErrorCode = getLastErrorCode();
	}
	return ErrorCode;

}

/**
 * Determines the name of the local machine
 *
 * @param HostName the string that receives the data
 *
 * @return true if successful, false otherwise
 */
bool SocketManager::getHostName(std::string& HostName)
{
//#if PLATFORM_HAS_BSD_SOCKET_FEATURE_GETHOSTNAME
	char Buffer[256];
	bool bRead = gethostname(Buffer,256) == 0;
	if (bRead == true)
	{
		HostName = Buffer;
	}
	return bRead;
//#else
//	UE_LOG(LogSockets, Error, TEXT("Platform has no gethostname(), but did not override SocketSubsystem::GetHostName()"));
//	return false;
//#endif
}


/**
 *	Get the name of the socket subsystem
 * @return a string naming this subsystem
 */
const std::string SocketManager::getSocketAPIName() const
{
	return std::string("Berkley Socket");
}

/**
 *	Create a proper IPAddress representation
 * @param Address host address
 * @param Port host port
 */
IPAddress* SocketManager::createInternetAddr(unsigned int Address, unsigned int Port)
{
//	TSharedRef<IPAddress> Result = MakeShareable(new IPAddress);
    IPAddress* Result = new IPAddress();
	Result->setIp(Address);
	Result->setPort(Port);
	return Result;
}

SocketErrors SocketManager::getLastErrorCode()
{
	return TranslateErrorCode(errno);
}


SocketErrors SocketManager::TranslateErrorCode(int Code)
{
	// @todo sockets: Windows for some reason doesn't seem to have all of the standard error messages,
	// but it overrides this function anyway - however, this
#if !PLATFORM_HAS_BSD_SOCKET_FEATURE_WINSOCKETS
    
	// handle the generic -1 error
	if (Code == SOCKET_ERROR)
	{
		return getLastErrorCode();
	}
    
	switch (Code)
	{
        case 0: return SE_NO_ERROR;
        case EINTR: return SE_EINTR;
        case EBADF: return SE_EBADF;
        case EACCES: return SE_EACCES;
        case EFAULT: return SE_EFAULT;
        case EINVAL: return SE_EINVAL;
        case EMFILE: return SE_EMFILE;
        case EWOULDBLOCK: return SE_EWOULDBLOCK;
        case EINPROGRESS: return SE_EINPROGRESS;
        case EALREADY: return SE_EALREADY;
        case ENOTSOCK: return SE_ENOTSOCK;
        case EDESTADDRREQ: return SE_EDESTADDRREQ;
        case EMSGSIZE: return SE_EMSGSIZE;
        case EPROTOTYPE: return SE_EPROTOTYPE;
        case ENOPROTOOPT: return SE_ENOPROTOOPT;
        case EPROTONOSUPPORT: return SE_EPROTONOSUPPORT;
        case ESOCKTNOSUPPORT: return SE_ESOCKTNOSUPPORT;
        case EOPNOTSUPP: return SE_EOPNOTSUPP;
        case EPFNOSUPPORT: return SE_EPFNOSUPPORT;
        case EAFNOSUPPORT: return SE_EAFNOSUPPORT;
        case EADDRINUSE: return SE_EADDRINUSE;
        case EADDRNOTAVAIL: return SE_EADDRNOTAVAIL;
        case ENETDOWN: return SE_ENETDOWN;
        case ENETUNREACH: return SE_ENETUNREACH;
        case ENETRESET: return SE_ENETRESET;
        case ECONNABORTED: return SE_ECONNABORTED;
        case ECONNRESET: return SE_ECONNRESET;
        case ENOBUFS: return SE_ENOBUFS;
        case EISCONN: return SE_EISCONN;
        case ENOTCONN: return SE_ENOTCONN;
        case ESHUTDOWN: return SE_ESHUTDOWN;
        case ETOOMANYREFS: return SE_ETOOMANYREFS;
        case ETIMEDOUT: return SE_ETIMEDOUT;
        case ECONNREFUSED: return SE_ECONNREFUSED;
        case ELOOP: return SE_ELOOP;
        case ENAMETOOLONG: return SE_ENAMETOOLONG;
        case EHOSTDOWN: return SE_EHOSTDOWN;
        case EHOSTUNREACH: return SE_EHOSTUNREACH;
        case ENOTEMPTY: return SE_ENOTEMPTY;
        case EUSERS: return SE_EUSERS;
        case EDQUOT: return SE_EDQUOT;
        case ESTALE: return SE_ESTALE;
        case EREMOTE: return SE_EREMOTE;
//#if !PLATFORM_HAS_NO_EPROCLIM
//        case EPROCLIM: return SE_EPROCLIM;
//#endif
            // 	case EDISCON: return SE_EDISCON;
            // 	case SYSNOTREADY: return SE_SYSNOTREADY;
            // 	case VERNOTSUPPORTED: return SE_VERNOTSUPPORTED;
            // 	case NOTINITIALISED: return SE_NOTINITIALISED;
#if PLATFORM_HAS_BSD_SOCKET_FEATURE_GETHOSTNAME
        case HOST_NOT_FOUND: return SE_HOST_NOT_FOUND;
        case TRY_AGAIN: return SE_TRY_AGAIN;
        case NO_RECOVERY: return SE_NO_RECOVERY;
#endif
            //	case NO_DATA: return SE_NO_DATA;
            // case : return SE_UDP_ERR_PORT_UNREACH; //@TODO Find it's replacement
	}
#endif
    
    CCLOG("Unhandled socket error! Error Code: %d", Code );

	return SE_NO_ERROR;
}



int SocketManager::BindNextPort(Socket* socket, IPAddress& Addr, int PortCount, int PortIncrement)
{
	// go until we reach the limit (or we succeed)
	for (int Index = 0; Index < PortCount; Index++)
	{
		// try to bind to the current port
		if (socket->bind(Addr) == true)
		{
			// if it succeeded, return the port
			if (Addr.getPort() != 0)
			{
				return Addr.getPort();
			}
			else
			{
				return socket->getPortNo();
			}
		}
		// if the address had no port, we are done
		if( Addr.getPort() == 0 )
		{
			break;
		}
        
		// increment to the next port, and loop!
		Addr.setPort(Addr.getPort() + PortIncrement);
	}
    
	return 0;
}

IPAddress& SocketManager::getLocalBindAddr()
{
	bool bCanBindAll;
	// look up the local host address
	IPAddress& BindAddr = getLocalHostAddr( bCanBindAll);
    
	// If we can bind to all addresses, return 0.0.0.0
	if (bCanBindAll)
	{
		BindAddr.setAnyAddress();
	}
    
	// return it
	return BindAddr;
    
}

//FResolveInfo* SocketManager::getHostByName(const std::string& HostName)
//{
//	return nullptr;
//}

IPAddress& SocketManager::getLocalHostAddr(bool& bCanBindAll)
{
	IPAddress* HostAddr = createInternetAddr();
	HostAddr->setAnyAddress();
    
	bCanBindAll = false;
    
//	char Home[256];//=std::string("");
	std::string HostName;
	if ( getHostName(HostName) == false)
	{
		CCLOG( "SocketManager::getLocalHostAddr() : gethostname failed (%s)", getSocketError().c_str() );
	}

    if (  getHostByName( HostName, *HostAddr) == SE_NO_ERROR)
    {

    }
    else
    {
        CCLOG( "SocketManager::getLocalHostAddr() : gethostname failed (%s)", getSocketError().c_str() );
    }
	
    
	// return the newly created address
	return *HostAddr;
}


bool SocketManager::getHostByNameFromCache(const std::string& HostName, IPAddress*& Addr)
{

	// Now search for the entry
	IPAddress* FoundAddr = _hostNameCache[HostName];
	if (FoundAddr)
	{
		Addr = FoundAddr;
	}
	return FoundAddr != NULL;
}

void SocketManager::addHostNameToCache(const std::string& HostName, IPAddress* Addr)
{
	// Lock for thread safety
//	ScopeLock sl(&HostNameCacheSync);
	_hostNameCache[HostName] = Addr;
}

void SocketManager::removeHostNameFromCache(const std::string& HostName)
{
	// Lock for thread safety
//	ScopeLock sl(&HostNameCacheSync);
    _hostNameCache.erase(HostName);
}


/**
 * Returns a human readable string from an error code
 *
 * @param Code the error code to check
 */
std::string SocketManager::getSocketError(SocketErrors Code)
{
#if (COCOS2D_DEBUG)
	if (Code == SE_GET_LAST_ERROR_CODE)
	{
		Code = getLastErrorCode();
	}
	switch (Code)
	{
		case SE_NO_ERROR: return std::string("SE_NO_ERROR");
		case SE_EINTR: return std::string("SE_EINTR");
		case SE_EBADF: return std::string("SE_EBADF");
		case SE_EACCES: return std::string("SE_EACCES");
		case SE_EFAULT: return std::string("SE_EFAULT");
		case SE_EINVAL: return std::string("SE_EINVAL");
		case SE_EMFILE: return std::string("SE_EMFILE");
		case SE_EWOULDBLOCK: return std::string("SE_EWOULDBLOCK");
		case SE_EINPROGRESS: return std::string("SE_EINPROGRESS");
		case SE_EALREADY: return std::string("SE_EALREADY");
		case SE_ENOTSOCK: return std::string("SE_ENOTSOCK");
		case SE_EDESTADDRREQ: return std::string("SE_EDESTADDRREQ");
		case SE_EMSGSIZE: return std::string("SE_EMSGSIZE");
		case SE_EPROTOTYPE: return std::string("SE_EPROTOTYPE");
		case SE_ENOPROTOOPT: return std::string("SE_ENOPROTOOPT");
		case SE_EPROTONOSUPPORT: return std::string("SE_EPROTONOSUPPORT");
		case SE_ESOCKTNOSUPPORT: return std::string("SE_ESOCKTNOSUPPORT");
		case SE_EOPNOTSUPP: return std::string("SE_EOPNOTSUPP");
		case SE_EPFNOSUPPORT: return std::string("SE_EPFNOSUPPORT");
		case SE_EAFNOSUPPORT: return std::string("SE_EAFNOSUPPORT");
		case SE_EADDRINUSE: return std::string("SE_EADDRINUSE");
		case SE_EADDRNOTAVAIL: return std::string("SE_EADDRNOTAVAIL");
		case SE_ENETDOWN: return std::string("SE_ENETDOWN");
		case SE_ENETUNREACH: return std::string("SE_ENETUNREACH");
		case SE_ENETRESET: return std::string("SE_ENETRESET");
		case SE_ECONNABORTED: return std::string("SE_ECONNABORTED");
		case SE_ECONNRESET: return std::string("SE_ECONNRESET");
		case SE_ENOBUFS: return std::string("SE_ENOBUFS");
		case SE_EISCONN: return std::string("SE_EISCONN");
		case SE_ENOTCONN: return std::string("SE_ENOTCONN");
		case SE_ESHUTDOWN: return std::string("SE_ESHUTDOWN");
		case SE_ETOOMANYREFS: return std::string("SE_ETOOMANYREFS");
		case SE_ETIMEDOUT: return std::string("SE_ETIMEDOUT");
		case SE_ECONNREFUSED: return std::string("SE_ECONNREFUSED");
		case SE_ELOOP: return std::string("SE_ELOOP");
		case SE_ENAMETOOLONG: return std::string("SE_ENAMETOOLONG");
		case SE_EHOSTDOWN: return std::string("SE_EHOSTDOWN");
		case SE_EHOSTUNREACH: return std::string("SE_EHOSTUNREACH");
		case SE_ENOTEMPTY: return std::string("SE_ENOTEMPTY");
		case SE_EPROCLIM: return std::string("SE_EPROCLIM");
		case SE_EUSERS: return std::string("SE_EUSERS");
		case SE_EDQUOT: return std::string("SE_EDQUOT");
		case SE_ESTALE: return std::string("SE_ESTALE");
		case SE_EREMOTE: return std::string("SE_EREMOTE");
		case SE_EDISCON: return std::string("SE_EDISCON");
		case SE_SYSNOTREADY: return std::string("SE_SYSNOTREADY");
		case SE_VERNOTSUPPORTED: return std::string("SE_VERNOTSUPPORTED");
		case SE_NOTINITIALISED: return std::string("SE_NOTINITIALISED");
		case SE_HOST_NOT_FOUND: return std::string("SE_HOST_NOT_FOUND");
		case SE_TRY_AGAIN: return std::string("SE_TRY_AGAIN");
		case SE_NO_RECOVERY: return std::string("SE_NO_RECOVERY");
		case SE_NO_DATA: return std::string("SE_NO_DATA");
		default: return std::string("Unknown Error");
	};
#else
	return std::string("");
#endif
}

NS_CC_END
