//
//  CCSocket.cpp
//  cocos2d_libs
//
//  Created by Federico Spadoni on 11/04/16.
//
//

#include "CCSocket.h"

#include "CCSocketManager.h"



NS_CC_BEGIN


bool Socket::close(void)
{
	if (_socket != INVALID_SOCKET)
	{
		int error = closesocket(_socket);
		_socket = INVALID_SOCKET;
		return error == 0;
	}
	return false;
}


bool Socket::bind(const IPAddress& Addr)
{
	return ::bind(_socket, (const sockaddr*)&Addr, sizeof(sockaddr_in)) == 0;
}


bool Socket::connect(const IPAddress& Addr)
{
	int Return = ::connect(_socket, (const sockaddr*)(IPAddress&)Addr, sizeof(sockaddr_in));
	
	CCASSERT( SocketManager::getInstance() != nullptr, "SocketManager::getInstance() == null_ptr " );
	SocketErrors Error = SocketManager::getInstance()->TranslateErrorCode(Return);
    
	// "would block" is not an error
	return ((Error == SE_NO_ERROR) || (Error == SE_EWOULDBLOCK));
}


bool Socket::listen(int MaxBacklog)
{
	return ::listen(_socket, MaxBacklog) == 0;
}

SocketInternalState::Return Socket::hasState(SocketInternalState::Param State, unsigned int WaitTime = 0)
{
//#if PLATFORM_has_BSD_SOCKET_FEATURE_SELECT
	// convert WaitTime to a timeval
	timeval Time;
//	Time.tv_sec = (int)WaitTime.GetTotalSeconds();
	Time.tv_usec = WaitTime*1000;//WaitTime.GetMilliseconds() * 1000;
    
	fd_set SocketSet;
    
	// Set up the socket sets we are interested in (just this one)
	FD_ZERO(&SocketSet);
	FD_SET(_socket, &SocketSet);
    
	// Check the status of the state
	int SelectStatus = 0;
	switch (State)
	{
		case SocketInternalState::CanRead:
			SelectStatus = select(SelectStatus + 1, &SocketSet, NULL, NULL, &Time);
			break;
		case SocketInternalState::CanWrite:
			SelectStatus = select(SelectStatus + 1, NULL, &SocketSet, NULL, &Time);
			break;
		case SocketInternalState::HasError:
			SelectStatus = select(SelectStatus + 1, NULL, NULL, &SocketSet, &Time);
			break;
	}
    
	// if the select returns a positive number, the socket had the state, 0 means didn't have it, and negative is API error condition (not socket's error state)
	return SelectStatus > 0  ? SocketInternalState::Yes :
    SelectStatus == 0 ? SocketInternalState::No :
    SocketInternalState::EncounteredError;
//#else
//	CCLOG(LogSockets, Fatal, TEXT("This platform doesn't support select(), but Socket::hasState was not overridden"));
//	return SocketInternalState::EncounteredError;
//#endif
}

bool Socket::hasPendingConnection(bool& bhasPendingConnection)
{
	bool bhasSucceeded = false;
	bhasPendingConnection = false;
    
	// make sure socket has no error state
	if (hasState(SocketInternalState::HasError) == SocketInternalState::No)
	{
		// get the read state
		SocketInternalState::Return State = hasState(SocketInternalState::CanRead);
		
		// turn the result into the outputs
		bhasSucceeded = State != SocketInternalState::EncounteredError;
		bhasPendingConnection = State == SocketInternalState::Yes;
	}
    
	return bhasSucceeded;
}


bool Socket::hasPendingData(uint& PendingDataSize)
{
	PendingDataSize = 0;
    
	// make sure socket has no error state
	if (hasState(SocketInternalState::CanRead) == SocketInternalState::Yes)
	{
#if PLATFORM_has_BSD_SOCKET_FEATURE_IOCTL
		// See if there is any pending data on the read socket
		if (ioctlsocket(Socket, FIONREAD, (u_long*)(&PendingDataSize)) == 0)
#endif
		{
			return (PendingDataSize > 0);
		}
	}
    
	return false;
}


Socket* Socket::accept(const std::string& socketDescription)
{
	SOCKET NewSocket = ::accept(_socket,NULL,NULL);
    
	if (NewSocket != INVALID_SOCKET)
	{
		// we need the subclass to create the actual Socket object
        CCASSERT( !SocketManager::getInstance(), "SocketManager::getInstance() == null_ptr " );
        return SocketManager::getInstance()->InternalSocketFactory(NewSocket, _socketType, _socketDescription);
//		check(SocketSubsystem);
//		SocketSubsystemBSD* BSDSystem = static_cast<SocketSubsystemBSD*>(SocketSubsystem);
//		return BSDSystem->InternalBSDSocketFactory(NewSocket, SocketType, SocketDescription);
	}
    
	return NULL;
}


Socket* Socket::accept(IPAddress& OutAddr, const std::string& socketDescription)
{
	SOCKLEN SizeOf = sizeof(sockaddr_in);
	SOCKET NewSocket = ::accept(_socket, (sockaddr*)(IPAddress&)OutAddr, &SizeOf);
    
	if (NewSocket != INVALID_SOCKET)
	{
		// we need the subclass to create the actual Socket object
        CCASSERT( !SocketManager::getInstance(), "SocketManager::getInstance() == null_ptr " );
        return SocketManager::getInstance()->InternalSocketFactory(NewSocket, _socketType, _socketDescription);
//		SocketSubsystemBSD* BSDSystem = static_cast<SocketSubsystemBSD*>(SocketSubsystem);
//		return BSDSystem->InternalBSDSocketFactory(NewSocket, SocketType, SocketDescription);
	}
    
	return NULL;
}


bool Socket::sendTo(const unsigned char* Data, size_t Count, ssize_t& BytesSent, const IPAddress& Destination)
{
	// Write the data and see how much was written
	BytesSent = sendto(_socket, (const char*)Data, Count, 0, (const sockaddr*)(IPAddress&)Destination, sizeof(sockaddr_in));
    
    //	NETWORK_PROFILER(Socket::SendTo(Data,Count,BytesSent,Destination));
    
	bool Result = BytesSent >= 0;
	if (Result)
	{
//		_lastActivityTime = std::chrono::system_clock::now();
        std::time_t t = std::time(NULL);
        _lastActivityTime = *std::gmtime(&t);
	}
	return Result;
}


bool Socket::send(const unsigned char* Data, size_t Count, ssize_t& BytesSent)
{
	BytesSent = ::send( _socket,(const char*)Data,Count,0);
    
    //	NETWORK_PROFILER(Socket::Send(Data,Count,BytesSent));
    
	bool Result = BytesSent >= 0;
	if (Result)
	{
//		LastActivityTime = FDateTime::UtcNow();
        std::time_t t = std::time(NULL);
        _lastActivityTime = *std::gmtime(&t);
	}
	return Result;
}


bool Socket::recvFrom(unsigned char* Data, size_t BufferSize, ssize_t& BytesRead, IPAddress& Source, SocketReceiveFlags::Type Flags)
{
	socklen_t Size = sizeof(sockaddr_in);
    sockaddr& Addr = *(IPAddress&)Source;
    
	const int TranslatedFlags = TranslateFlags(Flags);
    
	// Read into the buffer and set the source address
	BytesRead = recvfrom( _socket, (char*)Data, BufferSize, TranslatedFlags, &Addr, &Size);
    //	NETWORK_PROFILER(Socket::RecvFrom(Data,BufferSize,BytesRead,Source));
    
	bool Result = BytesRead >= 0;
	if (Result)
	{
//		LastActivityTime = FDateTime::UtcNow();
        std::time_t t = std::time(NULL);
        _lastActivityTime = *std::gmtime(&t);
	}
    
	return Result;
}


bool Socket::recv(unsigned char* Data, size_t BufferSize, ssize_t& BytesRead, SocketReceiveFlags::Type Flags)
{
	const int TranslatedFlags = TranslateFlags(Flags);
	BytesRead = ::recv(_socket, (char*)Data, BufferSize, TranslatedFlags);
    
    //	NETWORK_PROFILER(Socket::Recv(Data,BufferSize,BytesRead));
    
	bool Result = BytesRead >= 0;
	if (Result)
	{
//		LastActivityTime = FDateTime::UtcNow();
        std::time_t t = std::time(NULL);
        _lastActivityTime = *std::gmtime(&t);
	}
	return Result;
}


bool Socket::wait(SocketWaitConditions::Type Condition, unsigned int WaitTime)
{
	if ((Condition == SocketWaitConditions::WaitForRead) || (Condition == SocketWaitConditions::WaitForReadOrWrite))
	{
		if (hasState(SocketInternalState::CanRead, WaitTime) == SocketInternalState::Yes)
		{
			return true;
		}
	}
    
	if ((Condition == SocketWaitConditions::WaitForWrite) || (Condition == SocketWaitConditions::WaitForReadOrWrite))
	{
		if (hasState(SocketInternalState::CanWrite, WaitTime) == SocketInternalState::Yes)
		{
			return true;
		}
	}
    
	return false;
}


SocketConnectionState Socket::getConnectionState(void)
{
	SocketConnectionState CurrentState = SCS_ConnectionError;
    
	// look for an existing error
	if (hasState(SocketInternalState::HasError) == SocketInternalState::No)
	{
//		if (FDateTime::UtcNow() - LastActivityTime > FTimespan::FromSeconds(5))
        std::time_t t = std::time(NULL);
        double diff = std::difftime(t, std::mktime(&_lastActivityTime));
        if ( diff > 5.0 )
		{
			// get the write state
			SocketInternalState::Return WriteState = hasState(SocketInternalState::CanWrite, /*millisec*/(1) );
			SocketInternalState::Return ReadState = hasState(SocketInternalState::CanRead, /*millisec*/ (1) );
            
			// translate yes or no (error is already set)
			if (WriteState == SocketInternalState::Yes || ReadState == SocketInternalState::Yes)
			{
				CurrentState = SCS_Connected;
//				LastActivityTime = FDateTime::UtcNow();
			}
			else if (WriteState == SocketInternalState::No && ReadState == SocketInternalState::No)
			{
				CurrentState = SCS_NotConnected;
			}
		}
		else
		{
			CurrentState = SCS_Connected;
		}
	}
    
	return CurrentState;
}


void Socket::getAddress(IPAddress& OutAddr)
{
	IPAddress& Addr = (IPAddress&)OutAddr;
	SOCKLEN Size = sizeof(sockaddr_in);
    
	// Figure out what ip/port we are bound to
	bool bOk = getsockname(_socket, Addr, &Size) == 0;
    
	if (bOk == false)
	{
//		check(SocketSubsystem);
        CCASSERT( !SocketManager::getInstance(), "SocketManager::getInstance() == null_ptr " );
		CCLOG( "Failed to read address for socket (%s)", SocketManager::getInstance()->getSocketError().c_str() );
	}
}


bool Socket::setNonBlocking(bool bIsNonBlocking)
{
//	unsigned long Value = bIsNonBlocking ? true : false;
    
//#if PLATFORM_HTML5
//	// can't have blocking sockets.
//	ensureMsgf(bIsNonBlocking, TEXT("Can't have blocking sockets on HTML5"));
//    return true;
//#else
    
//#if PLATFORM_has_BSD_SOCKET_FEATURE_WINSOCKETS
#if defined(_MSC_VER) || defined(__MINGW32__)
	return ioctlsocket(Socket,FIONBIO,&Value) == 0;
#else
	int Flags = ::fcntl(_socket, F_GETFL, 0);
	//Set the flag or clear it, without destroying the other flags.
	Flags = bIsNonBlocking ? Flags | O_NONBLOCK : Flags ^ (Flags & O_NONBLOCK);
	int err = fcntl(_socket, F_SETFL, Flags);
	return (err == 0 ? true : false);
#endif
//#endif
}


bool Socket::setBroadcast(bool bAllowBroadcast)
{
	int Param = bAllowBroadcast ? 1 : 0;
	return setsockopt(_socket,SOL_SOCKET,SO_BROADCAST,(char*)&Param,sizeof(Param)) == 0;
}


bool Socket::joinMulticastGroup(const IPAddress& GroupAddress)
{
	ip_mreq imr;
    
	imr.imr_interface.s_addr = INADDR_ANY;
    imr.imr_multiaddr = ((sockaddr_in*)&**((IPAddress*)(&GroupAddress)))->sin_addr;
    
	return (setsockopt(_socket, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char*)&imr, sizeof(imr)) == 0);
}


bool Socket::leaveMulticastGroup(const IPAddress& GroupAddress)
{
	ip_mreq imr;
    
	imr.imr_interface.s_addr = INADDR_ANY;
    imr.imr_multiaddr = ((sockaddr_in*)&**((IPAddress*)(&GroupAddress)))->sin_addr;
    
	return (setsockopt(_socket, IPPROTO_IP, IP_DROP_MEMBERSHIP, (char*)&imr, sizeof(imr)) == 0);
}


bool Socket::setMulticastLoopback(bool bLoopback)
{
	return (setsockopt(_socket, IPPROTO_IP, IP_MULTICAST_LOOP, (char*)&bLoopback, sizeof(bLoopback)) == 0);
}


bool Socket::setMulticastTtl(unsigned char  TimeToLive)
{
	return (setsockopt(_socket, IPPROTO_IP, IP_MULTICAST_TTL, (char*)&TimeToLive, sizeof(TimeToLive)) == 0);
}


bool Socket::setReuseAddr(bool bAllowReuse)
{
	int Param = bAllowReuse ? 1 : 0;
	return setsockopt(_socket,SOL_SOCKET,SO_REUSEADDR,(char*)&Param,sizeof(Param)) == 0;
}


bool Socket::setLinger(bool bShouldLinger,int Timeout)
{
	linger ling;
    
	ling.l_onoff = bShouldLinger;
	ling.l_linger = Timeout;
    
	return setsockopt(_socket,SOL_SOCKET,SO_LINGER,(char*)&ling,sizeof(ling)) == 0;
}


bool Socket::setRecvErr(bool bUseErrorQueue)
{
	// Not supported, but return true to avoid spurious log messages
	return true;
}


bool Socket::setSendBufferSize(int Size,int& NewSize)
{
	SOCKLEN SizeSize = sizeof(int);
	bool bOk = setsockopt(_socket,SOL_SOCKET,SO_SNDBUF,(char*)&Size,sizeof(int)) == 0;
    
	// Read the value back in case the size was modified
	getsockopt(_socket,SOL_SOCKET,SO_SNDBUF,(char*)&NewSize, &SizeSize);
    
	return bOk;
}


bool Socket::setReceiveBufferSize(int Size,int& NewSize)
{
	SOCKLEN SizeSize = sizeof(int);
	bool bOk = setsockopt(_socket,SOL_SOCKET,SO_RCVBUF,(char*)&Size,sizeof(int)) == 0;
    
	// Read the value back in case the size was modified
	getsockopt(_socket,SOL_SOCKET,SO_RCVBUF,(char*)&NewSize, &SizeSize);
    
	return bOk;
}


int Socket::getPortNo(void)
{
	sockaddr_in Addr;
    
	SOCKLEN Size = sizeof(sockaddr_in);
    
	// Figure out what ip/port we are bound to
	bool bOk = getsockname(_socket, (sockaddr*)&Addr, &Size) == 0;
	
	if (bOk == false)
	{
        CCASSERT( !SocketManager::getInstance(), "SocketManager::getInstance() == null_ptr " );
		CCLOG( "Failed to read address for socket (%s)", SocketManager::getInstance()->getSocketError().c_str() );
//		check(SocketSubsystem);
//		UE_LOG(LogSockets, Error, TEXT("Failed to read address for socket (%s)"), SocketSubsystem->GetSocketError());
	}
    
	// Read the port number
	return ntohs(Addr.sin_port);
}

NS_CC_END
