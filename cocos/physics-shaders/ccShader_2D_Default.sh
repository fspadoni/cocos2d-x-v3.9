
                                                      
const char* ccShader_2D_def_vert = STRINGIFY(


attribute vec4 a_position;
attribute vec2 a_texCoord;
attribute vec4 a_color;

\n#ifdef GL_ES\n
varying lowp vec4 v_fragmentColor;
varying lowp vec2 v_texCoord;
\n#else\n
varying vec4 v_fragmentColor;
varying vec2 v_texCoord;
\n#endif\n

void main()
{
    gl_Position = CC_MVPMatrix * a_position;
    v_texCoord = a_texCoord;
    v_fragmentColor = a_color;
}


);


const char* ccShader_2D_def_frag = STRINGIFY(

\n#ifdef GL_ES\n
precision lowp float;
\n#endif\n

uniform float radius;
varying vec2 v_texCoord;
varying vec4 v_fragmentColor;


void main()
{
gl_FragColor = texture2D(CC_Texture1, v_texCoord);// + texture2D(CC_Texture0, v_texCoord);
}



);