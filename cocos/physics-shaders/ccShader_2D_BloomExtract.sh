
                                                      
const char* ccShader_2D_BloomExtract_vert = STRINGIFY(


attribute vec4 a_position;
attribute vec2 a_texCoord;
attribute vec4 a_color;

\n#ifdef GL_ES\n
varying lowp vec4 v_fragmentColor;
varying lowp vec2 v_texCoord;
\n#else\n
varying vec4 v_fragmentColor;
varying vec2 v_texCoord;
\n#endif\n

void main()
{
    gl_Position = CC_MVPMatrix * a_position;
    v_texCoord = a_texCoord;
    v_fragmentColor = a_color;
}


);


const char* ccShader_2D_BloomExtract_frag = STRINGIFY(

\n#ifdef GL_ES\n
precision lowp float;
\n#endif\n

uniform float bloomThreshold;
varying vec2 v_texCoord;
varying vec4 v_fragmentColor;


void main()
{
    vec4 color = texture2D(CC_Texture0, v_texCoord);
    gl_FragColor = clamp( (color - 0.8) / (1.0 - 0.8), 0.0, 1.0);
//    gl_FragColor = saturate((c - bloomThreshold) / (1.0 - bloomThreshold));
//    gl_FragColor = texture2D(CC_Texture0, v_texCoord);
//    gl_FragColor = texture2D(CC_Texture1, v_texCoord) + texture2D(CC_Texture0, v_texCoord);
//    gl_FragColor = vec4(0.5,0.5,0.5,0.5);
}



);