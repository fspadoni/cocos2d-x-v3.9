
                                                      
const char* ccShader_2D_LINE_vert = STRINGIFY(


attribute vec4 a_position;
attribute vec2 a_normal;
attribute vec2 a_texCoord;
attribute vec4 a_color;

uniform float thickness;
                                              
\n#ifdef GL_ES\n
varying lowp vec2 v_texCoord;
varying lowp vec4 v_fragmentColor;
\n#else\n
varying vec2 v_texCoord;
varying vec4 v_fragmentColor;
\n#endif\n

                                              
void main(void)
{
    vec2 normal = vec2( cos(a_normal.x), sin(a_normal.x) );
    vec2 pointPos = a_position.xy + vec2( thickness / 2.0  * normal );
    gl_Position = CC_PMatrix * vec4( pointPos, 0.0, 1.0);
//    gl_Position = CC_PMatrix * a_position;
    v_texCoord = a_texCoord;
    v_fragmentColor = a_color;
}



);



const char* ccShader_2D_LINE_POW_vert = STRINGIFY(
                                              
                                              
attribute vec4 a_position;
attribute vec2 a_normal;
attribute vec2 a_texCoord;
attribute vec4 a_color;
                                              
uniform float thickness;
                                              
\n#ifdef GL_ES\n
varying lowp vec2 v_texCoord;
varying lowp vec4 v_fragmentColor;
varying lowp vec2 v_normal;
\n#else\n
varying vec2 v_texCoord;
varying vec4 v_fragmentColor;
varying vec2 v_normal;
\n#endif\n
                                              
                                              
void main(void)
{
    v_normal = vec2( cos(a_normal.x), sin(a_normal.x) );
    vec2 pointPos = a_position.xy + vec2( thickness / 2.0  * v_normal );
    gl_Position = CC_PMatrix * vec4( pointPos, 0.0, 1.0);
    //    gl_Position = CC_PMatrix * a_position;
    v_texCoord = a_texCoord;// * vec2(1.0, 3.0);
    v_fragmentColor = a_color;// * vec4( 1.0, 1.0, 1.0, 1.0);
}
                                              
                                        
);

