
const char* ccShader_2D_LINE_frag = STRINGIFY(

\n#ifdef GL_ES\n
precision lowp float;
\n#endif\n
                                
varying vec2 v_texCoord;
varying vec4 v_fragmentColor;
        
uniform float thickness;

                                              
void main()
{
    float alpha = 1.0;//( 1.0 -  2.0*(v_texCoord.y - 0.5) );
    gl_FragColor = vec4(v_fragmentColor.xyz, alpha);// * texture2D(CC_Texture0, v_texCoord);
    gl_FragColor = alpha*texture2D(CC_Texture0, v_texCoord) + 0.0005* v_fragmentColor;  //(alpha-1.0);
}
                                            

);



const char* ccShader_2D_LINE_POW_frag = STRINGIFY(
                                              
\n#ifdef GL_ES\n
precision lowp float;
\n#endif\n
          
uniform float thickness;
varying vec2 v_texCoord;
varying vec4 v_fragmentColor;
varying vec2 v_normal;
                                            
                                              
void main()
{
//    vec4 color = 5.0 * (v_fragmentColor + vec4(1.0, 1.0, 1.0, 1.0) );
//    float t = pow( 1.0 / abs( (v_texCoord.y - 3.0/2.0 ) * 8.0 ), 0.7);
//    float t = pow( 1.0 / abs( (v_texCoord.y - 3.0/2.0 ) * 2.25), 1.0);
//    float t =  1.0 / pow( abs( (v_texCoord.y - 1.0/2.0) * 6.0 ), 1.8);
//    float t = pow( 1.0 / abs( (v_texCoord.y - 0.5) * 16.0 ), 1.8);
//    float alpha = 1.0 /( 1. + pow( 4.0 * (v_texCoord.y - 0.5), 2.0) );
    float t = 1.5 /( 1.0 + pow( 14.0 * (v_texCoord.y - 0.5), 1.8) );
//    float alpha = ( 1.0 - v_texCoord.y );
    gl_FragColor = vec4(v_fragmentColor * t );
//    gl_FragColor = v_fragmentColor;
}
                                              
                                              

);