//
//  CCPhysicsShaders.cpp
//  cocos2d_libs
//
//  Created by Federico Spadoni on 07/03/16.
//
//

#include "CCPhysicsShaders.h"


#define STRINGIFY(A)  #A

NS_CC_BEGIN
//

#include "ccShader_2D_Default.sh"
#include "ccShader_2D_LINE.vert"
#include "ccShader_2D_LINE.frag"
#include "ccShader_2D_Bullet.sh"
#include "ccShader_2D_BloomExtract.sh"
#include "ccShader_2D_BloomBlur.sh"

NS_CC_END