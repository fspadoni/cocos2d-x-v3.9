
                                                      
const char* ccShader_2D_BloomBlur_vert = STRINGIFY(


attribute vec4 a_position;
attribute vec2 a_texCoord;
attribute vec4 a_color;

\n#ifdef GL_ES\n
varying lowp vec4 v_fragmentColor;
varying lowp vec2 v_texCoord;
\n#else\n
varying vec4 v_fragmentColor;
varying vec2 v_texCoord;
\n#endif\n

void main()
{
    gl_Position = CC_MVPMatrix * a_position;
    v_texCoord = a_texCoord;
    v_fragmentColor = a_color;
}


);


const char* ccShader_2D_BloomBlur_frag = STRINGIFY(

\n#ifdef GL_ES\n
precision lowp float;
\n#endif\n

//uniform float bloomThreshold;
varying vec2 v_texCoord;
varying vec4 v_fragmentColor;

const float blurSize = 1.0/360.0; //4.0/920.0;
const float intensity = 1.0; //0.635;

void main()
{

    vec4 sum = vec4(0);
    vec2 texcoord = v_texCoord.xy;


//thank you! http://www.gamerendering.com/2008/10/11/gaussian-blur-filter-shader/ for the
//blur tutorial
// blur in y (vertical)
// take nine samples, with the distance blurSize between them
    sum += texture2D(CC_Texture0, vec2(texcoord.x - 4.0*blurSize, texcoord.y)) * 0.05;
    sum += texture2D(CC_Texture0, vec2(texcoord.x - 3.0*blurSize, texcoord.y)) * 0.09;
    sum += texture2D(CC_Texture0, vec2(texcoord.x - 2.0*blurSize, texcoord.y)) * 0.12;
    sum += texture2D(CC_Texture0, vec2(texcoord.x - blurSize, texcoord.y)) * 0.15;
    sum += texture2D(CC_Texture0, vec2(texcoord.x, texcoord.y)) * 0.16;
    sum += texture2D(CC_Texture0, vec2(texcoord.x + blurSize, texcoord.y)) * 0.15;
    sum += texture2D(CC_Texture0, vec2(texcoord.x + 2.0*blurSize, texcoord.y)) * 0.12;
    sum += texture2D(CC_Texture0, vec2(texcoord.x + 3.0*blurSize, texcoord.y)) * 0.09;
    sum += texture2D(CC_Texture0, vec2(texcoord.x + 4.0*blurSize, texcoord.y)) * 0.05;

// blur in y (vertical)
// take nine samples, with the distance blurSize between them
    sum += texture2D(CC_Texture0, vec2(texcoord.x, texcoord.y - 4.0*blurSize)) * 0.05;
    sum += texture2D(CC_Texture0, vec2(texcoord.x, texcoord.y - 3.0*blurSize)) * 0.09;
    sum += texture2D(CC_Texture0, vec2(texcoord.x, texcoord.y - 2.0*blurSize)) * 0.12;
    sum += texture2D(CC_Texture0, vec2(texcoord.x, texcoord.y - blurSize)) * 0.15;
    sum += texture2D(CC_Texture0, vec2(texcoord.x, texcoord.y)) * 0.16;
    sum += texture2D(CC_Texture0, vec2(texcoord.x, texcoord.y + blurSize)) * 0.15;
    sum += texture2D(CC_Texture0, vec2(texcoord.x, texcoord.y + 2.0*blurSize)) * 0.12;
    sum += texture2D(CC_Texture0, vec2(texcoord.x, texcoord.y + 3.0*blurSize)) * 0.09;
    sum += texture2D(CC_Texture0, vec2(texcoord.x, texcoord.y + 4.0*blurSize)) * 0.05;

    //increase blur with intensity!
    gl_FragColor = sum*intensity;// + texture2D(CC_Texture0, texcoord);
//    gl_FragColor = color;
}



);