
                                                      
const char* ccShader_2D_Bullet_vert = STRINGIFY(


attribute vec4 a_position;
attribute vec2 a_texCoord;
attribute vec4 a_color;

\n#ifdef GL_ES\n
varying lowp vec4 v_fragmentColor;
varying lowp vec2 v_texCoord;
\n#else\n
varying vec4 v_fragmentColor;
varying vec2 v_texCoord;
\n#endif\n

void main()
{
    gl_Position = CC_PMatrix * a_position;
    v_fragmentColor = a_color;
    v_texCoord = a_texCoord;
}


);


const char* ccShader_2D_Bullet_frag = STRINGIFY(

\n#ifdef GL_ES\n
precision lowp float;
\n#endif\n

uniform float radius;
varying vec2 v_texCoord;
varying vec4 v_fragmentColor;


void main()
{
    vec2 uv = vec2( (v_texCoord - 0.5) * 2.0 );
    const float R = 0.4; // radius;
    vec4 color = v_fragmentColor * vec4( 1.0 - smoothstep( length(uv), 0.0, R) );
    gl_FragColor = texture2D(CC_Texture0, v_texCoord);// * color;
}



);