//
//  CCParallelForTask.cpp
//  cocos2d_libs
//
//  Created by Federico Spadoni on 31/01/15.
//
//

#include "physics-thread/CCParallelForTask.h"

#include "physics-thread/CCTaskScheduler.h"

NS_CC_BEGIN


InternalForTask::InternalForTask( const ForTask* forTask, const BlockRange& blockRange, const Task::Status* status )
: Task(status)
, _blockRange(blockRange)
, _forTask(forTask)
{
}


bool InternalForTask::run( WorkerThread* pThread )
{
    bool bOk;
    
    bOk = _forTask->operator()(_blockRange, pThread);
    
    delete this;
    return bOk;
    
}



bool InternalForTask::split( WorkerThread* pThread, Task** ppTask)
{
    InternalForTask* pNew;
    int	CutIndex;
    
    *ppTask = NULL;
    
    /* check we can split */
    if (_blockRange.end - _blockRange.begin < 2*_blockRange.grainsize)
        return false;
    
    /* decide on cut point */
    CutIndex = (_blockRange.begin + _blockRange.end)/2;
    
    /* create new task and reduce our BlockRange*/
    pNew = new InternalForTask( _forTask, BlockRange(CutIndex, _blockRange.end, _blockRange.grainsize), _status );
    _blockRange.end = CutIndex;
    
    /* done */
    *ppTask = pNew;
    return true;
}



bool InternalForTask::partialPop(WorkerThread* pThread, Task** ppTask)
{
    InternalForTask*	pNew;
    int					CutIndex;
    
    *ppTask = NULL;
    
    /* check we can split */
    if (_blockRange.end - _blockRange.begin < 2*_blockRange.grainsize)
        return false;
    
    /* decide on cut point */
    CutIndex = _blockRange.begin + _blockRange.grainsize;
    
    /* create new task and reduce our _blockRange*/
    pNew = new InternalForTask( _forTask, BlockRange(_blockRange.begin, CutIndex, _blockRange.grainsize), _status);
    _blockRange.begin = CutIndex;
    
    /* done */
    *ppTask = pNew;
    return true;
}


bool InternalForTask::spread(TaskScheduler* const& taskScheduler)
{
//    WorkerThread*		pThread;
    InternalForTask*	pLastTask;
    int					begin, end, grainsize;
    int					PartSize;
    unsigned			iThread, Count;
    
    /* count parts */
    begin		= _blockRange.begin;
    end			= _blockRange.end;
    grainsize	= _blockRange.grainsize;
    
    Count = (_blockRange.end-_blockRange.begin)/_blockRange.grainsize;
    if (Count == 0 )
        return false;
    
    if (Count > taskScheduler->getThreadCount())
        Count = taskScheduler->getThreadCount();
    
    PartSize = (_blockRange.end-_blockRange.begin)/Count;
    
    /*  */
    _blockRange = BlockRange(begin, begin+PartSize, _blockRange.grainsize);
    
    pLastTask = this;
    WorkerThread* pThread   = taskScheduler->getWorkerThread(0);
    
    {
//        SpinLockMutex lock( pThread->getTaskMutex() );
        SpinLock::ScopedLock( pThread->getTaskLock() );
        
        getStatus()->markBusy(true);
        pThread->getTasks()[0]		= this;
        pThread->getTaskCount()	= 1;
    }
    
    begin += PartSize;
    
    for(iThread=1; iThread<Count; iThread++)
    {
        pLastTask = new InternalForTask( _forTask, BlockRange(begin, begin+PartSize, _blockRange.grainsize), _status);
        pThread   = taskScheduler->getWorkerThread(iThread);
        
        {
//            SpinLockMutex lock( pThread->getTaskMutex() );
            SpinLock::ScopedLock( pThread->getTaskLock() );
            
            getStatus()->markBusy(true);
            pThread->getTasks()[0]		= pLastTask;
            pThread->getTaskCount()	= 1;
        }
        
        begin += PartSize;
    }
    
    pLastTask->_blockRange.end = end;
    
    return true;
}



bool Parallel_For(WorkerThread* pThread, const ForTask *pTask, const BlockRange& blockRange)
{
    Task::Status status;
    InternalForTask*	pInternalTask;
    
    pInternalTask	= new InternalForTask( pTask, blockRange, &status );
    
    pThread->addTask(pInternalTask);
    pThread->workUntilDone(&status);
    
    return true;
}


bool Parallel_For(const ForTask *pTask, const BlockRange& blockRange)
{
    return Parallel_For(WorkerThread::getCurrent(), pTask, blockRange);
}


NS_CC_END