//
//  CCTaskScheduler.cpp
//  cocos2d_libs
//
//  Created by Federico Spadoni on 29/01/15.
//
//

#include "CCTaskScheduler.h"

NS_CC_BEGIN


//std::__thread_specific_ptr<WorkerThread> TaskScheduler::mWorkerThreadIndex;
//__thread WorkerThread* TaskScheduler::mWorkerThreadIndex = nullptr;
pthread_key_t TaskScheduler::mWorkerThreadIndex;


TaskScheduler& TaskScheduler::getInstance()
{
    static TaskScheduler instance;
    
    return instance;
}

TaskScheduler::TaskScheduler()
{
    mIsInitialized = false;
    mThreadCount = 0;
    mIsClosing = false;
    
    readyForWork = false;
    
    mThread[0] = new WorkerThread( this );
    mThread[0]->attachToThisThread( this );
    
}

TaskScheduler::~TaskScheduler()
{
    if ( mIsInitialized )
    {
        stop();
    }
    
    pthread_key_delete(TaskScheduler::mWorkerThreadIndex);
}

unsigned TaskScheduler::GetHardwareThreadsCount()
{
    return std::thread::hardware_concurrency();
}


WorkerThread* TaskScheduler::getWorkerThread(const unsigned int index)
{
    WorkerThread* thread = mThread[index];
    if ( index >= mThreadCount )
    {
        return thread = 0;
    }
    return thread;
}

bool TaskScheduler::start(const unsigned int NbThread )
{
    
    if ( mIsInitialized )
    {
        stop();
    }
    
    //if ( !mIsInitialized )
    {
        mIsClosing		= false;
        mWorkersIdle		= false;
        mainTaskStatus	= NULL;
        
        // only physicsal cores. no advantage from hyperthreading.
        mThreadCount = GetHardwareThreadsCount();
        
        if ( NbThread > 0 && NbThread <= MAX_THREADS  )
        {
            mThreadCount = NbThread;
        }
        
        CCLOG("TaskScheduler: number thread %d ", mThreadCount);
        //mThread[0] =  new WorkerThread( this ) ;
        //mThread[0]->attachToThisThread( this );
        
        /* start worker threads */
        for( unsigned int iThread=1; iThread<mThreadCount; ++iThread)
        {
            //mThread[iThread] = boost::shared_ptr<WorkerThread>(new WorkerThread(this) );
            mThread[iThread] = new WorkerThread(this);
            mThread[iThread]->create_and_attach( this );
            mThread[iThread]->start( this );
        }
        
        mWorkerCount = mThreadCount;
        mIsInitialized = true;
        return true;
    }
    //else
    //{
    //	return false;
    //}
    
}



bool TaskScheduler::stop()
{
    unsigned iThread;
    
    mIsClosing = true;
    
    if ( mIsInitialized )
    {
        // wait for all
        WaitForWorkersToBeReady();
        wakeUpWorkers();
        
        for(iThread=1; iThread<mThreadCount; ++iThread)
        {
            while (!mThread[iThread]->mFinished)
            {
                //mThread[iThread]->join();
                //WorkerThread::release( mThread[iThread] );
                //mThread[iThread].reset();
            }
        }
        for(iThread=1; iThread<mThreadCount; ++iThread)
        {
            mThread[iThread]->release();
        }
        
        
        mIsInitialized = false;
        mWorkerCount = 1;
    }
    
    
    return true;
}



void TaskScheduler::wakeUpWorkers()
{
    
    mWorkersIdle = false;
    
    {
//        std::lock_guard<std::mutex> lock(wakeUpMutex);
//        readyForWork = true;
    }
    
    wakeUpEvent.notify_all();
    
}


void TaskScheduler::WaitForWorkersToBeReady()
{
    
    for(unsigned i=0; i<mThreadCount-1; ++i)
    {}
    
    mWorkersIdle = true;
}




unsigned TaskScheduler::size()	const
{
    return mWorkerCount;
}



WorkerThread::WorkerThread(TaskScheduler* const& pScheduler)
: mTaskScheduler(pScheduler)
//, mTaskMutex(ATOMIC_FLAG_INIT)
{
    assert(pScheduler);
    
    mTaskCount		= 0;
    mFinished		= false;
    mCurrentStatus = NULL;
//    mTaskMutex.v_ = 0L;
}


WorkerThread::~WorkerThread()
{
    mThread.join();
}

bool WorkerThread::attachToThisThread(TaskScheduler* pScheduler)
{
    
    mTaskCount		= 0;
    mFinished		= false;
    
//    TaskScheduler::mWorkerThreadIndex.reset( this );
//    TaskScheduler::mWorkerThreadIndex = this;
    pthread_key_create(&TaskScheduler::mWorkerThreadIndex, nullptr);
    pthread_setspecific(TaskScheduler::mWorkerThreadIndex, this);
    
    return true;
}



bool WorkerThread::start(TaskScheduler* const& taskScheduler)
{
    assert(taskScheduler);
    mTaskScheduler = taskScheduler;
    mCurrentStatus = NULL;
    
    return true;
}




std::thread* WorkerThread::create_and_attach( TaskScheduler* const & taskScheduler)
{
    
    mThread = std::thread(std::bind(&WorkerThread::run, this));

    return &mThread;
}


bool WorkerThread::releaseThread()
{
    
//    if ( mThread = 0 )
    {
        mThread.join();
        
        return true;
    }
    
    return false;
}


WorkerThread* WorkerThread::getCurrent()
{
    return (WorkerThread*)pthread_getspecific( TaskScheduler::mWorkerThreadIndex );
//    return TaskScheduler::mWorkerThreadIndex;
//    return TaskScheduler::mWorkerThreadIndex.get();
}


void WorkerThread::run(void)
{
    
    // Thread Local Storage
//    TaskScheduler::mWorkerThreadIndex.reset( this );
//    TaskScheduler::mWorkerThreadIndex = this;
    pthread_setspecific(TaskScheduler::mWorkerThreadIndex, this);
    
    // main loop
    for(;;)
    {
        Idle();
        
        if ( mTaskScheduler->isClosing() )
            break;
        
        
        while ( !mTaskScheduler->mainTaskStatus->isBusy() )
        {
//            if ( !mTaskScheduler->mainTaskStatus->isBusy() )
//                break;
            
            doWork(0);
            
            
            if (mTaskScheduler->isClosing() )
            break;
        }
        
        mTaskScheduler->mainTaskStatus = nullptr;
        
    }
    
    mFinished = true;
    return;
}


std::thread::id WorkerThread::getId()
{
    return mThread.get_id();
}


int WorkerThread::GetWorkerIndex()
{
    return int(this - *mTaskScheduler->mThread);
}


void WorkerThread::Idle()
{
    {
        std::unique_lock<std::mutex> lock( mTaskScheduler->wakeUpMutex );
        
//    book: Effective Modern C++ Scott Meyers pag. 283-284
//        mTaskScheduler->wakeUpEvent.wait(lock, [] {return mTaskScheduler->readyForWork;} );
        
//        while(!mTaskScheduler->readyForWork)
        {
            mTaskScheduler->wakeUpEvent.wait(lock);
            
        }
        
    }
    return;
}




void WorkerThread::doWork(Task::Status* status)
{
    
    do
    {
        Task*		pTask;
        Task::Status*	pPrevStatus = nullptr;
        
        while (popTask(&pTask))
        {
            // run
            pPrevStatus = mCurrentStatus;
            mCurrentStatus = pTask->getStatus();
            
            pTask->run(this);
            
            mCurrentStatus->markBusy(false);
            mCurrentStatus = pPrevStatus;
            
            if ( status && !status->isBusy() )
            return;
        }
        
        /* check if main work is finished */
        if (!mTaskScheduler->mainTaskStatus->isBusy())
            return;
        
    } while (stealTasks());
    
    
    return;
    
}


void WorkerThread::workUntilDone(Task::Status* status)
{
    //PROFILE_SYNC_PREPARE( this );
    
    while (status->isBusy())
    {
//        std::this_thread::yield();
        doWork(status);
    }
    
    //PROFILE_SYNC_CANCEL( this );
    
    if (mTaskScheduler->mainTaskStatus == status)
    {
        
        mTaskScheduler->mainTaskStatus = nullptr;
        

//        std::lock_guard<std::mutex> lock(mTaskScheduler->wakeUpMutex);
        
        mTaskScheduler->readyForWork = false;
        
        
//        {
//            std::unique_lock<std::mutex> lock(mTaskScheduler->wakeUpMutex);
//            cv.wait(lk, []{return processed;});
//            
//        }

//        // wait for the worker
//        {
//            std::unique_lock<std::mutex> lk(m);
//            cv.wait(lk, []{return processed;});
//        }
    }
}


bool WorkerThread::popTask(Task** outTask)
{
//    SpinLockMutex lock( &mTaskMutex );
    SpinLock::ScopedLock lock( mTaskSpinLock );
    
    //
    if (mTaskCount == 0)
        return false;
    
    Task* task = mTask[mTaskCount-1];
    
    /* Check if we can pop a partial-task (ex: one iteration of a loop) */
    if (task->partialPop(this, outTask))
    {
        task->getStatus()->markBusy(true);
        return true;
    }
    
    // pop from top of the pile
    *outTask = task;
    --mTaskCount;
    return true;
}


bool WorkerThread::pushTask(Task* task)
{
    // if we're single threaded return false
    if ( mTaskScheduler->getThreadCount()<2 )
        return false;
    
    /* if task pool is empty, try to spread subtasks across all threads */
    if (!mTaskScheduler->mainTaskStatus)
    {
        /* check we're indeed the main thread			*/
        /* (no worker can push job, no task is queued)	*/
        CCASSERT( GetWorkerIndex()==0,  "no worker can push job, no task is queued");
        
        /* Ready ? */
        mTaskScheduler->WaitForWorkersToBeReady();
        
        /* Set... */
        if (task->spread( mTaskScheduler ))
        {	/* Go! Mark this task as the root task (see WorkUntilDone) */
            mTaskScheduler->mainTaskStatus = task->getStatus();
            mTaskScheduler->wakeUpWorkers();
            return true;
        }
    }
    
    
    {
//        SpinLockMutex lock( &mTaskMutex );
        SpinLock::ScopedLock lock( mTaskSpinLock );
        
        
        if (mTaskCount >= Max_TasksPerThread )
            return false;
        
        
        task->getStatus()->markBusy(true);
        mTask[mTaskCount] = task;
        ++mTaskCount;
    }
    
    
    if (!mTaskScheduler->mainTaskStatus)
    {
        mTaskScheduler->mainTaskStatus = task->getStatus();
        mTaskScheduler->wakeUpWorkers();
    }
    
    return true;
}

bool WorkerThread::addTask(Task* task)
{
    if (pushTask(task))
        return true;
    
    
    task->run(this);
        return false;
}


bool WorkerThread::giveUpSomeWork(WorkerThread* idleThread)
{
//    SpinLockMutex lock;
    
//    try lock and remember to unlock at the end of the function
    if ( !mTaskSpinLock.try_lock() )
    {
        return false;
    }
//    if ( !lock.try_lock( &mTaskMutex ) )
//        return false;
    
	
    if ( mTaskCount == 0 )
    {
//    unlock
        mTaskSpinLock.unlock();
        return false;
    }
    
//    SpinLockMutex	lockIdleThread( &idleThread->mTaskMutex );
    SpinLock::ScopedLock lock( idleThread->mTaskSpinLock );
    
    if ( idleThread->mTaskCount )
    {
        mTaskSpinLock.unlock();
        return false;
    }
    
    
    /* if only one task remaining, try to split it */
    if (mTaskCount==1)
    {
        Task* task;
        
        task = NULL;
        if (mTask[0]->split( idleThread, &task ) )
        {
            task->getStatus()->markBusy(true);
            
            idleThread->mTask[0] = task;
            idleThread->mTaskCount = 1;
            
            mTaskSpinLock.unlock();
            return true;
        }
    }
    
    unsigned int count = (mTaskCount+1) /2;
    
    
    Task** p = idleThread->mTask;
    
    unsigned int iTask;
    for( iTask=0; iTask< count; ++iTask)
    {
        *p++ = mTask[iTask];
        mTask[iTask] = NULL;
    }
    idleThread->mTaskCount = count;
    
    
    for( p = mTask; iTask<mTaskCount; ++iTask)
    {
        *p++ = mTask[iTask];
    }
    mTaskCount -= count;
    
    
//    unlock
    mTaskSpinLock.unlock();
    
    return true;
}


bool WorkerThread::stealTasks()
{
    
	int Offset = GetWorkerIndex();
    
    for( unsigned int iThread=0; iThread<mTaskScheduler->getThreadCount(); ++iThread)
    {
        //WorkerThread*	pThread;
        
        WorkerThread* pThread = mTaskScheduler->mThread[ (iThread+Offset) % mTaskScheduler->getThreadCount() ];
       
        if ( pThread == this)
            continue;
        
        if ( pThread->giveUpSomeWork(this) )
            return true;
        
        if ( mTaskCount > 0 )
            return true;
    }
    
    return false;
}



// called once by each thread used
// by the TaskScheduler
bool runThreadSpecificTask(WorkerThread* thread, const Task *task )
{
    
    
    //volatile long atomicCounter = TaskScheduler::getInstance().size();// mNbThread;
//    helper::system::atomic<int> atomicCounter( TaskScheduler::getInstance().size() );
    std::atomic<int> atomicCounter;
    atomicCounter = TaskScheduler::getInstance().size();
    
    std::mutex  InitThreadSpecificMutex;
    
    Task::Status status;
    
    const int nbThread = TaskScheduler::getInstance().size();
    
    for (int i=0; i<nbThread; ++i)
    {
        thread->addTask( new ThreadSpecificTask( &atomicCounter, &InitThreadSpecificMutex, &status ) );
    }
    
    
    thread->workUntilDone(&status);
    
    return true;
}


// called once by each thread used
// by the TaskScheduler
bool runThreadSpecificTask(const Task *task )
{
    return runThreadSpecificTask(WorkerThread::getCurrent(), task );
}


NS_CC_END
