//
//  CCTask.cpp
//  cocos2d_libs
//
//  Created by Federico Spadoni on 29/01/15.
//
//

#include "physics-thread/CCTask.h"


NS_CC_BEGIN


Task::Task(const Task::Status* status)
: _status(status)
{
}

Task::~Task()
{
    //m_Status;
    //delete this;
}



//EmptyTask::EmptyTask(const Task::Status* status)
//: Task(status)
//{
//}
//
//
//EmptyTask::~EmptyTask()
//{}
//
//
//bool EmptyTask::run(WorkerThread* thread)
//{
//    delete this;
//    return true;
//}




//InitPerThreadDataTask::InitPerThreadDataTask(volatile long* atomicCounter, boost::mutex* mutex, TaskStatus* pStatus )
ThreadSpecificTask::ThreadSpecificTask(std::atomic<int>* atomicCounter, std::mutex* mutex, Task::Status* status )
: Task(status)
, mThreadSpecificMutex(mutex)
, mAtomicCounter(atomicCounter)
{}

ThreadSpecificTask::~ThreadSpecificTask()
{
    //mAtomicCounter;
}

bool ThreadSpecificTask::run(WorkerThread* )
{
    
    runThreadSpecific();
    
    
    {
        std::lock_guard<std::mutex> lock(*mThreadSpecificMutex);
        
        runCriticalThreadSpecific();
        
    }
    
    //BOOST_INTERLOCKED_DECREMENT( mAtomicCounter );
    //BOOST_COMPILER_FENCE;
    
    --(*mAtomicCounter);
    
    
    while(mAtomicCounter->operator int() > 0)
    {
        // yield while waiting
        std::this_thread::yield();
    }  
    return NULL;  
}



NS_CC_END