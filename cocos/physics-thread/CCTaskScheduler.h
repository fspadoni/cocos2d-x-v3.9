//
//  CCTaskScheduler.h
//  cocos2d_libs
//
//  Created by Federico Spadoni on 29/01/15.
//
//

#ifndef __cocos2d_libs__CCTaskScheduler__
#define __cocos2d_libs__CCTaskScheduler__

#include "base/ccMacros.h"
#include "base/CCRef.h"

#include "physics-thread/CCTask.h"

#include <atomic>
#include <condition_variable>

#include <pthread.h>

NS_CC_BEGIN

class TaskScheduler;
class WorkerThread;




class SpinLockMutex
{
    public:
    
    SpinLockMutex()
    : _mutex(0)
    {
    }
    
    SpinLockMutex(std::atomic_flag* mutex, bool bLock=true)
    : _mutex(mutex)
    {
        if (bLock)
        {
            lock();
        }
    }

    ~SpinLockMutex()
    {
        unlock();
    }
    
    bool try_lock(std::atomic_flag* mutex)
    {
        if (mutex->test_and_set(std::memory_order_seq_cst))
        {
            return false;
        }
        
        _mutex = mutex;
        return true;
    }
    
    void lock()
    {
        while(_mutex->test_and_set(std::memory_order_seq_cst))
        {
//            std::this_thread::yield();
        }
    }
    
    void unlock()
    {
        if (_mutex)
            _mutex->clear(std::memory_order_seq_cst);
    }
    
private:
    std::atomic_flag* _mutex;
};




class SpinLock
{
    
    std::atomic_flag v_;
    
public:
    
    SpinLock() : v_(ATOMIC_FLAG_INIT)
    {}
    
    ~SpinLock()
    {
        unlock();
    }

    
public:
    
    bool try_lock()
    {
        return !v_.test_and_set( std::memory_order_seq_cst );
    }
    
    void lock()
    {
        while( v_.test_and_set(std::memory_order_seq_cst) )
        {
            std::this_thread::yield();
        }
    }
    
    void unlock()
    {
        v_ .clear( std::memory_order_seq_cst );
    }
    
public:
    
    class ScopedLock
    {
    private:
        
        SpinLock & sp_;
        
        ScopedLock( ScopedLock const & );
        ScopedLock & operator=( ScopedLock const & );
        
    public:
        
        explicit ScopedLock( SpinLock & sp ): sp_( sp )
        {
            sp.lock();
        }
        
        ~ScopedLock()
        {
            sp_.unlock();
        }
    };
};



class CC_DLL WorkerThread : public Ref
{
public:
    
    WorkerThread(TaskScheduler* const& taskScheduler);
    
    ~WorkerThread();
    
    static WorkerThread* getCurrent();
    
    // queue task if there is space, and run it otherwise
    bool addTask(Task* pTask);
    
    void workUntilDone(Task::Status* status);
    
    Task::Status* getCurrentStatus() const {return mCurrentStatus;}
    
//    std::atomic_flag* getTaskMutex() const {return &mTaskMutex;}
    
    SpinLock& getTaskLock()  {return mTaskSpinLock;}
    
    std::thread::id getId();
    
    Task** getTasks() {return mTask;}
    
    unsigned int& getTaskCount()  {return mTaskCount; }
    
    int GetWorkerIndex();
    
private:
    
    bool start(TaskScheduler* const& taskScheduler);
    
    
    std::thread* create_and_attach( TaskScheduler* const& taskScheduler);
    
    bool releaseThread();
    
    
    
    // queue task if there is space (or do nothing)
    bool pushTask(Task* pTask);
    
    // pop task from queue
    bool popTask(Task** ppTask);
    
    // steal and queue some task from another thread
    bool stealTasks();
    
    // give an idle thread some work
    bool giveUpSomeWork(WorkerThread* pIdleThread);
    
    
    void doWork(Task::Status* status);
    
    // boost thread main loop
    void run(void);
    
    
    //void	ThreadProc(void);
    void	Idle(void);
    
    bool attachToThisThread(TaskScheduler* pScheduler);
    
    
    
    
    
private:
    
    enum
    {
        Max_TasksPerThread = 256
    };
    
    
    
    
//    mutable std::atomic_flag		mTaskMutex;
    SpinLock mTaskSpinLock;
    
    Task*		mTask[Max_TasksPerThread];
    unsigned int	mTaskCount;
    Task::Status*	mCurrentStatus;
    
    
    TaskScheduler*     mTaskScheduler;
    std::thread  mThread;
    
    // The following members may be accessed by _multiple_ threads at the same time:
    bool	mFinished;
    
    
    friend class TaskScheduler;
    
};




class CC_DLL TaskScheduler

{
    enum
    {
        MAX_THREADS = 16,
        STACKSIZE = 64*1024 /* 64K */
    };
    
public:
    
    static TaskScheduler& getInstance();
    
    
    bool start(const unsigned int NbThread = 0);
    
    bool stop(void);
    
    bool isClosing(void) const { return mIsClosing; }
    
    unsigned int getThreadCount(void) const { return mThreadCount; }
    
    
    void	WaitForWorkersToBeReady();
    
    void	wakeUpWorkers();
    
    static unsigned GetHardwareThreadsCount();
    
    unsigned size()	const;
    
    WorkerThread* getWorkerThread(const unsigned int index);
    
    
private:
    
//    static std::__thread_specific_ptr<WorkerThread>	mWorkerThreadIndex;
//    static thread_local WorkerThread* mWorkerThreadIndex;
//    static __thread WorkerThread* mWorkerThreadIndex;
    static pthread_key_t mWorkerThreadIndex;
    
    //boost::thread_group mThreads;
    WorkerThread* 	mThread[MAX_THREADS];
    
    
    // The following members may be accessed by _multiple_ threads at the same time:
    Task::Status*	mainTaskStatus;
    static Task::Status _initStatus;
    
    bool readyForWork;
    
    std::mutex  wakeUpMutex;
    
    std::condition_variable wakeUpEvent;
    //boost::condition_variable sleepEvent;
    
    
    
private:
    
    TaskScheduler();
    
    TaskScheduler(const TaskScheduler& ) {}
    
    ~TaskScheduler();
    
    bool mIsInitialized;
    // The following members may be accessed by _multiple_ threads at the same time:
    unsigned mWorkerCount;
    unsigned mTargetWorkerCount;
    unsigned mActiveWorkerCount;
    
    
    bool						mWorkersIdle;
    
    
    bool mIsClosing;
    
    unsigned					mThreadCount;
    
    
    
    friend class WorkerThread;
};		





CC_DLL bool runThreadSpecificTask(WorkerThread* pThread, const Task *pTask );

CC_DLL bool runThreadSpecificTask(const Task *pTask );

NS_CC_END

#endif /* defined(__cocos2d_libs__CCTaskScheduler__) */
