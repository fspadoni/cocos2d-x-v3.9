//
//  CCParallelForTask.h
//  cocos2d_libs
//
//  Created by Federico Spadoni on 31/01/15.
//
//

#ifndef __cocos2d_libs__CCParallelForTask__
#define __cocos2d_libs__CCParallelForTask__

#include "base/ccMacros.h"

#include "physics-thread/CCTask.h"


NS_CC_BEGIN


// grainsize at least cache line size
class BlockRange
{
    public:
    BlockRange(){}
    
    BlockRange(const BlockRange& blockRange)
    {
        begin		= blockRange.begin;
        end			= blockRange.end;
        grainsize	= blockRange.grainsize;
    }
    
    BlockRange(int _begin, int _end)
    {
        begin		= _begin;
        end			= _end;
        grainsize	= 1;
    }
    
    BlockRange(int _begin, int _end, int _grainsize)
    {
        begin		= _begin;
        end			= _end;
        grainsize	= _grainsize;
    }
    
    public:
    
    int begin;
    int end;
    int grainsize;
    
};


class ForTask
{
    
    public:
    // intel TBB parallel_for
    virtual bool operator() ( const BlockRange &range, WorkerThread* pThread ) const = 0;
};




class InternalForTask : public Task
{
public:
    
    InternalForTask( const ForTask* forTask, const BlockRange& _blockRange, const Task::Status* status );
    
    
    virtual bool run( WorkerThread* pThread );
    
    
    virtual bool split( WorkerThread* pThread, Task** ppTask);
    
    virtual bool partialPop(WorkerThread* pThread, Task** ppTask);
    
    virtual bool spread(TaskScheduler* const& taskScheduler);
    
    
    
protected:
    
    BlockRange _blockRange;
    const ForTask* _forTask;
    
};




CC_DLL bool Parallel_For(WorkerThread* pThread, const ForTask *pTask, const BlockRange& blockRange);

CC_DLL bool Parallel_For(const ForTask *pTask, const BlockRange& blockRange);


NS_CC_END


#endif /* defined(__cocos2d_libs__CCParallelForTask__) */
