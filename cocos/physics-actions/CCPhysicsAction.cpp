/****************************************************************************
Copyright (c) 2008-2010 Ricardo Quesada
Copyright (c) 2010-2012 cocos2d-x.org
Copyright (c) 2011      Zynga Inc.
Copyright (c) 2013-2014 Chukong Technologies Inc.

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/

#include "physics-actions/CCPhysicsAction.h"
#include "physics-actions/CCPhysicsActionInterval.h"
#include "physics/CCPhysicsObject.h"
#include "base/CCDirector.h"
#include "deprecated/CCString.h"

NS_CC_BEGIN
//
// Action Base Class
//

PhysicsAction::PhysicsAction()
:_originalTarget(nullptr)
,_target(nullptr)
,_tag(PhysicsAction::INVALID_TAG)
,_flags(0)
{
#if CC_ENABLE_SCRIPT_BINDING
    ScriptEngineProtocol* engine = ScriptEngineManager::getInstance()->getScriptEngine();
    _scriptType = engine != nullptr ? engine->getScriptType() : kScriptTypeNone;
#endif
}

PhysicsAction::~PhysicsAction()
{
    CCLOGINFO("deallocing PhysicsAction: %p - tag: %i", this, _tag);
}

std::string PhysicsAction::description() const
{
    return StringUtils::format("<PhysicsAction | Tag = %d", _tag);
}

void PhysicsAction::startWithTarget(PhysicsObject *aTarget)
{
    _originalTarget = _target = aTarget;
}

void PhysicsAction::stop()
{
    _target = nullptr;
}

bool PhysicsAction::isDone() const
{
    return true;
}

void PhysicsAction::step(float dt)
{
    CC_UNUSED_PARAM(dt);
    CCLOG("[PhysicsAction step]. override me");
}

void PhysicsAction::update(float time)
{
    CC_UNUSED_PARAM(time);
    CCLOG("[PhysicsAction update]. override me");
}

//
// SpeedPhysicsAction
//
SpeedPhysicsAction::SpeedPhysicsAction()
: _speed(0.0)
, _innerAction(nullptr)
{
}

SpeedPhysicsAction::~SpeedPhysicsAction()
{
    CC_SAFE_RELEASE(_innerAction);
}

SpeedPhysicsAction* SpeedPhysicsAction::create(PhysicsActionInterval* action, float speed)
{
    SpeedPhysicsAction *ret = new (std::nothrow) SpeedPhysicsAction();
    if (ret && ret->initWithAction(action, speed))
    {
        ret->autorelease();
        return ret;
    }
    CC_SAFE_DELETE(ret);
    return nullptr;
}

bool SpeedPhysicsAction::initWithAction(PhysicsActionInterval *action, float speed)
{
    CCASSERT(action != nullptr, "action must not be NULL");
    action->retain();
    _innerAction = action;
    _speed = speed;
    return true;
}

SpeedPhysicsAction *SpeedPhysicsAction::clone() const
{
    // no copy constructor
    auto a = new (std::nothrow) SpeedPhysicsAction();
    a->initWithAction(_innerAction->clone(), _speed);
    a->autorelease();
    return a;
}

void SpeedPhysicsAction::startWithTarget(PhysicsObject* target)
{
    PhysicsAction::startWithTarget(target);
    _innerAction->startWithTarget(target);
}

void SpeedPhysicsAction::stop()
{
    _innerAction->stop();
    PhysicsAction::stop();
}

void SpeedPhysicsAction::step(float dt)
{
    _innerAction->step(dt * _speed);
}

bool SpeedPhysicsAction::isDone() const
{
    return _innerAction->isDone();
}

SpeedPhysicsAction *SpeedPhysicsAction::reverse() const
{
    return SpeedPhysicsAction::create(_innerAction->reverse(), _speed);
}

void SpeedPhysicsAction::setInnerAction(PhysicsActionInterval *action)
{
    if (_innerAction != action)
    {
        CC_SAFE_RELEASE(_innerAction);
        _innerAction = action;
        CC_SAFE_RETAIN(_innerAction);
    }
}

//
// FollowPhysicsAction
//
FollowPhysicsAction::~FollowPhysicsAction()
{
    CC_SAFE_RELEASE(_followedNode);
}

FollowPhysicsAction* FollowPhysicsAction::create(PhysicsObject *followedNode, const Rect& rect/* = Rect::ZERO*/)
{
    FollowPhysicsAction *follow = new (std::nothrow) FollowPhysicsAction();
    if (follow && follow->initWithTarget(followedNode, rect))
    {
        follow->autorelease();
        return follow;
    }
    CC_SAFE_DELETE(follow);
    return nullptr;
}

FollowPhysicsAction* FollowPhysicsAction::clone() const
{
    // no copy constructor
    auto a = new (std::nothrow) FollowPhysicsAction();
    a->initWithTarget(_followedNode, _worldRect);
    a->autorelease();
    return a;
}

FollowPhysicsAction* FollowPhysicsAction::reverse() const
{
    return clone();
}

bool FollowPhysicsAction::initWithTarget(PhysicsObject *followedNode, const Rect& rect/* = Rect::ZERO*/)
{
    CCASSERT(followedNode != nullptr, "FollowedNode can't be NULL");
 
    followedNode->retain();
    _followedNode = followedNode;
    _worldRect = rect;
    _boundarySet = !rect.equals(Rect::ZERO);
    _boundaryFullyCovered = false;

    Size winSize = Director::getInstance()->getWinSize();
    _fullScreenSize.set(winSize.width, winSize.height);
    _halfScreenSize = _fullScreenSize * 0.5f;

    if (_boundarySet)
    {
        _leftBoundary = -((rect.origin.x+rect.size.width) - _fullScreenSize.x);
        _rightBoundary = -rect.origin.x ;
        _topBoundary = -rect.origin.y;
        _bottomBoundary = -((rect.origin.y+rect.size.height) - _fullScreenSize.y);

        if(_rightBoundary < _leftBoundary)
        {
            // screen width is larger than world's boundary width
            //set both in the middle of the world
            _rightBoundary = _leftBoundary = (_leftBoundary + _rightBoundary) / 2;
        }
        if(_topBoundary < _bottomBoundary)
        {
            // screen width is larger than world's boundary width
            //set both in the middle of the world
            _topBoundary = _bottomBoundary = (_topBoundary + _bottomBoundary) / 2;
        }

        if( (_topBoundary == _bottomBoundary) && (_leftBoundary == _rightBoundary) )
        {
            _boundaryFullyCovered = true;
        }
    }
    
    return true;
}

void FollowPhysicsAction::step(float dt)
{
    CC_UNUSED_PARAM(dt);

    if(_boundarySet)
    {
        // whole map fits inside a single screen, no need to modify the position - unless map boundaries are increased
        if(_boundaryFullyCovered)
        {
            return;
        }

//        Vec2 tempPos = _halfScreenSize - _followedNode->getPosition();

//        _target->setPosition(clampf(tempPos.x, _leftBoundary, _rightBoundary),
//                                   clampf(tempPos.y, _bottomBoundary, _topBoundary));
    }
    else
    {
//        _target->setPosition(_halfScreenSize - _followedNode->getPosition());
    }
}

bool FollowPhysicsAction::isDone() const
{
//    return false;( !_followedNode->isRunning() );
    return false;
}

void FollowPhysicsAction::stop()
{
    _target = nullptr;
    PhysicsAction::stop();
}

NS_CC_END


