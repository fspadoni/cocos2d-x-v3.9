/****************************************************************************
Copyright (c) 2008-2010 Ricardo Quesada
Copyright (c) 2010-2012 cocos2d-x.org
Copyright (c) 2011      Zynga Inc.
Copyright (c) 2013-2014 Chukong Technologies Inc.
 
http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/

#include "physics-actions/CCPhysicsActionInterval.h"

#include <stdarg.h>


#include "physics/CCPhysicsObject.h"

#include "2d/CCActionInstant.h"
#include "base/CCDirector.h"
#include "base/CCEventCustom.h"
#include "base/CCEventDispatcher.h"
#include "platform/CCStdC.h"
#include "base/CCScriptSupport.h"

NS_CC_BEGIN

// Extra action for making a PhysicsSequence or Spawn when only adding one action to it.
class ExtraPhysicsAction : public FiniteTimePhysicsAction
{
public:
    static ExtraPhysicsAction* create();
    virtual ExtraPhysicsAction* clone() const;
    virtual ExtraPhysicsAction* reverse(void) const;
    virtual void update(float time);
    virtual void step(float dt);
};

ExtraPhysicsAction* ExtraPhysicsAction::create()
{
    ExtraPhysicsAction* ret = new (std::nothrow) ExtraPhysicsAction();
    if (ret)
    {
        ret->autorelease();
    }
    return ret;
}
ExtraPhysicsAction* ExtraPhysicsAction::clone() const
{
    // no copy constructor
    auto a = new (std::nothrow) ExtraPhysicsAction();
    a->autorelease();
    return a;
}

ExtraPhysicsAction* ExtraPhysicsAction::reverse() const
{
    return ExtraPhysicsAction::create();
}

void ExtraPhysicsAction::update(float time)
{
    CC_UNUSED_PARAM(time);
}

void ExtraPhysicsAction::step(float dt)
{
    CC_UNUSED_PARAM(dt);
}

//
// IntervalAction
//

bool PhysicsActionInterval::initWithDuration(float d)
{
    _duration = d;

    // prevent division by 0
    // This comparison could be in step:, but it might decrease the performance
    // by 3% in heavy based action games.
    if (_duration == 0)
    {
        _duration = FLT_EPSILON;
    }

    _elapsed = 0;
    _firstTick = true;

    return true;
}

bool PhysicsActionInterval::sendUpdateEventToScript(float dt, PhysicsAction *actionObject)
{
#if CC_ENABLE_SCRIPT_BINDING
    if (_scriptType == kScriptTypeJavascript)
    {
//        if (ScriptEngineManager::sendActionEventToJS(actionObject, kPhysicsActionUpdate, (void *)&dt))
//            return true;
    }
#endif
    return false;
}

bool PhysicsActionInterval::isDone() const
{
    return _elapsed >= _duration;
}

void PhysicsActionInterval::step(float dt)
{
    if (_firstTick)
    {
        _firstTick = false;
        _elapsed = 0;
    }
    else
    {
        _elapsed += dt;
    }
    
    
    float updateDt = MAX (0,                                  // needed for rewind. elapsed could be negative
                           MIN(1, _elapsed /
                               MAX(_duration, FLT_EPSILON)   // division by 0
                               )
                           );

    if (sendUpdateEventToScript(updateDt, this)) return;
    
    this->update(updateDt);
}

void PhysicsActionInterval::setAmplitudeRate(float amp)
{
    CC_UNUSED_PARAM(amp);
    // Abstract class needs implementation
    CCASSERT(0, "Subclass should implement this method!");
}

float PhysicsActionInterval::getAmplitudeRate()
{
    // Abstract class needs implementation
    CCASSERT(0, "Subclass should implement this method!");

    return 0;
}

void PhysicsActionInterval::startWithTarget(PhysicsObject *target)
{
    FiniteTimePhysicsAction::startWithTarget(target);
    _elapsed = 0.0f;
    _firstTick = true;
}

//
// PhysicsSequence
//

PhysicsSequence* PhysicsSequence::createWithTwoActions(FiniteTimePhysicsAction *actionOne, FiniteTimePhysicsAction *actionTwo)
{
    PhysicsSequence *sequence = new (std::nothrow) PhysicsSequence();
    sequence->initWithTwoActions(actionOne, actionTwo);
    sequence->autorelease();

    return sequence;
}

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
PhysicsSequence* PhysicsSequence::variadicCreate(FiniteTimePhysicsAction *action1, ...)
{
    va_list params;
    va_start(params, action1);

    PhysicsSequence *ret = PhysicsSequence::createWithVariableList(action1, params);

    va_end(params);
    
    return ret;
}
#else
PhysicsSequence* PhysicsSequence::create(FiniteTimePhysicsAction *action1, ...)
{
    va_list params;
    va_start(params, action1);

    PhysicsSequence *ret = PhysicsSequence::createWithVariableList(action1, params);

    va_end(params);
    
    return ret;
}
#endif

PhysicsSequence* PhysicsSequence::createWithVariableList(FiniteTimePhysicsAction *action1, va_list args)
{
    FiniteTimePhysicsAction *now;
    FiniteTimePhysicsAction *prev = action1;
    bool bOneAction = true;

    while (action1)
    {
        now = va_arg(args, FiniteTimePhysicsAction*);
        if (now)
        {
            prev = createWithTwoActions(prev, now);
            bOneAction = false;
        }
        else
        {
            // If only one action is added to PhysicsSequence, make up a PhysicsSequence by adding a simplest finite time action.
            if (bOneAction)
            {
                prev = createWithTwoActions(prev, ExtraPhysicsAction::create());
            }
            break;
        }
    }
    
    return ((PhysicsSequence*)prev);
}

PhysicsSequence* PhysicsSequence::create(const Vector<FiniteTimePhysicsAction*>& arrayOfActions)
{
    PhysicsSequence* ret = nullptr;
    do 
    {
        auto count = arrayOfActions.size();
        CC_BREAK_IF(count == 0);

        auto prev = arrayOfActions.at(0);

        if (count > 1)
        {
            for (int i = 1; i < count; ++i)
            {
                prev = createWithTwoActions(prev, arrayOfActions.at(i));
            }
        }
        else
        {
            // If only one action is added to PhysicsSequence, make up a PhysicsSequence by adding a simplest finite time action.
            prev = createWithTwoActions(prev, ExtraPhysicsAction::create());
        }
        ret = static_cast<PhysicsSequence*>(prev);
    }while (0);
    return ret;
}

bool PhysicsSequence::initWithTwoActions(FiniteTimePhysicsAction *actionOne, FiniteTimePhysicsAction *actionTwo)
{
    CCASSERT(actionOne != nullptr, "actionOne can't be nullptr!");
    CCASSERT(actionTwo != nullptr, "actionTwo can't be nullptr!");

    float d = actionOne->getDuration() + actionTwo->getDuration();
    PhysicsActionInterval::initWithDuration(d);

    _actions[0] = actionOne;
    actionOne->retain();

    _actions[1] = actionTwo;
    actionTwo->retain();

    return true;
}

PhysicsSequence* PhysicsSequence::clone() const
{
    // no copy constructor
    auto a = new (std::nothrow) PhysicsSequence();
    a->initWithTwoActions(_actions[0]->clone(), _actions[1]->clone() );
    a->autorelease();
    return a;
}

PhysicsSequence::~PhysicsSequence(void)
{
    CC_SAFE_RELEASE(_actions[0]);
    CC_SAFE_RELEASE(_actions[1]);
}

void PhysicsSequence::startWithTarget(PhysicsObject *target)
{
    PhysicsActionInterval::startWithTarget(target);
    _split = _actions[0]->getDuration() / _duration;
    _last = -1;
}

void PhysicsSequence::stop(void)
{
    // Issue #1305
    if( _last != - 1)
    {
        _actions[_last]->stop();
    }

    PhysicsActionInterval::stop();
}

void PhysicsSequence::update(float t)
{
    int found = 0;
    float new_t = 0.0f;

    if( t < _split ) {
        // action[0]
        found = 0;
        if( _split != 0 )
            new_t = t / _split;
        else
            new_t = 1;

    } else {
        // action[1]
        found = 1;
        if ( _split == 1 )
            new_t = 1;
        else
            new_t = (t-_split) / (1 - _split );
    }

    if ( found==1 ) {

        if( _last == -1 ) {
            // action[0] was skipped, execute it.
            _actions[0]->startWithTarget(_target);
            if (!(sendUpdateEventToScript(1.0f, _actions[0])))
                _actions[0]->update(1.0f);
            _actions[0]->stop();
        }
        else if( _last == 0 )
        {
            // switching to action 1. stop action 0.
            if (!(sendUpdateEventToScript(1.0f, _actions[0])))
                _actions[0]->update(1.0f);
            _actions[0]->stop();
        }
    }
    else if(found==0 && _last==1 )
    {
        // Reverse mode ?
        // FIXME: Bug. this case doesn't contemplate when _last==-1, found=0 and in "reverse mode"
        // since it will require a hack to know if an action is on reverse mode or not.
        // "step" should be overridden, and the "reverseMode" value propagated to inner PhysicsSequences.
        if (!(sendUpdateEventToScript(0, _actions[1])))
            _actions[1]->update(0);
        _actions[1]->stop();
    }
    // Last action found and it is done.
    if( found == _last && _actions[found]->isDone() )
    {
        return;
    }

    // Last action found and it is done
    if( found != _last )
    {
        _actions[found]->startWithTarget(_target);
    }
    if (!(sendUpdateEventToScript(new_t, _actions[found])))
        _actions[found]->update(new_t);
    _last = found;
}

PhysicsSequence* PhysicsSequence::reverse() const
{
    return PhysicsSequence::createWithTwoActions(_actions[1]->reverse(), _actions[0]->reverse());
}

//
// PhysicsRepeat
//

PhysicsRepeat* PhysicsRepeat::create(FiniteTimePhysicsAction *action, unsigned int times)
{
    PhysicsRepeat* repeat = new (std::nothrow) PhysicsRepeat();
    repeat->initWithAction(action, times);
    repeat->autorelease();

    return repeat;
}

bool PhysicsRepeat::initWithAction(FiniteTimePhysicsAction *action, unsigned int times)
{
    float d = action->getDuration() * times;

    if (PhysicsActionInterval::initWithDuration(d))
    {
        _times = times;
        _innerAction = action;
        action->retain();

        _actionInstant = dynamic_cast<ActionInstant*>(action) ? true : false;
        //an instant action needs to be executed one time less in the update method since it uses startWithTarget to execute the action
        if (_actionInstant) 
        {
            _times -=1;
        }
        _total = 0;

        return true;
    }

    return false;
}

PhysicsRepeat* PhysicsRepeat::clone(void) const
{
    // no copy constructor
    auto a = new (std::nothrow) PhysicsRepeat();
    a->initWithAction( _innerAction->clone(), _times );
    a->autorelease();
    return a;
}

PhysicsRepeat::~PhysicsRepeat(void)
{
    CC_SAFE_RELEASE(_innerAction);
}

void PhysicsRepeat::startWithTarget(PhysicsObject *target)
{
    _total = 0;
    _nextDt = _innerAction->getDuration()/_duration;
    PhysicsActionInterval::startWithTarget(target);
    _innerAction->startWithTarget(target);
}

void PhysicsRepeat::stop(void)
{
    _innerAction->stop();
    PhysicsActionInterval::stop();
}

// issue #80. Instead of hooking step:, hook update: since it can be called by any 
// container action like PhysicsRepeat, PhysicsSequence, Ease, etc..
void PhysicsRepeat::update(float dt)
{
    if (dt >= _nextDt)
    {
        while (dt > _nextDt && _total < _times)
        {
            if (!(sendUpdateEventToScript(1.0f, _innerAction)))
                _innerAction->update(1.0f);
            _total++;

            _innerAction->stop();
            _innerAction->startWithTarget(_target);
            _nextDt = _innerAction->getDuration()/_duration * (_total+1);
        }

        // fix for issue #1288, incorrect end value of repeat
        if(dt >= 1.0f && _total < _times) 
        {
            _total++;
        }

        // don't set an instant action back or update it, it has no use because it has no duration
        if (!_actionInstant)
        {
            if (_total == _times)
            {
                if (!(sendUpdateEventToScript(1, _innerAction)))
                    _innerAction->update(1);
                _innerAction->stop();
            }
            else
            {
                // issue #390 prevent jerk, use right update
                if (!(sendUpdateEventToScript(dt - (_nextDt - _innerAction->getDuration()/_duration), _innerAction)))
                    _innerAction->update(dt - (_nextDt - _innerAction->getDuration()/_duration));
            }
        }
    }
    else
    {
        if (!(sendUpdateEventToScript(fmodf(dt * _times,1.0f), _innerAction)))
            _innerAction->update(fmodf(dt * _times,1.0f));
    }
}

bool PhysicsRepeat::isDone(void) const
{
    return _total == _times;
}

PhysicsRepeat* PhysicsRepeat::reverse() const
{
    return PhysicsRepeat::create(_innerAction->reverse(), _times);
}

//
// PhysicsRepeatForever
//
PhysicsRepeatForever::~PhysicsRepeatForever()
{
    CC_SAFE_RELEASE(_innerAction);
}

PhysicsRepeatForever *PhysicsRepeatForever::create(PhysicsActionInterval *action)
{
    PhysicsRepeatForever *ret = new (std::nothrow) PhysicsRepeatForever();
    if (ret && ret->initWithAction(action))
    {
        ret->autorelease();
        return ret;
    }
    CC_SAFE_DELETE(ret);
    return nullptr;
}

bool PhysicsRepeatForever::initWithAction(PhysicsActionInterval *action)
{
    CCASSERT(action != nullptr, "action can't be nullptr!");
    action->retain();
    _innerAction = action;
    return true;
}

PhysicsRepeatForever *PhysicsRepeatForever::clone() const
{
    // no copy constructor    
    auto a = new (std::nothrow) PhysicsRepeatForever();
    a->initWithAction(_innerAction->clone());
    a->autorelease();
    return a;
}

void PhysicsRepeatForever::startWithTarget(PhysicsObject* target)
{
    PhysicsActionInterval::startWithTarget(target);
    _innerAction->startWithTarget(target);
}

void PhysicsRepeatForever::step(float dt)
{
    _innerAction->step(dt);
    if (_innerAction->isDone())
    {
        float diff = _innerAction->getElapsed() - _innerAction->getDuration();
        if (diff > _innerAction->getDuration())
            diff = fmodf(diff, _innerAction->getDuration());
        _innerAction->startWithTarget(_target);
        // to prevent jerk. issue #390, 1247
        _innerAction->step(0.0f);
        _innerAction->step(diff);
    }
}

bool PhysicsRepeatForever::isDone() const
{
    return false;
}

PhysicsRepeatForever *PhysicsRepeatForever::reverse() const
{
    return PhysicsRepeatForever::create(_innerAction->reverse());
}

//
// PhysicsSpawn
//

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
PhysicsSpawn* PhysicsSpawn::variadicCreate(FiniteTimePhysicsAction *action1, ...)
{
    va_list params;
    va_start(params, action1);

    PhysicsSpawn *ret = PhysicsSpawn::createWithVariableList(action1, params);

    va_end(params);
    
    return ret;
}
#else
PhysicsSpawn* PhysicsSpawn::create(FiniteTimePhysicsAction *action1, ...)
{
    va_list params;
    va_start(params, action1);

    PhysicsSpawn *ret = PhysicsSpawn::createWithVariableList(action1, params);

    va_end(params);
    
    return ret;
}
#endif

PhysicsSpawn* PhysicsSpawn::createWithVariableList(FiniteTimePhysicsAction *action1, va_list args)
{
    FiniteTimePhysicsAction *now;
    FiniteTimePhysicsAction *prev = action1;
    bool oneAction = true;

    while (action1)
    {
        now = va_arg(args, FiniteTimePhysicsAction*);
        if (now)
        {
            prev = createWithTwoActions(prev, now);
            oneAction = false;
        }
        else
        {
            // If only one action is added to PhysicsSpawn, make up a PhysicsSpawn by adding a simplest finite time action.
            if (oneAction)
            {
                prev = createWithTwoActions(prev, ExtraPhysicsAction::create());
            }
            break;
        }
    }

    return ((PhysicsSpawn*)prev);
}

PhysicsSpawn* PhysicsSpawn::create(const Vector<FiniteTimePhysicsAction*>& arrayOfActions)
{
    PhysicsSpawn* ret = nullptr;
    do 
    {
        auto count = arrayOfActions.size();
        CC_BREAK_IF(count == 0);
        auto prev = arrayOfActions.at(0);
        if (count > 1)
        {
            for (int i = 1; i < arrayOfActions.size(); ++i)
            {
                prev = createWithTwoActions(prev, arrayOfActions.at(i));
            }
        }
        else
        {
            // If only one action is added to PhysicsSpawn, make up a PhysicsSpawn by adding a simplest finite time action.
            prev = createWithTwoActions(prev, ExtraPhysicsAction::create());
        }
        ret = static_cast<PhysicsSpawn*>(prev);
    }while (0);

    return ret;
}

PhysicsSpawn* PhysicsSpawn::createWithTwoActions(FiniteTimePhysicsAction *action1, FiniteTimePhysicsAction *action2)
{
    PhysicsSpawn *spawn = new (std::nothrow) PhysicsSpawn();
    spawn->initWithTwoActions(action1, action2);
    spawn->autorelease();

    return spawn;
}

bool PhysicsSpawn::initWithTwoActions(FiniteTimePhysicsAction *action1, FiniteTimePhysicsAction *action2)
{
    CCASSERT(action1 != nullptr, "action1 can't be nullptr!");
    CCASSERT(action2 != nullptr, "action2 can't be nullptr!");

    bool ret = false;

    float d1 = action1->getDuration();
    float d2 = action2->getDuration();

    if (PhysicsActionInterval::initWithDuration(MAX(d1, d2)))
    {
        _one = action1;
        _two = action2;

        if (d1 > d2)
        {
            _two = PhysicsSequence::createWithTwoActions(action2, PhysicsDelayTime::create(d1 - d2));
        } 
        else if (d1 < d2)
        {
            _one = PhysicsSequence::createWithTwoActions(action1, PhysicsDelayTime::create(d2 - d1));
        }

        _one->retain();
        _two->retain();

        ret = true;
    }

    return ret;
}

PhysicsSpawn* PhysicsSpawn::clone(void) const
{
    // no copy constructor    
    auto a = new (std::nothrow) PhysicsSpawn();
    a->initWithTwoActions(_one->clone(), _two->clone());

    a->autorelease();
    return a;
}

PhysicsSpawn::~PhysicsSpawn(void)
{
    CC_SAFE_RELEASE(_one);
    CC_SAFE_RELEASE(_two);
}

void PhysicsSpawn::startWithTarget(PhysicsObject *target)
{
    PhysicsActionInterval::startWithTarget(target);
    _one->startWithTarget(target);
    _two->startWithTarget(target);
}

void PhysicsSpawn::stop(void)
{
    _one->stop();
    _two->stop();
    PhysicsActionInterval::stop();
}

void PhysicsSpawn::update(float time)
{
    if (_one)
    {
        if (!(sendUpdateEventToScript(time, _one)))
            _one->update(time);
    }
    if (_two)
    {
        if (!(sendUpdateEventToScript(time, _two)))
            _two->update(time);
    }
}

PhysicsSpawn* PhysicsSpawn::reverse() const
{
    return PhysicsSpawn::createWithTwoActions(_one->reverse(), _two->reverse());
}

//
// PhysicsRotateTo
//

PhysicsRotateTo* PhysicsRotateTo::create(float duration, float dstAngle)
{
    PhysicsRotateTo* rotateTo = new (std::nothrow) PhysicsRotateTo();
    rotateTo->initWithDuration(duration, dstAngle, dstAngle);
    rotateTo->autorelease();

    return rotateTo;
}

PhysicsRotateTo* PhysicsRotateTo::create(float duration, float dstAngleX, float dstAngleY)
{
    PhysicsRotateTo* rotateTo = new (std::nothrow) PhysicsRotateTo();
    rotateTo->initWithDuration(duration, dstAngleX, dstAngleY);
    rotateTo->autorelease();
    
    return rotateTo;
}

PhysicsRotateTo* PhysicsRotateTo::create(float duration, const Vec3& dstAngle3D)
{
    PhysicsRotateTo* rotateTo = new (std::nothrow) PhysicsRotateTo();
    rotateTo->initWithDuration(duration, dstAngle3D);
    rotateTo->autorelease();
    
    return rotateTo;
}

PhysicsRotateTo::PhysicsRotateTo()
: _is3D(false)
{
}

bool PhysicsRotateTo::initWithDuration(float duration, float dstAngleX, float dstAngleY)
{
    if (PhysicsActionInterval::initWithDuration(duration))
    {
        _dstAngle.x = dstAngleX;
        _dstAngle.y = dstAngleY;
        
        return true;
    }
    
    return false;
}

bool PhysicsRotateTo::initWithDuration(float duration, const Vec3& dstAngle3D)
{
    if (PhysicsActionInterval::initWithDuration(duration))
    {
        _dstAngle = dstAngle3D;
        _is3D = true;
        
        return true;
    }
    
    return false;
}

PhysicsRotateTo* PhysicsRotateTo::clone(void) const
{
    // no copy constructor
    auto a = new (std::nothrow) PhysicsRotateTo();
    if(_is3D)
       a->initWithDuration(_duration, _dstAngle);
    else
        a->initWithDuration(_duration, _dstAngle.x, _dstAngle.y);
    a->autorelease();
    return a;
}

void PhysicsRotateTo::calculateAngles(float &startAngle, float &diffAngle, float dstAngle)
{
    if (startAngle > 0)
    {
        startAngle = fmodf(startAngle, 360.0f);
    }
    else
    {
        startAngle = fmodf(startAngle, -360.0f);
    }

    diffAngle = dstAngle - startAngle;
    if (diffAngle > 180)
    {
        diffAngle -= 360;
    }
    if (diffAngle < -180)
    {
        diffAngle += 360;
    }
}

void PhysicsRotateTo::startWithTarget(PhysicsObject *target)
{
    PhysicsActionInterval::startWithTarget(target);
    
    if (_is3D)
    {
        _startAngle = _target->getActionRotation();
    }
    else
    {
        _startAngle = _target->getActionRotation();
//        _startAngle.y = _target->getRotationY();
    }

    calculateAngles(_startAngle.x, _diffAngle.x, _dstAngle.x);
    calculateAngles(_startAngle.y, _diffAngle.y, _dstAngle.y);
    calculateAngles(_startAngle.z, _diffAngle.z, _dstAngle.z);
}

void PhysicsRotateTo::update(float time)
{
    if (_target)
    {
        if(_is3D)
        {
            _target->setActionRotation(Vec3(
                _startAngle.x + _diffAngle.x * time,
                _startAngle.y + _diffAngle.y * time,
                _startAngle.z + _diffAngle.z * time
            ));
        }
        else
        {
#if CC_USE_PHYSICS
            if (_startAngle.x == _startAngle.y && _diffAngle.x == _diffAngle.y)
            {
//                _target->setActionRotation(_startAngle.x + _diffAngle.x * time);
                _target->setActionRotation(_startAngle + _diffAngle * time);
            }
            else
            {
                _target->setActionRotation(_startAngle + _diffAngle * time);
//                _target->setRotationSkewX(_startAngle.x + _diffAngle.x * time);
//                _target->setRotationSkewY(_startAngle.y + _diffAngle.y * time);
            }
#else
            _target->setRotationSkewX(_startAngle.x + _diffAngle.x * time);
            _target->setRotationSkewY(_startAngle.y + _diffAngle.y * time);
#endif // CC_USE_PHYSICS
        }
    }
}

PhysicsRotateTo *PhysicsRotateTo::reverse() const
{
    CCASSERT(false, "PhysicsRotateTo doesn't support the 'reverse' method");
    return nullptr;
}

//
// PhysicsRotateBy
//

PhysicsRotateBy* PhysicsRotateBy::create(float duration, float deltaAngle)
{
    PhysicsRotateBy *rotateBy = new (std::nothrow) PhysicsRotateBy();
    rotateBy->initWithDuration(duration, deltaAngle);
    rotateBy->autorelease();

    return rotateBy;
}

PhysicsRotateBy* PhysicsRotateBy::create(float duration, float deltaAngleX, float deltaAngleY)
{
    PhysicsRotateBy *rotateBy = new (std::nothrow) PhysicsRotateBy();
    rotateBy->initWithDuration(duration, deltaAngleX, deltaAngleY);
    rotateBy->autorelease();
    
    return rotateBy;
}

PhysicsRotateBy* PhysicsRotateBy::create(float duration, const Vec3& deltaAngle3D)
{
    PhysicsRotateBy *rotateBy = new (std::nothrow) PhysicsRotateBy();
    rotateBy->initWithDuration(duration, deltaAngle3D);
    rotateBy->autorelease();

    return rotateBy;
}

PhysicsRotateBy::PhysicsRotateBy()
: _is3D(false)
{
}

bool PhysicsRotateBy::initWithDuration(float duration, float deltaAngle)
{
    if (PhysicsActionInterval::initWithDuration(duration))
    {
        _deltaAngle.x = _deltaAngle.y = deltaAngle;
        return true;
    }

    return false;
}

bool PhysicsRotateBy::initWithDuration(float duration, float deltaAngleX, float deltaAngleY)
{
    if (PhysicsActionInterval::initWithDuration(duration))
    {
        _deltaAngle.x = deltaAngleX;
        _deltaAngle.y = deltaAngleY;
        return true;
    }
    
    return false;
}

bool PhysicsRotateBy::initWithDuration(float duration, const Vec3& deltaAngle3D)
{
    if (PhysicsActionInterval::initWithDuration(duration))
    {
        _deltaAngle = deltaAngle3D;
        _is3D = true;
        return true;
    }

    return false;
}


PhysicsRotateBy* PhysicsRotateBy::clone() const
{
    // no copy constructor
    auto a = new (std::nothrow) PhysicsRotateBy();
    if(_is3D)
        a->initWithDuration(_duration, _deltaAngle);
    else
        a->initWithDuration(_duration, _deltaAngle.x, _deltaAngle.y);
    a->autorelease();
    return a;
}

void PhysicsRotateBy::startWithTarget(PhysicsObject *target)
{
    PhysicsActionInterval::startWithTarget(target);
    if(_is3D)
    {
//        _startAngle = target->getRotation3D();
        _startAngle = target->getActionRotation();
    }
    else
    {
//        _startAngle.x = target->getRotationSkewX();
//        _startAngle.y = target->getRotationSkewY();
        _startAngle = target->getActionRotation();
    }
}

void PhysicsRotateBy::update(float time)
{
    // FIXME: shall I add % 360
    if (_target)
    {
        if(_is3D)
        {
            Vec3 v;
            v.x = _startAngle.x + _deltaAngle.x * time;
            v.y = _startAngle.y + _deltaAngle.y * time;
            v.z = _startAngle.z + _deltaAngle.z * time;
//            _target->setRotation3D(v);
        }
        else
        {
#if CC_USE_PHYSICS
            if (_startAngle.x == _startAngle.y && _deltaAngle.x == _deltaAngle.y)
            {
//                _target->setRotation(_startAngle.x + _deltaAngle.x * time);
                _target->setActionRotation(_startAngle + _deltaAngle * time);
            }
            else
            {
//                _target->setRotationSkewX(_startAngle.x + _deltaAngle.x * time);
//                _target->setRotationSkewY(_startAngle.y + _deltaAngle.y * time);
                _target->setActionRotation(_startAngle + _deltaAngle * time);
            }
#else
//            _target->setRotationSkewX(_startAngle.x + _deltaAngle.x * time);
//            _target->setRotationSkewY(_startAngle.y + _deltaAngle.y * time);
#endif // CC_USE_PHYSICS
        }
    }
}

PhysicsRotateBy* PhysicsRotateBy::reverse() const
{
    if(_is3D)
    {
        Vec3 v;
        v.x = - _deltaAngle.x;
        v.y = - _deltaAngle.y;
        v.z = - _deltaAngle.z;
        return PhysicsRotateBy::create(_duration, v);
    }
    else
    {
        return PhysicsRotateBy::create(_duration, -_deltaAngle.x, -_deltaAngle.y);
    }
}

//
// PhysicsMoveBy
//

PhysicsMoveBy* PhysicsMoveBy::create(float duration, const Vec2& deltaPosition)
{
    return PhysicsMoveBy::create(duration, Vec3(deltaPosition.x, deltaPosition.y, 0));
}

PhysicsMoveBy* PhysicsMoveBy::create(float duration, const Vec3 &deltaPosition)
{
    PhysicsMoveBy *ret = new (std::nothrow) PhysicsMoveBy();
    
    if (ret)
    {
        if (ret->initWithDuration(duration, deltaPosition))
        {
            ret->autorelease();
        }
        else
        {
            delete ret;
            ret = nullptr;
        }
    }
    
    return ret;
}

bool PhysicsMoveBy::initWithDuration(float duration, const Vec2& deltaPosition)
{
    return PhysicsMoveBy::initWithDuration(duration, Vec3(deltaPosition.x, deltaPosition.y, 0));
}

bool PhysicsMoveBy::initWithDuration(float duration, const Vec3& deltaPosition)
{
    bool ret = false;
    
    if (PhysicsActionInterval::initWithDuration(duration))
    {
        _positionDelta = deltaPosition;
        _is3D = true;
        ret = true;
    }
    
    return ret;
}

PhysicsMoveBy* PhysicsMoveBy::clone() const
{
    // no copy constructor
    auto a = new (std::nothrow) PhysicsMoveBy();
    a->initWithDuration(_duration, _positionDelta);
    a->autorelease();
    return a;
}

void PhysicsMoveBy::startWithTarget(PhysicsObject *target)
{
    PhysicsActionInterval::startWithTarget(target);
    _previousPosition = _startPosition = target->getActionPosition();
}

PhysicsMoveBy* PhysicsMoveBy::reverse() const
{
    return PhysicsMoveBy::create(_duration, -_positionDelta);
}


void PhysicsMoveBy::update(float t)
{
    if (_target)
    {
#if CC_ENABLE_STACKABLE_ACTIONS
        Vec3 currentPos = _target->getActionPosition();
        Vec3 diff = currentPos - _previousPosition;
        _startPosition = _startPosition + diff;
        Vec3 newPos =  _startPosition + (_positionDelta * t);
        _target->setActionPosition(newPos);
        _previousPosition = newPos;
#else
        _target->setPosition3D(_startPosition + _positionDelta * t);
#endif // CC_ENABLE_STACKABLE_ACTIONS
    }
}


void PhysicsMoveBy::stop()
{
    if (_target)
    {
        _target->stopAction();
    }
    
    PhysicsActionInterval::stop();
}

//
// PhysicsMoveTo
//

PhysicsMoveTo* PhysicsMoveTo::create(float duration, const Vec2& position)
{
    return PhysicsMoveTo::create(duration, Vec3(position.x, position.y, 0));
}

PhysicsMoveTo* PhysicsMoveTo::create(float duration, const Vec3& position)
{
    PhysicsMoveTo *ret = new (std::nothrow) PhysicsMoveTo();
    
    if (ret)
    {
        if (ret->initWithDuration(duration, position))
        {
            ret->autorelease();
        }
        else
        {
            delete ret;
            ret = nullptr;
        }
    }
    
    return ret;
}

bool PhysicsMoveTo::initWithDuration(float duration, const Vec2& position)
{
    return initWithDuration(duration, Vec3(position.x, position.y, 0));
}

bool PhysicsMoveTo::initWithDuration(float duration, const Vec3& position)
{
    bool ret = false;
    
    if (PhysicsActionInterval::initWithDuration(duration))
    {
        _endPosition = position;
        ret = true;
    }
    
    return ret;
}

PhysicsMoveTo* PhysicsMoveTo::clone() const
{
    // no copy constructor
    auto a = new (std::nothrow) PhysicsMoveTo();
    a->initWithDuration(_duration, _endPosition);
    a->autorelease();
    return a;
}

void PhysicsMoveTo::startWithTarget(PhysicsObject *target)
{
    PhysicsMoveBy::startWithTarget(target);
    _positionDelta = _endPosition - target->getActionPosition();
}

PhysicsMoveTo* PhysicsMoveTo::reverse() const
{
    CCASSERT(false, "reverse() not supported in PhysicsMoveTo");
    return nullptr;
}


//
// PhysicsSkewTo
//
PhysicsSkewTo* PhysicsSkewTo::create(float t, float sx, float sy)
{
    PhysicsSkewTo *skewTo = new (std::nothrow) PhysicsSkewTo();
    if (skewTo)
    {
        if (skewTo->initWithDuration(t, sx, sy))
        {
            skewTo->autorelease();
        }
        else
        {
            CC_SAFE_DELETE(skewTo);
        }
    }

    return skewTo;
}

bool PhysicsSkewTo::initWithDuration(float t, float sx, float sy)
{
    bool bRet = false;

    if (PhysicsActionInterval::initWithDuration(t))
    {
        _endSkewX = sx;
        _endSkewY = sy;

        bRet = true;
    }

    return bRet;
}

PhysicsSkewTo* PhysicsSkewTo::clone() const
{
    // no copy constructor
    auto a = new (std::nothrow) PhysicsSkewTo();
    a->initWithDuration(_duration, _endSkewX, _endSkewY);
    a->autorelease();
    return a;
}

PhysicsSkewTo* PhysicsSkewTo::reverse() const
{
    CCASSERT(false, "reverse() not supported in PhysicsSkewTo");
    return nullptr;
}

void PhysicsSkewTo::startWithTarget(PhysicsObject *target)
{
    PhysicsActionInterval::startWithTarget(target);

//    _startSkewX = target->getSkewX();
//
//    if (_startSkewX > 0)
//    {
//        _startSkewX = fmodf(_startSkewX, 180.f);
//    }
//    else
//    {
//        _startSkewX = fmodf(_startSkewX, -180.f);
//    }
//
//    _deltaX = _endSkewX - _startSkewX;
//
//    if (_deltaX > 180)
//    {
//        _deltaX -= 360;
//    }
//    if (_deltaX < -180)
//    {
//        _deltaX += 360;
//    }
//
//    _startSkewY = target->getSkewY();
//
//    if (_startSkewY > 0)
//    {
//        _startSkewY = fmodf(_startSkewY, 360.f);
//    }
//    else
//    {
//        _startSkewY = fmodf(_startSkewY, -360.f);
//    }
//
//    _deltaY = _endSkewY - _startSkewY;
//
//    if (_deltaY > 180)
//    {
//        _deltaY -= 360;
//    }
//    if (_deltaY < -180)
//    {
//        _deltaY += 360;
//    }
}

void PhysicsSkewTo::update(float t)
{
//    _target->setSkewX(_startSkewX + _deltaX * t);
//    _target->setSkewY(_startSkewY + _deltaY * t);
}

PhysicsSkewTo::PhysicsSkewTo()
: _skewX(0.0)
, _skewY(0.0)
, _startSkewX(0.0)
, _startSkewY(0.0)
, _endSkewX(0.0)
, _endSkewY(0.0)
, _deltaX(0.0)
, _deltaY(0.0)
{
}

//
// PhysicsSkewBy
//
PhysicsSkewBy* PhysicsSkewBy::create(float t, float sx, float sy)
{
    PhysicsSkewBy *skewBy = new (std::nothrow) PhysicsSkewBy();
    if (skewBy)
    {
        if (skewBy->initWithDuration(t, sx, sy))
        {
            skewBy->autorelease();
        }
        else
        {
            CC_SAFE_DELETE(skewBy);
        }
    }

    return skewBy;
}

PhysicsSkewBy * PhysicsSkewBy::clone() const
{
    // no copy constructor
    auto a = new (std::nothrow) PhysicsSkewBy();
    a->initWithDuration(_duration, _skewX, _skewY);
    a->autorelease();
    return a;
}

bool PhysicsSkewBy::initWithDuration(float t, float deltaSkewX, float deltaSkewY)
{
    bool ret = false;

    if (PhysicsSkewTo::initWithDuration(t, deltaSkewX, deltaSkewY))
    {
        _skewX = deltaSkewX;
        _skewY = deltaSkewY;

        ret = true;
    }

    return ret;
}

void PhysicsSkewBy::startWithTarget(PhysicsObject *target)
{
    PhysicsSkewTo::startWithTarget(target);
    _deltaX = _skewX;
    _deltaY = _skewY;
    _endSkewX = _startSkewX + _deltaX;
    _endSkewY = _startSkewY + _deltaY;
}

PhysicsSkewBy* PhysicsSkewBy::reverse() const
{
    return PhysicsSkewBy::create(_duration, -_skewX, -_skewY);
}

//
// PhysicsJumpBy
//

PhysicsJumpBy* PhysicsJumpBy::create(float duration, const Vec2& position, float height, int jumps)
{
    PhysicsJumpBy *jumpBy = new (std::nothrow) PhysicsJumpBy();
    jumpBy->initWithDuration(duration, position, height, jumps);
    jumpBy->autorelease();

    return jumpBy;
}

bool PhysicsJumpBy::initWithDuration(float duration, const Vec2& position, float height, int jumps)
{
    CCASSERT(jumps>=0, "Number of jumps must be >= 0");
    
    if (PhysicsActionInterval::initWithDuration(duration) && jumps>=0)
    {
        _delta = position;
        _height = height;
        _jumps = jumps;

        return true;
    }

    return false;
}

PhysicsJumpBy* PhysicsJumpBy::clone() const
{
    // no copy constructor
    auto a = new (std::nothrow) PhysicsJumpBy();
    a->initWithDuration(_duration, _delta, _height, _jumps);
    a->autorelease();
    return a;
}

void PhysicsJumpBy::startWithTarget(PhysicsObject *target)
{
    PhysicsActionInterval::startWithTarget(target);
//    _previousPos = _startPosition = target->getPosition();
}

void PhysicsJumpBy::update(float t)
{
    // parabolic jump (since v0.8.2)
    if (_target)
    {
        float frac = fmodf( t * _jumps, 1.0f );
        float y = _height * 4 * frac * (1 - frac);
        y += _delta.y * t;

        float x = _delta.x * t;
#if CC_ENABLE_STACKABLE_ACTIONS
//        Vec2 currentPos = _target->getPosition();
//
//        Vec2 diff = currentPos - _previousPos;
//        _startPosition = diff + _startPosition;
//
//        Vec2 newPos = _startPosition + Vec2(x,y);
//        _target->setPosition(newPos);
//
//        _previousPos = newPos;
#else
        _target->setPosition(_startPosition + Vec2(x,y));
#endif // !CC_ENABLE_STACKABLE_ACTIONS
    }
}

PhysicsJumpBy* PhysicsJumpBy::reverse() const
{
    return PhysicsJumpBy::create(_duration, Vec2(-_delta.x, -_delta.y),
        _height, _jumps);
}

//
// PhysicsJumpTo
//

PhysicsJumpTo* PhysicsJumpTo::create(float duration, const Vec2& position, float height, int jumps)
{
    PhysicsJumpTo *jumpTo = new (std::nothrow) PhysicsJumpTo();
    jumpTo->initWithDuration(duration, position, height, jumps);
    jumpTo->autorelease();

    return jumpTo;
}

bool PhysicsJumpTo::initWithDuration(float duration, const Vec2& position, float height, int jumps)
{
    CCASSERT(jumps>=0, "Number of jumps must be >= 0");

    if (PhysicsActionInterval::initWithDuration(duration) && jumps>=0)
    {
        _endPosition = position;
        _height = height;
        _jumps = jumps;

        return true;
    }

    return false;
}

PhysicsJumpTo* PhysicsJumpTo::clone() const
{
    // no copy constructor
    auto a = new (std::nothrow) PhysicsJumpTo();
    a->initWithDuration(_duration, _endPosition, _height, _jumps);
    a->autorelease();
    return a;
}

PhysicsJumpTo* PhysicsJumpTo::reverse() const
{
    CCASSERT(false, "reverse() not supported in PhysicsJumpTo");
    return nullptr;
}

void PhysicsJumpTo::startWithTarget(PhysicsObject *target)
{
    PhysicsJumpBy::startWithTarget(target);
    _delta.set(_endPosition.x - _startPosition.x, _endPosition.y - _startPosition.y);
}

// Bezier cubic formula:
//    ((1 - t) + t)3 = 1 
// Expands to ...
//   (1 - t)3 + 3t(1-t)2 + 3t2(1 - t) + t3 = 1 
static inline float bezierat( float a, float b, float c, float d, float t )
{
    return (powf(1-t,3) * a + 
            3*t*(powf(1-t,2))*b + 
            3*powf(t,2)*(1-t)*c +
            powf(t,3)*d );
}

//
// PhysicsBezierBy
//

PhysicsBezierBy* PhysicsBezierBy::create(float t, const ccPhysicsBezierConfig& c)
{
    PhysicsBezierBy *bezierBy = new (std::nothrow) PhysicsBezierBy();
    bezierBy->initWithDuration(t, c);
    bezierBy->autorelease();

    return bezierBy;
}

bool PhysicsBezierBy::initWithDuration(float t, const ccPhysicsBezierConfig& c)
{
    if (PhysicsActionInterval::initWithDuration(t))
    {
        _config = c;
        return true;
    }

    return false;
}

void PhysicsBezierBy::startWithTarget(PhysicsObject *target)
{
    PhysicsActionInterval::startWithTarget(target);
//    _previousPosition = _startPosition = target->getPosition();
}

PhysicsBezierBy* PhysicsBezierBy::clone() const
{
    // no copy constructor
    auto a = new (std::nothrow) PhysicsBezierBy();
    a->initWithDuration(_duration, _config);
    a->autorelease();
    return a;
}

void PhysicsBezierBy::update(float time)
{
    if (_target)
    {
        float xa = 0;
        float xb = _config.controlPoint_1.x;
        float xc = _config.controlPoint_2.x;
        float xd = _config.endPosition.x;

        float ya = 0;
        float yb = _config.controlPoint_1.y;
        float yc = _config.controlPoint_2.y;
        float yd = _config.endPosition.y;

        float x = bezierat(xa, xb, xc, xd, time);
        float y = bezierat(ya, yb, yc, yd, time);

#if CC_ENABLE_STACKABLE_ACTIONS
//        Vec2 currentPos = _target->getPosition();
//        Vec2 diff = currentPos - _previousPosition;
//        _startPosition = _startPosition + diff;
//
//        Vec2 newPos = _startPosition + Vec2(x,y);
//        _target->setPosition(newPos);
//
//        _previousPosition = newPos;
#else
        _target->setPosition( _startPosition + Vec2(x,y));
#endif // !CC_ENABLE_STACKABLE_ACTIONS
    }
}

PhysicsBezierBy* PhysicsBezierBy::reverse() const
{
    ccPhysicsBezierConfig r;

    r.endPosition = -_config.endPosition;
    r.controlPoint_1 = _config.controlPoint_2 + (-_config.endPosition);
    r.controlPoint_2 = _config.controlPoint_1 + (-_config.endPosition);

    PhysicsBezierBy *action = PhysicsBezierBy::create(_duration, r);
    return action;
}

//
// PhysicsBezierTo
//

PhysicsBezierTo* PhysicsBezierTo::create(float t, const ccPhysicsBezierConfig& c)
{
    PhysicsBezierTo *bezierTo = new (std::nothrow) PhysicsBezierTo();
    bezierTo->initWithDuration(t, c);
    bezierTo->autorelease();

    return bezierTo;
}

bool PhysicsBezierTo::initWithDuration(float t, const ccPhysicsBezierConfig &c)
{
    if (PhysicsActionInterval::initWithDuration(t))
    {
        _toConfig = c;
        return true;
    }
    
    return false;
}

PhysicsBezierTo* PhysicsBezierTo::clone() const
{
    // no copy constructor
    auto a = new (std::nothrow) PhysicsBezierTo();
    a->initWithDuration(_duration, _toConfig);
    a->autorelease();
    return a;
}

void PhysicsBezierTo::startWithTarget(PhysicsObject *target)
{
    PhysicsBezierBy::startWithTarget(target);
    _config.controlPoint_1 = _toConfig.controlPoint_1 - _startPosition;
    _config.controlPoint_2 = _toConfig.controlPoint_2 - _startPosition;
    _config.endPosition = _toConfig.endPosition - _startPosition;
}

PhysicsBezierTo* PhysicsBezierTo::reverse() const
{
    CCASSERT(false, "CCPhysicsBezierTo doesn't support the 'reverse' method");
    return nullptr;
}


//
// PhysicsScaleTo
//
PhysicsScaleTo* PhysicsScaleTo::create(float duration, float s)
{
    PhysicsScaleTo *scaleTo = new (std::nothrow) PhysicsScaleTo();
    scaleTo->initWithDuration(duration, s);
    scaleTo->autorelease();

    return scaleTo;
}

PhysicsScaleTo* PhysicsScaleTo::create(float duration, float sx, float sy)
{
    PhysicsScaleTo *scaleTo = new (std::nothrow) PhysicsScaleTo();
    scaleTo->initWithDuration(duration, sx, sy);
    scaleTo->autorelease();

    return scaleTo;
}

PhysicsScaleTo* PhysicsScaleTo::create(float duration, float sx, float sy, float sz)
{
    PhysicsScaleTo *scaleTo = new (std::nothrow) PhysicsScaleTo();
    scaleTo->initWithDuration(duration, sx, sy, sz);
    scaleTo->autorelease();

    return scaleTo;
}

bool PhysicsScaleTo::initWithDuration(float duration, float s)
{
    if (PhysicsActionInterval::initWithDuration(duration))
    {
        _endScaleX = s;
        _endScaleY = s;
        _endScaleZ = s;

        return true;
    }

    return false;
}

bool PhysicsScaleTo::initWithDuration(float duration, float sx, float sy)
{
    if (PhysicsActionInterval::initWithDuration(duration))
    {
        _endScaleX = sx;
        _endScaleY = sy;
        _endScaleZ = 1.f;

        return true;
    }

    return false;
}

bool PhysicsScaleTo::initWithDuration(float duration, float sx, float sy, float sz)
{
    if (PhysicsActionInterval::initWithDuration(duration))
    {
        _endScaleX = sx;
        _endScaleY = sy;
        _endScaleZ = sz;

        return true;
    }

    return false;
}

PhysicsScaleTo* PhysicsScaleTo::clone() const
{
    // no copy constructor
    auto a = new (std::nothrow) PhysicsScaleTo();
    a->initWithDuration(_duration, _endScaleX, _endScaleY, _endScaleZ);
    a->autorelease();
    return a;
}

PhysicsScaleTo* PhysicsScaleTo::reverse() const
{
    CCASSERT(false, "reverse() not supported in PhysicsScaleTo");
    return nullptr;
}


void PhysicsScaleTo::startWithTarget(PhysicsObject *target)
{
    PhysicsActionInterval::startWithTarget(target);
//    _startScaleX = target->getScaleX();
//    _startScaleY = target->getScaleY();
//    _startScaleZ = target->getScaleZ();
//    _deltaX = _endScaleX - _startScaleX;
//    _deltaY = _endScaleY - _startScaleY;
//    _deltaZ = _endScaleZ - _startScaleZ;
}

void PhysicsScaleTo::update(float time)
{
    if (_target)
    {
//        _target->setScaleX(_startScaleX + _deltaX * time);
//        _target->setScaleY(_startScaleY + _deltaY * time);
//        _target->setScaleZ(_startScaleZ + _deltaZ * time);
    }
}

//
// PhysicsScaleBy
//

PhysicsScaleBy* PhysicsScaleBy::create(float duration, float s)
{
    PhysicsScaleBy *scaleBy = new (std::nothrow) PhysicsScaleBy();
    scaleBy->initWithDuration(duration, s);
    scaleBy->autorelease();

    return scaleBy;
}

PhysicsScaleBy* PhysicsScaleBy::create(float duration, float sx, float sy)
{
    PhysicsScaleBy *scaleBy = new (std::nothrow) PhysicsScaleBy();
    scaleBy->initWithDuration(duration, sx, sy, 1.f);
    scaleBy->autorelease();

    return scaleBy;
}

PhysicsScaleBy* PhysicsScaleBy::create(float duration, float sx, float sy, float sz)
{
    PhysicsScaleBy *scaleBy = new (std::nothrow) PhysicsScaleBy();
    scaleBy->initWithDuration(duration, sx, sy, sz);
    scaleBy->autorelease();

    return scaleBy;
}

PhysicsScaleBy* PhysicsScaleBy::clone() const
{
    // no copy constructor
    auto a = new (std::nothrow) PhysicsScaleBy();
    a->initWithDuration(_duration, _endScaleX, _endScaleY, _endScaleZ);
    a->autorelease();
    return a;
}

void PhysicsScaleBy::startWithTarget(PhysicsObject *target)
{
    PhysicsScaleTo::startWithTarget(target);
    _deltaX = _startScaleX * _endScaleX - _startScaleX;
    _deltaY = _startScaleY * _endScaleY - _startScaleY;
    _deltaZ = _startScaleZ * _endScaleZ - _startScaleZ;
}

PhysicsScaleBy* PhysicsScaleBy::reverse() const
{
    return PhysicsScaleBy::create(_duration, 1 / _endScaleX, 1 / _endScaleY, 1/ _endScaleZ);
}


//
// PhysicsDelayTime
//
PhysicsDelayTime* PhysicsDelayTime::create(float d)
{
    PhysicsDelayTime* action = new (std::nothrow) PhysicsDelayTime();

    action->initWithDuration(d);
    action->autorelease();

    return action;
}

PhysicsDelayTime* PhysicsDelayTime::clone() const
{
    // no copy constructor
    auto a = new (std::nothrow) PhysicsDelayTime();
    a->initWithDuration(_duration);
    a->autorelease();
    return a;
}

void PhysicsDelayTime::update(float time)
{
    CC_UNUSED_PARAM(time);
    return;
}

PhysicsDelayTime* PhysicsDelayTime::reverse() const
{
    return PhysicsDelayTime::create(_duration);
}

//
// PhysicsReverseTime
//

PhysicsReverseTime* PhysicsReverseTime::create(FiniteTimePhysicsAction *action)
{
    // casting to prevent warnings
    PhysicsReverseTime *reverseTime = new (std::nothrow) PhysicsReverseTime();
    reverseTime->initWithAction( action->clone() );
    reverseTime->autorelease();

    return reverseTime;
}

bool PhysicsReverseTime::initWithAction(FiniteTimePhysicsAction *action)
{
    CCASSERT(action != nullptr, "action can't be nullptr!");
    CCASSERT(action != _other, "action doesn't equal to _other!");

    if (PhysicsActionInterval::initWithDuration(action->getDuration()))
    {
        // Don't leak if action is reused
        CC_SAFE_RELEASE(_other);

        _other = action;
        action->retain();

        return true;
    }

    return false;
}

PhysicsReverseTime* PhysicsReverseTime::clone() const
{
    // no copy constructor
    auto a = new (std::nothrow) PhysicsReverseTime();
    a->initWithAction( _other->clone() );
    a->autorelease();
    return a;
}

PhysicsReverseTime::PhysicsReverseTime() : _other(nullptr)
{

}

PhysicsReverseTime::~PhysicsReverseTime()
{
    CC_SAFE_RELEASE(_other);
}

void PhysicsReverseTime::startWithTarget(PhysicsObject *target)
{
    PhysicsActionInterval::startWithTarget(target);
    _other->startWithTarget(target);
}

void PhysicsReverseTime::stop(void)
{
    _other->stop();
    PhysicsActionInterval::stop();
}

void PhysicsReverseTime::update(float time)
{
    if (_other)
    {
        if (!(sendUpdateEventToScript(1 - time, _other)))
            _other->update(1 - time);
    }
}

PhysicsReverseTime* PhysicsReverseTime::reverse() const
{
    // FIXME: This looks like a bug
    return (PhysicsReverseTime*)_other->clone();
}





// TargetedPhysicsAction

TargetedPhysicsAction::TargetedPhysicsAction()
: _action(nullptr)
, _forcedTarget(nullptr)
{

}

TargetedPhysicsAction::~TargetedPhysicsAction()
{
    CC_SAFE_RELEASE(_forcedTarget);
    CC_SAFE_RELEASE(_action);
}

TargetedPhysicsAction* TargetedPhysicsAction::create(Node* target, FiniteTimePhysicsAction* action)
{
    TargetedPhysicsAction* p = new (std::nothrow) TargetedPhysicsAction();
    p->initWithTarget(target, action);
    p->autorelease();
    return p;
}


bool TargetedPhysicsAction::initWithTarget(Node* target, FiniteTimePhysicsAction* action)
{
    if(PhysicsActionInterval::initWithDuration(action->getDuration()))
    {
        CC_SAFE_RETAIN(target);
        _forcedTarget = target;
        CC_SAFE_RETAIN(action);
        _action = action;
        return true;
    }
    return false;
}

TargetedPhysicsAction* TargetedPhysicsAction::clone() const
{
    // no copy constructor    
    auto a = new (std::nothrow) TargetedPhysicsAction();
    // win32 : use the _other's copy object.
    a->initWithTarget(_forcedTarget, _action->clone());
    a->autorelease();
    return a;
}

TargetedPhysicsAction* TargetedPhysicsAction::reverse() const
{
    // just reverse the internal action
    auto a = new (std::nothrow) TargetedPhysicsAction();
    a->initWithTarget(_forcedTarget, _action->reverse());
    a->autorelease();
    return a;
}

void TargetedPhysicsAction::startWithTarget(PhysicsObject *target)
{
    PhysicsActionInterval::startWithTarget(target);
//    _action->startWithTarget(_forcedTarget);
}

void TargetedPhysicsAction::stop()
{
    _action->stop();
}

void TargetedPhysicsAction::update(float time)
{
    if (!(sendUpdateEventToScript(time, _action)))
        _action->update(time);
}

bool TargetedPhysicsAction::isDone(void) const
{
    return _action->isDone();
}

void TargetedPhysicsAction::setForcedTarget(Node* forcedTarget)
{
    if( _forcedTarget != forcedTarget ) {
        CC_SAFE_RETAIN(forcedTarget);
        CC_SAFE_RELEASE(_forcedTarget);
        _forcedTarget = forcedTarget;
    }
}


NS_CC_END
