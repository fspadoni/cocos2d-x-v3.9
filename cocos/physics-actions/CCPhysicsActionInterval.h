/****************************************************************************
Copyright (c) 2008-2010 Ricardo Quesada
Copyright (c) 2011      Zynga Inc.
Copyright (c) 2010-2012 cocos2d-x.org
Copyright (c) 2013-2014 Chukong Technologies Inc.

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/

#ifndef __ACTION_CCPHYSICS_INTERVAL_ACTION_H__
#define __ACTION_CCPHYSICS_INTERVAL_ACTION_H__

#include <vector>

#include "physics-actions/CCPhysicsAction.h"
#include "2d/CCAnimation.h"
#include "base/CCProtocols.h"
#include "base/CCVector.h"

NS_CC_BEGIN

class Node;
class SpriteFrame;
class EventCustom;

/**
 * @addtogroup actions
 * @{
 */

/** @class PhysicsActionInterval
@brief An interval action is an action that takes place within a certain period of time.
It has an start time, and a finish time. The finish time is the parameter
duration plus the start time.

These PhysicsActionInterval actions have some interesting properties, like:
- They can run normally (default)
- They can run reversed with the reverse method
- They can run with the time altered with the Accelerate, AccelDeccel and Speed actions.

For example, you can simulate a Ping Pong effect running the action normally and
then running it again in Reverse mode.

Example:

Action *pingPongAction = PhysicsSequence::actions(action, action->reverse(), nullptr);
*/
class CC_DLL PhysicsActionInterval : public FiniteTimePhysicsAction
{
public:
    /** How many seconds had elapsed since the actions started to run.
     *
     * @return The seconds had elapsed since the actions started to run.
     */
    inline float getElapsed(void) { return _elapsed; }

    /** Sets the amplitude rate, extension in GridAction
     *
     * @param amp   The amplitude rate.
     */
    void setAmplitudeRate(float amp);
    
    /** Gets the amplitude rate, extension in GridAction
     *
     * @return  The amplitude rate.
     */
    float getAmplitudeRate(void);

    //
    // Overrides
    //
    virtual bool isDone(void) const override;
    /**
     * @param dt in seconds
     */
    virtual void step(float dt) override;
    virtual void startWithTarget(PhysicsObject *target) override;
    virtual PhysicsActionInterval* reverse() const override
    {
        CC_ASSERT(0);
        return nullptr;
    }

    virtual PhysicsActionInterval *clone() const override
    {
        CC_ASSERT(0);
        return nullptr;
    }

CC_CONSTRUCTOR_ACCESS:
    /** initializes the action */
    bool initWithDuration(float d);

protected:
    float _elapsed;
    bool   _firstTick;

protected:
    bool sendUpdateEventToScript(float dt, PhysicsAction *actionObject);
};

/** @class PhysicsSequence
 * @brief Runs actions sequentially, one after another.
 */
class CC_DLL PhysicsSequence : public PhysicsActionInterval
{
public:
    /** Helper constructor to create an array of sequenceable actions.
     *
     * @return An autoreleased Sequence object.
     */
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
    // VS2013 does not support nullptr in variable args lists and variadic templates are also not supported
    typedef FiniteTimePhysicsAction* M;
    static PhysicsSequence* create(M m1, std::nullptr_t listEnd) { return variadicCreate(m1, NULL); }
    static PhysicsSequence* create(M m1, M m2, std::nullptr_t listEnd) { return variadicCreate(m1, m2, NULL); }
    static PhysicsSequence* create(M m1, M m2, M m3, std::nullptr_t listEnd) { return variadicCreate(m1, m2, m3, NULL); }
    static PhysicsSequence* create(M m1, M m2, M m3, M m4, std::nullptr_t listEnd) { return variadicCreate(m1, m2, m3, m4, NULL); }
    static PhysicsSequence* create(M m1, M m2, M m3, M m4, M m5, std::nullptr_t listEnd) { return variadicCreate(m1, m2, m3, m4, m5, NULL); }
    static PhysicsSequence* create(M m1, M m2, M m3, M m4, M m5, M m6, std::nullptr_t listEnd) { return variadicCreate(m1, m2, m3, m4, m5, m6, NULL); }
    static PhysicsSequence* create(M m1, M m2, M m3, M m4, M m5, M m6, M m7, std::nullptr_t listEnd) { return variadicCreate(m1, m2, m3, m4, m5, m6, m7, NULL); }
    static PhysicsSequence* create(M m1, M m2, M m3, M m4, M m5, M m6, M m7, M m8, std::nullptr_t listEnd) { return variadicCreate(m1, m2, m3, m4, m5, m6, m7, m8, NULL); }
    static PhysicsSequence* create(M m1, M m2, M m3, M m4, M m5, M m6, M m7, M m8, M m9, std::nullptr_t listEnd) { return variadicCreate(m1, m2, m3, m4, m5, m6, m7, m8, m9, NULL); }
    static PhysicsSequence* create(M m1, M m2, M m3, M m4, M m5, M m6, M m7, M m8, M m9, M m10, std::nullptr_t listEnd) { return variadicCreate(m1, m2, m3, m4, m5, m6, m7, m8, m9, m10,  NULL); }

    // On WP8 for variable argument lists longer than 10 items, use the other create functions or variadicCreate with NULL as the last argument
    static PhysicsSequence* variadicCreate(FiniteTimePhysicsAction* item, ...);
#else
    static PhysicsSequence* create(FiniteTimePhysicsAction *action1, ...) CC_REQUIRES_NULL_TERMINATION;
#endif

    /** Helper constructor to create an array of sequenceable actions given an array.
     * @code
     * When this function bound to the js or lua,the input params changed
     * in js  :var   create(var   object1,var   object2, ...)
     * in lua :local create(local object1,local object2, ...)
     * @endcode
     *
     * @param arrayOfActions An array of sequenceable actions.
     * @return An autoreleased Sequence object.
     */
    static PhysicsSequence* create(const Vector<FiniteTimePhysicsAction*>& arrayOfActions);
    /** Helper constructor to create an array of sequence-able actions.
     *
     * @param action1 The first sequenceable action.
     * @param args The va_list variable.
     * @return An autoreleased Sequence object.
     * @js NA
     */
    static PhysicsSequence* createWithVariableList(FiniteTimePhysicsAction *action1, va_list args);
    /** Creates the action.
     * @param actionOne The first sequenceable action.
     * @param actionTwo The second sequenceable action.
     * @return An autoreleased Sequence object.
     * @js NA
     */
    static PhysicsSequence* createWithTwoActions(FiniteTimePhysicsAction *actionOne, FiniteTimePhysicsAction *actionTwo);

    //
    // Overrides
    //
    virtual PhysicsSequence* clone() const override;
    virtual PhysicsSequence* reverse() const override;
    virtual void startWithTarget(PhysicsObject *target) override;
    virtual void stop(void) override;
    /**
     * @param t In seconds.
     */
    virtual void update(float t) override;
    
CC_CONSTRUCTOR_ACCESS:
    PhysicsSequence() {}
    virtual ~PhysicsSequence(void);

    /** initializes the action */
    bool initWithTwoActions(FiniteTimePhysicsAction *pActionOne, FiniteTimePhysicsAction *pActionTwo);

protected:
    FiniteTimePhysicsAction *_actions[2];
    float _split;
    int _last;

private:
    CC_DISALLOW_COPY_AND_ASSIGN(PhysicsSequence);
};

/** @class PhysicsRepeat
 * @brief Repeats an action a number of times.
 * To repeat an action forever use the RepeatForever action.
 */
class CC_DLL PhysicsRepeat : public PhysicsActionInterval
{
public:
    /** Creates a Repeat action. Times is an unsigned integer between 1 and pow(2,30).
     *
     * @param action The action needs to repeat.
     * @param times The repeat times.
     * @return An autoreleased Repeat object.
     */
    static PhysicsRepeat* create(FiniteTimePhysicsAction *action, unsigned int times);

    /** Sets the inner action.
     *
     * @param action The inner action.
     */
    inline void setInnerAction(FiniteTimePhysicsAction *action)
    {
        if (_innerAction != action)
        {
            CC_SAFE_RETAIN(action);
            CC_SAFE_RELEASE(_innerAction);
            _innerAction = action;
        }
    }

    /** Gets the inner action.
     *
     * @return The inner action.
     */
    inline FiniteTimePhysicsAction* getInnerAction()
    {
        return _innerAction;
    }

    //
    // Overrides
    //
    virtual PhysicsRepeat* clone() const override;
    virtual PhysicsRepeat* reverse() const override;
    virtual void startWithTarget(PhysicsObject *target) override;
    virtual void stop(void) override;
    /**
     * @param dt In seconds.
     */
    virtual void update(float dt) override;
    virtual bool isDone(void) const override;
    
CC_CONSTRUCTOR_ACCESS:
    PhysicsRepeat() {}
    virtual ~PhysicsRepeat();

    /** initializes a Repeat action. Times is an unsigned integer between 1 and pow(2,30) */
    bool initWithAction(FiniteTimePhysicsAction *pAction, unsigned int times);

protected:
    unsigned int _times;
    unsigned int _total;
    float _nextDt;
    bool _actionInstant;
    /** Inner action */
    FiniteTimePhysicsAction *_innerAction;

private:
    CC_DISALLOW_COPY_AND_ASSIGN(PhysicsRepeat);
};

/** @class PhysicsRepeatForever
 * @brief Repeats an action for ever.
 To repeat the an action for a limited number of times use the Repeat action.
 * @warning This action can't be Sequenceable because it is not an IntervalAction.
 */
class CC_DLL PhysicsRepeatForever : public PhysicsActionInterval
{
public:
    /** Creates the action.
     *
     * @param action The action need to repeat forever.
     * @return An autoreleased RepeatForever object.
     */
    static PhysicsRepeatForever* create(PhysicsActionInterval *action);

    /** Sets the inner action.
     *
     * @param action The inner action.
     */
    inline void setInnerAction(PhysicsActionInterval *action)
    {
        if (_innerAction != action)
        {
            CC_SAFE_RELEASE(_innerAction);
            _innerAction = action;
            CC_SAFE_RETAIN(_innerAction);
        }
    }

    /** Gets the inner action.
     *
     * @return The inner action.
     */
    inline PhysicsActionInterval* getInnerAction()
    {
        return _innerAction;
    }

    //
    // Overrides
    //
    virtual PhysicsRepeatForever* clone() const override;
    virtual PhysicsRepeatForever* reverse(void) const override;
    virtual void startWithTarget(PhysicsObject* target) override;
    /**
     * @param dt In seconds.
     */
    virtual void step(float dt) override;
    virtual bool isDone(void) const override;
    
CC_CONSTRUCTOR_ACCESS:
    PhysicsRepeatForever()
    : _innerAction(nullptr)
    {}
    virtual ~PhysicsRepeatForever();

    /** initializes the action */
    bool initWithAction(PhysicsActionInterval *action);

protected:
    /** Inner action */
    PhysicsActionInterval *_innerAction;

private:
    CC_DISALLOW_COPY_AND_ASSIGN(PhysicsRepeatForever);
};

/** @class PhysicsSpawn
 * @brief Spawn a new action immediately
 */
class CC_DLL PhysicsSpawn : public PhysicsActionInterval
{
public:
    /** Helper constructor to create an array of spawned actions.
     * @code
     * When this function bound to the js or lua, the input params changed.
     * in js  :var   create(var   object1,var   object2, ...)
     * in lua :local create(local object1,local object2, ...)
     * @endcode
     *
     * @return An autoreleased PhysicsSpawn object.
     */
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
    // VS2013 does not support nullptr in variable args lists and variadic templates are also not supported.
    typedef FiniteTimePhysicsAction* M;
    static PhysicsSpawn* create(M m1, std::nullptr_t listEnd) { return variadicCreate(m1, NULL); }
    static PhysicsSpawn* create(M m1, M m2, std::nullptr_t listEnd) { return variadicCreate(m1, m2, NULL); }
    static PhysicsSpawn* create(M m1, M m2, M m3, std::nullptr_t listEnd) { return variadicCreate(m1, m2, m3, NULL); }
    static PhysicsSpawn* create(M m1, M m2, M m3, M m4, std::nullptr_t listEnd) { return variadicCreate(m1, m2, m3, m4, NULL); }
    static PhysicsSpawn* create(M m1, M m2, M m3, M m4, M m5, std::nullptr_t listEnd) { return variadicCreate(m1, m2, m3, m4, m5, NULL); }
    static PhysicsSpawn* create(M m1, M m2, M m3, M m4, M m5, M m6, std::nullptr_t listEnd) { return variadicCreate(m1, m2, m3, m4, m5, m6, NULL); }
    static PhysicsSpawn* create(M m1, M m2, M m3, M m4, M m5, M m6, M m7, std::nullptr_t listEnd) { return variadicCreate(m1, m2, m3, m4, m5, m6, m7, NULL); }
    static PhysicsSpawn* create(M m1, M m2, M m3, M m4, M m5, M m6, M m7, M m8, std::nullptr_t listEnd) { return variadicCreate(m1, m2, m3, m4, m5, m6, m7, m8, NULL); }
    static PhysicsSpawn* create(M m1, M m2, M m3, M m4, M m5, M m6, M m7, M m8, M m9, std::nullptr_t listEnd) { return variadicCreate(m1, m2, m3, m4, m5, m6, m7, m8, m9, NULL); }
    static PhysicsSpawn* create(M m1, M m2, M m3, M m4, M m5, M m6, M m7, M m8, M m9, M m10, std::nullptr_t listEnd) { return variadicCreate(m1, m2, m3, m4, m5, m6, m7, m8, m9, m10,  NULL); }

    // On WP8 for variable argument lists longer than 10 items, use the other create functions or createPhysicsSpawn with NULL as the last argument.
    static PhysicsSpawn* variadicCreate(FiniteTimePhysicsAction* item, ...);
#else
    static PhysicsSpawn* create(FiniteTimePhysicsAction *action1, ...) CC_REQUIRES_NULL_TERMINATION;
#endif

    /** Helper constructor to create an array of spawned actions. 
     *
     * @param action1   The first sequenceable action.
     * @param args  The va_list variable.
     * @return  An autoreleased PhysicsSpawn object.
     * @js NA
     */
    static PhysicsSpawn* createWithVariableList(FiniteTimePhysicsAction *action1, va_list args);

    /** Helper constructor to create an array of spawned actions given an array.
     *
     * @param arrayOfActions    An array of spawned actions.
     * @return  An autoreleased PhysicsSpawn object.
     */
    static PhysicsSpawn* create(const Vector<FiniteTimePhysicsAction*>& arrayOfActions);

    /** Creates the PhysicsSpawn action.
     *
     * @param action1   The first spawned action.
     * @param action2   THe second spawned action.
     * @return An autoreleased PhysicsSpawn object.
     * @js NA
     */
    static PhysicsSpawn* createWithTwoActions(FiniteTimePhysicsAction *action1, FiniteTimePhysicsAction *action2);

    //
    // Overrides
    //
    virtual PhysicsSpawn* clone() const override;
    virtual PhysicsSpawn* reverse(void) const override;
    virtual void startWithTarget(PhysicsObject *target) override;
    virtual void stop(void) override;
    /**
     * @param time In seconds.
     */
    virtual void update(float time) override;
    
CC_CONSTRUCTOR_ACCESS:
    PhysicsSpawn() {}
    virtual ~PhysicsSpawn();

    /** initializes the PhysicsSpawn action with the 2 actions to spawn */
    bool initWithTwoActions(FiniteTimePhysicsAction *action1, FiniteTimePhysicsAction *action2);

protected:
    FiniteTimePhysicsAction *_one;
    FiniteTimePhysicsAction *_two;

private:
    CC_DISALLOW_COPY_AND_ASSIGN(PhysicsSpawn);
};

/** @class PhysicsRotateTo
 * @brief Rotates a Node object to a certain angle by modifying it's rotation attribute.
 The direction will be decided by the shortest angle.
*/ 
class CC_DLL PhysicsRotateTo : public PhysicsActionInterval
{
public:
    /** 
     * Creates the action with separate rotation angles.
     *
     * @param duration Duration time, in seconds.
     * @param dstAngleX In degreesCW.
     * @param dstAngleY In degreesCW.
     * @return An autoreleased PhysicsRotateTo object.
     */
    static PhysicsRotateTo* create(float duration, float dstAngleX, float dstAngleY);

    /** 
     * Creates the action.
     *
     * @param duration Duration time, in seconds.
     * @param dstAngle In degreesCW.
     * @return An autoreleased PhysicsRotateTo object.
     */
    static PhysicsRotateTo* create(float duration, float dstAngle);

    /** 
     * Creates the action with 3D rotation angles.
     * @param duration Duration time, in seconds.
     * @param dstAngle3D A Vec3 angle.
     * @return An autoreleased PhysicsRotateTo object.
     */
    static PhysicsRotateTo* create(float duration, const Vec3& dstAngle3D);

    //
    // Overrides
    //
    virtual PhysicsRotateTo* clone() const override;
    virtual PhysicsRotateTo* reverse() const override;
    virtual void startWithTarget(PhysicsObject *target) override;
    /**
     * @param time In seconds.
     */
    virtual void update(float time) override;
    
CC_CONSTRUCTOR_ACCESS:
    PhysicsRotateTo();
    virtual ~PhysicsRotateTo() {}

    /** 
     * initializes the action
     * @param duration in seconds
     * @param dstAngleX in degreesCW
     * @param dstAngleY in degreesCW
     */
    bool initWithDuration(float duration, float dstAngleX, float dstAngleY);
    /**
     * initializes the action
     * @param duration in seconds
     */
    bool initWithDuration(float duration, const Vec3& dstAngle3D);

    /** 
     * calculates the start and diff angles
     * @param dstAngle in degreesCW
     */
    void calculateAngles(float &startAngle, float &diffAngle, float dstAngle);
    
protected:
    bool _is3D;
    Vec3 _dstAngle;
    Vec3 _startAngle;
    Vec3 _diffAngle;

private:
    CC_DISALLOW_COPY_AND_ASSIGN(PhysicsRotateTo);
};

/** @class PhysicsRotateBy
 * @brief Rotates a Node object clockwise a number of degrees by modifying it's rotation attribute.
*/
class CC_DLL PhysicsRotateBy : public PhysicsActionInterval
{
public:
    /** 
     * Creates the action.
     *
     * @param duration Duration time, in seconds.
     * @param deltaAngle In degreesCW.
     * @return An autoreleased PhysicsRotateBy object.
     */
    static PhysicsRotateBy* create(float duration, float deltaAngle);
    /**
     * Creates the action with separate rotation angles.
     *
     * @param duration Duration time, in seconds.
     * @param deltaAngleZ_X In degreesCW.
     * @param deltaAngleZ_Y In degreesCW.
     * @return An autoreleased PhysicsRotateBy object.
     * @warning The physics body contained in Node doesn't support rotate with different x and y angle.
     */
    static PhysicsRotateBy* create(float duration, float deltaAngleZ_X, float deltaAngleZ_Y);
    /** Creates the action with 3D rotation angles.
     *
     * @param duration Duration time, in seconds.
     * @param deltaAngle3D A Vec3 angle.
     * @return An autoreleased PhysicsRotateBy object.
     */
    static PhysicsRotateBy* create(float duration, const Vec3& deltaAngle3D);

    //
    // Override
    //
    virtual PhysicsRotateBy* clone() const override;
    virtual PhysicsRotateBy* reverse(void) const override;
    virtual void startWithTarget(PhysicsObject *target) override;
    /**
     * @param time In seconds.
     */
    virtual void update(float time) override;
    
CC_CONSTRUCTOR_ACCESS:
    PhysicsRotateBy();
    virtual ~PhysicsRotateBy() {}

    /** initializes the action */
    bool initWithDuration(float duration, float deltaAngle);
    /** 
     * @warning The physics body contained in Node doesn't support rotate with different x and y angle.
     * @param deltaAngleZ_X in degreesCW
     * @param deltaAngleZ_Y in degreesCW
     */
    bool initWithDuration(float duration, float deltaAngleZ_X, float deltaAngleZ_Y);
    bool initWithDuration(float duration, const Vec3& deltaAngle3D);
    
protected:
    bool _is3D;
    Vec3 _deltaAngle;
    Vec3 _startAngle;

private:
    CC_DISALLOW_COPY_AND_ASSIGN(PhysicsRotateBy);
};

/** @class PhysicsMoveBy
 * @brief Moves a Node object x,y pixels by modifying it's position attribute.
 x and y are relative to the position of the object.
 Several PhysicsMoveBy actions can be concurrently called, and the resulting
 movement will be the sum of individual movements.
 @since v2.1beta2-custom
 */
class CC_DLL PhysicsMoveBy : public PhysicsActionInterval
{
public:
    /** 
     * Creates the action.
     *
     * @param duration Duration time, in seconds.
     * @param deltaPosition The delta distance in 2d, it's a Vec2 type.
     * @return An autoreleased PhysicsMoveBy object.
     */
    static PhysicsMoveBy* create(float duration, const Vec2& deltaPosition);
    /**
     * Creates the action.
     *
     * @param duration Duration time, in seconds.
     * @param deltaPosition The delta distance in 3d, it's a Vec3 type.
     * @return An autoreleased PhysicsMoveBy object.
     */
    static PhysicsMoveBy* create(float duration, const Vec3& deltaPosition);

    //
    // Overrides
    //
    virtual PhysicsMoveBy* clone() const override;
    virtual PhysicsMoveBy* reverse(void) const  override;
    virtual void startWithTarget(PhysicsObject *target) override;
    /**
     * @param time in seconds
     */
    virtual void update(float time) override;
    
    virtual void stop() override;
    
    
CC_CONSTRUCTOR_ACCESS:
    PhysicsMoveBy():_is3D(false) {}
    virtual ~PhysicsMoveBy() {}

    /** initializes the action */
    bool initWithDuration(float duration, const Vec2& deltaPosition);
    bool initWithDuration(float duration, const Vec3& deltaPosition);

protected:
    bool _is3D;
    Vec3 _positionDelta;
    Vec3 _startPosition;
    Vec3 _previousPosition;

private:
    CC_DISALLOW_COPY_AND_ASSIGN(PhysicsMoveBy);
};

/** @class PhysicsMoveTo
 * @brief Moves a Node object to the position x,y. x and y are absolute coordinates by modifying it's position attribute.
 Several PhysicsMoveTo actions can be concurrently called, and the resulting
 movement will be the sum of individual movements.
 @since v2.1beta2-custom
 */
class CC_DLL PhysicsMoveTo : public PhysicsMoveBy
{
public:
    /** 
     * Creates the action.
     * @param duration Duration time, in seconds.
     * @param position The destination position in 2d.
     * @return An autoreleased PhysicsMoveTo object.
     */
    static PhysicsMoveTo* create(float duration, const Vec2& position);
    /**
     * Creates the action.
     * @param duration Duration time, in seconds.
     * @param position The destination position in 3d.
     * @return An autoreleased PhysicsMoveTo object.
     */
    static PhysicsMoveTo* create(float duration, const Vec3& position);

    //
    // Overrides
    //
    virtual PhysicsMoveTo* clone() const override;
    virtual PhysicsMoveTo* reverse() const  override;
    virtual void startWithTarget(PhysicsObject *target) override;
    
CC_CONSTRUCTOR_ACCESS:
    PhysicsMoveTo() {}
    virtual ~PhysicsMoveTo() {}

    /** 
     * initializes the action
     * @param duration in seconds
     */
    bool initWithDuration(float duration, const Vec2& position);
    /**
     * initializes the action
     * @param duration in seconds
     */
    bool initWithDuration(float duration, const Vec3& position);

protected:
    Vec3 _endPosition;

private:
    CC_DISALLOW_COPY_AND_ASSIGN(PhysicsMoveTo);
};

/** @class PhysicsSkewTo
 * @brief Skews a Node object to given angles by modifying it's skewX and skewY attributes
@since v1.0
*/
class CC_DLL PhysicsSkewTo : public PhysicsActionInterval
{
public:
    /** 
     * Creates the action.
     * @param t Duration time, in seconds.
     * @param sx Skew x angle.
     * @param sy Skew y angle.
     * @return An autoreleased PhysicsSkewTo object.
     */
    static PhysicsSkewTo* create(float t, float sx, float sy);

    //
    // Overrides
    //
    virtual PhysicsSkewTo* clone() const override;
    virtual PhysicsSkewTo* reverse(void) const override;
    virtual void startWithTarget(PhysicsObject *target) override;
    /**
     * @param time In seconds.
     */
    virtual void update(float time) override;
    
CC_CONSTRUCTOR_ACCESS:
    PhysicsSkewTo();
    virtual ~PhysicsSkewTo() {}
    /**
     * @param t In seconds.
     */
    bool initWithDuration(float t, float sx, float sy);

protected:
    float _skewX;
    float _skewY;
    float _startSkewX;
    float _startSkewY;
    float _endSkewX;
    float _endSkewY;
    float _deltaX;
    float _deltaY;

private:
    CC_DISALLOW_COPY_AND_ASSIGN(PhysicsSkewTo);
};

/** @class PhysicsSkewBy
* @brief Skews a Node object by skewX and skewY degrees.
@since v1.0
*/
class CC_DLL PhysicsSkewBy : public PhysicsSkewTo
{
public:
    /** 
     * Creates the action.
     * @param t Duration time, in seconds.
     * @param deltaSkewX Skew x delta angle.
     * @param deltaSkewY Skew y delta angle.
     * @return An autoreleased PhysicsSkewBy object.
     */
    static PhysicsSkewBy* create(float t, float deltaSkewX, float deltaSkewY);

    //
    // Overrides
    //
    virtual void startWithTarget(PhysicsObject *target) override;
    virtual PhysicsSkewBy* clone() const  override;
    virtual PhysicsSkewBy* reverse(void) const override;
    
CC_CONSTRUCTOR_ACCESS:
    PhysicsSkewBy() {}
    virtual ~PhysicsSkewBy() {}
    /**
     * @param t In seconds.
     */
    bool initWithDuration(float t, float sx, float sy);

private:
    CC_DISALLOW_COPY_AND_ASSIGN(PhysicsSkewBy);
};

/** @class PhysicsJumpBy
 * @brief Moves a Node object simulating a parabolic jump movement by modifying it's position attribute.
*/
class CC_DLL PhysicsJumpBy : public PhysicsActionInterval
{
public:
    /** 
     * Creates the action.
     * @param duration Duration time, in seconds.
     * @param position The jumping distance.
     * @param height The jumping height.
     * @param jumps The jumping times.
     * @return An autoreleased PhysicsJumpBy object.
     */
    static PhysicsJumpBy* create(float duration, const Vec2& position, float height, int jumps);

    //
    // Overrides
    //
    virtual PhysicsJumpBy* clone() const override;
    virtual PhysicsJumpBy* reverse(void) const override;
    virtual void startWithTarget(PhysicsObject *target) override;
    /**
     * @param time In seconds.
     */
    virtual void update(float time) override;
    
CC_CONSTRUCTOR_ACCESS:
    PhysicsJumpBy() {}
    virtual ~PhysicsJumpBy() {}

    /** 
     * initializes the action
     * @param duration in seconds
     */
    bool initWithDuration(float duration, const Vec2& position, float height, int jumps);

protected:
    Vec2           _startPosition;
    Vec2           _delta;
    float           _height;
    int             _jumps;
    Vec2           _previousPos;

private:
    CC_DISALLOW_COPY_AND_ASSIGN(PhysicsJumpBy);
};

/** @class PhysicsJumpTo
 * @brief Moves a Node object to a parabolic position simulating a jump movement by modifying it's position attribute.
*/ 
class CC_DLL PhysicsJumpTo : public PhysicsJumpBy
{
public:
    /** 
     * Creates the action.
     * @param duration Duration time, in seconds.
     * @param position The jumping destination position.
     * @param height The jumping height.
     * @param jumps The jumping times.
     * @return An autoreleased PhysicsJumpTo object.
     */
    static PhysicsJumpTo* create(float duration, const Vec2& position, float height, int jumps);

    //
    // Override
    //
    virtual void startWithTarget(PhysicsObject *target) override;
    virtual PhysicsJumpTo* clone() const override;
    virtual PhysicsJumpTo* reverse(void) const override;

CC_CONSTRUCTOR_ACCESS:
    PhysicsJumpTo() {}
    virtual ~PhysicsJumpTo() {}

    /** 
     * initializes the action
     * @param duration In seconds.
     */
    bool initWithDuration(float duration, const Vec2& position, float height, int jumps);

protected:
    Vec2 _endPosition;

private:
    CC_DISALLOW_COPY_AND_ASSIGN(PhysicsJumpTo);
};

/** @struct Bezier configuration structure
 */
typedef struct _ccPhysicsBezierConfig {
    //! end position of the bezier
    Vec2 endPosition;
    //! Bezier control point 1
    Vec2 controlPoint_1;
    //! Bezier control point 2
    Vec2 controlPoint_2;
} ccPhysicsBezierConfig;

/** @class PhysicsBezierBy
 * @brief An action that moves the target with a cubic Bezier curve by a certain distance.
 */
class CC_DLL PhysicsBezierBy : public PhysicsActionInterval
{
public:
    /** Creates the action with a duration and a bezier configuration.
     * @param t Duration time, in seconds.
     * @param c Bezier config.
     * @return An autoreleased PhysicsBezierBy object.
     * @code
     * When this function bound to js or lua,the input params are changed.
     * in js: var create(var t,var table)
     * in lua: lcaol create(local t, local table)
     * @endcode
     */
    static PhysicsBezierBy* create(float t, const ccPhysicsBezierConfig& c);

    //
    // Overrides
    //
    virtual PhysicsBezierBy* clone() const override;
    virtual PhysicsBezierBy* reverse(void) const override;
    virtual void startWithTarget(PhysicsObject *target) override;
    /**
     * @param time In seconds.
     */
    virtual void update(float time) override;
    
CC_CONSTRUCTOR_ACCESS:
    PhysicsBezierBy() {}
    virtual ~PhysicsBezierBy() {}

    /** 
     * initializes the action with a duration and a bezier configuration
     * @param t in seconds
     */
    bool initWithDuration(float t, const ccPhysicsBezierConfig& c);

protected:
    ccPhysicsBezierConfig _config;
    Vec2 _startPosition;
    Vec2 _previousPosition;

private:
    CC_DISALLOW_COPY_AND_ASSIGN(PhysicsBezierBy);
};

/** @class PhysicsBezierTo
 * @brief An action that moves the target with a cubic Bezier curve to a destination point.
 @since v0.8.2
 */
class CC_DLL PhysicsBezierTo : public PhysicsBezierBy
{
public:
    /** Creates the action with a duration and a bezier configuration.
     * @param t Duration time, in seconds.
     * @param c Bezier config.
     * @return An autoreleased PhysicsBezierTo object.
     * @code
     * when this function bound to js or lua,the input params are changed
     * in js: var create(var t,var table)
     * in lua: lcaol create(local t, local table)
     * @endcode
     */
    static PhysicsBezierTo* create(float t, const ccPhysicsBezierConfig& c);

    //
    // Overrides
    //
    virtual void startWithTarget(PhysicsObject *target) override;
    virtual PhysicsBezierTo* clone() const override;
    virtual PhysicsBezierTo* reverse(void) const override;
    
CC_CONSTRUCTOR_ACCESS:
    PhysicsBezierTo() {}
    virtual ~PhysicsBezierTo() {}
    /**
     * @param t In seconds.
     */
    bool initWithDuration(float t, const ccPhysicsBezierConfig &c);

protected:
    ccPhysicsBezierConfig _toConfig;

private:
    CC_DISALLOW_COPY_AND_ASSIGN(PhysicsBezierTo);
};

/** @class PhysicsScaleTo
 @brief Scales a Node object to a zoom factor by modifying it's scale attribute.
 @warning This action doesn't support "reverse".
 @warning The physics body contained in Node doesn't support this action.
 */
class CC_DLL PhysicsScaleTo : public PhysicsActionInterval
{
public:
    /** 
     * Creates the action with the same scale factor for X and Y.
     * @param duration Duration time, in seconds.
     * @param s Scale factor of x and y.
     * @return An autoreleased PhysicsScaleTo object.
     */
    static PhysicsScaleTo* create(float duration, float s);

    /** 
     * Creates the action with and X factor and a Y factor.
     * @param duration Duration time, in seconds.
     * @param sx Scale factor of x.
     * @param sy Scale factor of y.
     * @return An autoreleased PhysicsScaleTo object.
     */
    static PhysicsScaleTo* create(float duration, float sx, float sy);

    /** 
     * Creates the action with X Y Z factor.
     * @param duration Duration time, in seconds.
     * @param sx Scale factor of x.
     * @param sy Scale factor of y.
     * @param sz Scale factor of z.
     * @return An autoreleased PhysicsScaleTo object.
     */
    static PhysicsScaleTo* create(float duration, float sx, float sy, float sz);

    //
    // Overrides
    //
    virtual PhysicsScaleTo* clone() const override;
    virtual PhysicsScaleTo* reverse(void) const override;
    virtual void startWithTarget(PhysicsObject *target) override;
    /**
     * @param time In seconds.
     */
    virtual void update(float time) override;
    
CC_CONSTRUCTOR_ACCESS:
    PhysicsScaleTo() {}
    virtual ~PhysicsScaleTo() {}

    /** 
     * initializes the action with the same scale factor for X and Y
     * @param duration in seconds
     */
    bool initWithDuration(float duration, float s);
    /** 
     * initializes the action with and X factor and a Y factor 
     * @param duration in seconds
     */
    bool initWithDuration(float duration, float sx, float sy);
    /** 
     * initializes the action with X Y Z factor 
     * @param duration in seconds
     */
    bool initWithDuration(float duration, float sx, float sy, float sz);

protected:
    float _scaleX;
    float _scaleY;
    float _scaleZ;
    float _startScaleX;
    float _startScaleY;
    float _startScaleZ;
    float _endScaleX;
    float _endScaleY;
    float _endScaleZ;
    float _deltaX;
    float _deltaY;
    float _deltaZ;

private:
    CC_DISALLOW_COPY_AND_ASSIGN(PhysicsScaleTo);
};

/** @class PhysicsScaleBy
 * @brief Scales a Node object a zoom factor by modifying it's scale attribute.
 @warning The physics body contained in Node doesn't support this action.
*/
class CC_DLL PhysicsScaleBy : public PhysicsScaleTo
{
public:
    /** 
     * Creates the action with the same scale factor for X and Y.
     * @param duration Duration time, in seconds.
     * @param s Scale factor of x and y.
     * @return An autoreleased PhysicsScaleBy object.
     */
    static PhysicsScaleBy* create(float duration, float s);

    /** 
     * Creates the action with and X factor and a Y factor.
     * @param duration Duration time, in seconds.
     * @param sx Scale factor of x.
     * @param sy Scale factor of y.
     * @return An autoreleased PhysicsScaleBy object.
     */
    static PhysicsScaleBy* create(float duration, float sx, float sy);

    /** 
     * Creates the action with X Y Z factor.
     * @param duration Duration time, in seconds.
     * @param sx Scale factor of x.
     * @param sy Scale factor of y.
     * @param sz Scale factor of z.
     * @return An autoreleased PhysicsScaleBy object.
     */
    static PhysicsScaleBy* create(float duration, float sx, float sy, float sz);

    //
    // Overrides
    //
    virtual void startWithTarget(PhysicsObject *target) override;
    virtual PhysicsScaleBy* clone() const override;
    virtual PhysicsScaleBy* reverse(void) const override;

CC_CONSTRUCTOR_ACCESS:
    PhysicsScaleBy() {}
    virtual ~PhysicsScaleBy() {}

private:
    CC_DISALLOW_COPY_AND_ASSIGN(PhysicsScaleBy);
};




/** @class PhysicsDelayTime
 * @brief Delays the action a certain amount of seconds.
*/
class CC_DLL PhysicsDelayTime : public PhysicsActionInterval
{
public:
    /** 
     * Creates the action.
     * @param d Duration time, in seconds.
     * @return An autoreleased PhysicsDelayTime object.
     */
    static PhysicsDelayTime* create(float d);

    //
    // Overrides
    //
    /**
     * @param time In seconds.
     */
    virtual void update(float time) override;
    virtual PhysicsDelayTime* reverse() const override;
    virtual PhysicsDelayTime* clone() const override;

CC_CONSTRUCTOR_ACCESS:
    PhysicsDelayTime() {}
    virtual ~PhysicsDelayTime() {}

private:
    CC_DISALLOW_COPY_AND_ASSIGN(PhysicsDelayTime);
};

/** @class PhysicsReverseTime
 * @brief Executes an action in reverse order, from time=duration to time=0
 
 @warning Use this action carefully. This action is not
 sequenceable. Use it as the default "reversed" method
 of your own actions, but using it outside the "reversed"
 scope is not recommended.
*/
class CC_DLL PhysicsReverseTime : public PhysicsActionInterval
{
public:
    /** Creates the action.
     *
     * @param action a certain action.
     * @return An autoreleased PhysicsReverseTime object.
     */
    static PhysicsReverseTime* create(FiniteTimePhysicsAction *action);

    //
    // Overrides
    //
    virtual PhysicsReverseTime* reverse() const override;
    virtual PhysicsReverseTime* clone() const override;
    virtual void startWithTarget(PhysicsObject *target) override;
    virtual void stop(void) override;
    /**
     * @param time In seconds.
     */
    virtual void update(float time) override;
    
CC_CONSTRUCTOR_ACCESS:
    PhysicsReverseTime();
    virtual ~PhysicsReverseTime(void);

    /** initializes the action */
    bool initWithAction(FiniteTimePhysicsAction *action);

protected:
    FiniteTimePhysicsAction *_other;

private:
    CC_DISALLOW_COPY_AND_ASSIGN(PhysicsReverseTime);
};




/** @class TargetedPhysicsAction
 * @brief Overrides the target of an action so that it always runs on the target
 * specified at action creation rather than the one specified by runAction.
 */
class CC_DLL TargetedPhysicsAction : public PhysicsActionInterval
{
public:
    /** Create an action with the specified action and forced target.
     * 
     * @param target The target needs to override.
     * @param action The action needs to override.
     * @return An autoreleased TargetedPhysicsAction object.
     */
    static TargetedPhysicsAction* create(Node* target, FiniteTimePhysicsAction* action);

    /** Sets the target that the action will be forced to run with.
     *
     * @param forcedTarget The target that the action will be forced to run with.
     */
    void setForcedTarget(Node* forcedTarget);
    /** returns the target that the action is forced to run with. 
     *
     * @return The target that the action is forced to run with.
     */
    Node* getForcedTarget() { return _forcedTarget; }
    const Node* getForcedTarget() const { return _forcedTarget; }

    //
    // Overrides
    //
    virtual TargetedPhysicsAction* clone() const override;
    virtual TargetedPhysicsAction* reverse() const  override;
    virtual void startWithTarget(PhysicsObject *target) override;
    virtual void stop(void) override;
    /**
     * @param time In seconds.
     */
    virtual void update(float time) override;
    //
    // Overrides
    //
    virtual bool isDone(void) const override;
    
CC_CONSTRUCTOR_ACCESS:
    TargetedPhysicsAction();
    virtual ~TargetedPhysicsAction();

    /** Init an action with the specified action and forced target */
    bool initWithTarget(Node* target, FiniteTimePhysicsAction* action);

protected:
    FiniteTimePhysicsAction* _action;
    Node* _forcedTarget;

private:
    CC_DISALLOW_COPY_AND_ASSIGN(TargetedPhysicsAction);
};


// end of actions group
/// @}

NS_CC_END

#endif //__ACTION_CCPHYSICS_INTERVAL_ACTION_H__
